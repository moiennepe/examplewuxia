"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DVAL_JSON = (function () {
    function DVAL_JSON() {
    }
    return DVAL_JSON;
}());
DVAL_JSON.EOM = { type: 'eom' };
DVAL_JSON.REQ = { type: 'req' };
DVAL_JSON.REP = { type: 'rep' };
DVAL_JSON.ERR = { type: 'err' };
DVAL_JSON.NFY = { type: 'nfy' };
exports.DVAL_JSON = DVAL_JSON;
//# sourceMappingURL=Common.js.map