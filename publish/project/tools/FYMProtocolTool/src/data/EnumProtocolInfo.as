package data
{
	public class EnumProtocolInfo
	{
		public var name: String;
		
		public var msgID: String;
		
		public function EnumProtocolInfo(name: String, msgID: String)
		{
			this.name = name;
			this.msgID = msgID;
		}
	}
}