package data
{
	public class InternalTypeInfo
	{
		public var type: String;
		
		public var byteCount: String;
		
		
		public function InternalTypeInfo(type: String, byteCount: String)
		{
			this.type = type;
			this.byteCount = byteCount;
		}
	}
}