package data 
{
	public class EnumMacro
	{
		public var name: String;
		
		public var value: int;
		
		public var desc: String;
		
		public function EnumMacro(name: String, value: int, desc: String)
		{
			this.name = name;
			this.value = value;
			this.desc = desc;
		}
	}
}