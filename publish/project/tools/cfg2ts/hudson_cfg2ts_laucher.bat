:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: 本脚本仅适用于在前台构建机上运行，如有疑问请联系teppei
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
@echo off

title cfg2ts启动器@teppei

echo ==============================
echo ======转手游配置结构启动脚本======
echo ==============================

set hudson_cfg2tsToolPath=E:\FYM_cfg2ts

E:
cd %hudson_cfg2tsToolPath%

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

REM 解析传入的参数，主线还是分支
set hudson_cfg2tsVer=%1

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

REM 调用构建脚本
call %hudson_cfg2tsToolPath%\cfg2ts_launcher.bat %hudson_cfg2tsVer%

echo 构建结束，请检查构建日志