import { TabSubForm } from 'System/uilib/TabForm'

export abstract class KfhdBasePanel extends TabSubForm {
    abstract onServerOverDay();
}