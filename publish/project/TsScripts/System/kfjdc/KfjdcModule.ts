﻿import { Global as G } from 'System/global'
import { EventDispatcher } from 'System/EventDispatcher'
import { ProtocolUtil } from 'System/protocol/ProtocolUtil'
import { MessageBoxConst, ConfirmCheck } from 'System/tip/TipManager'
import { ActionHandler } from 'System/ActionHandler'
import { ErrorId } from 'System/protocol/ErrorId'
import { Macros } from 'System/protocol/Macros'
import { Color } from 'System/utils/ColorUtil'
import { TextFieldUtil } from 'System/utils/TextFieldUtil'
import { KeyWord } from 'System/constants/KeyWord'
import { EnumKuafuPvpStatus } from 'System/constants/GameEnum'
import { Constants } from 'System/constants/Constants'
import { KfjdcData } from 'System/data/KfjdcData'
import { ActHomeView } from 'System/activity/actHome/ActHomeView'
import { KuaFu3v3Panel } from 'System/activity/actHome/KuaFu3v3Panel'
import { KuaFu3v3RankView } from 'System/activity/actHome/KuaFu3v3RankView'
import { Events } from 'System/Events'
import { SxdscRankView } from 'System/pinstance/selfChallenge/SxdscRankView'
import { SiXiangBattleView } from 'System/pinstance/selfChallenge/SiXiangBattleView'
import { EnumMainViewChild } from 'System/main/view/MainView'
import { BwdhRankView } from 'System/kfjdc/view/BwdhRankView'
import { BwdhChampionBetView } from 'System/kfjdc/view/BwdhChampionBetView'

/**
 * 跨服角斗场模块。
 * 
 * 本文件代码由模板生成，你可能需要继续修改其他代码文件（比如EnumModuleName）才能正常使用。
 * Code generated by Bat&Perl.
 * 
 * @author teppei
 * 
 */
export class KfjdcModule extends EventDispatcher {

    /**倒计时*/
    private countDownTimer: Game.Timer;

    private _3v3DelayTimer: Game.Timer;

    constructor() {
        super();

        this.addNetListener(Macros.MsgID_CROSS_CS_Response, this._onCrossCsResponse);
        this.addEvent(Events.HeroAliveDeadChange, this._onHeroAliveDeadChange);
    }

    private _onHeroAliveDeadChange(): void {
        if (!G.DataMgr.heroData.isAlive) {
            // 死了要离队
            if (EnumKuafuPvpStatus.queue == G.DataMgr.kf3v3Data.myStatus) {
                G.ModuleMgr.netModule.sendMsg(ProtocolUtil.getCrossSingleCommonRequest(Macros.CROSS_SINGLE_EXIT));
            }
        }
    }

    private _onCrossCsResponse(response: Protocol.Cross_CS_Response): void {
        //uts.log('response.m_usType = ' + response.m_usType + ', response.m_iResultID = ' + response.m_iResultID);
        // 比武大会
        let bwdhTypes = [Macros.CROSS_SINGLE_OPEN, Macros.CROSS_SINGLE_RANK, Macros.CROSS_SINGLE_JOIN, Macros.CROSS_SINGLE_EXIT,
        Macros.CROSS_SINGLE_START, Macros.CROSS_SINGLE_REWARD, Macros.CROSS_SINGLE_SESSION_OPEN, Macros.CROSS_SINGLE_BUY, Macros.CROSS_SINGLE_FINAL_OPEN,
        Macros.CROSS_SINGLE_FINAL_BETGET, Macros.CROSS_SINGLE_FINAL_BET,
        Macros.CROSS_SINGLE_FINAL_WINBET, Macros.CROSS_SINGLE_FINAL_WINBETGET, Macros.CROSS_SINGLE_FINAL_PKNTF];
        // 3v3
        let i3v3Types = [Macros.CROSS_MULTI_RANK, Macros.CROSS_MULTI_JOIN, Macros.CROSS_MULTI_EXIT, Macros.CROSS_MULTI_START];
        // 四象斗兽场
        let sxdscTypes = [Macros.CROSS_COLOSSEUM_PANEL, Macros.CROSS_COLOSSEUM_RANK, Macros.CROSS_COLOSSEUM_REWARD, Macros.CROSS_COLOSSEUM_START_PK];

        let isBwdhType = bwdhTypes.indexOf(response.m_usType) >= 0;
        let is3v3Type = i3v3Types.indexOf(response.m_usType) >= 0;
        let isSxdscType = sxdscTypes.indexOf(response.m_usType) >= 0;
        if (isBwdhType || is3v3Type || isSxdscType ||
            (Macros.CROSS_SYN_ROLE == response.m_usType && KeyWord.NAVIGATION_CROSS_PVP == response.m_stValue.m_stCSCrossSynRoleRes.m_ucSynType)) {
            if (ErrorId.EQEC_Success != response.m_iResultID) {
                //uts.log(response.m_iResultID+'  错误码  ');
                G.TipMgr.addMainFloatTip(G.DataMgr.errorData.getErrorStringById(response.m_iResultID));

                if (ErrorId.EQEC_TRANS_PINCROSS_FAIL == response.m_iResultID || ErrorId.EQEC_TRANS_GUOYUN == response.m_iResultID) {
                    this._cancelAll();
                }
            }
            else {
                let kf3v3Data = G.DataMgr.kf3v3Data;
                let kfjdcData = G.DataMgr.kfjdcData;
                let gameParas = G.DataMgr.gameParas;

                if (isBwdhType) {
                    // 比武大会
                    if (Macros.CROSS_SINGLE_OPEN == response.m_usType) {
                        // 拉取面板
                        kfjdcData.updateUiData(response.m_stValue.m_stCSSingleOpenRes);
                    }
                    else if (Macros.CROSS_SINGLE_RANK == response.m_usType) {
                        // 排行榜
                        kfjdcData.updateRankData(response.m_stValue.m_stCSSingleRankRes);
                    }
                    else if (Macros.CROSS_SINGLE_JOIN == response.m_usType) {
                        // 开始排队
                        kfjdcData.myStatus = EnumKuafuPvpStatus.queue;
                    }
                    else if (Macros.CROSS_SINGLE_EXIT == response.m_usType) {
                        // 退出排队
                        kfjdcData.myStatus = EnumKuafuPvpStatus.none;
                    }
                    else if (Macros.CROSS_SINGLE_START == response.m_usType) {
                        // 匹配上了
                        // 设置等待同步数据
                        // 取出队长，查看是否与队长同线
                        let startRes: Protocol.CSCross_StartPin_Response = response.m_stValue.m_stCSSingleStartRes;
                        kfjdcData.updateByStart(startRes);

                        uts.log('[跨服角斗场]匹配，队长：' + kfjdcData.captain.m_stRoleID.m_uiUin);

                        kfjdcData.myStatus = EnumKuafuPvpStatus.start;
                        this.onStart();
                    }
                    else if (Macros.CROSS_SINGLE_BUY == response.m_usType) {
                        // 购买奖励次数
                        kfjdcData.updateByBuy(response.m_stValue.m_stCSSingleBuyRes);
                    }
                    else if (Macros.CROSS_SINGLE_REWARD == response.m_usType) {
                        kfjdcData.updateByGetSingleReward(response.m_stValue.m_stCSSingleRewardRsp);
                    }
                    else if (Macros.CROSS_SINGLE_SESSION_OPEN == response.m_usType) {
                        kfjdcData.m_oldChampionData = response.m_stValue.m_stCSSingleSessionOpenRsp;
                    }
                    else if (Macros.CROSS_SINGLE_FINAL_OPEN == response.m_usType) {
                        kfjdcData.updateByFinalPanelRsp(response.m_stValue.m_stCSSingleFinalOpenRsp);
                    }
                    else if (Macros.CROSS_SINGLE_FINAL_BETGET == response.m_usType) {
                        kfjdcData.updateByBetGetRsp(response.m_stValue.m_stCSSingleFinalBetGetRsp);
                    }
                    else if (Macros.CROSS_SINGLE_FINAL_BET == response.m_usType) {
                        // 普通支持回复
                        kfjdcData.updateByFinalBetRsp(response.m_stValue.m_stCSSingleFinalBetRsp);
                        G.TipMgr.addMainFloatTip('支持成功！');
                    }
                    else if (Macros.CROSS_SINGLE_FINAL_WINBET == response.m_usType) {
                        // 冠军支持回复
                        G.TipMgr.addMainFloatTip('支持成功！');
                        kfjdcData.updateByChampionBetRsp(response.m_stValue.m_stCSSingleFinalWinBetRsp);
                    }
                    else if (Macros.CROSS_SINGLE_FINAL_WINBETGET == response.m_usType) {
                        // 冠军支持领奖
                        kfjdcData.updateByChampionBetGetRsp(response.m_stValue.m_stCSSingleFinalWinBetGetRsp);                    }
                    else if (Macros.CROSS_SINGLE_FINAL_PKNTF == response.m_usType) {
                        G.TipMgr.showConfirm('您有一场比武大会决赛即将开始，请提前入场。准备时间内未到场以及中途离场选手判为弃权。', ConfirmCheck.noCheck, '确定|取消', delegate(this, this.onBwdhFinalNtfConfirm));
                    }

                    let view = G.Uimgr.getForm<ActHomeView>(ActHomeView);
                    if (null != view) {
                        view.onBiWuDaHuiChange(response.m_usType);
                    }

                    if (Macros.CROSS_SINGLE_RANK == response.m_usType || Macros.CROSS_SINGLE_REWARD == response.m_usType) {

                        let rankView = G.Uimgr.getForm<BwdhRankView>(BwdhRankView);
                        uts.log(rankView + '  排行榜面板回复');

                        if (null != rankView) {
                            rankView.onRankChange();
                        }
                    }

                    let championBetView = G.Uimgr.getForm<BwdhChampionBetView>(BwdhChampionBetView);
                    if (championBetView) {
                        championBetView.onChampionBetChange();
                    }
                }
                else if (is3v3Type) {
                    // 3v3
                    if (Macros.CROSS_MULTI_RANK == response.m_usType) {
                        // 3V3排行榜
                        kf3v3Data.updateRankData(response.m_stValue.m_stCSMultiRankRes);
                        let view = G.Uimgr.getForm<KuaFu3v3RankView>(KuaFu3v3RankView);
                        if (null != view) {
                            view.onRankDataChanged();
                        }
                    }
                    else if (Macros.CROSS_MULTI_JOIN == response.m_usType) {
                        // 3V3开始排队
                        kf3v3Data.myStatus = EnumKuafuPvpStatus.queue;
                        let view = G.Uimgr.getSubFormByID<KuaFu3v3Panel>(ActHomeView, KeyWord.OTHER_FUNCTION_CROSS3V3);
                        if (null != view) {
                            view.onMatchingStarted();
                        }
                    }
                    else if (Macros.CROSS_MULTI_EXIT == response.m_usType) {
                        // 3V3榜退出排队
                        kf3v3Data.myStatus = EnumKuafuPvpStatus.none;
                        let view = G.Uimgr.getSubFormByID<KuaFu3v3Panel>(ActHomeView, KeyWord.OTHER_FUNCTION_CROSS3V3);
                        if (null != view) {
                            view.onMatchingCanceled();
                        }
                    }
                    else if (Macros.CROSS_MULTI_START == response.m_usType) {
                        // 	3V3匹配上了
                        // 设置等待同步数据
                        // 取出队长，查看是否与队长同线
                        let startPvpV3Res: Protocol.CSCross_StartPin_Response = response.m_stValue.m_stCSMultiStartRes;
                        kfjdcData.updateByStart(startPvpV3Res);
                        kf3v3Data.myStatus = EnumKuafuPvpStatus.start;

                        uts.log('[3V3战场]匹配，队长：' + kfjdcData.captain.m_stRoleID.m_uiUin);
                        uts.log(uts.format('captain.m_szDomain:{0}---captain.m_szIP:{1}---captain.m_uiPort:{2}', kfjdcData.captain.m_szDomain, kfjdcData.captain.m_szIP, kfjdcData.captain.m_uiPort));
                        uts.log(uts.format('gameParas.m_szDomain:{0}---gameParas.m_szIP:{1}---gameParas.m_uiPort:{2}', gameParas.domain, gameParas.serverIp, gameParas.serverPort));
                        let view = G.Uimgr.getSubFormByID<KuaFu3v3Panel>(ActHomeView, KeyWord.OTHER_FUNCTION_CROSS3V3);
                        if (null != view) {
                            view.onMatchingSuccess();
                        }
                        //3v3延迟 进入副本,要加一个匹配成功
                        if (null == this.countDownTimer) {
                            uts.log("kingsly 跨服3v3 倒计时空");
                            this._3v3DelayTimer = new Game.Timer("_3v3DelayTimer", 2000, 1, delegate(this, this.onStart));
                        } else {
                            uts.log("kingsly 跨服3v3 倒计时不空");
                            this._3v3DelayTimer.ResetTimer(2000, 1, delegate(this, this.onStart));
                        }
                        
                        //this.onStart();
                    }
                }
                else if (isSxdscType) {
                    //四象斗兽场相关
                    if (Macros.CROSS_COLOSSEUM_PANEL == response.m_usType) {
                        G.DataMgr.siXiangData.updateByPanelResp(response.m_stValue.m_stCSColosseumPanelRes);
                    }
                    else if (Macros.CROSS_COLOSSEUM_RANK == response.m_usType) {
                        G.DataMgr.siXiangData.updateByRankResp(response.m_stValue.m_stCSColosseumRankRes);
                    }
                    else if (Macros.CROSS_COLOSSEUM_REWARD == response.m_usType) {
                        G.DataMgr.siXiangData.updateByRewardResp(response.m_stValue.m_stCSColosseumRewardRes);
                    }
                    else if (Macros.CROSS_COLOSSEUM_START_PK == response.m_usType) {
                        G.DataMgr.siXiangData.updateByStartPkResp(response.m_stValue.m_stCSColosseumStartPKRes);
                    }
                    else if (Macros.CROSS_COLOSSEUM_MONEY == response.m_usType) {
                        G.DataMgr.siXiangData.updateByMoneyResp(response.m_stValue.m_uiSSColosseumMoneyRes);
                    }

                    if (Macros.CROSS_COLOSSEUM_START_PK == response.m_usType) {
                        G.Uimgr.closeForm(ActHomeView);
                        G.ViewCacher.mainView.createChildForm<SiXiangBattleView>(SiXiangBattleView, EnumMainViewChild.siXiangBattle).open();
                        G.ViewCacher.mainView.setTaskTeamActive(false);
                    } else {
                        let view = G.Uimgr.getForm<ActHomeView>(ActHomeView);
                        if (null != view && view.isOpened) {
                            view.onSxdscKuaFuChange();
                        }
                        let rankView = G.Uimgr.getForm<SxdscRankView>(SxdscRankView);
                        if (null != rankView && rankView.isOpened) {
                            if (Macros.CROSS_COLOSSEUM_RANK == response.m_usType) {
                                rankView.onRankChange();
                            } else {
                                rankView.onSxdscKuaFuChange();
                            }
                        }
                    }
                }
                else if (Macros.CROSS_SYN_ROLE == response.m_usType) {
                    // 同步需要跨服的玩家数据
                    kfjdcData.waitSyncRole = false;
                }

                G.ActBtnCtrl.update(false);
            }
        }
    }

    private onStart() {
        // 与队长不在同一服，需要等待同步数据
        let kfjdcData = G.DataMgr.kfjdcData;
        let gameParas = G.DataMgr.gameParas;
        kfjdcData.waitSyncRole = (kfjdcData.captain.m_szDomain != gameParas.domain ||
            kfjdcData.captain.m_szIP != gameParas.serverIp ||
            kfjdcData.captain.m_uiPort != gameParas.serverPort);
        // 检查是否死了
        if (G.DataMgr.heroData.isAlive) {
            if (kfjdcData.amICaptain()) {
                // 队长是主服的话，后台会先拉进场景去创建副本，然后再下发Start，因此不需要转圈圈了
                // 否则可能一直卡在转圈圈里了
                this.stopCountDown();
            }
            else {
                // 不是队长的话5秒后进副本
                if (null == this.countDownTimer) {
                    this.countDownTimer = new Game.Timer("kfjdc countDownTimer", 1000 * Constants.KuaFuCountDown, 1, delegate(this, this._onCountDown));
                } else {
                    this.countDownTimer.ResetTimer(1000 * Constants.KuaFuCountDown, 1, delegate(this, this._onCountDown));
                }
                // 开始转圈圈
                G.ModuleMgr.loadingModule.setActive(true);
            }
            // 开始5秒倒计时
            //this.m_countDownID = mainEffectCtrl.showTimerEffect(KfData.COUNT_DOWN);
            // 关闭所有对话框
            G.Uimgr.closeAllForKuaFu();
            // 冻结所有功能
            G.DataMgr.runtime.isAllFuncLocked = true;
        } 
    }

    private _cancelAll() {
        this.stopCountDown();
        G.ModuleMgr.loginModule.cancelCross();
    }

    private stopCountDown() {
        if (null != this.countDownTimer) {
            this.countDownTimer.Stop();
            this.countDownTimer = null;
        }
    }

    private _onCountDown(timer: Game.Timer) {
        let kfjdcData = G.DataMgr.kfjdcData;
        // 清除当前状态
        if (kfjdcData.pistanceId == Macros.PINSTANCE_ID_MULTIPVP || kfjdcData.pistanceId == Macros.PINSTANCE_ID_SINGLEPVP) {
            G.DataMgr.kf3v3Data.myStatus = EnumKuafuPvpStatus.none;
        }
        else {
            kfjdcData.myStatus = EnumKuafuPvpStatus.none;
        }

        // 倒计时后，如果自己在副本中后台也会拉过去，所以这里不严格检查进入副本
        if (kfjdcData.waitSyncRole || !G.DataMgr.heroData.isAlive || !G.ActionHandler.canEnterPinstance(kfjdcData.pistanceId, 0, false, true)) {
            // 取消转圈
            uts.log(uts.format('cancel cross when count down, waitSyncRole = {0}, isAlive = {1}', kfjdcData.waitSyncRole, G.DataMgr.heroData.isAlive));
            this._cancelAll();
            return;
        }

        this.stopCountDown();

        let captain: Protocol.Cross_OneTeamMem = kfjdcData.captain;
        let navType: number = KeyWord.NAVIGATION_CROSS_PVP;
        if (kfjdcData.pistanceId == Macros.PINSTANCE_ID_MULTIPVP) {
            navType = KeyWord.NAVIGATION_CROSS_PIN;
        }
        else if (Macros.PINSTANCE_ID_SINGLEPVP_FINALID == kfjdcData.pistanceId) {
            navType = KeyWord.NAVIGATION_CROSS_FINAL;
        }

        let gameParas = G.DataMgr.gameParas;
        if (captain.m_szDomain == gameParas.domain &&
            captain.m_szIP == gameParas.serverIp &&
            captain.m_uiPort == gameParas.serverPort) {
            // 跟队长在同一个服，直接进副本
            uts.log('same server with captain, navType = ' + navType + ', uin = ' + captain.m_stRoleID.m_uiUin);
            G.Mapmgr.trySpecialTransport(navType, captain.m_stRoleID.m_uiUin, captain.m_stRoleID.m_uiSeq);
        }
        else {
            // 与队长不在同一个服，需要切线，开始切线
            G.DataMgr.systemData.setLoginEnterParam(Macros.LOGINOUT_REAZON_CROSSPIN, navType, captain.m_stRoleID.m_uiUin, captain.m_stRoleID.m_uiSeq);
            G.ModuleMgr.loginModule.crossServer(captain.m_usWorldID, captain.m_szDomain, captain.m_szIP, captain.m_uiPort);
        }
    }

    private onBwdhFinalNtfConfirm(state: MessageBoxConst = 0, isCheckSelected: boolean) {
        if (MessageBoxConst.yes == state) {
            G.ActionHandler.executeFunction(KeyWord.OTHER_FUNCTION_BIWUDAHUI);
        }
    }
}
