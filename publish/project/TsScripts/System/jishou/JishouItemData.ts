import { ThingItemData } from 'System/data/thing/ThingItemData'

export class JishouItemData {
    data: ThingItemData;
    price: number = 0;
    id: Protocol.PSBID;
    page: number = 0;
}
