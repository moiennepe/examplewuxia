/**
 * 创建1个HYDOperate_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newHYDOperate_Request(): Protocol.HYDOperate_Request {
    return {m_usType:0, m_stValue:{m_ucList:0, m_ucGetGiftIndex:0, m_ucGetDayGiftId:0}} as Protocol.HYDOperate_Request;
}
