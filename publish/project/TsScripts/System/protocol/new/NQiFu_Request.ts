/**
 * 创建1个QiFu_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newQiFu_Request(): Protocol.QiFu_Request {
    return {m_usType:0, m_stValue:{m_ucOpenPanel:0, m_uiQiFuID:0}} as Protocol.QiFu_Request;
}
