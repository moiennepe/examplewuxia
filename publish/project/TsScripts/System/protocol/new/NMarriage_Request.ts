/**
 * 创建1个Marriage_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newMarriage_Request(): Protocol.Marriage_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_usType:0, m_stValue:{m_ucAppleyMarryLevel:0, m_stDealMarryReq:0, m_stDealDivorceReq:0, m_stGiveFlower:{m_iThingID:0, m_usPosition:0, m_iNumber:0}}} as Protocol.Marriage_Request;
}
