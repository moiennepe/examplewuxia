/**
 * 创建1个HunGuMerge_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newHunGuMerge_Request(): Protocol.HunGuMerge_Request {
    return {m_usType:0, m_stValue:{m_stHunGuUpColorReq:{m_iTargetID:0, m_stContainerThingObjList:{m_iThingNumber:0, m_astSimpleContainerThingInfo:[]}}, m_stHunGuCreateReq:{m_iTargetID:0, m_stContainerThingObjList:{m_iThingNumber:0, m_astSimpleContainerThingInfo:[]}}}} as Protocol.HunGuMerge_Request;
}
