/**
 * 创建1个LoginServer_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newLoginServer_Request(): Protocol.LoginServer_Request {
    return {m_stLianYunPlatInfo:{m_szPlatNameStr:"", m_iClient:0, m_iIsAdult:0, m_iServerID:0, m_iPlatTime:0, m_ucPlatSignStr:"", m_usType:0, m_szChannelID:"", m_uiTokenID:0, m_uiSessionID:0}, m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_stEnterType:{m_cEnterType:0, m_iPara1:0, m_iPara2:0, m_iPara3:0}, m_ucReason:0, m_ucClientType:0, m_szDeviceUID:"", m_stDeviceInfo:{m_szStrIemi:"", m_szStrMac:"", m_szStrIdfa:"", m_szStrAndroid:"", m_szStrDeviceBrand:"", m_szStrDeviceType:"", m_iIsAndriod:0, m_szClientVersion:""}} as Protocol.LoginServer_Request;
}
