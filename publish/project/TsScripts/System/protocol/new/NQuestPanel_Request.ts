/**
 * 创建1个QuestPanel_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newQuestPanel_Request(): Protocol.QuestPanel_Request {
    return {m_usType:0, m_usQuestType:0, m_stValue:{m_ucList:0, m_stUplevelType:0, m_ucRefresh:0, m_ucGetExtReward:0, m_iOneKeyQuestId:0, m_ucXuKongRefresh:0, m_iQuickOneKey:0}} as Protocol.QuestPanel_Request;
}
