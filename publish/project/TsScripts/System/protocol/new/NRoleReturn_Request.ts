/**
 * 创建1个RoleReturn_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newRoleReturn_Request(): Protocol.RoleReturn_Request {
    return {m_usType:0, m_stValue:{m_ucListReq:0, m_ucLoginDaysReq:0, m_szFriendNickName:"", m_ucGetChargeGiftReq:0}} as Protocol.RoleReturn_Request;
}
