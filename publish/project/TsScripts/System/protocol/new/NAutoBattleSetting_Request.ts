/**
 * 创建1个AutoBattleSetting_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newAutoBattleSetting_Request(): Protocol.AutoBattleSetting_Request {
    return {m_stAutoBattleSetting:{m_ucDefaultFlag:0, m_uiBattleFunctionList:0, m_stAutoBattleSkill:{m_ucNumber:0, m_ucValue:[]}, m_stAutoAnalyze:{m_ucDefaultFlag:0, m_ucQuality:0, m_ucStage:0}, m_stBossAttention:{m_ucNumber:0, m_iValue:[]}}} as Protocol.AutoBattleSetting_Request;
}
