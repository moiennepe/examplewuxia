/**
 * 创建1个PinstanceRank_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newPinstanceRank_Request(): Protocol.PinstanceRank_Request {
    return {m_usType:0, m_stValue:{m_ucYMZCReq:0, m_ucYSZCReq:0}} as Protocol.PinstanceRank_Request;
}
