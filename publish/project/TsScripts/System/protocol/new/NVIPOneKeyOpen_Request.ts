/**
 * 创建1个VIPOneKeyOpen_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newVIPOneKeyOpen_Request(): Protocol.VIPOneKeyOpen_Request {
    return {m_iID:0, m_ucPara:0} as Protocol.VIPOneKeyOpen_Request;
}
