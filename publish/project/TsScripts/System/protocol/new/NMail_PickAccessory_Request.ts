/**
 * 创建1个Mail_PickAccessory_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newMail_PickAccessory_Request(): Protocol.Mail_PickAccessory_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_uiMailID:0} as Protocol.Mail_PickAccessory_Request;
}
