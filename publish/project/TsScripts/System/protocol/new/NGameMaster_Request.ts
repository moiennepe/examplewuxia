/**
 * 创建1个GameMaster_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newGameMaster_Request(): Protocol.GameMaster_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_cCommandType:0, m_szCommand:""} as Protocol.GameMaster_Request;
}
