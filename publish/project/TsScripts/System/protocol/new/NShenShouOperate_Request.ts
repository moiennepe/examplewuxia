/**
 * 创建1个ShenShouOperate_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newShenShouOperate_Request(): Protocol.ShenShouOperate_Request {
    return {m_iType:0, m_iID:0, m_ucPosition:0, m_uiParam:0} as Protocol.ShenShouOperate_Request;
}
