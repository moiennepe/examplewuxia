/**
 * 创建1个Friend_Apply_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newFriend_Apply_Request(): Protocol.Friend_Apply_Request {
    return {m_stOperatorRoleID:{m_uiUin:0, m_uiSeq:0}, m_stTargetRoleID:{m_uiUin:0, m_uiSeq:0}} as Protocol.Friend_Apply_Request;
}
