﻿/**
 * 创建1个CSCross_JoinTeam_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCross_JoinTeam_Request(): Protocol.CSCross_JoinTeam_Request {
    return {m_uiPinstanceID:0, m_uiTeamID:0, m_szDomain:"", m_szIP:"", m_uiPort:0} as Protocol.CSCross_JoinTeam_Request;
}
