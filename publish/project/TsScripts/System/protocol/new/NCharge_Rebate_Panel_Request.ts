/**
 * 创建1个Charge_Rebate_Panel_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newCharge_Rebate_Panel_Request(): Protocol.Charge_Rebate_Panel_Request {
    return {m_ucFlag:0} as Protocol.Charge_Rebate_Panel_Request;
}
