/**
 * 创建1个WYYZ_FightSet_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newWYYZ_FightSet_Request(): Protocol.WYYZ_FightSet_Request {
    return {m_stSelfFightList:{m_aiPetID:[]}} as Protocol.WYYZ_FightSet_Request;
}
