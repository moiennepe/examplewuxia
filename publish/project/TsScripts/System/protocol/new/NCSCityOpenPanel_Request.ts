﻿/**
 * 创建1个CSCityOpenPanel_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCityOpenPanel_Request(): Protocol.CSCityOpenPanel_Request {
    return {m_ucTag:0} as Protocol.CSCityOpenPanel_Request;
}
