﻿/**
 * 创建1个CSSingleRank_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSSingleRank_Request(): Protocol.CSSingleRank_Request {
    return {m_uiPinstanceID:0} as Protocol.CSSingleRank_Request;
}
