/**
 * 创建1个DiBang_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newDiBang_Request(): Protocol.DiBang_Request {
    return {m_usType:0, m_stValue:{m_ucRefreshTime:0, m_ucCrossRefreshTime:0, m_ucType:0, m_ucCrossType:0}} as Protocol.DiBang_Request;
}
