/**
 * 创建1个AASIdentityRecord_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newAASIdentityRecord_Request(): Protocol.AASIdentityRecord_Request {
    return {m_szRoleName:"", m_szIdentifyCardNum:""} as Protocol.AASIdentityRecord_Request;
}
