﻿/**
 * 创建1个CSStart_PK_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSStart_PK_Request(): Protocol.CSStart_PK_Request {
    return {m_usRankVal:0, m_ucPetPos:0} as Protocol.CSStart_PK_Request;
}
