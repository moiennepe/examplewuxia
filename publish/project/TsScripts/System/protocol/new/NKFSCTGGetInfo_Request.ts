/**
 * 创建1个KFSCTGGetInfo_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newKFSCTGGetInfo_Request(): Protocol.KFSCTGGetInfo_Request {
    return {m_ucReserved:0} as Protocol.KFSCTGGetInfo_Request;
}
