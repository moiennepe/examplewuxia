/**
 * 创建1个ListActivityLimit_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newListActivityLimit_Request(): Protocol.ListActivityLimit_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}} as Protocol.ListActivityLimit_Request;
}
