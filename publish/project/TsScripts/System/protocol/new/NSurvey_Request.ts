/**
 * 创建1个Survey_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSurvey_Request(): Protocol.Survey_Request {
    return {m_ucCount:0, m_astAnswerList:[]} as Protocol.Survey_Request;
}
