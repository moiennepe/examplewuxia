/**
 * 创建1个HunLi_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newHunLi_Request(): Protocol.HunLi_Request {
    return {m_usType:0, m_stValue:{m_stHunLiLevelReq:{m_iHunLiLevel:0, m_iHunLiSubLevel:0}, m_stHunLiRewardReq:{m_iHunLiLevel:0, m_iHunLiSubLevel:0, m_iConditionID:0}, m_stHunHuanZhuRuReq:{m_iHunHuanID:0}, m_uiHunHuanActiveID:0, m_uiHunHuanLevelUpID:0}} as Protocol.HunLi_Request;
}
