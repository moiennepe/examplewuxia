/**
 * 创建1个SaiJiActive_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSaiJiActive_Request(): Protocol.SaiJiActive_Request {
    return {m_iSaiJiID:0, m_iShowID:0} as Protocol.SaiJiActive_Request;
}
