/**
 * 创建1个FaQiOperate_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newFaQiOperate_Request(): Protocol.FaQiOperate_Request {
    return {m_iType:0, m_iID:0, m_uiParam:0} as Protocol.FaQiOperate_Request;
}
