﻿/**
 * 创建1个CSSingleOpen_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSSingleOpen_Request(): Protocol.CSSingleOpen_Request {
    return {m_uiTag:0} as Protocol.CSSingleOpen_Request;
}
