/**
 * 创建1个ChangeCDKey_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newChangeCDKey_Request(): Protocol.ChangeCDKey_Request {
    return {m_iType:0, m_szCDKey:""} as Protocol.ChangeCDKey_Request;
}
