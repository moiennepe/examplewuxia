/**
 * 创建1个SkillEffectTarget的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSkillEffectTarget(): Protocol.SkillEffectTarget {
    return {m_iUnitID:0, m_ucEffectMask:0} as Protocol.SkillEffectTarget;
}
