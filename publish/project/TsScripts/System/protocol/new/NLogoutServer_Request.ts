/**
 * 创建1个LogoutServer_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newLogoutServer_Request(): Protocol.LogoutServer_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_ucReason:0, m_usCrossSvrId:0} as Protocol.LogoutServer_Request;
}
