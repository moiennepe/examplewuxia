/**
 * 创建1个CSAddGMQA_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newCSAddGMQA_Request(): Protocol.CSAddGMQA_Request {
    return {m_shType:0, m_szQuestion:""} as Protocol.CSAddGMQA_Request;
}
