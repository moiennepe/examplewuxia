/**
 * 创建1个CSGetIconMonsterRequest的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newCSGetIconMonsterRequest(): Protocol.CSGetIconMonsterRequest {
    return {m_iSceneID:0} as Protocol.CSGetIconMonsterRequest;
}
