﻿/**
 * 创建1个CSSingleExit_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSSingleExit_Request(): Protocol.CSSingleExit_Request {
    return {m_uiPinstanceID:0} as Protocol.CSSingleExit_Request;
}
