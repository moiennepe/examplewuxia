/**
 * 创建1个ModifyRole_Account_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newModifyRole_Account_Request(): Protocol.ModifyRole_Account_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_cProfession:0, m_cGender:0, m_szNickName:"", m_cModifyType:0} as Protocol.ModifyRole_Account_Request;
}
