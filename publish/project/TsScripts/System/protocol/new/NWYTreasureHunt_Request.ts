/**
 * 创建1个WYTreasureHunt_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newWYTreasureHunt_Request(): Protocol.WYTreasureHunt_Request {
    return {m_usType:0, m_stValue:{m_ucTag:0, m_stTreasureHuntStart:{m_iBeautyID:0, m_iLeftBeautyID:0, m_iRightBeautyID:0}, m_stTreasureHuntEnd:{m_iBeautyID:0, m_iLeftBeautyID:0, m_iRightBeautyID:0}}} as Protocol.WYTreasureHunt_Request;
}
