﻿/**
 * 创建1个CSCrossOpen_Panel_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCrossOpen_Panel_Request(): Protocol.CSCrossOpen_Panel_Request {
    return {m_ucType:0} as Protocol.CSCrossOpen_Panel_Request;
}
