/**
 * 创建1个InterruptSkill_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newInterruptSkill_Request(): Protocol.InterruptSkill_Request {
    return {m_ucReserved:0} as Protocol.InterruptSkill_Request;
}
