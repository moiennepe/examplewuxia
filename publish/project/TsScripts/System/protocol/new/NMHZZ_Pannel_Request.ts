/**
 * 创建1个MHZZ_Pannel_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newMHZZ_Pannel_Request(): Protocol.MHZZ_Pannel_Request {
    return {m_ucTag:0} as Protocol.MHZZ_Pannel_Request;
}
