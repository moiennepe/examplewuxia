/**
 * 创建1个GroupBuy_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newGroupBuy_Request(): Protocol.GroupBuy_Request {
    return {m_usType:0, m_stValue:{m_ucListReq:0, m_stBuyReq:{m_uiThingID:0, m_uiThingNum:0}}} as Protocol.GroupBuy_Request;
}
