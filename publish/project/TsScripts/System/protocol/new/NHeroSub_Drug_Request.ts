/**
 * 创建1个HeroSub_Drug_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newHeroSub_Drug_Request(): Protocol.HeroSub_Drug_Request {
    return {m_ucType:0, m_ucDrugType:0, m_ucOpType:0} as Protocol.HeroSub_Drug_Request;
}
