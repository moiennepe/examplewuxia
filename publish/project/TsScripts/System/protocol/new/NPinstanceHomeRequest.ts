/**
 * 创建1个PinstanceHomeRequest的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newPinstanceHomeRequest(): Protocol.PinstanceHomeRequest {
    return {m_usType:0, m_stValue:{m_iList:0, m_stListAll:{m_ucNum:0, m_iPinID:[]}, m_iReset:0, m_iGetGiftLv:0, m_iGetRankPin:0, m_iGetRankTimePin:0, m_iFreshPinTime:0, m_ucNanDu:0, m_ucFreshNanDu:0, m_ucBuyMultiBossTimes:0}, m_iPinID:0} as Protocol.PinstanceHomeRequest;
}
