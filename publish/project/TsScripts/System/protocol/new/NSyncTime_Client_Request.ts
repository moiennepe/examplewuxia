/**
 * 创建1个SyncTime_Client_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSyncTime_Client_Request(): Protocol.SyncTime_Client_Request {
    return {m_uiClientTime_low:0, m_uiClientTime_high:0} as Protocol.SyncTime_Client_Request;
}
