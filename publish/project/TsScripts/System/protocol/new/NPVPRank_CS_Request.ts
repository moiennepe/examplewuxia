/**
 * 创建1个PVPRank_CS_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newPVPRank_CS_Request(): Protocol.PVPRank_CS_Request {
    return {m_usType:0, m_stValue:{m_stCSOpenPanelReq:{m_ucType:0}, m_stCSStartPKReq:{m_usRankVal:0, m_ucPetPos:0}, m_ucBuyTimesReq:0, m_ucMoBaiRank:0, m_stCSCrossOpenPanelReq:{m_ucType:0}, m_stCSCrossOpenRankReq:{m_ucType:0}, m_stCSCrossStartPKReq:{m_usRankVal:0}, m_ucBuyCrossTimesReq:0, m_stCSCrossGetRewardReq:0, m_ucGetMaxRankRewardReq:0}} as Protocol.PVPRank_CS_Request;
}
