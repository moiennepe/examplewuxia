/**
 * 创建1个CSListGMQA_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newCSListGMQA_Request(): Protocol.CSListGMQA_Request {
    return {m_ucID:0} as Protocol.CSListGMQA_Request;
}
