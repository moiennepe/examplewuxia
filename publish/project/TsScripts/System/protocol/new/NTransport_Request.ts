/**
 * 创建1个Transport_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newTransport_Request(): Protocol.Transport_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_iTransportPointID:0, m_iQuestID:0, m_ucQuestType:0} as Protocol.Transport_Request;
}
