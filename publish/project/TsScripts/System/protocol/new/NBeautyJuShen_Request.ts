/**
 * 创建1个BeautyJuShen_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newBeautyJuShen_Request(): Protocol.BeautyJuShen_Request {
    return {m_iBeautyID:0, m_bIsFeiSheng:0} as Protocol.BeautyJuShen_Request;
}
