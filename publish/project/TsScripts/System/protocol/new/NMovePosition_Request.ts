/**
 * 创建1个MovePosition_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newMovePosition_Request(): Protocol.MovePosition_Request {
    return {m_ushSeq:0, m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_stDstPath:{m_iNumber:0, m_astPosition:[]}} as Protocol.MovePosition_Request;
}
