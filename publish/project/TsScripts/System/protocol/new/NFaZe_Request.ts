/**
 * 创建1个FaZe_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newFaZe_Request(): Protocol.FaZe_Request {
    return {m_usType:0, m_stValue:{m_ucOpenPanel:0, m_uiValueID:0}} as Protocol.FaZe_Request;
}
