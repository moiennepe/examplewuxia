/**
 * 创建1个Revival_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newRevival_Request(): Protocol.Revival_Request {
    return {m_stOperatorID:{m_uiUin:0, m_uiSeq:0}, m_stTargetID:{m_uiUin:0, m_uiSeq:0}, m_ucRevivalType:0} as Protocol.Revival_Request;
}
