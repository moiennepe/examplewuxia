/**
 * 创建1个Dress_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newDress_Request(): Protocol.Dress_Request {
    return {m_usType:0, m_stValue:{m_ucLevelType:0, m_ucImageList:0, m_stChangeReq:{m_uiImageID:0, m_ucColorIndex:0}, m_bIsHide:0, m_stTrainReq:{m_ucType:0, m_ucSubType:0, m_uiImageID:0}, m_uiStrengID:0}} as Protocol.Dress_Request;
}
