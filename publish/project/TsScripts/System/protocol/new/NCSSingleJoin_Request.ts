﻿/**
 * 创建1个CSSingleJoin_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSSingleJoin_Request(): Protocol.CSSingleJoin_Request {
    return {m_uiPinstanceID:0, m_szDomain:"", m_szIP:"", m_uiPort:0} as Protocol.CSSingleJoin_Request;
}
