﻿/**
 * 创建1个CSCross_GetTeamList_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCross_GetTeamList_Request(): Protocol.CSCross_GetTeamList_Request {
    return {m_uiPinstanceID:0} as Protocol.CSCross_GetTeamList_Request;
}
