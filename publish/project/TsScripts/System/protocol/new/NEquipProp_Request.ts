/**
 * 创建1个EquipProp_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newEquipProp_Request(): Protocol.EquipProp_Request {
    return {m_usType:0, m_ucEquipPart:0, m_stContainerID:{m_ucContainerType:0, m_stOwnerID:{m_uiUin:0, m_uiSeq:0}}, m_stValue:{m_ucEquipUpColorPart:0, m_ucEquipStrengReq:0, m_stDiamondMount:{m_iDiamondID:0, m_ucHolePos:0, m_usPosition:0}, m_ucDiamondUnMountPos:0, m_stDiamondUpLevel:{m_ucHolePos:0, m_stSrcThingList:{m_iThingNumber:0, m_astThing:[]}}, m_stMeltList:{m_iThingNumber:0, m_astThing:[]}, m_ucEquipWash:0, m_ucEquipWashResultPos:0, m_ucWashLockReq:0, m_ucDiamondWashCnt:0, m_ucWashBuy:0, m_stLQReq:{m_iItemID:0, m_ucProtect:0}, m_ucEquipSlotUpLv:0, m_ucEquipSlotOneUpLv:0, m_stSuitAct:{m_ucStage:0, m_ucNum:0, m_ucActDress:0}, m_ucSlotSuitAct:0, m_ucSlotSuitUpType:0, m_ucSlotLianTiUp:0, m_ucSlotLTSBAct:0, m_ucHunGuFZ:0, m_ucHunGuSlotUpLv:0, m_ucHunGuSlotOneUpLv:0, m_ucHunGuStrengReq:0, m_ucHunGuSlotWash:0, m_ucHunGuSlotWashResultPos:0, m_ucHunGuSlotWashLockReq:0, m_ucHunGuSlotWashBuy:0, m_iHunGuSkillFZ:0}} as Protocol.EquipProp_Request;
}
