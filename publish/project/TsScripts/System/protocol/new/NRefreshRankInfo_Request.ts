/**
 * 创建1个RefreshRankInfo_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newRefreshRankInfo_Request(): Protocol.RefreshRankInfo_Request {
    return {m_ucRankType:0, m_ucCurPage:0} as Protocol.RefreshRankInfo_Request;
}
