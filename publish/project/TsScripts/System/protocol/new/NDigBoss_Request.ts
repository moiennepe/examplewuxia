/**
 * 创建1个DigBoss_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newDigBoss_Request(): Protocol.DigBoss_Request {
    return {m_uiMonsterID:0} as Protocol.DigBoss_Request;
}
