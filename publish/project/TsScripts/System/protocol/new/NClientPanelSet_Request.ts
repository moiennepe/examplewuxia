/**
 * 创建1个ClientPanelSet_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newClientPanelSet_Request(): Protocol.ClientPanelSet_Request {
    return {m_iPanelID:0, m_iStatus:0} as Protocol.ClientPanelSet_Request;
}
