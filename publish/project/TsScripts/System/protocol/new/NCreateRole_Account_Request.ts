/**
 * 创建1个CreateRole_Account_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newCreateRole_Account_Request(): Protocol.CreateRole_Account_Request {
    return {m_stLianYunPlatInfo:{m_szPlatNameStr:"", m_iClient:0, m_iIsAdult:0, m_iServerID:0, m_iPlatTime:0, m_ucPlatSignStr:"", m_usType:0, m_szChannelID:"", m_uiTokenID:0, m_uiSessionID:0}, m_uiUin:0, m_shWorldID:0, m_cProfession:0, m_cGender:0, m_szNickName:"", m_stInviteRoleID:{m_uiUin:0, m_uiSeq:0}, m_cCountry:0, m_ucClientType:0, m_szDeviceUID:"", m_stDeviceInfo:{m_szStrIemi:"", m_szStrMac:"", m_szStrIdfa:"", m_szStrAndroid:"", m_szStrDeviceBrand:"", m_szStrDeviceType:"", m_iIsAndriod:0, m_szClientVersion:""}} as Protocol.CreateRole_Account_Request;
}
