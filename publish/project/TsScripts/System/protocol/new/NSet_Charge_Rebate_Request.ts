/**
 * 创建1个Set_Charge_Rebate_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSet_Charge_Rebate_Request(): Protocol.Set_Charge_Rebate_Request {
    return {m_uiChargeValue:0, m_bIsSet:0} as Protocol.Set_Charge_Rebate_Request;
}
