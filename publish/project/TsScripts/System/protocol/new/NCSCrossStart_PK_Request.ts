﻿/**
 * 创建1个CSCrossStart_PK_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCrossStart_PK_Request(): Protocol.CSCrossStart_PK_Request {
    return {m_usRankVal:0} as Protocol.CSCrossStart_PK_Request;
}
