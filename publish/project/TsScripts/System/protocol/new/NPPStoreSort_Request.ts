/**
 * 创建1个PPStoreSort_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newPPStoreSort_Request(): Protocol.PPStoreSort_Request {
    return {m_iClassID:0, m_iPageNo:0, m_iSortType:0, m_ucSortOrder:0, m_szName:"", m_iMinLevel:0, m_iMaxLevel:0, m_iColor:0, m_iNPCID:0} as Protocol.PPStoreSort_Request;
}
