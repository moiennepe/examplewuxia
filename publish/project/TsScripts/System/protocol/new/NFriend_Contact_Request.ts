/**
 * 创建1个Friend_Contact_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newFriend_Contact_Request(): Protocol.Friend_Contact_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_stContactList:{m_ushNumber:0, m_astContactInfo:[]}} as Protocol.Friend_Contact_Request;
}
