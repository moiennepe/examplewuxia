/**
 * 创建1个SyncTime_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSyncTime_Request(): Protocol.SyncTime_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}} as Protocol.SyncTime_Request;
}
