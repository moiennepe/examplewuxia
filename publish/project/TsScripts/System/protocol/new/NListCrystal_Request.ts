/**
 * 创建1个ListCrystal_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newListCrystal_Request(): Protocol.ListCrystal_Request {
    return {m_cType:0} as Protocol.ListCrystal_Request;
}
