/**
 * 创建1个SkillTarget的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSkillTarget(): Protocol.SkillTarget {
    return {m_ushTargetType:0, m_iUnitID:0, m_stUnitPosition:{m_uiX:0, m_uiY:0}} as Protocol.SkillTarget;
}
