﻿/**
 * 创建1个CSCrossOpen_Rank_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCrossOpen_Rank_Request(): Protocol.CSCrossOpen_Rank_Request {
    return {m_ucType:0} as Protocol.CSCrossOpen_Rank_Request;
}
