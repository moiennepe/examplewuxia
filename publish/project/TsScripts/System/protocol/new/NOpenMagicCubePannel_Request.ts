/**
 * 创建1个OpenMagicCubePannel_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newOpenMagicCubePannel_Request(): Protocol.OpenMagicCubePannel_Request {
    return {m_ucID:0} as Protocol.OpenMagicCubePannel_Request;
}
