/**
 * 创建1个RecruitAlchemist_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newRecruitAlchemist_Request(): Protocol.RecruitAlchemist_Request {
    return {m_iOperateType:0, m_iCrystalID:0} as Protocol.RecruitAlchemist_Request;
}
