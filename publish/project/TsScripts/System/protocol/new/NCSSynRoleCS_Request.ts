﻿/**
 * 创建1个CSSynRoleCS_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSSynRoleCS_Request(): Protocol.CSSynRoleCS_Request {
    return {m_uiActID:0, m_uiToWorldID:0, m_uiExpandPara:0} as Protocol.CSSynRoleCS_Request;
}
