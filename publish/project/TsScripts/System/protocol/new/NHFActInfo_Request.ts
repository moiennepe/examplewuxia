/**
 * 创建1个HFActInfo_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newHFActInfo_Request(): Protocol.HFActInfo_Request {
    return {m_usType:0, m_stValue:{m_ucHFHDOpenLJCZPanel:0, m_ucHFHDGetBit:0, m_ucHFHDOpenLJXFPanel:0, m_ucHFHDLJXFGetBit:0, m_ucHFHDOpenQTDLPanel:0, m_ucHFHDGetQTDLReward:0, m_ucHFHDHFZZDBPannel:0, m_ucHFHDZCMPannel:0, m_ucHFHDZCMReward:0, m_ucHFHDBXYLOpenPannle:0, m_ucHFHDBXYLBuyChest:0, m_iHFHDBXYLShopExchange:0}} as Protocol.HFActInfo_Request;
}
