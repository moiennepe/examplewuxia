/**
 * 创建1个NPCTransport_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newNPCTransport_Request(): Protocol.NPCTransport_Request {
    return {m_iNPCID:0, m_iTransportPointID:0} as Protocol.NPCTransport_Request;
}
