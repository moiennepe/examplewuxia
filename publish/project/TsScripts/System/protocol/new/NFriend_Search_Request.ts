/**
 * 创建1个Friend_Search_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newFriend_Search_Request(): Protocol.Friend_Search_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_szNickName:""} as Protocol.Friend_Search_Request;
}
