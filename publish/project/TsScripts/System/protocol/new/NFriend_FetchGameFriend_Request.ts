/**
 * 创建1个Friend_FetchGameFriend_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newFriend_FetchGameFriend_Request(): Protocol.Friend_FetchGameFriend_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_cFlag:0} as Protocol.Friend_FetchGameFriend_Request;
}
