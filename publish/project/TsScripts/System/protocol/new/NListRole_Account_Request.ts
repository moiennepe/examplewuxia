/**
 * 创建1个ListRole_Account_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newListRole_Account_Request(): Protocol.ListRole_Account_Request {
    return {m_stLianYunPlatInfo:{m_szPlatNameStr:"", m_iClient:0, m_iIsAdult:0, m_iServerID:0, m_iPlatTime:0, m_ucPlatSignStr:"", m_usType:0, m_szChannelID:"", m_uiTokenID:0, m_uiSessionID:0}, m_uiUin:0, m_shWorldID:0} as Protocol.ListRole_Account_Request;
}
