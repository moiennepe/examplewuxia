/**
 * 创建1个PPStoreQuery_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newPPStoreQuery_Request(): Protocol.PPStoreQuery_Request {
    return {m_iClassID:0, m_iPageNo:0, m_szName:"", m_iMinLevel:0, m_iMaxLevel:0, m_iColor:0, m_iNPCID:0, m_ucQueryFalg:0} as Protocol.PPStoreQuery_Request;
}
