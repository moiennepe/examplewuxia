/**
 * 创建1个ChangeMountImage_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newChangeMountImage_Request(): Protocol.ChangeMountImage_Request {
    return {m_stEquipContainerID:{m_ucContainerType:0, m_stOwnerID:{m_uiUin:0, m_uiSeq:0}}, m_ucEquipPosition:0, m_ucImageID:0} as Protocol.ChangeMountImage_Request;
}
