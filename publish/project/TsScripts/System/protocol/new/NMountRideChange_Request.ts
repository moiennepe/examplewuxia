/**
 * 创建1个MountRideChange_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newMountRideChange_Request(): Protocol.MountRideChange_Request {
    return {m_ucMountRide:0} as Protocol.MountRideChange_Request;
}
