/**
 * 创建1个CompleteRewardQuest_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newCompleteRewardQuest_Request(): Protocol.CompleteRewardQuest_Request {
    return {m_ucQuestType:0} as Protocol.CompleteRewardQuest_Request;
}
