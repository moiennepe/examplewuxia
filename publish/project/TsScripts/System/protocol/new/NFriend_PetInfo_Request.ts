/**
 * 创建1个Friend_PetInfo_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newFriend_PetInfo_Request(): Protocol.Friend_PetInfo_Request {
    return {m_stTargetRoleID:{m_uiUin:0, m_uiSeq:0}} as Protocol.Friend_PetInfo_Request;
}
