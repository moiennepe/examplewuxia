/**
 * 创建1个CSGetSceneMonsterRequest的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newCSGetSceneMonsterRequest(): Protocol.CSGetSceneMonsterRequest {
    return {m_iSceneID:0} as Protocol.CSGetSceneMonsterRequest;
}
