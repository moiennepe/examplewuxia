/**
 * 创建1个KFSCTGSendMsg_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newKFSCTGSendMsg_Request(): Protocol.KFSCTGSendMsg_Request {
    return {m_ucTag:0} as Protocol.KFSCTGSendMsg_Request;
}
