/**
 * 创建1个FaZhen_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newFaZhen_Request(): Protocol.FaZhen_Request {
    return {m_usType:0, m_stValue:{m_ucOpenPanel:0, m_uiActiveID:0, m_uiImageID:0}} as Protocol.FaZhen_Request;
}
