/**
 * 创建1个EnterRegion_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newEnterRegion_Request(): Protocol.EnterRegion_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_iSceneID:0, m_iRegionID:0} as Protocol.EnterRegion_Request;
}
