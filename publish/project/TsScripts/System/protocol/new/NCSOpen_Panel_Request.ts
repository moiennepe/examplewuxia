﻿/**
 * 创建1个CSOpen_Panel_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSOpen_Panel_Request(): Protocol.CSOpen_Panel_Request {
    return {m_ucType:0} as Protocol.CSOpen_Panel_Request;
}
