/**
 * 创建1个OpCrystal_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newOpCrystal_Request(): Protocol.OpCrystal_Request {
    return {m_iOperateType:0, m_cType:0, m_iNum:0, m_ascPos:[]} as Protocol.OpCrystal_Request;
}
