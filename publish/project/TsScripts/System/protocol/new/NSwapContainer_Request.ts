/**
 * 创建1个SwapContainer_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSwapContainer_Request(): Protocol.SwapContainer_Request {
    return {m_stSrcContainerID:{m_ucContainerType:0, m_stOwnerID:{m_uiUin:0, m_uiSeq:0}}, m_stSrcThingList:{m_iThingNumber:0, m_astThing:[]}, m_stDstContainerID:{m_ucContainerType:0, m_stOwnerID:{m_uiUin:0, m_uiSeq:0}}, m_ucDstPosition:0} as Protocol.SwapContainer_Request;
}
