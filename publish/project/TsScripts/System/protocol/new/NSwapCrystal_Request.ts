/**
 * 创建1个SwapCrystal_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSwapCrystal_Request(): Protocol.SwapCrystal_Request {
    return {m_cSrcPos:0, m_cSrcType:0, m_cDstPos:0, m_cDstType:0} as Protocol.SwapCrystal_Request;
}
