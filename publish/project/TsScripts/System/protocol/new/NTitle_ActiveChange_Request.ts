/**
 * 创建1个Title_ActiveChange_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newTitle_ActiveChange_Request(): Protocol.Title_ActiveChange_Request {
    return {m_usType:0, m_stValue:{m_stSetTitleReq:{m_usSetTitleID:0, m_usSetType:0}, m_ucGetTitleDataReq:0}} as Protocol.Title_ActiveChange_Request;
}
