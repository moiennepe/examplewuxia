/**
 * 创建1个ZZHC_Reward_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newZZHC_Reward_Request(): Protocol.ZZHC_Reward_Request {
    return {m_ucType:0} as Protocol.ZZHC_Reward_Request;
}
