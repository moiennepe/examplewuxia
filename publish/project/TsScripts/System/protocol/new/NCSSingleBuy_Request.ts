﻿/**
 * 创建1个CSSingleBuy_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSSingleBuy_Request(): Protocol.CSSingleBuy_Request {
    return {m_uiTimes:0} as Protocol.CSSingleBuy_Request;
}
