/**
 * 创建1个BeautyDrug_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newBeautyDrug_Request(): Protocol.BeautyDrug_Request {
    return {m_iBeautyID:0, m_ucDrugType:0, m_ucOpType:0} as Protocol.BeautyDrug_Request;
}
