/**
 * 创建1个SpecialTransport_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSpecialTransport_Request(): Protocol.SpecialTransport_Request {
    return {m_stEnterType:{m_cEnterType:0, m_iPara1:0, m_iPara2:0, m_iPara3:0}} as Protocol.SpecialTransport_Request;
}
