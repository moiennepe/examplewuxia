/**
 * 创建1个PPStoreSell_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newPPStoreSell_Request(): Protocol.PPStoreSell_Request {
    return {m_stSellThing:{m_iThingID:0, m_usPosition:0, m_iNumber:0}, m_iEachPrice:0, m_iExpireTime:0, m_iNPCID:0} as Protocol.PPStoreSell_Request;
}
