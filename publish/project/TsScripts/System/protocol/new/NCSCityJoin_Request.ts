﻿/**
 * 创建1个CSCityJoin_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCityJoin_Request(): Protocol.CSCityJoin_Request {
    return {m_ucCityID:0} as Protocol.CSCityJoin_Request;
}
