/**
 * 创建1个ContainerBuySpace_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newContainerBuySpace_Request(): Protocol.ContainerBuySpace_Request {
    return {m_stContainerID:{m_ucContainerType:0, m_stOwnerID:{m_uiUin:0, m_uiSeq:0}}, m_ushSpaceNum:0} as Protocol.ContainerBuySpace_Request;
}
