/**
 * 创建1个ItemMerge_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newItemMerge_Request(): Protocol.ItemMerge_Request {
    return {m_uiTargetID:0, m_uiTargetNum:0} as Protocol.ItemMerge_Request;
}
