﻿/**
 * 创建1个CSCitySign_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCitySign_Request(): Protocol.CSCitySign_Request {
    return {m_ucCityID:0} as Protocol.CSCitySign_Request;
}
