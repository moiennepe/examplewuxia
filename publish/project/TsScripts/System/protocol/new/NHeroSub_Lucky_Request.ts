/**
 * 创建1个HeroSub_Lucky_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newHeroSub_Lucky_Request(): Protocol.HeroSub_Lucky_Request {
    return {m_ucType:0, m_ucOperate:0, m_uiParam:0} as Protocol.HeroSub_Lucky_Request;
}
