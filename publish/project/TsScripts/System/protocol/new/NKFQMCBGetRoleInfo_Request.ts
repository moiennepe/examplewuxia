/**
 * 创建1个KFQMCBGetRoleInfo_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newKFQMCBGetRoleInfo_Request(): Protocol.KFQMCBGetRoleInfo_Request {
    return {m_ucRankType:0} as Protocol.KFQMCBGetRoleInfo_Request;
}
