/**
 * 创建1个JiuXing_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newJiuXing_Request(): Protocol.JiuXing_Request {
    return {m_usType:0, m_stValue:{m_ucOpenPanelRequest:0, m_stUpgradeRequest:{m_ucType:0, m_ucAutoBuy:0}, m_ucKeepReq:0, m_uiFillReq:0}} as Protocol.JiuXing_Request;
}
