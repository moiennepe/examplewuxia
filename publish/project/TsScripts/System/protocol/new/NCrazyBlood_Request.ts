/**
 * 创建1个CrazyBlood_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newCrazyBlood_Request(): Protocol.CrazyBlood_Request {
    return {m_usType:0, m_stValue:{m_ucOpen:0, m_ucUpStage:0}} as Protocol.CrazyBlood_Request;
}
