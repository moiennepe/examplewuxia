/**
 * 创建1个RoleWing_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newRoleWing_Request(): Protocol.RoleWing_Request {
    return {m_ucType:0, m_stValue:{m_stWingCreateReq:{m_iID:0, m_stContainerThingA:{m_iThingID:0, m_usPosition:0, m_iNumber:0}, m_stContainerThingB:{m_iThingID:0, m_usPosition:0, m_iNumber:0}, m_stContainerThingC:{m_iThingID:0, m_usPosition:0, m_iNumber:0}}, m_ucStrengthenReq:0}} as Protocol.RoleWing_Request;
}
