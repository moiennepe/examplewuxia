/**
 * 创建1个CastSkillParameter的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newCastSkillParameter(): Protocol.CastSkillParameter {
    return {m_ucType:0, m_stValue:{m_iSkillID:0, m_iPrevSkillID:0, m_stContainerThing:{m_iThingID:0, m_usPosition:0, m_iNumber:0}}, m_ushAngle:0, m_ucTag:0} as Protocol.CastSkillParameter;
}
