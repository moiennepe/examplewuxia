﻿/**
 * 创建1个Guild_UpdateGrade_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newGuild_UpdateGrade_Request(): Protocol.Guild_UpdateGrade_Request {
    return {m_stTargetRoleID:{m_uiUin:0, m_uiSeq:0}, m_ushGuildGrade:0} as Protocol.Guild_UpdateGrade_Request;
}
