/**
 * 创建1个CastSkill_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newCastSkill_Request(): Protocol.CastSkill_Request {
    return {m_ushCasterUnitID:0, m_stCastSkillParamerter:{m_ucType:0, m_stValue:{m_iSkillID:0, m_iPrevSkillID:0, m_stContainerThing:{m_iThingID:0, m_usPosition:0, m_iNumber:0}}, m_ushAngle:0, m_ucTag:0}, m_stSkillTarget:{m_ushTargetType:0, m_iUnitID:0, m_stUnitPosition:{m_uiX:0, m_uiY:0}}, m_stEffectTargetList:{m_ucEffectTargetNumber:0, m_astSkillEffectTarget:[]}} as Protocol.CastSkill_Request;
}
