/**
 * 创建1个Guild_CROSSPVP_CS_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newGuild_CROSSPVP_CS_Request(): Protocol.Guild_CROSSPVP_CS_Request {
    return {m_usType:0, m_stValue:{m_usOpenPanelReq:0, m_usDoRewardReq:0}} as Protocol.Guild_CROSSPVP_CS_Request;
}
