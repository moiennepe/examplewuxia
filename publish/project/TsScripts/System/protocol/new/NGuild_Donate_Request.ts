﻿/**
 * 创建1个Guild_Donate_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newGuild_Donate_Request(): Protocol.Guild_Donate_Request {
    return {m_ucType:0, m_uiCount:0} as Protocol.Guild_Donate_Request;
}
