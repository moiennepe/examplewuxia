﻿/**
 * 创建1个CSCross_RecruitTeam_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCross_RecruitTeam_Request(): Protocol.CSCross_RecruitTeam_Request {
    return {m_uiPinstanceID:0} as Protocol.CSCross_RecruitTeam_Request;
}
