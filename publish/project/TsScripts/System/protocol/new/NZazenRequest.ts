/**
 * 创建1个ZazenRequest的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newZazenRequest(): Protocol.ZazenRequest {
    return {m_ucType:0, m_stValue:{m_ucStartZazen:0, m_ucOfflineQuery:0, m_ucOfflineReward:0}} as Protocol.ZazenRequest;
}
