/**
 * 创建1个BeautyAwake_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newBeautyAwake_Request(): Protocol.BeautyAwake_Request {
    return {m_ucType:0, m_stValue:{m_iStrengthenBeautyID:0}} as Protocol.BeautyAwake_Request;
}
