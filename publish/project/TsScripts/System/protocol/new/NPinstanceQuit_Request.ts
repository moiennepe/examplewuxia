/**
 * 创建1个PinstanceQuit_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newPinstanceQuit_Request(): Protocol.PinstanceQuit_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}} as Protocol.PinstanceQuit_Request;
}
