/**
 * 创建1个StarLottery_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newStarLottery_Request(): Protocol.StarLottery_Request {
    return {m_ucType:0, m_stValue:{m_ucPanel:0, m_ucLotteryNum:0, m_ucListType:0, m_ucGetTopPrize:0, m_ucTopPrizeChooseID:0, m_ucHongBaoEntry:0, m_ucHongBaoHistory:0}} as Protocol.StarLottery_Request;
}
