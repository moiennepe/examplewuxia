﻿/**
 * 创建1个CSCross_KickTeam_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCross_KickTeam_Request(): Protocol.CSCross_KickTeam_Request {
    return {m_uiPinstanceID:0, m_uiTeamID:0, m_stRoleID:{m_uiUin:0, m_uiSeq:0}} as Protocol.CSCross_KickTeam_Request;
}
