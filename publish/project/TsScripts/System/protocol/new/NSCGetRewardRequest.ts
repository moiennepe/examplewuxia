/**
 * 创建1个SCGetRewardRequest的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSCGetRewardRequest(): Protocol.SCGetRewardRequest {
    return {m_ucGetBit:0} as Protocol.SCGetRewardRequest;
}
