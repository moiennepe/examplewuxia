/**
 * 创建1个VIPOperate_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newVIPOperate_Request(): Protocol.VIPOperate_Request {
    return {m_usType:0, m_stValue:{m_iListData:0, m_iGetGiftType:0, m_ucGetVipLevel:0, m_iListMonthData:0, m_iMonthBuyLevel:0, m_stGetMonthGiftReq:{m_iType:0, m_iLevel:0}, m_iBuyPinstance:0, m_ucSpecialPriBuy:0, m_ucSpecialPriList:0, m_iVPIPriHYListReq:0}} as Protocol.VIPOperate_Request;
}
