/**
 * 创建1个ItemTransport_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newItemTransport_Request(): Protocol.ItemTransport_Request {
    return {m_ucTransportType:0, m_iSceneID:0, m_stPositionInfo:{m_stUnitPosition:{m_uiX:0, m_uiY:0}, m_iNPCID:0}, m_iQuestFeedback:0, m_ucExtraType:0, m_iRdmRadius:0} as Protocol.ItemTransport_Request;
}
