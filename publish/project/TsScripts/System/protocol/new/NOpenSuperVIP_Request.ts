/**
 * 创建1个OpenSuperVIP_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newOpenSuperVIP_Request(): Protocol.OpenSuperVIP_Request {
    return {m_uiTag:0} as Protocol.OpenSuperVIP_Request;
}
