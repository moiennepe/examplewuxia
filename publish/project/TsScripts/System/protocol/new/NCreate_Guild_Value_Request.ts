﻿/**
 * 创建1个Create_Guild_Value_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCreate_Guild_Value_Request(): Protocol.Create_Guild_Value_Request {
    return {m_szGuildName:"", m_szNitce:""} as Protocol.Create_Guild_Value_Request;
}
