/**
 * 创建1个SevenDayFund_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSevenDayFund_Request(): Protocol.SevenDayFund_Request {
    return {m_usType:0, m_stValue:{m_ucBuyType:0, m_stGetTReq:{m_ucType:0, m_ucGetID:0}}} as Protocol.SevenDayFund_Request;
}
