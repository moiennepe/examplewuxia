/**
 * 创建1个PPStoreGetAllThingNum_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newPPStoreGetAllThingNum_Request(): Protocol.PPStoreGetAllThingNum_Request {
    return {m_iClassID:0, m_stRoleID:{m_uiUin:0, m_uiSeq:0}} as Protocol.PPStoreGetAllThingNum_Request;
}
