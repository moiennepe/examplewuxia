/**
 * 创建1个HideSightRole_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newHideSightRole_Request(): Protocol.HideSightRole_Request {
    return {m_iHideLevel:0} as Protocol.HideSightRole_Request;
}
