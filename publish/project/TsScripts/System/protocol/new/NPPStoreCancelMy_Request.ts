/**
 * 创建1个PPStoreCancelMy_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newPPStoreCancelMy_Request(): Protocol.PPStoreCancelMy_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_stPSBID:{m_uiTime:0, m_uiSeq:0}, m_iPageNo:0, m_iNPCID:0} as Protocol.PPStoreCancelMy_Request;
}
