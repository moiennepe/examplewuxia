/**
 * 创建1个QianKunLu_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newQianKunLu_Request(): Protocol.QianKunLu_Request {
    return {m_usType:0, m_stValue:{m_ucOpenPanel:0, m_uiLianZhiID:0, m_ucBuyDouble:0, m_uiDuiHuanID:0}} as Protocol.QianKunLu_Request;
}
