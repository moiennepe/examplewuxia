/**
 * 创建1个MonthCard_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newMonthCard_Request(): Protocol.MonthCard_Request {
    return {m_usType:0, m_stValue:{m_ucOpenMCPanel:0, m_ucType:0, m_ucTypeByDaily:0, m_ucBuyType:0}} as Protocol.MonthCard_Request;
}
