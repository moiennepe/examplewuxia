/**
 * 创建1个NPCStoreLimitList_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newNPCStoreLimitList_Request(): Protocol.NPCStoreLimitList_Request {
    return {m_iStoreID:0} as Protocol.NPCStoreLimitList_Request;
}
