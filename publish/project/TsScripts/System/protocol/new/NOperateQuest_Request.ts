/**
 * 创建1个OperateQuest_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newOperateQuest_Request(): Protocol.OperateQuest_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_iQuestID:0, m_ucQuestOperation:0, m_uiParam:0} as Protocol.OperateQuest_Request;
}
