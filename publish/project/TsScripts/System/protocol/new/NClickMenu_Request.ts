/**
 * 创建1个ClickMenu_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newClickMenu_Request(): Protocol.ClickMenu_Request {
    return {m_iOwnerID:0, m_iExtraParam:0, m_ucMenuNodeIndex:0} as Protocol.ClickMenu_Request;
}
