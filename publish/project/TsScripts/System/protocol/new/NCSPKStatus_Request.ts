/**
 * 创建1个CSPKStatus_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newCSPKStatus_Request(): Protocol.CSPKStatus_Request {
    return {m_ucNewPKStaus:0} as Protocol.CSPKStatus_Request;
}
