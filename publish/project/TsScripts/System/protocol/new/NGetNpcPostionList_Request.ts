/**
 * 创建1个GetNpcPostionList_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newGetNpcPostionList_Request(): Protocol.GetNpcPostionList_Request {
    return {m_ucPlace:0} as Protocol.GetNpcPostionList_Request;
}
