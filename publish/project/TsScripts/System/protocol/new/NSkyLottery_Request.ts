/**
 * 创建1个SkyLottery_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSkyLottery_Request(): Protocol.SkyLottery_Request {
    return {m_ucType:0, m_ucLotterType:0, m_stValue:{m_usLotteryNum:0, m_ucListType:0, m_iNeedCount:0, m_ucOpenPanel:0}} as Protocol.SkyLottery_Request;
}
