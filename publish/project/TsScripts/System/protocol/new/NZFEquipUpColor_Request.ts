/**
 * 创建1个ZFEquipUpColor_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newZFEquipUpColor_Request(): Protocol.ZFEquipUpColor_Request {
    return {m_stContainerID:{m_ucContainerType:0, m_stOwnerID:{m_uiUin:0, m_uiSeq:0}}, m_stContainerThing:{m_iThingID:0, m_usPosition:0, m_iNumber:0}} as Protocol.ZFEquipUpColor_Request;
}
