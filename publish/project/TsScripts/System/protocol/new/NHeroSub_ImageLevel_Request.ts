/**
 * 创建1个HeroSub_ImageLevel_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newHeroSub_ImageLevel_Request(): Protocol.HeroSub_ImageLevel_Request {
    return {m_ucType:0, m_uiImageID:0} as Protocol.HeroSub_ImageLevel_Request;
}
