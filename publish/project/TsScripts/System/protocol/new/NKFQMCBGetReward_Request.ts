/**
 * 创建1个KFQMCBGetReward_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newKFQMCBGetReward_Request(): Protocol.KFQMCBGetReward_Request {
    return {m_iID:0} as Protocol.KFQMCBGetReward_Request;
}
