/**
 * 创建1个HeroSub_Wish_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newHeroSub_Wish_Request(): Protocol.HeroSub_Wish_Request {
    return {m_ucType:0, m_ucAutoBuy:0} as Protocol.HeroSub_Wish_Request;
}
