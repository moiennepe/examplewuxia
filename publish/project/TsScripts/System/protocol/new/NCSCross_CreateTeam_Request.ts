﻿/**
 * 创建1个CSCross_CreateTeam_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCross_CreateTeam_Request(): Protocol.CSCross_CreateTeam_Request {
    return {m_uiPinstanceID:0, m_uiFight:0, m_ucAutoStart:0, m_szPassword:"", m_szDomain:"", m_szIP:"", m_uiPort:0} as Protocol.CSCross_CreateTeam_Request;
}
