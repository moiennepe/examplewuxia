/**
 * 创建1个GetThingProperty_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newGetThingProperty_Request(): Protocol.GetThingProperty_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_stThingGUID:{m_iCreateTime:0, m_ucWorldID:0, m_ucZoneID:0, m_usSequence:0}} as Protocol.GetThingProperty_Request;
}
