/**
 * 创建1个GetPlayerPosRequest的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newGetPlayerPosRequest(): Protocol.GetPlayerPosRequest {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}} as Protocol.GetPlayerPosRequest;
}
