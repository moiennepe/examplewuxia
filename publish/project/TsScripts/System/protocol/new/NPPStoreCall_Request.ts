/**
 * 创建1个PPStoreCall_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newPPStoreCall_Request(): Protocol.PPStoreCall_Request {
    return {m_stID:{m_uiTime:0, m_uiSeq:0}, m_uiItemID:0, m_uiItemCount:0, m_uiItemPrice:0} as Protocol.PPStoreCall_Request;
}
