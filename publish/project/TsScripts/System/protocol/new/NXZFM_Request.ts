/**
 * 创建1个XZFM_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newXZFM_Request(): Protocol.XZFM_Request {
    return {m_ucType:0, m_stValue:{m_ucPanel:0, m_ucRewardID:0, m_ucRank:0}} as Protocol.XZFM_Request;
}
