﻿/**
 * 创建1个Dress_Train_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newDress_Train_Request(): Protocol.Dress_Train_Request {
    return {m_ucType:0, m_ucSubType:0, m_uiImageID:0} as Protocol.Dress_Train_Request;
}
