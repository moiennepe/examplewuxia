/**
 * 创建1个HeroSub_Show_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newHeroSub_Show_Request(): Protocol.HeroSub_Show_Request {
    return {m_ucType:0, m_uiImageID:0} as Protocol.HeroSub_Show_Request;
}
