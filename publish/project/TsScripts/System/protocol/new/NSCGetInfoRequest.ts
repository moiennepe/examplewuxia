/**
 * 创建1个SCGetInfoRequest的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSCGetInfoRequest(): Protocol.SCGetInfoRequest {
    return {m_ucFlag:0} as Protocol.SCGetInfoRequest;
}
