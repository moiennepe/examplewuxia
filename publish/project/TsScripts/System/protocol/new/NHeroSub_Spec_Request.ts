/**
 * 创建1个HeroSub_Spec_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newHeroSub_Spec_Request(): Protocol.HeroSub_Spec_Request {
    return {m_ucType:0, m_ucOperate:0, m_stValue:{m_ucTZMountReq:0}} as Protocol.HeroSub_Spec_Request;
}
