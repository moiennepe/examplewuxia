/**
 * 创建1个WorldPaiMai_Buy_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newWorldPaiMai_Buy_Request(): Protocol.WorldPaiMai_Buy_Request {
    return {m_iItemFlowID:0, m_uiPrice:0} as Protocol.WorldPaiMai_Buy_Request;
}
