/**
 * 创建1个OperateTeam_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newOperateTeam_Request(): Protocol.OperateTeam_Request {
    return {m_ucOperation:0, m_stTeamID:{m_uiCreateTime:0, m_uiSeq:0}, m_stDstRoleID:{m_uiUin:0, m_uiSeq:0}, m_stTeamRestriction:{m_ucMemberCanInvite:0, m_ucCanOtherJoin:0}} as Protocol.OperateTeam_Request;
}
