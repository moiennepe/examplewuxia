/**
 * 创建1个OperateSkill_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newOperateSkill_Request(): Protocol.OperateSkill_Request {
    return {m_ucOperate:0, m_stValue:{m_stStudySkill:{m_iSkillID:0, m_ucOneKey:0, m_ucStep:0}, m_stValidSkill:{m_iSkillID:0, m_ucForbidden:0}, m_stSetFetterSkill:{m_iSkillID:0, m_ucPos:0}}} as Protocol.OperateSkill_Request;
}
