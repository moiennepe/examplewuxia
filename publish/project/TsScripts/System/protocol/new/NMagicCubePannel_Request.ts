/**
 * 创建1个MagicCubePannel_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newMagicCubePannel_Request(): Protocol.MagicCubePannel_Request {
    return {m_usType:0, m_stValue:{m_uiFill:0, m_ucKeep:0}} as Protocol.MagicCubePannel_Request;
}
