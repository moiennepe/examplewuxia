/**
 * 创建1个Chat_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newChat_Request(): Protocol.Chat_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_ucChannel:0, m_stPrivateRoleID:{m_uiUin:0, m_uiSeq:0}, m_szMessage:"", m_iCampID:0, m_ucMsgDataNum:0, m_astMsgData:[], m_ucCheck:""} as Protocol.Chat_Request;
}
