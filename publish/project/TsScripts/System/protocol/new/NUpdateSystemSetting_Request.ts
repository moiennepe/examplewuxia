/**
 * 创建1个UpdateSystemSetting_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newUpdateSystemSetting_Request(): Protocol.UpdateSystemSetting_Request {
    return {m_stSystemSettingList:{m_ucNumber:0, m_ucValueList:[], m_ucCacheNumber:0, m_ucCacheValueList:[], m_szCacheValue:""}} as Protocol.UpdateSystemSetting_Request;
}
