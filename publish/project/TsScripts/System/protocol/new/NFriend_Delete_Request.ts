/**
 * 创建1个Friend_Delete_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newFriend_Delete_Request(): Protocol.Friend_Delete_Request {
    return {m_stOperatorRoleID:{m_uiUin:0, m_uiSeq:0}, m_stTargetRoleID:{m_uiUin:0, m_uiSeq:0}, m_ucType:0} as Protocol.Friend_Delete_Request;
}
