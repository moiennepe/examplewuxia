/**
 * 创建1个KFActInfo_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newKFActInfo_Request(): Protocol.KFActInfo_Request {
    return {m_usType:0, m_stValue:{m_ucBossType:0, m_uiGetID:0, m_ucZZZDOpenPanel:0, m_ucOpenLJCZPanel:0, m_ucGetBit:0, m_ucOpenKFJJRPanel:0, m_iStageDayId:0, m_ucYYDBOpenPanel:0, m_ucYYDBGetGift:0, m_ucYYDBJoin:0, m_ucKFXFFLOpen:0, m_ucKFXFFLGet:0, m_uc7DayLJCZOpen:0, m_uc7DayLJCZGet:0, m_uiKFLCFLOpen:0, m_uiKFLCFLGet:0, m_ucKFXFLBOpen:0, m_ucKFXFLBGet:0, m_ucVIPShopPanel:0, m_stVIPShopBuyReq:{m_iID:0, m_iNum:0, m_iVipLevel:0}, m_ucBossSummon:0, m_ucSummonType:0, m_stHappyChargeRebateReq:{m_uiRebateID:0, m_uiCharge:0}, m_ucJJRRankPannel:0, m_usSummonWarn:0, m_ucLuckyCatPannelReq:0, m_ucLuckyCatDrawReq:0, m_ucZGLBOpenReq:0, m_ucZGLBGetReq:0}} as Protocol.KFActInfo_Request;
}
