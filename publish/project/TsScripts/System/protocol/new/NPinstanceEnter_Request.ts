/**
 * 创建1个PinstanceEnter_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newPinstanceEnter_Request(): Protocol.PinstanceEnter_Request {
    return {m_iNPCID:0, m_iPinstanceID:0, m_ucRetryTag:0, m_ucDiff:0, m_ucJump:0, m_ucIsCost:0} as Protocol.PinstanceEnter_Request;
}
