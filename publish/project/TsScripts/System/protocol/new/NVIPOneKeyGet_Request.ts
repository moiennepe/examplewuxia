/**
 * 创建1个VIPOneKeyGet_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newVIPOneKeyGet_Request(): Protocol.VIPOneKeyGet_Request {
    return {m_iID:0, m_ucType:0, m_ucPara:0} as Protocol.VIPOneKeyGet_Request;
}
