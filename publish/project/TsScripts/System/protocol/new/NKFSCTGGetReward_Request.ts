/**
 * 创建1个KFSCTGGetReward_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newKFSCTGGetReward_Request(): Protocol.KFSCTGGetReward_Request {
    return {m_ucRewardID:0} as Protocol.KFSCTGGetReward_Request;
}
