/**
 * 创建1个KFQMCBGetInfo_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newKFQMCBGetInfo_Request(): Protocol.KFQMCBGetInfo_Request {
    return {m_ucRankType:0, m_ucDay:0} as Protocol.KFQMCBGetInfo_Request;
}
