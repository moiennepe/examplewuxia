/**
 * 创建1个DeleteRole_Account_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newDeleteRole_Account_Request(): Protocol.DeleteRole_Account_Request {
    return {m_stLianYunPlatInfo:{m_szPlatNameStr:"", m_iClient:0, m_iIsAdult:0, m_iServerID:0, m_iPlatTime:0, m_ucPlatSignStr:"", m_usType:0, m_szChannelID:"", m_uiTokenID:0, m_uiSessionID:0}, m_stRoleID:{m_uiUin:0, m_uiSeq:0}} as Protocol.DeleteRole_Account_Request;
}
