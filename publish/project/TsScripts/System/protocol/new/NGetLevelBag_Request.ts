/**
 * 创建1个GetLevelBag_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newGetLevelBag_Request(): Protocol.GetLevelBag_Request {
    return {m_usLevel:0} as Protocol.GetLevelBag_Request;
}
