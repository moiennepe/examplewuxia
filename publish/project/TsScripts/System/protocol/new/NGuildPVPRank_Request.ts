/**
 * 创建1个GuildPVPRank_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newGuildPVPRank_Request(): Protocol.GuildPVPRank_Request {
    return {m_ucRankType:0} as Protocol.GuildPVPRank_Request;
}
