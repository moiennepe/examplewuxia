/**
 * 创建1个NPCBehaviour_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newNPCBehaviour_Request(): Protocol.NPCBehaviour_Request {
    return {m_stRoleID:{m_uiUin:0, m_uiSeq:0}, m_iNPCID:0, m_iStoreID:0, m_ucAmount:0, m_ucBehaviourID:0, m_stThing:{m_iThingID:0, m_usPosition:0, m_iNumber:0}, m_iExchangeID:0, m_ucAutoUse:0} as Protocol.NPCBehaviour_Request;
}
