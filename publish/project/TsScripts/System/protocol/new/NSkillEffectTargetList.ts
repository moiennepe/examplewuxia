/**
 * 创建1个SkillEffectTargetList的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newSkillEffectTargetList(): Protocol.SkillEffectTargetList {
    return {m_ucEffectTargetNumber:0, m_astSkillEffectTarget:[]} as Protocol.SkillEffectTargetList;
}
