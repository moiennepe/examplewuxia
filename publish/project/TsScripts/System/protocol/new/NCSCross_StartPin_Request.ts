﻿/**
 * 创建1个CSCross_StartPin_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCross_StartPin_Request(): Protocol.CSCross_StartPin_Request {
    return {m_uiPinstanceID:0, m_uiTeamID:0} as Protocol.CSCross_StartPin_Request;
}
