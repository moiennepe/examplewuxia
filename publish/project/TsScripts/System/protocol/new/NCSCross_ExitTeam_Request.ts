﻿/**
 * 创建1个CSCross_ExitTeam_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newCSCross_ExitTeam_Request(): Protocol.CSCross_ExitTeam_Request {
    return {m_uiPinstanceID:0, m_uiTeamID:0} as Protocol.CSCross_ExitTeam_Request;
}
