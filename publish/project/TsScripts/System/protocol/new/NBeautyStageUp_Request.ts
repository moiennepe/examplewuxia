/**
 * 创建1个BeautyStageUp_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newBeautyStageUp_Request(): Protocol.BeautyStageUp_Request {
    return {m_iBeautyID:0} as Protocol.BeautyStageUp_Request;
}
