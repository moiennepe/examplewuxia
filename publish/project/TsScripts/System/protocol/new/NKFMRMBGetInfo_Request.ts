/**
 * 创建1个KFMRMBGetInfo_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newKFMRMBGetInfo_Request(): Protocol.KFMRMBGetInfo_Request {
    return {m_ucTag:0} as Protocol.KFMRMBGetInfo_Request;
}
