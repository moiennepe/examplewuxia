﻿/**
 * 创建1个Guild_Apply_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker@TEPPEI-PC
 * @exports
 * @version 502
 */
export function newGuild_Apply_Request(): Protocol.Guild_Apply_Request {
    return {m_uiGuildID:0, m_ucCode:0} as Protocol.Guild_Apply_Request;
}
