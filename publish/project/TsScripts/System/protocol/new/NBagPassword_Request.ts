/**
 * 创建1个BagPassword_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newBagPassword_Request(): Protocol.BagPassword_Request {
    return {m_usType:0, m_stValue:{m_stBagPasswordStruct:{m_szPassword:"", m_szMail:""}, m_ucLock:0, m_szPassword:"", m_ucStatus:0}} as Protocol.BagPassword_Request;
}
