/**
 * 创建1个RefreshRewardQuest_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newRefreshRewardQuest_Request(): Protocol.RefreshRewardQuest_Request {
    return {m_ucQuestType:0, m_ucRefreshTimes:0, m_ucRefreshColore:0, m_ucIsAutobuy:0} as Protocol.RefreshRewardQuest_Request;
}
