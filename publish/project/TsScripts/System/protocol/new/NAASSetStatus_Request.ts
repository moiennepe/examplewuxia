/**
 * 创建1个AASSetStatus_Request的实例(工具生成，请勿手动修改)
 * @author TsClassMaker
 * @exports
 */
export function newAASSetStatus_Request(): Protocol.AASSetStatus_Request {
    return {m_usType:0} as Protocol.AASSetStatus_Request;
}
