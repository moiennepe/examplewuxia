/**
 * 协议宏定义(defined in Common.xml.xml)
 * @author TsClassMaker
 * @exports
 */
export class Macros {
    /**
     * 1, 背包物品
     */
    static readonly ACCESSORY_BAG: number = 1;

    /**
     * 2, 道具
     */
    static readonly ACCESSORY_BITMAP_BAG: number = 2;

    /**
     * 8, 历练
     */
    static readonly ACCESSORY_BITMAP_EXPLOIT: number = 8;

    /**
     * 1, 铜钱
     */
    static readonly ACCESSORY_BITMAP_TONGQIAN: number = 1;

    /**
     * 4, 元宝
     */
    static readonly ACCESSORY_BITMAP_YUANBAO: number = 4;

    /**
     * 3, 货币类型
     */
    static readonly ACCESSORY_CURRENCY: number = 3;

    /**
     * 2, 掉落方案
     */
    static readonly ACCESSORY_DROP: number = 2;

    /**
     * 4, 类型最大值无意义
     */
    static readonly ACCESSORY_MAX: number = 4;

    /**
     * 0, 占位无意义
     */
    static readonly ACCESSORY_MIN: number = 0;

    /**
     * 4, 铜钱寄售类型
     */
    static readonly ACCESSORY_TONGQIAN: number = 4;

    /**
     * 2, 激活月卡
     */
    static readonly ACTIVE_MONTH_CARD: number = 2;

    /**
     * 43, 温泉活动，查询当前状态协议
     */
    static readonly ACTIVITY_BATHE_LIST: number = 43;

    /**
     * 42, 温泉活动，动作操作协议请求
     */
    static readonly ACTIVITY_BATHE_REQ: number = 42;

    /**
     * 159, 步步高升，领取奖励
     */
    static readonly ACTIVITY_BBGS_GET_REWARD: number = 159;

    /**
     * 158, 步步高升，打开活动面板
     */
    static readonly ACTIVITY_BBGS_OPEN_PANEL: number = 158;

    /**
     * 61, 跨服3v3 领取赛季奖励
     */
    static readonly ACTIVITY_CROSS3V3_GET_REWARD: number = 61;

    /**
     * 60, 跨服3V3 打开面板
     */
    static readonly ACTIVITY_CROSS3V3_LIST: number = 60;

    /**
     * 23, 跨服boss列表信息
     */
    static readonly ACTIVITY_CROSS_BOSS_LIST: number = 23;

    /**
     * 24, 跨服Boss挖宝
     */
    static readonly ACTIVITY_CROSS_BOSS_REWARD: number = 24;

    /**
     * 76, 充值送豪礼, 领取充值礼包
     */
    static readonly ACTIVITY_CZSHL_GET: number = 76;

    /**
     * 75, 充值送豪礼，查询请求
     */
    static readonly ACTIVITY_CZSHL_LIST: number = 75;

    /**
     * 49, 领取收藏礼包
     */
    static readonly ACTIVITY_DESKTOPCOLLECTION_GET: number = 49;

    /**
     * 48, 查询收藏礼包
     */
    static readonly ACTIVITY_DESKTOPCOLLECTION_LIST: number = 48;

    /**
     * 18, 封魔塔宗派召集
     */
    static readonly ACTIVITY_FMT_GUILDCALL: number = 18;

    /**
     * 17, 封魔塔信息
     */
    static readonly ACTIVITY_FMT_LIST: number = 17;

    /**
     * 219, 落日BOSS打开面板
     */
    static readonly ACTIVITY_FOREST_BOSS_OPEN: number = 219;

    /**
     * 47, 国运活动，查询当前状态协议
     */
    static readonly ACTIVITY_GUOYUN_LIST: number = 47;

    /**
     * 46, 国运任务护送过程中的操作请求
     */
    static readonly ACTIVITY_GUOYUN_OPERATE: number = 46;

    /**
     * 45, 国运任务刷新运送等级操作请求
     */
    static readonly ACTIVITY_GUOYUN_REFRESH_LEVLE: number = 45;

    /**
     * 178, 欢聚小年折扣充值，打开活动面板
     */
    static readonly ACTIVITY_HJXN_CHARGE_PANNEL: number = 178;

    /**
     * 179, 欢聚小年折扣充值，领取奖励
     */
    static readonly ACTIVITY_HJXN_CHARGE_REWARD: number = 179;

    /**
     * 164, BOSS之家增加时间
     */
    static readonly ACTIVITY_HOME_BOSS_ADDTIME: number = 164;

    /**
     * 163, BOSS之家打开面板
     */
    static readonly ACTIVITY_HOME_BOSS_OPEN: number = 163;

    /**
     * 124, 运营活动_对应计费额度充值
     */
    static readonly ACTIVITY_ID_124DBCZ: number = 124;

    /**
     * 125, 运营活动_循环充值
     */
    static readonly ACTIVITY_ID_125XHCZ: number = 125;

    /**
     * 126, 运营活动_跨服消费
     */
    static readonly ACTIVITY_ID_126CROSSCHARGE: number = 126;

    /**
     * 71, 活动_360福利活动
     */
    static readonly ACTIVITY_ID_360FULI_ACT: number = 71;

    /**
     * 11, 活动_温泉活动
     */
    static readonly ACTIVITY_ID_BATHE: number = 11;

    /**
     * 45, 活动_百服庆典
     */
    static readonly ACTIVITY_ID_BFQD: number = 45;

    /**
     * 37, 活动_八方玄妖
     */
    static readonly ACTIVITY_ID_BFXYACT: number = 37;

    /**
     * 63, 活动_黑市商店
     */
    static readonly ACTIVITY_ID_BLACK_STORE: number = 63;

    /**
     * 90, 活动_步步高升
     */
    static readonly ACTIVITY_ID_BU_BU_GAO_SHENG: number = 90;

    /**
     * 14, 活动_宝箱争霸
     */
    static readonly ACTIVITY_ID_BXZB: number = 14;

    /**
     * 15, 活动_阵营战
     */
    static readonly ACTIVITY_ID_CAMPBATTLE: number = 15;

    /**
     * 88, 活动_充值红包
     */
    static readonly ACTIVITY_ID_CHARGE_REDBAG: number = 88;

    /**
     * 77, 活动_双倍扫荡
     */
    static readonly ACTIVITY_ID_CLEAN_OUT: number = 77;

    /**
     * 104, 活动_收集兑换
     */
    static readonly ACTIVITY_ID_COLLECT_EXCHANGE: number = 104;

    /**
     * 91, 活动_四象斗兽场
     */
    static readonly ACTIVITY_ID_COLOSSEUM: number = 91;

    /**
     * 87, 活动_限时云购
     */
    static readonly ACTIVITY_ID_CROSSYUNGOU: number = 87;

    /**
     * 40, 活动_跨服点灯
     */
    static readonly ACTIVITY_ID_CROSS_DDL: number = 40;

    /**
     * 80, 活动_跨服魅力榜
     */
    static readonly ACTIVITY_ID_CROSS_FLOWER_RANK: number = 80;

    /**
     * 52, 活动_跨服宗门战
     */
    static readonly ACTIVITY_ID_CROSS_GUILDPVPBATTLE: number = 52;

    /**
     * 41, 活动_跨服至尊争夺
     */
    static readonly ACTIVITY_ID_CROSS_ZZZD: number = 41;

    /**
     * 28, 活动_充值送好礼
     */
    static readonly ACTIVITY_ID_CZSHL: number = 28;

    /**
     * 26, 活动_消费赠好礼
     */
    static readonly ACTIVITY_ID_DAILYCONSUME_CONSUME: number = 26;

    /**
     * 76, 活动_每日首充送大礼
     */
    static readonly ACTIVITY_ID_DAILY_FIRST_RECHARGE: number = 76;

    /**
     * 6, 活动_收藏桌面喜开怀
     */
    static readonly ACTIVITY_ID_DESKTOPCOLLECTION: number = 6;

    /**
     * 36, 活动_限时折扣
     */
    static readonly ACTIVITY_ID_DISCOUT: number = 36;

    /**
     * 43, 活动_双倍挖宝
     */
    static readonly ACTIVITY_ID_DOUBLE_DIGGER: number = 43;

    /**
     * 56, 活动_双倍掉宝
     */
    static readonly ACTIVITY_ID_DOUBLE_DROP: number = 56;

    /**
     * 55, 活动_经验爽翻天
     */
    static readonly ACTIVITY_ID_DOUBLE_EXP: number = 55;

    /**
     * 10, 活动_双倍国运
     */
    static readonly ACTIVITY_ID_DOUBLE_GUOYUN: number = 10;

    /**
     * 34, 活动_端午
     */
    static readonly ACTIVITY_ID_DUANWU: number = 34;

    /**
     * 18, 活动_黄蓉专属
     */
    static readonly ACTIVITY_ID_DYZSPIN: number = 18;

    /**
     * 57, 活动_装备幸运日
     */
    static readonly ACTIVITY_ID_EQUIP_LUCKY: number = 57;

    /**
     * 67, 活动_节日充值
     */
    static readonly ACTIVITY_ID_FESTIVAL_CHARGE: number = 67;

    /**
     * 68, 活动_节日消费
     */
    static readonly ACTIVITY_ID_FESTIVAL_CONSUME: number = 68;

    /**
     * 69, 活动_节日兑换
     */
    static readonly ACTIVITY_ID_FESTIVAL_CONVERSION: number = 69;

    /**
     * 122, 活动_落日森林
     */
    static readonly ACTIVITY_ID_FOREST_BOSS: number = 122;

    /**
     * 102, 活动_个人BOSS
     */
    static readonly ACTIVITY_ID_GRBOSS: number = 102;

    /**
     * 16, 活动_宗派群英会
     */
    static readonly ACTIVITY_ID_GUILDPVPBATTLE: number = 16;

    /**
     * 13, 活动_国运活动
     */
    static readonly ACTIVITY_ID_GUOYUN: number = 13;

    /**
     * 39, 活动_黑暗侵袭
     */
    static readonly ACTIVITY_ID_HAQXACT: number = 39;

    /**
     * 73, 活动_铭文试炼
     */
    static readonly ACTIVITY_ID_HISTORICAL_REMAINS: number = 73;

    /**
     * 109, 欢聚小年活动_折扣充值
     */
    static readonly ACTIVITY_ID_HJXN_CHARGE: number = 109;

    /**
     * 53, 活动_欢乐翻牌
     */
    static readonly ACTIVITY_ID_HLFP: number = 53;

    /**
     * 59, 活动_欢乐探宝
     */
    static readonly ACTIVITY_ID_HLTB: number = 59;

    /**
     * 89, 活动_欢乐转盘
     */
    static readonly ACTIVITY_ID_HLZP: number = 89;

    /**
     * 70, 活动_BOSS之家
     */
    static readonly ACTIVITY_ID_HOME_BOSS_ACT: number = 70;

    /**
     * 78, 活动_双倍探宝
     */
    static readonly ACTIVITY_ID_HUNT_TREASURE: number = 78;

    /**
     * 25, 活动_极品大兑换
     */
    static readonly ACTIVITY_ID_JPDDH: number = 25;

    /**
     * 35, 活动_聚划算
     */
    static readonly ACTIVITY_ID_JUHSACT: number = 35;

    /**
     * 30, 活动_聚宝盆
     */
    static readonly ACTIVITY_ID_JU_BAO_PENG: number = 30;

    /**
     * 79, 活动_九转星宫
     */
    static readonly ACTIVITY_ID_JZXG: number = 79;

    /**
     * 8, 活动_开服冲榜
     */
    static readonly ACTIVITY_ID_KFCB: number = 8;

    /**
     * 7, 活动_开服活动
     */
    static readonly ACTIVITY_ID_KFHD: number = 7;

    /**
     * 103, 活动_跨服年兽
     */
    static readonly ACTIVITY_ID_KFNS: number = 103;

    /**
     * 82, 活动_开服首充团购
     */
    static readonly ACTIVITY_ID_KFSCTG: number = 82;

    /**
     * 62, 活动_老虎机
     */
    static readonly ACTIVITY_ID_LHJ: number = 62;

    /**
     * 31, 活动_灵狐仙府
     */
    static readonly ACTIVITY_ID_LING_HU_XIAN_FU: number = 31;

    /**
     * 119, 活动_连续返利
     */
    static readonly ACTIVITY_ID_LXFL: number = 119;

    /**
     * 81, 活动_魔化战争
     */
    static readonly ACTIVITY_ID_MHZZ: number = 81;

    /**
     * 66, 活动_怪物掉落
     */
    static readonly ACTIVITY_ID_MONSTER_DROP: number = 66;

    /**
     * 120, 活动_多人BOSS
     */
    static readonly ACTIVITY_ID_MULTIPLAYER_BOSS: number = 120;

    /**
     * 54, 活动_多倍经验
     */
    static readonly ACTIVITY_ID_MULTIPLE_EXP: number = 54;

    /**
     * 27, 活动_魔焰窟寻宝(魔帝遗迹)
     */
    static readonly ACTIVITY_ID_MYKXB: number = 27;

    /**
     * 5, 活动_在线奖励拿不完
     */
    static readonly ACTIVITY_ID_ONLINEGIFT: number = 5;

    /**
     * 23, 活动_开服签到
     */
    static readonly ACTIVITY_ID_OPENSIGN: number = 23;

    /**
     * 64, 活动_充值特惠
     */
    static readonly ACTIVITY_ID_PREFER_CHARGE: number = 64;

    /**
     * 85, 活动_平台福利
     */
    static readonly ACTIVITY_ID_PTFL: number = 85;

    /**
     * 49, 活动_跨服角斗场（积分赛）
     */
    static readonly ACTIVITY_ID_PVP_BASE: number = 49;

    /**
     * 50, 活动_跨服角斗场（决赛）
     */
    static readonly ACTIVITY_ID_PVP_FINAL: number = 50;

    /**
     * 95, 活动_跨服3V3
     */
    static readonly ACTIVITY_ID_PVP_MULTI: number = 95;

    /**
     * 84, 活动_庆典消费活动
     */
    static readonly ACTIVITY_ID_QINGDIAN_CONSUME: number = 84;

    /**
     * 113, 活动_全民嗨点
     */
    static readonly ACTIVITY_ID_QMHD: number = 113;

    /**
     * 101, 活动_情人节排行榜
     */
    static readonly ACTIVITY_ID_QRJPHB: number = 101;

    /**
     * 100, 活动_情人节转盘
     */
    static readonly ACTIVITY_ID_QRJZP: number = 100;

    /**
     * 21, 活动_答题活动
     */
    static readonly ACTIVITY_ID_QUESTIONACTIVITY: number = 21;

    /**
     * 29, 活动_充值兑换
     */
    static readonly ACTIVITY_ID_RECHARGEEXCHANGE: number = 29;

    /**
     * 83, 活动_发红包
     */
    static readonly ACTIVITY_ID_REDBAG: number = 83;

    /**
     * 86, 活动_rmb战场
     */
    static readonly ACTIVITY_ID_RMBZC: number = 86;

    /**
     * 61, 活动_回归同庆
     */
    static readonly ACTIVITY_ID_RRHAPPY: number = 61;

    /**
     * 112, 活动_限时秒杀
     */
    static readonly ACTIVITY_ID_RUSH_PURCHASE: number = 112;

    /**
     * 115, 活动_盛典宝箱
     */
    static readonly ACTIVITY_ID_SDBX: number = 115;

    /**
     * 38, 活动_神魔遮天
     */
    static readonly ACTIVITY_ID_SHENMOZHETIAN: number = 38;

    /**
     * 9, 活动_每日签到
     */
    static readonly ACTIVITY_ID_SIGN: number = 9;

    /**
     * 17, 活动_南蛮入侵
     */
    static readonly ACTIVITY_ID_SOUTHERNMAN: number = 17;

    /**
     * 106, 活动_充值奖励
     */
    static readonly ACTIVITY_ID_SPRING_CHARGE: number = 106;

    /**
     * 105, 活动_登录奖励
     */
    static readonly ACTIVITY_ID_SPRING_LOGIN: number = 105;

    /**
     * 19, 活动_死亡战场
     */
    static readonly ACTIVITY_ID_SWZC: number = 19;

    /**
     * 44, 活动_天地玄门
     */
    static readonly ACTIVITY_ID_TDXM: number = 44;

    /**
     * 20, 活动_天降福神
     */
    static readonly ACTIVITY_ID_TIANJIANGFUSHEN: number = 20;

    /**
     * 24, 活动_特卖大酬宾
     */
    static readonly ACTIVITY_ID_TMDCB: number = 24;

    /**
     * 65, 活动_图腾祝福
     */
    static readonly ACTIVITY_ID_TTZF: number = 65;

    /**
     * 75, 活动_卫城城主夺战
     */
    static readonly ACTIVITY_ID_WC_CROSSCITY: number = 75;

    /**
     * 60, 活动_王侯将相
     */
    static readonly ACTIVITY_ID_WHJX: number = 60;

    /**
     * 12, 活动_世界boss
     */
    static readonly ACTIVITY_ID_WORLDBOSS: number = 12;

    /**
     * 117, 活动_世界杯
     */
    static readonly ACTIVITY_ID_WORLDCUP: number = 117;

    /**
     * 118, 活动_世界杯冠军之路
     */
    static readonly ACTIVITY_ID_WORLDCUPCHAMPION: number = 118;

    /**
     * 33, 活动_星语心愿
     */
    static readonly ACTIVITY_ID_XINGYUAN: number = 33;

    /**
     * 48, 活动_幸运转盘
     */
    static readonly ACTIVITY_ID_XYZP: number = 48;

    /**
     * 92, 活动_血战封魔
     */
    static readonly ACTIVITY_ID_XZFM: number = 92;

    /**
     * 46, 活动_一本万利
     */
    static readonly ACTIVITY_ID_YBWL: number = 46;

    /**
     * 51, 活动_原梦战场
     */
    static readonly ACTIVITY_ID_YMZC: number = 51;

    /**
     * 32, 活动_元神战场
     */
    static readonly ACTIVITY_ID_YSZC: number = 32;

    /**
     * 74, 活动_主城城主夺战
     */
    static readonly ACTIVITY_ID_ZC_CROSSCITY: number = 74;

    /**
     * 97, 活动_珍珑棋局
     */
    static readonly ACTIVITY_ID_ZLQJ: number = 97;

    /**
     * 47, 活动_宗派封魔战
     */
    static readonly ACTIVITY_ID_ZPFM: number = 47;

    /**
     * 99, 活动_至尊皇城主城
     */
    static readonly ACTIVITY_ID_ZZHCMAIN: number = 99;

    /**
     * 98, 活动_至尊皇城护城
     */
    static readonly ACTIVITY_ID_ZZHCSUB: number = 98;

    /**
     * 42, 活动_至尊争夺
     */
    static readonly ACTIVITY_ID_ZZZD: number = 42;

    /**
     * 64, 聚宝盆，领取奖励
     */
    static readonly ACTIVITY_JBP_GET_REWARD: number = 64;

    /**
     * 63, 聚宝盆，打开活动面板
     */
    static readonly ACTIVITY_JBP_OPEN_PANEL: number = 63;

    /**
     * 95, 极品大兑换活动兑换奖品
     */
    static readonly ACTIVITY_JIPINDADUIHUAN: number = 95;

    /**
     * 41, 积善成礼，领取奖励
     */
    static readonly ACTIVITY_JSCL_GET_REWARD: number = 41;

    /**
     * 40, 积善成礼，信息查询
     */
    static readonly ACTIVITY_JSCL_LIST: number = 40;

    /**
     * 54, 跨服角斗场 领取每日奖励
     */
    static readonly ACTIVITY_KFJDC_GET_DAY: number = 54;

    /**
     * 55, 跨服角斗场 领取达成奖励
     */
    static readonly ACTIVITY_KFJDC_GET_REACH: number = 55;

    /**
     * 53, 跨服角斗场 打开面板
     */
    static readonly ACTIVITY_KFJDC_LIST: number = 53;

    /**
     * 174, 跨服年兽打开面板
     */
    static readonly ACTIVITY_KFNS_OPEN: number = 174;

    /**
     * 16, 跨服鲜花榜领取达成礼包
     */
    static readonly ACTIVITY_KFXHB_GET_REWARD: number = 16;

    /**
     * 15, 跨服鲜花榜信息
     */
    static readonly ACTIVITY_KFXHB_LIST: number = 15;

    /**
     * 176, 连续返利 领取奖励
     */
    static readonly ACTIVITY_LXFL_GET: number = 176;

    /**
     * 175, 连续返利 打开面板
     */
    static readonly ACTIVITY_LXFL_OPEN: number = 175;

    /**
     * 45, 活动超过45分钟，不会弹开活动进行的面板
     */
    static readonly ACTIVITY_NOTIFY_PANNEL_TIME: number = 45;

    /**
     * 50, 开服签到领取礼包
     */
    static readonly ACTIVITY_OPENSVR_SIGN_GET: number = 50;

    /**
     * 51, 开服签到查询礼包领取信息
     */
    static readonly ACTIVITY_OPENSVR_SIGN_LIST: number = 51;

    /**
     * 188, 全民嗨点面板
     */
    static readonly ACTIVITY_QMHD_PANEL: number = 188;

    /**
     * 189, 全民嗨点领取奖励
     */
    static readonly ACTIVITY_QMHD_REWARD: number = 189;

    /**
     * 30, 答题活动随机抽出20题
     */
    static readonly ACTIVITY_QUESTION_RANDOM_COUNT: number = 30;

    /**
     * 59, RMB战场活动 购买次数
     */
    static readonly ACTIVITY_RMBZC_BUYTIME: number = 59;

    /**
     * 58, RMB战场活动 取消加入
     */
    static readonly ACTIVITY_RMBZC_CANCEL: number = 58;

    /**
     * 57, RMB战场活动 加入
     */
    static readonly ACTIVITY_RMBZC_JOIN: number = 57;

    /**
     * 56, RMB战场活动 查询
     */
    static readonly ACTIVITY_RMBZC_LIST: number = 56;

    /**
     * 10, 活动，如阵营真排行版个数
     */
    static readonly ACTIVITY_ROLE_RANK_NUM: number = 10;

    /**
     * 80, 同庆回归玩家，领取充值礼包
     */
    static readonly ACTIVITY_RR_CHARE_GET: number = 80;

    /**
     * 79, 同庆回归玩家，查询请求
     */
    static readonly ACTIVITY_RR_CHARE_LIST: number = 79;

    /**
     * 218, 盛典宝箱达成奖励
     */
    static readonly ACTIVITY_SDBX_GET_REWARD: number = 218;

    /**
     * 217, 盛典宝箱打开面板
     */
    static readonly ACTIVITY_SDBX_OPEN_PANEL: number = 217;

    /**
     * 172, 收集兑换 打开面板
     */
    static readonly ACTIVITY_SJDH_PANNEL: number = 172;

    /**
     * 173, 收集兑换 开始兑换
     */
    static readonly ACTIVITY_SJDH_START: number = 173;

    /**
     * 177, 收集兑换 定制提醒
     */
    static readonly ACTIVITY_SJDH_WARN: number = 177;

    /**
     * 170, 春节充值，活动面板
     */
    static readonly ACTIVITY_SPRING_CHARGE_PANEL: number = 170;

    /**
     * 171, 春节充值，领取奖励
     */
    static readonly ACTIVITY_SPRING_CHARGE_REWARD: number = 171;

    /**
     * 168, 春节登录，活动面板
     */
    static readonly ACTIVITY_SPRING_LOGIN_PANEL: number = 168;

    /**
     * 169, 春节登录，领取奖励
     */
    static readonly ACTIVITY_SPRING_LOGIN_REWARD: number = 169;

    /**
     * 3, 活动已结束
     */
    static readonly ACTIVITY_STATUS_END: number = 3;

    /**
     * 1, 活动已完成
     */
    static readonly ACTIVITY_STATUS_FINISH: number = 1;

    /**
     * 0, 活动进行中
     */
    static readonly ACTIVITY_STATUS_RUNNING: number = 0;

    /**
     * 2, 活动未开始
     */
    static readonly ACTIVITY_STATUS_UNOPEN: number = 2;

    /**
     * 70, 特卖大酬宾购买
     */
    static readonly ACTIVITY_TMDCB_BUY: number = 70;

    /**
     * 69, 特卖大酬宾查询
     */
    static readonly ACTIVITY_TMDCB_LIST: number = 69;

    /**
     * 212, 世界杯比分投注
     */
    static readonly ACTIVITY_WORLDCUP_BET_SCORE: number = 212;

    /**
     * 211, 世界杯胜负投注
     */
    static readonly ACTIVITY_WORLDCUP_BET_WIN: number = 211;

    /**
     * 216, 世界杯冠军之路领取竞猜奖励
     */
    static readonly ACTIVITY_WORLDCUP_CHAMPION_GET: number = 216;

    /**
     * 215, 世界杯冠军之路竞猜
     */
    static readonly ACTIVITY_WORLDCUP_CHAMPION_GUESS: number = 215;

    /**
     * 214, 打开世界杯冠军之路面板
     */
    static readonly ACTIVITY_WORLDCUP_CHAMPION_PANEL: number = 214;

    /**
     * 210, 打开世界杯面板
     */
    static readonly ACTIVITY_WORLDCUP_PANEL: number = 210;

    /**
     * 213, 世界杯排行榜
     */
    static readonly ACTIVITY_WORLDCUP_RANK: number = 213;

    /**
     * 10, 世界Boss设置提醒
     */
    static readonly ACTIVITY_WORLD_BOSS_ALERT: number = 10;

    /**
     * 9, 世界Boss设置关注
     */
    static readonly ACTIVITY_WORLD_BOSS_ATTENT: number = 9;

    /**
     * 11, 世界Boss宗派召集
     */
    static readonly ACTIVITY_WORLD_BOSS_GUILDCALL: number = 11;

    /**
     * 7, 世界boss列表信息
     */
    static readonly ACTIVITY_WORLD_BOSS_LIST: number = 7;

    /**
     * 14, 世界Boss刷新提醒
     */
    static readonly ACTIVITY_WORLD_BOSS_REFRESH: number = 14;

    /**
     * 8, 世界Boss打开箱子奖励
     */
    static readonly ACTIVITY_WORLD_BOSS_REWARD: number = 8;

    /**
     * 67, 一本万利购买请求
     */
    static readonly ACTIVITY_YBWL_BUY: number = 67;

    /**
     * 68, 一本万利礼包领取请求
     */
    static readonly ACTIVITY_YBWL_GET_REWARD: number = 68;

    /**
     * 66, 一本万利打开面板请求
     */
    static readonly ACTIVITY_YBWL_OPEN_PANEL: number = 66;

    /**
     * 22, 宗门秘境宗派召集
     */
    static readonly ACTIVITY_ZPFM_GUILDCALL: number = 22;

    /**
     * 72, 欢乐翻牌发牌
     */
    static readonly ACT_HLFP_DEAL: number = 72;

    /**
     * 73, 欢乐翻牌领礼包
     */
    static readonly ACT_HLFP_GET: number = 73;

    /**
     * 71, 欢乐翻牌查询
     */
    static readonly ACT_HLFP_LIST: number = 71;

    /**
     * 74, 欢乐翻牌换牌
     */
    static readonly ACT_HLFP_REFRESH: number = 74;

    /**
     * 48, BOSS之家 BOSS数量16个
     */
    static readonly ACT_HOME_BOSS_MAX_NUM: number = 48;

    /**
     * 187, 限时秒杀购买
     */
    static readonly ACT_RUSH_PURCHASE_BUY: number = 187;

    /**
     * 186, 限时秒杀面板
     */
    static readonly ACT_RUSH_PURCHASE_PANEL: number = 186;

    /**
     * 3, 召唤boss种类
     */
    static readonly ACT_SUMMON_BOSS_TYPE_NUM: number = 3;

    /**
     * 3, 过期前一天推送
     */
    static readonly ADVANCE_TYPE: number = 3;

    /**
     * 2, 召唤爱德华,召唤
     */
    static readonly ALCHEMIST_EDWARD_TYPE: number = 2;

    /**
     * 3, 普通召唤,使用道具猎魂
     */
    static readonly ALCHEMIST_ITEM_TYPE: number = 3;

    /**
     * 1, 普通召唤,猎魂
     */
    static readonly ALCHEMIST_NORMAL_TYPE: number = 1;

    /**
     * 4, 嗨点奖励总数
     */
    static readonly ALLPEOPLE_REWARD_ITEM_COUNT: number = 4;

    /**
     * 4, 所有背包类型,即(0+1+2+3)
     */
    static readonly ALL_BAG_TYPE: number = 4;

    /**
     * 60405026, APP下载礼包 掉落ID
     */
    static readonly APP_DOWN_GIFT_DROPID: number = 60405026;

    /**
     * 1
     */
    static readonly AREA_EFFECT_TYPE_SCENE: number = 1;

    /**
     * 480, 定时恢复体力时间间隔 15分钟
     */
    static readonly AUTO_ENERGY_WY_INTERVAL: number = 480;

    /**
     * 150, 拾取物品时的范围限制
     */
    static readonly AUTO_PICKUP_LIMIT_RANGE: number = 150;

    /**
     * 228, 打开面板
     */
    static readonly Act124_PANNEL: number = 228;

    /**
     * 229, 领取奖励
     */
    static readonly Act124_REWARD: number = 229;

    /**
     * 230, 打开面板
     */
    static readonly Act125_PANNEL: number = 230;

    /**
     * 231, 领取奖励
     */
    static readonly Act125_REWARD: number = 231;

    /**
     * 236, 领取参与奖励
     */
    static readonly Act126_GET_REWARD: number = 236;

    /**
     * 235, 打开面板
     */
    static readonly Act126_PANEL: number = 235;

    /**
     * 237, 奖励达成通知
     */
    static readonly Act126_REWARD_NOTIFY: number = 237;

    /**
     * 233, 打开面板
     */
    static readonly Act63_PANEL: number = 233;

    /**
     * 234, 刷新
     */
    static readonly Act63_REFRESH: number = 234;

    /**
     * 1, 背包密码状态  锁定
     */
    static readonly BAG_PASSWORD_STATUS_LOCK: number = 1;

    /**
     * 0, 背包密码状态  无密码
     */
    static readonly BAG_PASSWORD_STATUS_NONE: number = 0;

    /**
     * 2, 背包密码状态  未锁定
     */
    static readonly BAG_PASSWORD_STATUS_UNLOCK: number = 2;

    /**
     * 0, 背包
     */
    static readonly BAG_TYPE: number = 0;

    /**
     * 3, 温泉活动，搓背操作类型
     */
    static readonly BATHE_BACK_RUBS_TYPE: number = 3;

    /**
     * 18, 温泉场景ID
     */
    static readonly BATHE_SCENE_ID: number = 18;

    /**
     * 1, 温泉活动，沐浴操作类型
     */
    static readonly BATHE_SPLASH_WATER_TYPE: number = 1;

    /**
     * 2, 温泉活动，擦肥皂操作类型
     */
    static readonly BATHE_WIPING_SOAP_TYPE: number = 2;

    /**
     * 1, 武缘觉醒
     */
    static readonly BEAUTY_AWAKE_STRENGTHEN: number = 1;

    /**
     * 10, 每个红颜的装备数量
     */
    static readonly BEAUTY_EQUIP_NUMBER: number = 10;

    /**
     * 2, 武缘形象
     */
    static readonly BEAUTY_IMAGE: number = 2;

    /**
     * 3, 结束武缘寻宝
     */
    static readonly BEAUTY_TREASURE_HUNT_END: number = 3;

    /**
     * 1, 打开武缘寻宝面板
     */
    static readonly BEAUTY_TREASURE_HUNT_LIST: number = 1;

    /**
     * 2, 开始武缘寻宝
     */
    static readonly BEAUTY_TREASURE_HUNT_START: number = 2;

    /**
     * 2, 激活武缘阵图
     */
    static readonly BEAUTY_ZT_ACT: number = 2;

    /**
     * 1, 打开武缘阵图面板
     */
    static readonly BEAUTY_ZT_LIST: number = 1;

    /**
     * 3, 升级武缘阵图
     */
    static readonly BEAUTY_ZT_UPLV: number = 3;

    /**
     * 133, 百服庆典-领取庆典充值奖励
     */
    static readonly BFQD_ACT_CHARGE_REWARD: number = 133;

    /**
     * 132, 百服庆典-打开庆典充值奖励面板
     */
    static readonly BFQD_ACT_CHARGE_REWARD_PANEL: number = 132;

    /**
     * 129, 百服庆典-抽奖
     */
    static readonly BFQD_ACT_DRAW: number = 129;

    /**
     * 128, 百服庆典-打开庆典抽奖面板
     */
    static readonly BFQD_ACT_DRAW_PANEL: number = 128;

    /**
     * 131, 百服庆典-领取庆典登录奖励
     */
    static readonly BFQD_ACT_LOGIN_REWARD: number = 131;

    /**
     * 130, 百服庆典-打开庆典登录奖励面板
     */
    static readonly BFQD_ACT_LOGIN_REWARD_PANEL: number = 130;

    /**
     * 134, 百服庆典-打开跨服消费排行榜
     */
    static readonly BFQD_ACT_RANK_PANEL: number = 134;

    /**
     * 60135001, 百服庆典，每日登陆， 掉落方案
     */
    static readonly BFQD_LOGIN_DROP_ID_DAY_1: number = 60135001;

    /**
     * 60135002, 百服庆典，每日登陆， 掉落方案
     */
    static readonly BFQD_LOGIN_DROP_ID_DAY_2: number = 60135002;

    /**
     * 60135003, 百服庆典，每日登陆， 掉落方案
     */
    static readonly BFQD_LOGIN_DROP_ID_DAY_3: number = 60135003;

    /**
     * 60135004, 百服庆典，每日登陆， 掉落方案
     */
    static readonly BFQD_LOGIN_DROP_ID_DAY_4: number = 60135004;

    /**
     * 60135005, 百服庆典，每日登陆， 掉落方案
     */
    static readonly BFQD_LOGIN_DROP_ID_DAY_5: number = 60135005;

    /**
     * 60135006, 百服庆典，每日登陆， 掉落方案
     */
    static readonly BFQD_LOGIN_DROP_ID_DAY_6: number = 60135006;

    /**
     * 60135007, 百服庆典，每日登陆， 掉落方案
     */
    static readonly BFQD_LOGIN_DROP_ID_DAY_7: number = 60135007;

    /**
     * 20, 百服庆典跨服消费榜可显示数量
     */
    static readonly BFQD_RANK_MAX_SHOW_COUNT: number = 20;

    /**
     * 1001, 绑元商城ID
     */
    static readonly BIND_YB_STORE_ID: number = 1001;

    /**
     * 25, 开服 BOSS召唤
     */
    static readonly BOSS_SUMMON: number = 25;

    /**
     * 24, 开服 BOSS召唤面板
     */
    static readonly BOSS_SUMMON_PANEL: number = 24;

    /**
     * 28, 开服 BOSS召唤定制提醒
     */
    static readonly BOSS_SUMMON_WARN: number = 28;

    /**
     * 51028901, 属主标识Buff
     */
    static readonly BUFF_ID_BOSS_OWNER: number = 51028901;

    /**
     * 2
     */
    static readonly BUFF_OPERATE_ADD: number = 2;

    /**
     * 1
     */
    static readonly BUFF_OPERATE_CANCEL: number = 1;

    /**
     * 30330002, 宝箱争霸 宝箱怪物ID
     */
    static readonly BXZB_MONSTER_ID_1: number = 30330002;

    /**
     * 30330003, 宝箱争霸 宝箱怪物ID
     */
    static readonly BXZB_MONSTER_ID_2: number = 30330003;

    /**
     * 2, 锁定背包
     */
    static readonly Bag_Password_Lock: number = 2;

    /**
     * 1, 设置背包二次密码
     */
    static readonly Bag_Password_Set: number = 1;

    /**
     * 4, 查询背包锁定状态
     */
    static readonly Bag_Password_Status: number = 4;

    /**
     * 3, 解锁背包
     */
    static readonly Bag_Password_Unlock: number = 3;

    /**
     * 2, 物品技能释放 
     */
    static readonly CASTSKILL_INTERFACE_ITEM: number = 2;

    /**
     * 3, 技能动作广播
     */
    static readonly CASTSKILL_INTERFACE_PREVIEW: number = 3;

    /**
     * 1, 技能释放     
     */
    static readonly CASTSKILL_INTERFACE_SKILL: number = 1;

    /**
     * 1, 开始释放
     */
    static readonly CASTSKILL_STATUS_BEGIN: number = 1;

    /**
     * 2, 结束释放
     */
    static readonly CASTSKILL_STATUS_END: number = 2;

    /**
     * 2, 使用位置坐标
     */
    static readonly CASTSKILL_TARGET_TYPE_POSITION: number = 2;

    /**
     * 1, 使用UnitID
     */
    static readonly CASTSKILL_TARGET_TYPE_UNITID: number = 1;

    /**
     * 16, 日期记录的最大长度xxxx-xx-xx
     */
    static readonly CDKEY_MAX_DATE_LENG: number = 16;

    /**
     * 2, CDKEY领取
     */
    static readonly CDKEY_OPERATE_EXCHANGE: number = 2;

    /**
     * 1, CDKEY预览
     */
    static readonly CDKEY_OPERATE_PREVIEW: number = 1;

    /**
     * 50, 挑战BOSS活动最大配置数
     */
    static readonly CHALLENGE_BOSS_MAX_NUM: number = 50;

    /**
     * 3, 打开挑战BOSS面板
     */
    static readonly CHALLENGE_BOSS_OPEN_PANEL: number = 3;

    /**
     * 9, 阵营说话
     */
    static readonly CHANNEL_CAMP: number = 9;

    /**
     * 8, 传闻
     */
    static readonly CHANNEL_CHUANWEN: number = 8;

    /**
     * 10, 国家说话
     */
    static readonly CHANNEL_COUNTRY: number = 10;

    /**
     * 3, 宗派
     */
    static readonly CHANNEL_GUILD: number = 3;

    /**
     * 12, 最大消息类型
     */
    static readonly CHANNEL_MAX: number = 12;

    /**
     * 0, 占位，无意义
     */
    static readonly CHANNEL_MIN: number = 0;

    /**
     * 1, 附近
     */
    static readonly CHANNEL_NEARBY: number = 1;

    /**
     * 7, 悄悄话
     */
    static readonly CHANNEL_PRIVATE: number = 7;

    /**
     * 6, 喇叭
     */
    static readonly CHANNEL_SPEAKER: number = 6;

    /**
     * 4, 系统
     */
    static readonly CHANNEL_SYSTEM: number = 4;

    /**
     * 2, 组队
     */
    static readonly CHANNEL_TEAM: number = 2;

    /**
     * 11, 组队通知
     */
    static readonly CHANNEL_TEAM_NOTIFY: number = 11;

    /**
     * 5, 世界
     */
    static readonly CHANNEL_WORLD: number = 5;

    /**
     * 0, 充值活动 不可领取
     */
    static readonly CHARGE_AWARD_CANT_GET: number = 0;

    /**
     * 2, 充值活动 已领取
     */
    static readonly CHARGE_AWARD_DONE_GET: number = 2;

    /**
     * 1, 充值活动 可领取
     */
    static readonly CHARGE_AWARD_WAIT_GET: number = 1;

    /**
     * 153, 充值红包活动 打开面板
     */
    static readonly CHARGE_REDBAG_PANEL: number = 153;

    /**
     * 154, 充值红包活动 领取奖励
     */
    static readonly CHARGE_REDBAG_REWARD: number = 154;

    /**
     * 1, 跨服争夺：九州城
     */
    static readonly CITY_NAME_JZ: number = 1;

    /**
     * 3, 跨服争夺：漠城
     */
    static readonly CITY_NAME_MC: number = 3;

    /**
     * 2, 跨服争夺：瀑城
     */
    static readonly CITY_NAME_PC: number = 2;

    /**
     * 0, 面板关闭
     */
    static readonly CLIENTPANEL_STATUS_CLOSE: number = 0;

    /**
     * 1, 面板打开
     */
    static readonly CLIENTPANEL_STATUS_OPEN: number = 1;

    /**
     * 10, 客户端面板最大值
     */
    static readonly CLIENTPANEL_TYPE_MAX: number = 10;

    /**
     * 1, 直购礼包面板
     */
    static readonly CLIENTPANEL_TYPE_ZGLB: number = 1;

    /**
     * 1, Cluster 玩家Uin数据Bit1 是否已发放测试服充值返利
     */
    static readonly CLUSTER_ROLE_UIN_BIT_1: number = 1;

    /**
     * 10, Cluster 玩家Uin数据Bit10 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_10: number = 10;

    /**
     * 11, Cluster 玩家Uin数据Bit11 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_11: number = 11;

    /**
     * 12, Cluster 玩家Uin数据Bit12 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_12: number = 12;

    /**
     * 13, Cluster 玩家Uin数据Bit13 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_13: number = 13;

    /**
     * 14, Cluster 玩家Uin数据Bit14 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_14: number = 14;

    /**
     * 15, Cluster 玩家Uin数据Bit15 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_15: number = 15;

    /**
     * 16, Cluster 玩家Uin数据Bit16 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_16: number = 16;

    /**
     * 2, Cluster 玩家Uin数据Bit2 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_2: number = 2;

    /**
     * 3, Cluster 玩家Uin数据Bit3 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_3: number = 3;

    /**
     * 4, Cluster 玩家Uin数据Bit4 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_4: number = 4;

    /**
     * 5, Cluster 玩家Uin数据Bit5 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_5: number = 5;

    /**
     * 6, Cluster 玩家Uin数据Bit6 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_6: number = 6;

    /**
     * 7, Cluster 玩家Uin数据Bit7 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_7: number = 7;

    /**
     * 8, Cluster 玩家Uin数据Bit8 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_8: number = 8;

    /**
     * 9, Cluster 玩家Uin数据Bit9 预留
     */
    static readonly CLUSTER_ROLE_UIN_BIT_9: number = 9;

    /**
     * 197, 四象斗兽场-购买挑战次数
     */
    static readonly COLOSSEUM_ACT_BUY_TIME: number = 197;

    /**
     * 198, 四象斗兽场-清空等待CD
     */
    static readonly COLOSSEUM_ACT_CLEAR_TIME: number = 198;

    /**
     * 200, 四象斗兽场-领取段位达成奖励，终身一次
     */
    static readonly COLOSSEUM_ACT_GET_GRADE_REWARD: number = 200;

    /**
     * 196, 四象斗兽场-领取昨日段位奖励
     */
    static readonly COLOSSEUM_ACT_GET_REWARD: number = 196;

    /**
     * 199, 四象斗兽场-上阵四象神兽
     */
    static readonly COLOSSEUM_ACT_ON_BATTLE: number = 199;

    /**
     * 201, 四象斗兽场-打开面板
     */
    static readonly COLOSSEUM_ACT_OPEN: number = 201;

    /**
     * 5, 竞争首刀有效时间5s
     */
    static readonly COMPETE_FIRST_HIT_VALID_TIME: number = 5;

    /**
     * 2, 购买海螺
     */
    static readonly CONCH_TYPE_BUY: number = 2;

    /**
     * 1, 拉取海螺信息
     */
    static readonly CONCH_TYPE_LIST: number = 1;

    /**
     * 2, 宝箱争霸条件不足类型
     */
    static readonly CONDITION_BXZB: number = 2;

    /**
     * 1, 福神宝箱条件不足类型
     */
    static readonly CONDITION_FSBX: number = 1;

    /**
     * 3, 血战封魔条件不足类型
     */
    static readonly CONDITION_XZFM: number = 3;

    /**
     * 109, 消费排行榜领取奖励
     */
    static readonly CONSUME_RANK_GET_REWARD: number = 109;

    /**
     * 110, 获取消费排行榜列表
     */
    static readonly CONSUME_RANK_LIST_GET: number = 110;

    /**
     * 10, 消费排行榜最多显示个数
     */
    static readonly CONSUME_RANK_MAX_SHOW_COUNT: number = 10;

    /**
     * 108, 消费排行榜打开面板
     */
    static readonly CONSUME_RANK_PANEL: number = 108;

    /**
     * 2, 8月促销活动
     */
    static readonly CONSUME_TPYE_201408: number = 2;

    /**
     * 1, 百服庆典消费活动
     */
    static readonly CONSUME_TPYE_BAIFU: number = 1;

    /**
     * 107, 玩家操作
     */
    static readonly CONTAINER_CHANGED_REASON_OPERATE: number = 107;

    /**
     * 106, 购买格子
     */
    static readonly CONTAINER_OPERATE_BUYSPACE: number = 106;

    /**
     * 112, 宝箱到背包
     */
    static readonly CONTAINER_OPERATE_CHEST_DROP: number = 112;

    /**
     * 104, 丢弃物品
     */
    static readonly CONTAINER_OPERATE_DROP_THING: number = 104;

    /**
     * 101, 查询容器
     */
    static readonly CONTAINER_OPERATE_LIST: number = 101;

    /**
     * 111, 邮件到背包
     */
    static readonly CONTAINER_OPERATE_MAIL_DROP: number = 111;

    /**
     * 110, 怪物掉落物品到背包
     */
    static readonly CONTAINER_OPERATE_MONSTER_DROP: number = 110;

    /**
     * 114, 预使用物品
     */
    static readonly CONTAINER_OPERATE_PER_USE_THING: number = 114;

    /**
     * 113, 任务获得
     */
    static readonly CONTAINER_OPERATE_QUEST_REWARD: number = 113;

    /**
     * 102, 整理容器
     */
    static readonly CONTAINER_OPERATE_SORT: number = 102;

    /**
     * 105, 交换格子
     */
    static readonly CONTAINER_OPERATE_SWAP: number = 105;

    /**
     * 103, 使用物品
     */
    static readonly CONTAINER_OPERATE_USE_THING: number = 103;

    /**
     * 109, 玩家红颜装备面板
     */
    static readonly CONTAINER_TYPE_BEAUTY_EQUIP: number = 109;

    /**
     * 101, 系统宝箱
     */
    static readonly CONTAINER_TYPE_CHEST: number = 101;

    /**
     * 116, 宗门仓库，仅前台自己使用
     */
    static readonly CONTAINER_TYPE_GUILDSTORE: number = 116;

    /**
     * 139, 祝福_当前最大
     */
    static readonly CONTAINER_TYPE_HEROSUB_CURMAX: number = 139;

    /**
     * 136, 祝福_法阵装备ID
     */
    static readonly CONTAINER_TYPE_HEROSUB_FAZHEN: number = 136;

    /**
     * 138, 祝福_火精装备ID
     */
    static readonly CONTAINER_TYPE_HEROSUB_HUOJING: number = 138;

    /**
     * 135, 祝福_精灵装备ID
     */
    static readonly CONTAINER_TYPE_HEROSUB_JINGLING: number = 135;

    /**
     * 137, 祝福_足迹装备ID
     */
    static readonly CONTAINER_TYPE_HEROSUB_LEILING: number = 137;

    /**
     * 131, 祝福_灵兽装备ID
     */
    static readonly CONTAINER_TYPE_HEROSUB_LINGSHOU: number = 131;

    /**
     * 160, 子系统装备最大ID
     */
    static readonly CONTAINER_TYPE_HEROSUB_MAX: number = 160;

    /**
     * 131, 子系统装备最小ID
     */
    static readonly CONTAINER_TYPE_HEROSUB_MIN: number = 131;

    /**
     * 133, 祝福_武魂装备ID
     */
    static readonly CONTAINER_TYPE_HEROSUB_WUHUN: number = 133;

    /**
     * 134, 祝福_羽翼装备ID
     */
    static readonly CONTAINER_TYPE_HEROSUB_YUYI: number = 134;

    /**
     * 139, 祝福_战灵装备ID
     */
    static readonly CONTAINER_TYPE_HEROSUB_ZHANLING: number = 139;

    /**
     * 132, 祝福_坐骑装备ID
     */
    static readonly CONTAINER_TYPE_HEROSUB_ZUOQI: number = 132;

    /**
     * 110, 玩家魂骨装备面板
     */
    static readonly CONTAINER_TYPE_HUNGU_EQUIP: number = 110;

    /**
     * 103, NPC交易面板
     */
    static readonly CONTAINER_TYPE_NPC_TRADE: number = 103;

    /**
     * 104, NPC回购面板
     */
    static readonly CONTAINER_TYPE_NPC_TRADEBACK: number = 104;

    /**
     * 105, 玩家背包
     */
    static readonly CONTAINER_TYPE_ROLE_BAG: number = 105;

    /**
     * 108, 玩家装备面板
     */
    static readonly CONTAINER_TYPE_ROLE_EQUIP: number = 108;

    /**
     * 106, 玩家仓库
     */
    static readonly CONTAINER_TYPE_ROLE_STORE: number = 106;

    /**
     * 107, 玩家交易面板
     */
    static readonly CONTAINER_TYPE_ROLE_TRADE: number = 107;

    /**
     * 115, 天宫宝境抽奖仓库面板
     */
    static readonly CONTAINER_TYPE_SKYLOTTERY: number = 115;

    /**
     * 117, 星斗宝库抽奖仓库面板
     */
    static readonly CONTAINER_TYPE_STARLOTTERY: number = 117;

    /**
     * 5, 投票
     */
    static readonly COUNTRYFRAME_DO_VOTE: number = 5;

    /**
     * 20, 国王拉取职位可选人员信息
     */
    static readonly COUNTRYFRAME_GET_JOB_ENABLE_ROLES: number = 20;

    /**
     * 8, 查看日志列表
     */
    static readonly COUNTRYFRAME_GET_LOSTLIST: number = 8;

    /**
     * 7, 查看成员列表
     */
    static readonly COUNTRYFRAME_GET_MEMBERLIST: number = 7;

    /**
     * 11, 查看国库分成
     */
    static readonly COUNTRYFRAME_GET_MONEY_ASSIGN: number = 11;

    /**
     * 10, 查看国库信息
     */
    static readonly COUNTRYFRAME_GET_MONEY_STORE: number = 10;

    /**
     * 22, 摘星台查询操作
     */
    static readonly COUNTRYFRAME_LOTTERY_LIST: number = 22;

    /**
     * 16, 摘星台操作
     */
    static readonly COUNTRYFRAME_LOTTERY_OPERATE: number = 16;

    /**
     * 1, 打开国家面板
     */
    static readonly COUNTRYFRAME_OPEN_CONTRY_PANNEL: number = 1;

    /**
     * 3, 打开职位面板
     */
    static readonly COUNTRYFRAME_OPEN_JOBTITLE_PANNEL: number = 3;

    /**
     * 4, 打开预告面板
     */
    static readonly COUNTRYFRAME_OPEN_PREVIEW_PANNEL: number = 4;

    /**
     * 2, 打开竞选面板
     */
    static readonly COUNTRYFRAME_OPEN_VOTE_PANNEL: number = 2;

    /**
     * 13, 国家核心成员发送位置信息
     */
    static readonly COUNTRYFRAME_SEND_ROLE_POSTION: number = 13;

    /**
     * 6, 设置职位
     */
    static readonly COUNTRYFRAME_SET_JOBTITLE: number = 6;

    /**
     * 12, 设置国库分成
     */
    static readonly COUNTRYFRAME_SET_MONEY_ASSIGN: number = 12;

    /**
     * 9, 设置国家公告
     */
    static readonly COUNTRYFRAME_SET_PUBLIC_NOTICE: number = 9;

    /**
     * 23, 国家状态改变子协议
     */
    static readonly COUNTRYFRAME_STATUS_CHANGE: number = 23;

    /**
     * 15, 天灵宝莲查询
     */
    static readonly COUNTRYFRAME_TIANLING_LIST: number = 15;

    /**
     * 14, 天灵宝莲操作
     */
    static readonly COUNTRYFRAME_TIANLING_OPERATE: number = 14;

    /**
     * 18, 英灵之魂增加互动次数
     */
    static readonly COUNTRYFRAME_YINGLING_BUY: number = 18;

    /**
     * 19, 英灵之魂抽取猎魂
     */
    static readonly COUNTRYFRAME_YINGLING_CRYSTAL: number = 19;

    /**
     * 17, 英灵之魂互动
     */
    static readonly COUNTRYFRAME_YINGLING_FRIENDLY: number = 17;

    /**
     * 21, 英灵之魂查询操作
     */
    static readonly COUNTRYFRAME_YINGLING_LIST: number = 21;

    /**
     * 4, 活动状态通知
     */
    static readonly COUNTRYFRAME_ZW_ACT_STATUS: number = 4;

    /**
     * 1, Boss宗派召集
     */
    static readonly COUNTRYFRAME_ZW_BOSS_GUILD_CALL: number = 1;

    /**
     * 2, 封魔塔Boss宗派召集
     */
    static readonly COUNTRYFRAME_ZW_FMTBOSS_GUILD_CALL: number = 2;

    /**
     * 6, 竞技场排名变更
     */
    static readonly COUNTRYFRAME_ZW_JJC_RANK_CHANGE: number = 6;

    /**
     * 12, 服务器合服时间
     */
    static readonly COUNTRYFRAME_ZW_MERGETIME: number = 12;

    /**
     * 7, 玩家登陆成功通知到world
     */
    static readonly COUNTRYFRAME_ZW_ROLE_LOGIN_OK: number = 7;

    /**
     * 11, 服务器开服时间
     */
    static readonly COUNTRYFRAME_ZW_STARTTIME: number = 11;

    /**
     * 5, 武缘远征同步关卡
     */
    static readonly COUNTRYFRAME_ZW_WYYZ_SYN_LEVEL: number = 5;

    /**
     * 13, 跨服血战封魔宗派召集
     */
    static readonly COUNTRYFRAME_ZW_XZFM_GUILD_CALL: number = 13;

    /**
     * 3, 活动召集 暂时用于宗门秘境
     */
    static readonly COUNTRYFRAME_ZW_ZPFM_GUILD_CALL: number = 3;

    /**
     * 6, 任命
     */
    static readonly COUNTRY_FRAME_LOG_TYPE_APPOINT: number = 6;

    /**
     * 1, 阵营战
     */
    static readonly COUNTRY_FRAME_LOG_TYPE_CAMPBATTLE: number = 1;

    /**
     * 7, 解任
     */
    static readonly COUNTRY_FRAME_LOG_TYPE_CONGE: number = 7;

    /**
     * 4, 每日世界boss
     */
    static readonly COUNTRY_FRAME_LOG_TYPE_DAILIBOSS: number = 4;

    /**
     * 2, 边境战
     */
    static readonly COUNTRY_FRAME_LOG_TYPE_GUILDBATTLE: number = 2;

    /**
     * 5, 神魔遮天boss
     */
    static readonly COUNTRY_FRAME_LOG_TYPE_SHENMO: number = 5;

    /**
     * 3, 南蛮入侵
     */
    static readonly COUNTRY_FRAME_LOG_TYPE_SOUTHMAN: number = 3;

    /**
     * 1, 阵营战
     */
    static readonly COUNTRY_FRAME_MEDAL_TYPE_CAMPBATTLE: number = 1;

    /**
     * 4, 每日世界boss
     */
    static readonly COUNTRY_FRAME_MEDAL_TYPE_DAILIBOSS: number = 4;

    /**
     * 2, 边境战
     */
    static readonly COUNTRY_FRAME_MEDAL_TYPE_GUILDBATTLE: number = 2;

    /**
     * 5, 神魔遮天boss
     */
    static readonly COUNTRY_FRAME_MEDAL_TYPE_SHENMO: number = 5;

    /**
     * 3, 南蛮入侵
     */
    static readonly COUNTRY_FRAME_MEDAL_TYPE_SOUTHMAN: number = 3;

    /**
     * 1, 国家运行状态，未选举
     */
    static readonly COUNTRY_FRAME_RUN_STATE_NONE: number = 1;

    /**
     * 3, 正常
     */
    static readonly COUNTRY_FRAME_RUN_STATE_NORMAL: number = 3;

    /**
     * 2, 选举中
     */
    static readonly COUNTRY_FRAME_RUN_STATE_VOTE: number = 2;

    /**
     * 1, 摘星台设施，使用元宝抽奖
     */
    static readonly COUNTRY_LOTTERY_OPERATE_YUANBAO: number = 1;

    /**
     * 2, 摘星台设施，使用元宝抽奖
     */
    static readonly COUNTRY_LOTTERY_OPERATE_ZHANXUN: number = 2;

    /**
     * 1, 天灵宝莲设施，使用元宝浇灌
     */
    static readonly COUNTRY_TIANLING_OPERATE_YUANBAO: number = 1;

    /**
     * 2, 天灵宝莲设施，使用战勋浇灌
     */
    static readonly COUNTRY_TIANLING_OPERATE_ZHANXUN: number = 2;

    /**
     * 1, 英灵之魂设施，使用元宝
     */
    static readonly COUNTRY_YINGLING_OPERATE_YUANBAO: number = 1;

    /**
     * 2, 英灵之魂设施，使用100战勋
     */
    static readonly COUNTRY_YINGLING_OPERATE_ZHANXUN_100: number = 2;

    /**
     * 3, 英灵之魂设施，使用500战勋
     */
    static readonly COUNTRY_YINGLING_OPERATE_ZHANXUN_500: number = 3;

    /**
     * 1, 打开血脉面板
     */
    static readonly CRAZYBLOOD_OPEN_PANEL: number = 1;

    /**
     * 2, 提升血脉阶级
     */
    static readonly CRAZYBLOOD_UP_STAGE: number = 2;

    /**
     * 115, RMB增加兑换信息
     */
    static readonly CROSS_ADD_RMB_GETINFO: number = 115;

    /**
     * 66, 百服庆典_消费排行榜_奖励通知
     */
    static readonly CROSS_BFQD_AWARD: number = 66;

    /**
     * 65, 百服庆典_消费排行榜_下拉
     */
    static readonly CROSS_BFQD_GET: number = 65;

    /**
     * 64, 百服庆典_消费排行榜_上报
     */
    static readonly CROSS_BFQD_REPORT: number = 64;

    /**
     * 112, 跨服_消息
     */
    static readonly CROSS_CHAT_NOTIFY: number = 112;

    /**
     * 106, 跨服_城战领礼包
     */
    static readonly CROSS_CITY_GIFT: number = 106;

    /**
     * 105, 跨服_城战参战
     */
    static readonly CROSS_CITY_JOIN: number = 105;

    /**
     * 102, 跨服_城战打开板子
     */
    static readonly CROSS_CITY_OPENPANEL: number = 102;

    /**
     * 103, 跨服_城战报名
     */
    static readonly CROSS_CITY_SIGN: number = 103;

    /**
     * 95, 跨服_四象斗兽请求获取四象币
     */
    static readonly CROSS_COLOSSEUM_MONEY: number = 95;

    /**
     * 90, 跨服_四象斗兽面板
     */
    static readonly CROSS_COLOSSEUM_PANEL: number = 90;

    /**
     * 92, 跨服_四象斗兽排行榜
     */
    static readonly CROSS_COLOSSEUM_RANK: number = 92;

    /**
     * 91, 跨服_四象斗兽结果上报
     */
    static readonly CROSS_COLOSSEUM_REPORT: number = 91;

    /**
     * 93, 跨服_领取四象斗兽排行榜奖励
     */
    static readonly CROSS_COLOSSEUM_REWARD: number = 93;

    /**
     * 94, 跨服_四象斗兽开始PK
     */
    static readonly CROSS_COLOSSEUM_START_PK: number = 94;

    /**
     * 132, Act126跨服_消费排行榜_奖励通知
     */
    static readonly CROSS_CONSUME_AWARD: number = 132;

    /**
     * 131, Act126跨服_消费排行榜_下拉
     */
    static readonly CROSS_CONSUME_GET: number = 131;

    /**
     * 133, Act126跨服_消费排行榜_邮件通知
     */
    static readonly CROSS_CONSUME_MAIL: number = 133;

    /**
     * 130, Act126跨服_消费排行榜_上报
     */
    static readonly CROSS_CONSUME_REPORT: number = 130;

    /**
     * 2, 跨服_创建队伍
     */
    static readonly CROSS_CREATE_TEAM: number = 2;

    /**
     * 82, 跨服_充值排行榜_奖励通知
     */
    static readonly CROSS_CZ_AWARD: number = 82;

    /**
     * 81, 跨服_充值排行榜_下拉
     */
    static readonly CROSS_CZ_GET: number = 81;

    /**
     * 83, 跨服_充值排行榜_邮件通知
     */
    static readonly CROSS_CZ_MAIL: number = 83;

    /**
     * 80, 跨服_充值排行榜_上报
     */
    static readonly CROSS_CZ_REPORT: number = 80;

    /**
     * 63, 跨服_点灯排行榜_奖励通知
     */
    static readonly CROSS_DDL_AWARD: number = 63;

    /**
     * 62, 跨服_点灯排行榜_下拉
     */
    static readonly CROSS_DDL_GET: number = 62;

    /**
     * 61, 跨服_点灯排行榜_上报
     */
    static readonly CROSS_DDL_REPORT: number = 61;

    /**
     * 109, 跨服_打开天地竞技跨服面板
     */
    static readonly CROSS_DIBANG_PANEL: number = 109;

    /**
     * 4, 跨服_退出队伍
     */
    static readonly CROSS_EXIT_TEAM: number = 4;

    /**
     * 111, 跨服_拉取玩家天榜竞技 称号
     */
    static readonly CROSS_GET_DIBANG_CROSS_TITLE: number = 111;

    /**
     * 31, 跨服_拉取榜单数据
     */
    static readonly CROSS_GET_RANK: number = 31;

    /**
     * 114, RMB已兑换信息
     */
    static readonly CROSS_GET_RMB_GETINFO: number = 114;

    /**
     * 1, 跨服_查看某副本队伍列表
     */
    static readonly CROSS_GET_TEAMLIST: number = 1;

    /**
     * 99, 跨服_GM命令
     */
    static readonly CROSS_GM_CMD: number = 99;

    /**
     * 3, 跨服_加入队伍
     */
    static readonly CROSS_JOIN_TEAM: number = 3;

    /**
     * 5, 跨服_踢出队伍
     */
    static readonly CROSS_KICK_TEAM: number = 5;

    /**
     * 69, 跨服_老虎机 打开面板
     */
    static readonly CROSS_LHJ_OPEN: number = 69;

    /**
     * 70, 跨服_老虎机 转动信息上传
     */
    static readonly CROSS_LHJ_TRUN: number = 70;

    /**
     * 34, 跨服_多人PVP退出排队
     */
    static readonly CROSS_MULTI_EXIT: number = 34;

    /**
     * 33, 跨服_多人PVP参加排队
     */
    static readonly CROSS_MULTI_JOIN: number = 33;

    /**
     * 36, 跨服_多人PVP排行榜
     */
    static readonly CROSS_MULTI_RANK: number = 36;

    /**
     * 35, 跨服_多人PVP结果上报
     */
    static readonly CROSS_MULTI_REPORT: number = 35;

    /**
     * 37, 跨服_多人PVP排行榜发放奖励
     */
    static readonly CROSS_MULTI_REWARD: number = 37;

    /**
     * 38, 跨服_多人PVP开打通知
     */
    static readonly CROSS_MULTI_START: number = 38;

    /**
     * 15, 活动联服通知 现有SMZT跨服Boss用到 其他活动用到再加上去
     */
    static readonly CROSS_NTF_ACT_LINKINFO: number = 15;

    /**
     * 17, 跨服年兽BOSS击杀数据
     */
    static readonly CROSS_NTF_KFNS_BOSS: number = 17;

    /**
     * 24, 魔化战争 第一名列表
     */
    static readonly CROSS_NTF_MHZZ_FIRST_LST: number = 24;

    /**
     * 22, 魔化战争 通知拉取第一名请求
     */
    static readonly CROSS_NTF_MHZZ_FIRST_REQ: number = 22;

    /**
     * 23, 魔化战争 通知拉取第一名相应
     */
    static readonly CROSS_NTF_MHZZ_FIRST_RSP: number = 23;

    /**
     * 11, 宗派拍卖 活动排名通知
     */
    static readonly CROSS_NTF_PAIMAI_ACT_RANK: number = 11;

    /**
     * 12, 跨服角色排名通知
     */
    static readonly CROSS_NTF_ROLE_RANK: number = 12;

    /**
     * 10, OSS修改开服时间通知
     */
    static readonly CROSS_NTF_SERVER_STARTTIME: number = 10;

    /**
     * 16, 神魔遮天BOSS击杀数据
     */
    static readonly CROSS_NTF_SMZT_BOSS: number = 16;

    /**
     * 4, 世界Boss宗门召集
     */
    static readonly CROSS_NTF_WORLDBOSS_CALL: number = 4;

    /**
     * 3, 世界Boss状态同步
     */
    static readonly CROSS_NTF_WORLDBOSS_STATUS: number = 3;

    /**
     * 1, 血战封魔BOSS同步
     */
    static readonly CROSS_NTF_XZFM_BOSS: number = 1;

    /**
     * 21, 跨服血战封魔宗门召集
     */
    static readonly CROSS_NTF_XZFM_CALL: number = 21;

    /**
     * 20, 跨服血战封魔拍卖
     */
    static readonly CROSS_NTF_XZFM_PAIMAI: number = 20;

    /**
     * 2, 血战封魔积分同步
     */
    static readonly CROSS_NTF_XZFM_SCORE: number = 2;

    /**
     * 19, 跨服血战封魔任务同步
     */
    static readonly CROSS_NTF_XZFM_TASK_NUM: number = 19;

    /**
     * 5, 跨服_至尊皇城 城市占领同步
     */
    static readonly CROSS_NTF_ZZHC_CITY_SYN: number = 5;

    /**
     * 6, 跨服_至尊皇城 宗派收到攻击通知
     */
    static readonly CROSS_NTF_ZZHC_GUILD_ATTACK: number = 6;

    /**
     * 7, 跨服_至尊皇城 宗派占领奖励
     */
    static readonly CROSS_NTF_ZZHC_GUILD_REWARD: number = 7;

    /**
     * 9, 跨服_至尊皇城 占领的宗主同步
     */
    static readonly CROSS_NTF_ZZHC_LEADER_SYN: number = 9;

    /**
     * 8, 跨服_至尊皇城 城市占领邮件
     */
    static readonly CROSS_NTF_ZZHC_LOOT_MAIL: number = 8;

    /**
     * 116, 处理RMB战场操作
     */
    static readonly CROSS_OPERATE_RMBZC: number = 116;

    /**
     * 3, 跨服排行榜类型——副本奔跑吧兄弟
     */
    static readonly CROSS_RANK_TYPE_FB_BPXD: number = 3;

    /**
     * 1, 跨服排行榜类型——鲜花榜男
     */
    static readonly CROSS_RANK_TYPE_XHB_BOY: number = 1;

    /**
     * 2, 跨服排行榜类型——鲜花榜女
     */
    static readonly CROSS_RANK_TYPE_XHB_GIRL: number = 2;

    /**
     * 6, 跨服_副本准备
     */
    static readonly CROSS_READY_TEAM: number = 6;

    /**
     * 9, 跨服_招募队友
     */
    static readonly CROSS_RECRUIT_TEAM: number = 9;

    /**
     * 118, 跨服RMB战场开始
     */
    static readonly CROSS_RMBZC_START: number = 118;

    /**
     * 122, 跨服_限时秒杀 购买
     */
    static readonly CROSS_RUSH_PURCHASE_BUY: number = 122;

    /**
     * 121, 跨服_限时秒杀 打开面板
     */
    static readonly CROSS_RUSH_PURCHASE_OPEN: number = 121;

    /**
     * 57, 跨服 脚本发邮件给宗派
     */
    static readonly CROSS_SCRIP_GUILD_DROPMAIL: number = 57;

    /**
     * 120, 盛典宝箱购买
     */
    static readonly CROSS_SDBX_ACT_BUY: number = 120;

    /**
     * 119, 盛典宝箱面板
     */
    static readonly CROSS_SDBX_ACT_PANEL: number = 119;

    /**
     * 8, 跨服_设置队伍信息(战力，自动开)
     */
    static readonly CROSS_SET_TEAM: number = 8;

    /**
     * 24, 跨服_单人PVP购买次数
     */
    static readonly CROSS_SINGLE_BUY: number = 24;

    /**
     * 23, 跨服_单人PVP退出排队
     */
    static readonly CROSS_SINGLE_EXIT: number = 23;

    /**
     * 154, 跨服_投注
     */
    static readonly CROSS_SINGLE_FINAL_BET: number = 154;

    /**
     * 155, 跨服_领取投注奖励
     */
    static readonly CROSS_SINGLE_FINAL_BETGET: number = 155;

    /**
     * 152, 跨服_通知ZoneSvr决赛信息
     */
    static readonly CROSS_SINGLE_FINAL_GAMEINFO: number = 152;

    /**
     * 151, 跨服_单人PVP打开决赛面板
     */
    static readonly CROSS_SINGLE_FINAL_OPEN: number = 151;

    /**
     * 157, 跨服_决赛参赛选手通知
     */
    static readonly CROSS_SINGLE_FINAL_PKNTF: number = 157;

    /**
     * 153, 跨服_比赛结果上报给cluster
     */
    static readonly CROSS_SINGLE_FINAL_REPORT: number = 153;

    /**
     * 156, 跨服_决赛奖励通知  暂时用在轮空奖励
     */
    static readonly CROSS_SINGLE_FINAL_REWARD: number = 156;

    /**
     * 158, 跨服_冠军投注
     */
    static readonly CROSS_SINGLE_FINAL_WINBET: number = 158;

    /**
     * 159, 跨服_领取冠军投注奖励
     */
    static readonly CROSS_SINGLE_FINAL_WINBETGET: number = 159;

    /**
     * 22, 跨服_单人PVP参加排队
     */
    static readonly CROSS_SINGLE_JOIN: number = 22;

    /**
     * 29, 跨服_单人PVP打开面板
     */
    static readonly CROSS_SINGLE_OPEN: number = 29;

    /**
     * 21, 跨服_单人PVP排行榜(32强)
     */
    static readonly CROSS_SINGLE_RANK: number = 21;

    /**
     * 25, 跨服_单人PVP决赛结果上报
     */
    static readonly CROSS_SINGLE_REPORT: number = 25;

    /**
     * 27, 跨服_单人PVP领取奖励
     */
    static readonly CROSS_SINGLE_REWARD: number = 27;

    /**
     * 150, 跨服_单人PVP打开历届面板
     */
    static readonly CROSS_SINGLE_SESSION_OPEN: number = 150;

    /**
     * 26, 跨服_单人PVP开打通知
     */
    static readonly CROSS_SINGLE_START: number = 26;

    /**
     * 1, dcluster到zone
     */
    static readonly CROSS_SS_CUSTER2ZONE: number = 1;

    /**
     * 0, zoned到cluster
     */
    static readonly CROSS_SS_ZONE2CUSTER: number = 0;

    /**
     * 7, 跨服_开始副本
     */
    static readonly CROSS_START_PINSTANCE: number = 7;

    /**
     * 108, 跨服_同步城战族长数据
     */
    static readonly CROSS_SYN_CITY_MASTER: number = 108;

    /**
     * 107, 跨服_同步城战结果
     */
    static readonly CROSS_SYN_CITY_RESULT: number = 107;

    /**
     * 110, 跨服_上报天地竞技PK结果
     */
    static readonly CROSS_SYN_DIBANG_PKINFO: number = 110;

    /**
     * 113, 跨服_宗派战结果同步信息
     */
    static readonly CROSS_SYN_GUILDCROSS_PKINFO: number = 113;

    /**
     * 60, 跨服_发物品给全宗派成员
     */
    static readonly CROSS_SYN_GUILD_GIVEITEM: number = 60;

    /**
     * 117, 同步RMB战场数据
     */
    static readonly CROSS_SYN_RMBZC: number = 117;

    /**
     * 50, 跨服_玩家信息同步包
     */
    static readonly CROSS_SYN_ROLE: number = 50;

    /**
     * 52, 跨服_玩家信息同步请求(Client)
     */
    static readonly CROSS_SYN_ROLE_CS: number = 52;

    /**
     * 77, CS SS 跨服玩家数据同步测试
     */
    static readonly CROSS_SYN_ROLE_GMTEST: number = 77;

    /**
     * 55, 跨服_宗派战跨服信息
     */
    static readonly CROSS_SYN_ROLE_GUILD_CROSSPVP: number = 55;

    /**
     * 13, 跨服_组队副本角色同步到cluster
     */
    static readonly CROSS_SYN_ROLE_TEAM: number = 13;

    /**
     * 59, 跨服_同步神魔遮天BOSS
     */
    static readonly CROSS_SYN_SMZT_BOSS: number = 59;

    /**
     * 53, 跨服_同步世界BOSS
     */
    static readonly CROSS_SYN_WORLDBOSS: number = 53;

    /**
     * 56, 阵营战奖励掉落同步--个人
     */
    static readonly CROSS_SYN_ZYZ_AWARD: number = 56;

    /**
     * 58, 阵营战称号同步--个人
     */
    static readonly CROSS_SYN_ZYZ_TITLE: number = 58;

    /**
     * 20, 跨服_队伍详细信息广播
     */
    static readonly CROSS_TEAM_DETAIL: number = 20;

    /**
     * 78, CS SS 测服玩家礼包 查询
     */
    static readonly CROSS_TEST_ROLE_GIFT_ASK: number = 78;

    /**
     * 30, 跨服_更新榜单数据
     */
    static readonly CROSS_UPDATE_RANK: number = 30;

    /**
     * 44, CS 跨服_王侯将相 处理被挑战  REQ
     */
    static readonly CROSS_WHJX_APPLY_DEAL: number = 44;

    /**
     * 43, CS 跨服_王侯将相 通知被挑战  RSP
     */
    static readonly CROSS_WHJX_APPLY_NTF: number = 43;

    /**
     * 42, CS 跨服_王侯将相 申请挑战    REQ RSP
     */
    static readonly CROSS_WHJX_APPLY_PK: number = 42;

    /**
     * 41, CS 跨服_王侯将相 购买礼包    REQ RSP
     */
    static readonly CROSS_WHJX_BUY: number = 41;

    /**
     * 39, CS 跨服_王侯将相 清CD        REQ RSP
     */
    static readonly CROSS_WHJX_CDCLEAN: number = 39;

    /**
     * 40, CS 跨服_王侯将相 打开面板    REQ RSP
     */
    static readonly CROSS_WHJX_PANNLE: number = 40;

    /**
     * 47, SS 跨服_王侯将相 处理被挑战  REQ  回给申请服
     */
    static readonly CROSS_WHJX_SS_APPLY_DEAL: number = 47;

    /**
     * 46, SS 跨服_王侯将相 申请挑战    REQ  发给主服
     */
    static readonly CROSS_WHJX_SS_APPLY_PK: number = 46;

    /**
     * 45, SS 跨服_王侯将相 购买礼包    REQ  发给主服
     */
    static readonly CROSS_WHJX_SS_BUY: number = 45;

    /**
     * 49, SS 跨服_王侯将相 同步DB数据  REQ  同步给非主服
     */
    static readonly CROSS_WHJX_SS_DATA_SYN: number = 49;

    /**
     * 48, SS 跨服_王侯将相 同步排行榜  REQ  同步给非主服
     */
    static readonly CROSS_WHJX_SS_RANK_SYN: number = 48;

    /**
     * 71, SS 跨服_王侯将相 发奖励  REQ
     */
    static readonly CROSS_WHJX_SS_REWARD: number = 71;

    /**
     * 72, SS 跨服_王侯将相 同步挑战状态  REQ  同步给非主服
     */
    static readonly CROSS_WHJX_SS_STATUS_SYN: number = 72;

    /**
     * 172, 世界杯比分投注
     */
    static readonly CROSS_WORLDCUP_BET_SCORE: number = 172;

    /**
     * 171, 世界杯胜负投注
     */
    static readonly CROSS_WORLDCUP_BET_WIN: number = 171;

    /**
     * 175, 发奖时候离线，上线领奖
     */
    static readonly CROSS_WORLDCUP_GET: number = 175;

    /**
     * 170, 打开世界杯面板
     */
    static readonly CROSS_WORLDCUP_PANEL: number = 170;

    /**
     * 173, 世界杯排行榜
     */
    static readonly CROSS_WORLDCUP_RANK: number = 173;

    /**
     * 177, 排行榜奖励
     */
    static readonly CROSS_WORLDCUP_RANK_REWARD: number = 177;

    /**
     * 176, 上报获得的总奖励
     */
    static readonly CROSS_WORLDCUP_REPORT: number = 176;

    /**
     * 174, 世界杯结算发奖
     */
    static readonly CROSS_WORLDCUP_REWARD: number = 174;

    /**
     * 101, 跨服_自动分组域名信息广播
     */
    static readonly CROSS_WORLDGROUP_NOTIFY: number = 101;

    /**
     * 100, 跨服_本服域名信息上报
     */
    static readonly CROSS_WORLDINFO_REPORT: number = 100;

    /**
     * 32, 跨服_鲜花榜发放奖励
     */
    static readonly CROSS_XHB_REWARD: number = 32;

    /**
     * 68, 跨服_幸运转盘 抽奖上传记录
     */
    static readonly CROSS_XYZP_DRAW: number = 68;

    /**
     * 67, 跨服_幸运转盘 打开面板
     */
    static readonly CROSS_XYZP_OPEN: number = 67;

    /**
     * 141, 跨服云购-购买礼包
     */
    static readonly CROSS_YUNGOU_ACT_BUY: number = 141;

    /**
     * 140, 跨服云购-打开面板
     */
    static readonly CROSS_YUNGOU_ACT_PANEL: number = 140;

    /**
     * 143, 跨服_云购购买
     */
    static readonly CROSS_YUNGOU_BUY: number = 143;

    /**
     * 1, 跨服云购购买次数
     */
    static readonly CROSS_YUNGOU_BUYT_TYPE1: number = 1;

    /**
     * 10, 跨服云购购买次数
     */
    static readonly CROSS_YUNGOU_BUYT_TYPE2: number = 10;

    /**
     * 144, 跨服_云购发奖
     */
    static readonly CROSS_YUNGOU_GIFT: number = 144;

    /**
     * 142, 跨服_云购板子
     */
    static readonly CROSS_YUNGOU_OPEN: number = 142;

    /**
     * 145, 跨服_云购购买满了同步信息
     */
    static readonly CROSS_YUNGOU_SYN_COUNT: number = 145;

    /**
     * 76, CS SS 跨服_珍珑棋局 结束
     */
    static readonly CROSS_ZLQJ_END: number = 76;

    /**
     * 73, CS 跨服_珍珑棋局 面板
     */
    static readonly CROSS_ZLQJ_PANNEL: number = 73;

    /**
     * 74, CS SS 跨服_珍珑棋局 报名
     */
    static readonly CROSS_ZLQJ_SIGNUP: number = 74;

    /**
     * 75, CS SS 跨服_珍珑棋局 开始
     */
    static readonly CROSS_ZLQJ_START: number = 75;

    /**
     * 1, 分解
     */
    static readonly CRYSTAL_DECOMPOSE_TYPE: number = 1;

    /**
     * 2, 升级
     */
    static readonly CRYSTAL_LEVELUP_TYPE: number = 2;

    /**
     * 4, 锁定
     */
    static readonly CRYSTAL_LOCK_TYPE: number = 4;

    /**
     * 100, 猎魂仓库
     */
    static readonly CRYSTAL_STORE_TYPE: number = 100;

    /**
     * 5, 解锁
     */
    static readonly CRYSTAL_UNLOCK_TYPE: number = 5;

    /**
     * 2
     */
    static readonly CS_ID_CLT: number = 2;

    /**
     * 1
     */
    static readonly CS_ID_SVR: number = 1;

    /**
     * 2, 增加货币
     */
    static readonly CURRENCY_OP_ADD: number = 2;

    /**
     * 4, 增加货币同时影响VIP
     */
    static readonly CURRENCY_OP_ADDVIP: number = 4;

    /**
     * 14, 平台充值同时影响VIP
     */
    static readonly CURRENCY_OP_CHARGEADDVIP: number = 14;

    /**
     * 5, 仅在线减少货币
     */
    static readonly CURRENCY_OP_ONLINE_SUB: number = 5;

    /**
     * 1, 货币查询
     */
    static readonly CURRENCY_OP_QUERY: number = 1;

    /**
     * 3, 减少货币
     */
    static readonly CURRENCY_OP_SUB: number = 3;

    /**
     * 21, 充值排行榜 金仙争夺, 跨服至尊夺宝
     */
    static readonly CZJXZD_OPEN_PANNEL: number = 21;

    /**
     * 232, 充值排行榜 金仙争夺, 跨服至尊夺宝
     */
    static readonly CZJXZD_REWARD: number = 232;

    /**
     * 10, 充值排行榜最多显示个数
     */
    static readonly CZ_RANK_MAX_SHOW_COUNT: number = 10;

    /**
     * 94, 消费赠好礼领取奖励
     */
    static readonly DAILY_CONSUME_GET: number = 94;

    /**
     * 93, 消费赠好礼面板
     */
    static readonly DAILY_CONSUME_PANEL: number = 93;

    /**
     * 82, 每日首充送大礼  领取奖励
     */
    static readonly DAILY_FIRST_RECHARGE_GET: number = 82;

    /**
     * 81, 每日首充送大礼  面板
     */
    static readonly DAILY_FIRST_RECHARGE_PANEL: number = 81;

    /**
     * 60401135, 每日首充送大礼掉落ID
     */
    static readonly DAILY_FRIST_RECHARGE_DROPID: number = 60401135;

    /**
     * 128, 每日是否打开过精彩活动页签
     */
    static readonly DAY_JINGAI_OPEN: number = 128;

    /**
     * 8, 王蓉NPC，每日赠送宝鉴
     */
    static readonly DAY_JUANZHOU_GET: number = 8;

    /**
     * 4, 开服至尊争夺 每天配置个数
     */
    static readonly DAY_KFZZZD_CONFIG_COUNT: number = 4;

    /**
     * 1024, qq客服礼包
     */
    static readonly DAY_QQ_SERVICE: number = 1024;

    /**
     * 256, 每日日常任务付费领取奖励
     */
    static readonly DAY_QUEST_DAILY: number = 256;

    /**
     * 512, 每日宗门任务付费领取奖励
     */
    static readonly DAY_QUEST_GUILD: number = 512;

    /**
     * 2048, qq客服礼包
     */
    static readonly DAY_SUPPERVIP_SERVICE: number = 2048;

    /**
     * 15, 124活动 充值档次个数
     */
    static readonly DBACT124_LIST_COUNT: number = 15;

    /**
     * 20, 125活动 充值档次个数
     */
    static readonly DBACT125_LIST_COUNT: number = 20;

    /**
     * 256
     */
    static readonly DBMS_CONNECTION_INFO_LEN: number = 256;

    /**
     * 64
     */
    static readonly DBMS_DATABASE_NAME_LEN: number = 64;

    /**
     * 32
     */
    static readonly DBMS_NAME_LEN: number = 32;

    /**
     * 64
     */
    static readonly DBMS_PASSWORD_LEN: number = 64;

    /**
     * 256
     */
    static readonly DBMS_SOCK_FILE_LEN: number = 256;

    /**
     * 64
     */
    static readonly DBMS_TABLE_CHARTSET_LEN: number = 64;

    /**
     * 64
     */
    static readonly DBMS_TABLE_ENGINE_LEN: number = 64;

    /**
     * 256
     */
    static readonly DBMS_USER_LEN: number = 256;

    /**
     * 4, 最多签到字节数*8
     */
    static readonly DB_SIGN_MAX_SIZE: number = 4;

    /**
     * 123, 跨服点灯-刷新列表
     */
    static readonly DDL_ACT_FRESH: number = 123;

    /**
     * 126, 跨服点灯-领取参与奖励
     */
    static readonly DDL_ACT_GET_REWARD: number = 126;

    /**
     * 124, 跨服点灯-点灯
     */
    static readonly DDL_ACT_LIGHT: number = 124;

    /**
     * 122, 跨服点灯-打开点灯面板
     */
    static readonly DDL_ACT_PANEL: number = 122;

    /**
     * 125, 跨服点灯-打开排行面板
     */
    static readonly DDL_ACT_RANK_PANEL: number = 125;

    /**
     * 10, 点灯排行榜最多显示个数
     */
    static readonly DDL_RANK_MAX_SHOW_COUNT: number = 10;

    /**
     * 51030901, 死亡惩罚buff
     */
    static readonly DEAD_PUNISH_BUFF_ID: number = 51030901;

    /**
     * 3, 帝者之路副本任务关卡个数
     */
    static readonly DEAR_LOAD_MAX_NUM: number = 3;

    /**
     * 4, 时装形象
     */
    static readonly DERESS_IMAGE: number = 4;

    /**
     * 3, 地宫场景宗门信息最大数量
     */
    static readonly DG_HURTINFO_MAX_NUM: number = 3;

    /**
     * 4, 天榜竞技 跨服榜
     */
    static readonly DIBANG_CROSS: number = 4;

    /**
     * 2, 地榜竞技 跨服榜 刷新冷却时间
     */
    static readonly DIBANG_CROSS_REFRESH_TIME: number = 2;

    /**
     * 3, 地榜竞技 本服榜
     */
    static readonly DIBANG_LOCAL: number = 3;

    /**
     * 1, 地榜竞技 本服榜 刷新冷却时间
     */
    static readonly DIBANG_REFRESH_TIME: number = 1;

    /**
     * 1, 地榜竞技本服榜职位1
     */
    static readonly DI_BANG_LOCAL_BIT_1: number = 1;

    /**
     * 2, 地榜竞技本服榜职位2
     */
    static readonly DI_BANG_LOCAL_BIT_2: number = 2;

    /**
     * 3, 地榜竞技本服榜职位3
     */
    static readonly DI_BANG_LOCAL_BIT_3: number = 3;

    /**
     * 4, 地榜竞技本服榜职位4
     */
    static readonly DI_BANG_LOCAL_BIT_4: number = 4;

    /**
     * 5, 地榜竞技本服榜职位5
     */
    static readonly DI_BANG_LOCAL_BIT_5: number = 5;

    /**
     * 6, 地榜竞技本服榜职位6
     */
    static readonly DI_BANG_LOCAL_BIT_6: number = 6;

    /**
     * 10, 地榜竞技刷新价格
     */
    static readonly DI_BANG_REFRESH_MONEY: number = 10;

    /**
     * 1, 是否领取过微端登录的奖励
     */
    static readonly DOT_WEIDUAN_LOGIN: number = 1;

    /**
     * 2, 终生一次标志 数组长度 char个数 2个够记录16Bit
     */
    static readonly DOUBLE_REBATE_BIT_MAX_LENGTH: number = 2;

    /**
     * 3, 时装形象设置
     */
    static readonly DRESS_IMAGE_CHANGE: number = 3;

    /**
     * 2, 时装形象列表
     */
    static readonly DRESS_IMAGE_LIST: number = 2;

    /**
     * 4, 时装形象显示/隐藏设置
     */
    static readonly DRESS_IMAGE_SHOW: number = 4;

    /**
     * 6, 时装强化
     */
    static readonly DRESS_STRENG: number = 6;

    /**
     * 5, 时装培养
     */
    static readonly DRESS_TRAIN: number = 5;

    /**
     * 1, 时装升级
     */
    static readonly DRESS_UPLEVEL: number = 1;

    /**
     * 5, 属主是宗派
     */
    static readonly DROPPEDTHING_OWNERTYPE_GUILD: number = 5;

    /**
     * 0, 属主是个人
     */
    static readonly DROPPEDTHING_OWNERTYPE_NORMAL: number = 0;

    /**
     * 2, 无属主
     */
    static readonly DROPPEDTHING_OWNERTYPE_NULL: number = 2;

    /**
     * 1, 属主是队伍
     */
    static readonly DROPPEDTHING_OWNERTYPE_TEAM: number = 1;

    /**
     * 3, 属主是区服
     */
    static readonly DROPPEDTHING_OWNERTYPE_WORLD: number = 3;

    /**
     * 1, 变更的物品
     */
    static readonly DROPPEDTHING_PARAM_MOD: number = 1;

    /**
     * 0, 新加入的物品
     */
    static readonly DROPPEDTHING_PARAM_NEW: number = 0;

    /**
     * 136, 端午活动-兑换粽子
     */
    static readonly DUANWU_ACT_EXCHANGE: number = 136;

    /**
     * 137, 端午活动-领取达成奖励
     */
    static readonly DUANWU_ACT_GET_REWARD: number = 137;

    /**
     * 160, 端午活动，领取登录奖励
     */
    static readonly DUANWU_ACT_LOGIN_REWARD: number = 160;

    /**
     * 135, 端午活动-打开面板
     */
    static readonly DUANWU_ACT_PANEL: number = 135;

    /**
     * 2, 第一次召唤后，紫色星魂已经给过了
     */
    static readonly EDWARD_FIRST_RECRUITED_STATUS: number = 2;

    /**
     * 1, 第一次召唤
     */
    static readonly EDWARD_FIRST_RECRUITING_STATUS: number = 1;

    /**
     * 0, 从没召唤过
     */
    static readonly EDWARD_NEVER_RECRUIT_STATUS: number = 0;

    /**
     * 3, 宝石镶嵌
     */
    static readonly EQUIP_DIAMOND_MOUNT: number = 3;

    /**
     * 4, 宝石摘除
     */
    static readonly EQUIP_DIAMOND_UNMOUNT: number = 4;

    /**
     * 5, 宝石升级
     */
    static readonly EQUIP_DIAMOND_UPLEVEL: number = 5;

    /**
     * 1030, 装备兑换的商店ID
     */
    static readonly EQUIP_EXCHANGE_STORE_ID: number = 1030;

    /**
     * 60144001, 装备熔炼， 紫色掉落方案
     */
    static readonly EQUIP_FORGE_DROP_ID_1: number = 60144001;

    /**
     * 60144002, 装备熔炼， 橙色掉落方案
     */
    static readonly EQUIP_FORGE_DROP_ID_2: number = 60144002;

    /**
     * 60144003, 装备熔炼， 金色掉落方案
     */
    static readonly EQUIP_FORGE_DROP_ID_3: number = 60144003;

    /**
     * 21, 魂骨装备封装
     */
    static readonly EQUIP_HUNGU_FZ: number = 21;

    /**
     * 26, 保存装备洗炼结果
     */
    static readonly EQUIP_HUNGU_SAVE_WASH: number = 26;

    /**
     * 29, 魂骨装备技能封装
     */
    static readonly EQUIP_HUNGU_SKILL_FZ: number = 29;

    /**
     * 23, 魂骨装备位一键升级
     */
    static readonly EQUIP_HUNGU_SLOT_ONE_UPLV: number = 23;

    /**
     * 22, 魂骨装备位升级
     */
    static readonly EQUIP_HUNGU_SLOT_UPLEVEL: number = 22;

    /**
     * 25, 魂骨位洗炼
     */
    static readonly EQUIP_HUNGU_SLOT_WASH: number = 25;

    /**
     * 28, 元宝开启洗炼条
     */
    static readonly EQUIP_HUNGU_SLOT_WASH_BUY: number = 28;

    /**
     * 24, 魂骨装备强化
     */
    static readonly EQUIP_HUNGU_STRENG: number = 24;

    /**
     * 27, 装备洗炼,属性锁定信息变更
     */
    static readonly EQUIP_HUNGU_WASH_LOCK: number = 27;

    /**
     * 12, 装备炼器升级
     */
    static readonly EQUIP_LQ_UPLEVEL: number = 12;

    /**
     * 6, 装备熔炼
     */
    static readonly EQUIP_MELT: number = 6;

    /**
     * 8, 保存装备洗炼结果
     */
    static readonly EQUIP_SAVE_WASH_RESULT: number = 8;

    /**
     * 18, 装备位炼体升级
     */
    static readonly EQUIP_SLOTLIANTI_UP: number = 18;

    /**
     * 19, 装备位炼体神宝
     */
    static readonly EQUIP_SLOTLTSB_ACT: number = 19;

    /**
     * 16, 装备位套装激活
     */
    static readonly EQUIP_SLOTSUIT_ACT: number = 16;

    /**
     * 17, 装备位套装升级
     */
    static readonly EQUIP_SLOTSUIT_UP: number = 17;

    /**
     * 14, 装备位一键升级
     */
    static readonly EQUIP_SLOT_ONE_UPLV: number = 14;

    /**
     * 13, 装备位升级
     */
    static readonly EQUIP_SLOT_UPLEVEL: number = 13;

    /**
     * 2, 装备强化
     */
    static readonly EQUIP_STRENG: number = 2;

    /**
     * 2, 装备塑魂涨经验
     */
    static readonly EQUIP_SUHUN_ADDEXP: number = 2;

    /**
     * 0, 装备塑魂升级失败
     */
    static readonly EQUIP_SUHUN_UPFAIL: number = 0;

    /**
     * 1, 装备塑魂升级成功
     */
    static readonly EQUIP_SUHUN_UPSUCCESS: number = 1;

    /**
     * 15, 套装激活
     */
    static readonly EQUIP_SUIT_ACT: number = 15;

    /**
     * 1, 装备
     */
    static readonly EQUIP_TYPE: number = 1;

    /**
     * 7, 法阵装备
     */
    static readonly EQUIP_TYPE_FZ: number = 7;

    /**
     * 9, 火精装备
     */
    static readonly EQUIP_TYPE_HJ: number = 9;

    /**
     * 6, 精灵装备
     */
    static readonly EQUIP_TYPE_JL: number = 6;

    /**
     * 8, 雷灵装备
     */
    static readonly EQUIP_TYPE_LL: number = 8;

    /**
     * 2, 坐骑装备
     */
    static readonly EQUIP_TYPE_MOUNT: number = 2;

    /**
     * 4, 美人装备
     */
    static readonly EQUIP_TYPE_PET: number = 4;

    /**
     * 1, 玩家装备
     */
    static readonly EQUIP_TYPE_ROLE: number = 1;

    /**
     * 5, 武魂装备
     */
    static readonly EQUIP_TYPE_WH: number = 5;

    /**
     * 3, 翅膀装备
     */
    static readonly EQUIP_TYPE_WING: number = 3;

    /**
     * 10, 战灵装备
     */
    static readonly EQUIP_TYPE_ZL: number = 10;

    /**
     * 1, 装备升阶
     */
    static readonly EQUIP_UPCOLOR: number = 1;

    /**
     * 7, 装备洗炼
     */
    static readonly EQUIP_WASH: number = 7;

    /**
     * 11, 元宝开启洗炼条
     */
    static readonly EQUIP_WASH_BUY: number = 11;

    /**
     * 10, 钻石洗炼
     */
    static readonly EQUIP_WASH_DIAMOND: number = 10;

    /**
     * 9, 装备洗炼,属性锁定信息变更
     */
    static readonly EQUIP_WASH_LOCK: number = 9;

    /**
     * 2, 翅膀转化
     */
    static readonly EQUIP_WING_CHANGE: number = 2;

    /**
     * 4, 翅膀形象设置
     */
    static readonly EQUIP_WING_IMAGE_CHANGE: number = 4;

    /**
     * 3, 翅膀形象列表
     */
    static readonly EQUIP_WING_IMAGE_LIST: number = 3;

    /**
     * 1, 翅膀升级
     */
    static readonly EQUIP_WING_UPLEVEL: number = 1;

    /**
     * 11, 攻击压制|    伤害加成 万分比
     */
    static readonly EUAI_ATKPRESS: number = 11;

    /**
     * 46, 格挡率
     */
    static readonly EUAI_BLOCK_RATE: number = 46;

    /**
     * 35, 当前精血
     */
    static readonly EUAI_BLOOD: number = 35;

    /**
     * 47, 破挡率
     */
    static readonly EUAI_BREAKBLOCK_RATE: number = 47;

    /**
     * 37, 破击
     */
    static readonly EUAI_BREAK_ATT: number = 37;

    /**
     * 38, 抗破
     */
    static readonly EUAI_BREAK_DEF: number = 38;

    /**
     * 49, 破防比例
     */
    static readonly EUAI_BREAK_DEF_PER: number = 49;

    /**
     * 48, 破防率
     */
    static readonly EUAI_BREAK_DEF_RATE: number = 48;

    /**
     * 25, 爆击
     */
    static readonly EUAI_CRITICAL: number = 25;

    /**
     * 26, 爆击率
     */
    static readonly EUAI_CRITICALRATE: number = 26;

    /**
     * 44, 暴击加成
     */
    static readonly EUAI_CRITICAL_EXT: number = 44;

    /**
     * 13, 暴伤
     */
    static readonly EUAI_CRITICAL_HURT: number = 13;

    /**
     * 5, 当前经验
     */
    static readonly EUAI_CUREXP: number = 5;

    /**
     * 2, 当前生命
     */
    static readonly EUAI_CURHP: number = 2;

    /**
     * 4, 当前法力
     */
    static readonly EUAI_CURMP: number = 4;

    /**
     * 30, 被动伤害加深
     */
    static readonly EUAI_DEEPHURT: number = 30;

    /**
     * 9, 普防
     */
    static readonly EUAI_DEFENSE: number = 9;

    /**
     * 10, 防御压制|    伤害减免 万分比
     */
    static readonly EUAI_DEFPRESS: number = 10;

    /**
     * 20, 伤害减免 固定值
     */
    static readonly EUAI_DEFPRESS_VAL: number = 20;

    /**
     * 21, 闪避
     */
    static readonly EUAI_DODGE: number = 21;

    /**
     * 22, 闪避率
     */
    static readonly EUAI_DODGERATE: number = 22;

    /**
     * 42, 闪避加成
     */
    static readonly EUAI_DODGE_EXT: number = 42;

    /**
     * 50, 经验值加成
     */
    static readonly EUAI_EXP_ADD: number = 50;

    /**
     * 27, 战斗力
     */
    static readonly EUAI_FIGHT: number = 27;

    /**
     * 62, 显示格挡显示
     */
    static readonly EUAI_GDSHOW: number = 62;

    /**
     * 19, 命中
     */
    static readonly EUAI_GOAL: number = 19;

    /**
     * 43, 命中加成
     */
    static readonly EUAI_GOAL_EXT: number = 43;

    /**
     * 53, 神圣之力
     */
    static readonly EUAI_GOD_POWER: number = 53;

    /**
     * 28, 生命药包
     */
    static readonly EUAI_HPSTORE: number = 28;

    /**
     * 52, 回血加成
     */
    static readonly EUAI_HP_RESTORE_ADD: number = 52;

    /**
     * 18, 绝对伤害|    伤害
     */
    static readonly EUAI_HURTEXTRA: number = 18;

    /**
     * 0, 等级
     */
    static readonly EUAI_LEVEL: number = 0;

    /**
     * 8, 属攻
     */
    static readonly EUAI_MAGATK: number = 8;

    /**
     * 12, 属防
     */
    static readonly EUAI_MAGICRESIST: number = 12;

    /**
     * 56, 最大EUAI值
     */
    static readonly EUAI_MAX: number = 56;

    /**
     * 1, 最大生命
     */
    static readonly EUAI_MAXHP: number = 1;

    /**
     * 31, 被动最大生命额外伤害
     */
    static readonly EUAI_MAXHPHURT: number = 31;

    /**
     * 3, 最大法力
     */
    static readonly EUAI_MAXMP: number = 3;

    /**
     * 36, 当前最大精血
     */
    static readonly EUAI_MAX_BLOOD: number = 36;

    /**
     * 34, 当前最大灵魂值
     */
    static readonly EUAI_MAX_SOUL: number = 34;

    /**
     * 29, 法力药包
     */
    static readonly EUAI_MPSTORE: number = 29;

    /**
     * 7, 普攻
     */
    static readonly EUAI_PHYATK: number = 7;

    /**
     * 60, 显示破击伤害
     */
    static readonly EUAI_PJSHSHOW: number = 60;

    /**
     * 32, 当前怒气值
     */
    static readonly EUAI_RAGE: number = 32;

    /**
     * 55, 反伤率
     */
    static readonly EUAI_REBOUND_RATE: number = 55;

    /**
     * 17, 守护盾
     */
    static readonly EUAI_SHIELDGOD: number = 17;

    /**
     * 33, 当前灵魂值
     */
    static readonly EUAI_SOUL: number = 33;

    /**
     * 6, 速度
     */
    static readonly EUAI_SPEED: number = 6;

    /**
     * 54, 吸血率
     */
    static readonly EUAI_SUCK_BLOOD_RATE: number = 54;

    /**
     * 14, 破甲
     */
    static readonly EUAI_THROUGH: number = 14;

    /**
     * 51, 银两加成
     */
    static readonly EUAI_TONGQIAN_ADD: number = 51;

    /**
     * 23, 抗暴
     */
    static readonly EUAI_TOUGHNESS: number = 23;

    /**
     * 24, 抗暴率
     */
    static readonly EUAI_TOUGHNESSRATE: number = 24;

    /**
     * 45, 抗暴加成
     */
    static readonly EUAI_TOUGHNESS_EXT: number = 45;

    /**
     * 40, 真神伤害
     */
    static readonly EUAI_TRUEGOD_ATT: number = 40;

    /**
     * 41, 真神抵抗
     */
    static readonly EUAI_TRUEGOD_DEF: number = 41;

    /**
     * 39, 真神概率
     */
    static readonly EUAI_TRUEGOD_RATE: number = 39;

    /**
     * 63, 显示伤害显示
     */
    static readonly EUAI_XSSHSHOW: number = 63;

    /**
     * 61, 显示真神伤害
     */
    static readonly EUAI_ZSSHSHOW: number = 61;

    /**
     * 4096, 1左移12  	主动攻击怪物
     */
    static readonly EUS_ACTIVEATTACK: number = 4096;

    /**
     * 1, 1  		1: 生存, 0: 死亡
     */
    static readonly EUS_ALIVE: number = 1;

    /**
     * 64, 1左移6  	1: 昏迷状态 0：正常状态
     */
    static readonly EUS_COMA: number = 64;

    /**
     * 1024, 1左移10  	0: 不删除, 1: 删除
     */
    static readonly EUS_DELETED: number = 1024;

    /**
     * 4, 1左移2  	1: 处于战斗中 0: 不在战斗中
     */
    static readonly EUS_FIGHT: number = 4;

    /**
     * 262144, 1左移17  	1:离线挂机 0正常状态 不会踢下线
     */
    static readonly EUS_GUAJI: number = 262144;

    /**
     * 16384, 1左移14  	是否GM权限
     */
    static readonly EUS_ISGM: number = 16384;

    /**
     * 8192, 1左移13  	登录成功了
     */
    static readonly EUS_LOGIN_OK: number = 8192;

    /**
     * 512, 1左移9  	1: 准备退出 0:正常状态
     */
    static readonly EUS_LOGOUT: number = 512;

    /**
     * 2, 1左移1  	1: 在线, 0: 下线 掉线会删除内存中玩家数据
     */
    static readonly EUS_ONLINE: number = 2;

    /**
     * 131072, 1左移16  	1: 处于PVP战斗中 0: 不在PVP战斗中。在pvp战斗，肯定是在战斗状态。在战斗状态，但不是PVP战斗状态，说明是在PVE
     */
    static readonly EUS_PVP_FIGHT: number = 131072;

    /**
     * 8, 1左移3  	1: 处于招募中 0: 不在招募中
     */
    static readonly EUS_RECRUIT: number = 8;

    /**
     * 65536, 1左移15  	是否骑乘状态
     */
    static readonly EUS_RIDE: number = 65536;

    /**
     * 32, 1左移5  	1: 秒杀状态 0: 正常状态
     */
    static readonly EUS_SMASH: number = 32;

    /**
     * 16, 1左移4  	1: 无敌状态 0: 正常状态
     */
    static readonly EUS_STRONG: number = 16;

    /**
     * 2048, 1左移11  	性能测试帐号, 不用强制拉入出生场景
     */
    static readonly EUS_TESTING: number = 2048;

    /**
     * 128, 1左移7  	1: 单位变身
     */
    static readonly EUS_TRANSFORM: number = 128;

    /**
     * 256, 1左移8  	1: 打坐状态 0：正常状态
     */
    static readonly EUS_ZAZEN: number = 256;

    /**
     * 104, 宝箱
     */
    static readonly EUT_CHEST: number = 104;

    /**
     * 105, 掉落物品
     */
    static readonly EUT_DROPPEDTHING: number = 105;

    /**
     * 101, 怪物
     */
    static readonly EUT_MONSTER: number = 101;

    /**
     * 106, NPC
     */
    static readonly EUT_NPC: number = 106;

    /**
     * 102, 散仙
     */
    static readonly EUT_PET: number = 102;

    /**
     * 100, 角色
     */
    static readonly EUT_ROLE: number = 100;

    /**
     * 1090, 兑换商城的商店ID
     */
    static readonly EXCHANGE_STORE_ID: number = 1090;

    /**
     * 2, Buff状态
     */
    static readonly EffectMask_Buff: number = 2;

    /**
     * 3, 暴击
     */
    static readonly EffectMask_Critical: number = 3;

    /**
     * 1, Miss
     */
    static readonly EffectMask_Miss: number = 1;

    /**
     * 0, 无效果
     */
    static readonly EffectMask_None: number = 0;

    /**
     * 4, 法宝激活物品个数
     */
    static readonly FABAO_ACCESS_ITEM_COUNT: number = 4;

    /**
     * 4, 法宝镶嵌个数
     */
    static readonly FABAO_XIANGQIAN_COUNT: number = 4;

    /**
     * 10, 法器等阶个数
     */
    static readonly FAQI_MAX_LYAER_COUNT: number = 10;

    /**
     * 1, 激活法器
     */
    static readonly FAQI_OP_ACT: number = 1;

    /**
     * 3, 法器变换形象
     */
    static readonly FAQI_OP_CHANGE_IMAGE: number = 3;

    /**
     * 4, 获取法器信息
     */
    static readonly FAQI_OP_LIST: number = 4;

    /**
     * 7, 法器祝福值补满
     */
    static readonly FAQI_OP_LUCKY_FILL: number = 7;

    /**
     * 6, 法器祝福值保留
     */
    static readonly FAQI_OP_LUCKY_KEEP: number = 6;

    /**
     * 2, 升级法器
     */
    static readonly FAQI_OP_UPLEVEL: number = 2;

    /**
     * 5, 法器注魂
     */
    static readonly FAQI_OP_ZHUHUN: number = 5;

    /**
     * 10, 法器技能等阶个数
     */
    static readonly FAQI_SKILL_MAX_LYAER_COUNT: number = 10;

    /**
     * 3, 红颜缘分最大数
     */
    static readonly FATE_MAX_NUMS: number = 3;

    /**
     * 2, 法则升级
     */
    static readonly FAZE_LEVEL_UP: number = 2;

    /**
     * 1, 打开法则面板
     */
    static readonly FAZE_OPEN_PANEL: number = 1;

    /**
     * 2, 法阵激活
     */
    static readonly FAZHEN_ACTIVATE: number = 2;

    /**
     * 3, 法阵幻化
     */
    static readonly FAZHEN_IMAGE_CHANGE: number = 3;

    /**
     * 1, 打开法阵面板
     */
    static readonly FAZHEN_OPEN_PANEL: number = 1;

    /**
     * 90, 圣诞卡片  兑换物品
     */
    static readonly FESTIVAL_CARD_CONVERSION_GET: number = 90;

    /**
     * 89, 圣诞卡片兑换  面板
     */
    static readonly FESTIVAL_CARD_CONVERSION_PANEL: number = 89;

    /**
     * 86, 节日充值  领取奖励
     */
    static readonly FESTIVAL_CHARGE_GET: number = 86;

    /**
     * 85, 节日充值  面板
     */
    static readonly FESTIVAL_CHARGE_PANEL: number = 85;

    /**
     * 88, 节日消费  领取奖励
     */
    static readonly FESTIVAL_CONSUME_GET: number = 88;

    /**
     * 87, 节日消费  面板
     */
    static readonly FESTIVAL_CONSUME_PANEL: number = 87;

    /**
     * 60, 封魔塔 boss总共的最大数量
     */
    static readonly FMT_BOSS_MAX_NUM: number = 60;

    /**
     * 5, 封魔塔 记录击杀者的最大数量
     */
    static readonly FMT_KILLER_MAX_NUM: number = 5;

    /**
     * 12, 封魔塔场小怪种类 入库
     */
    static readonly FMT_MONSTER_TYPE_NUMBER: number = 12;

    /**
     * 262, 封魔塔场景1
     */
    static readonly FMT_SCENE_1: number = 262;

    /**
     * 263, 封魔塔场景2
     */
    static readonly FMT_SCENE_2: number = 263;

    /**
     * 264, 封魔塔场景3
     */
    static readonly FMT_SCENE_3: number = 264;

    /**
     * 0, 禁止0小时,相当于踢人
     */
    static readonly FORBID_TYPE_0: number = 0;

    /**
     * 1, 禁止1小时
     */
    static readonly FORBID_TYPE_1: number = 1;

    /**
     * 4, 禁止24小时
     */
    static readonly FORBID_TYPE_24: number = 4;

    /**
     * 2, 禁止3小时
     */
    static readonly FORBID_TYPE_3: number = 2;

    /**
     * 3, 禁止5小时
     */
    static readonly FORBID_TYPE_5: number = 3;

    /**
     * 5, 永久
     */
    static readonly FORBID_TYPE_FOREVER: number = 5;

    /**
     * 4, 黑名单bit位
     */
    static readonly FRIENDTYPE_BLACK_BIT: number = 4;

    /**
     * 8, 宿敌bit位
     */
    static readonly FRIENDTYPE_ENEMY_BIT: number = 8;

    /**
     * 1, 普通朋友bit位
     */
    static readonly FRIENDTYPE_NORMAL_BIT: number = 1;

    /**
     * 1, 上线
     */
    static readonly FRIEND_LOGIN: number = 1;

    /**
     * 2, 下线
     */
    static readonly FRIEND_LOGOUT: number = 2;

    /**
     * 1, 黑名单
     */
    static readonly FRIEND_TYPE_BLACK: number = 1;

    /**
     * 2, 宿敌
     */
    static readonly FRIEND_TYPE_ENEMY: number = 2;

    /**
     * 0, 普通好友
     */
    static readonly FRIEND_TYPE_FRIEND: number = 0;

    /**
     * 16384, 1 左移 14 APP下载礼包
     */
    static readonly FST_APP_DOWN_REWARD: number = 16384;

    /**
     * 16, 1 左移 16 终生充值返元宝
     */
    static readonly FST_CHARGE_GIFT_LEVEL_1: number = 16;

    /**
     * 17, 1 左移 17 终生充值返元宝
     */
    static readonly FST_CHARGE_GIFT_LEVEL_2: number = 17;

    /**
     * 18, 1 左移 18 终生充值返元宝
     */
    static readonly FST_CHARGE_GIFT_LEVEL_3: number = 18;

    /**
     * 19, 1 左移 19 终生充值返元宝
     */
    static readonly FST_CHARGE_GIFT_LEVEL_4: number = 19;

    /**
     * 20, 1 左移 20 终生充值返元宝
     */
    static readonly FST_CHARGE_GIFT_LEVEL_5: number = 20;

    /**
     * 21, 1 左移 21 终生充值返元宝
     */
    static readonly FST_CHARGE_GIFT_LEVEL_6: number = 21;

    /**
     * 22, 1 左移 22 终生充值返元宝
     */
    static readonly FST_CHARGE_GIFT_LEVEL_7: number = 22;

    /**
     * 23, 1 左移 23 终生充值返元宝
     */
    static readonly FST_CHARGE_GIFT_LEVEL_8: number = 23;

    /**
     * 4, 灵宝过期是否已推送过
     */
    static readonly FST_LINGBAO_OVERTIME: number = 4;

    /**
     * 1, 1 左移 0 是否手机注册过礼包领过了
     */
    static readonly FST_OPEN_MOBILE: number = 1;

    /**
     * 8192, 1 左移 13 是否首次通关RMB战场
     */
    static readonly FST_PASS_LEAD_FUBEN: number = 8192;

    /**
     * 2048, 1 左移 11 是否执行了免费经验祈福
     */
    static readonly FST_QIFU_JINGYAN: number = 2048;

    /**
     * 4, 1 左移 2 是否第一次刷护送任务
     */
    static readonly FST_REFRESH_GY_QUEST: number = 4;

    /**
     * 4096, 1 左移 12 是否强化过装备
     */
    static readonly FST_STRONG_EQUIP: number = 4096;

    /**
     * 8, 1 左移 3 是否领过超级VIP礼包
     */
    static readonly FST_SUPERVIP_GIFT: number = 8;

    /**
     * 32, 1 左移 5 是否领过微信礼包
     */
    static readonly FST_WEIXING_GIFT: number = 32;

    /**
     * 4, 领取挑战BOSS奖励
     */
    static readonly GET_CHALLENGE_BOSS_REWARD: number = 4;

    /**
     * 7, 领取累计充值奖励
     */
    static readonly GET_LJCZ_REWARD: number = 7;

    /**
     * 3, 领取月卡返还奖励
     */
    static readonly GET_MONTH_CARD_BACKTO_REWARD: number = 3;

    /**
     * 4, 领取月卡每日奖励
     */
    static readonly GET_MONTH_CARD_DAILY_REWARD: number = 4;

    /**
     * 9, 领取进阶日活动奖励
     */
    static readonly GET_STAGEDAY_REWARD: number = 9;

    /**
     * 31050004, 女主播掉落普通宝箱怪物ID
     */
    static readonly GIRL_NORMAL_ID: number = 31050004;

    /**
     * 16, 自动组队
     */
    static readonly GJ_AUTO_ADD_TEAM: number = 16;

    /**
     * 256, 自动使用红颜技能
     */
    static readonly GJ_AUTO_BEAUTY_SKILL: number = 256;

    /**
     * 64, 自动购买血药
     */
    static readonly GJ_AUTO_BUY_HP: number = 64;

    /**
     * 128, 自动元宝复活
     */
    static readonly GJ_AUTO_BUY_REVLIE: number = 128;

    /**
     * 512, 自动定点挂机
     */
    static readonly GJ_AUTO_HUANG_UP: number = 512;

    /**
     * 32, 自动使用怒气技能
     */
    static readonly GJ_AUTO_NUQI_SKILL: number = 32;

    /**
     * 4, 自动拾取
     */
    static readonly GJ_AUTO_PICKUP_SETTING: number = 4;

    /**
     * 8, 开启角色死亡后原地复活设置
     */
    static readonly GJ_AUTO_ROLE_RELIVE: number = 8;

    /**
     * 2, 药品耗尽后停止战斗
     */
    static readonly GJ_AUTO_STOP_MEDICINE_END: number = 2;

    /**
     * 1, 受到攻击后自动反击
     */
    static readonly GJ_AUTO_STRIKE_BACK: number = 1;

    /**
     * 1, 应用管理员
     */
    static readonly GMType_Application: number = 1;

    /**
     * 3, GM管理员
     */
    static readonly GMType_GM: number = 3;

    /**
     * 4, 范围最大值，校验时使用
     */
    static readonly GMType_Max: number = 4;

    /**
     * 0, 没有任何权限
     */
    static readonly GMType_None: number = 0;

    /**
     * 2, 系统管理员
     */
    static readonly GMType_System: number = 2;

    /**
     * 0, GM命令
     */
    static readonly GM_TYPE_COMMAND: number = 0;

    /**
     * 1, 系统消息
     */
    static readonly GM_TYPE_MESSAGE: number = 1;

    /**
     * 2, 界面上提示信息
     */
    static readonly GM_TYPE_WARNING: number = 2;

    /**
     * 1, 成仙之路 不可领取
     */
    static readonly GOD_LOAD_AWARD_CANT_GET: number = 1;

    /**
     * 3, 成仙之路 已领取
     */
    static readonly GOD_LOAD_AWARD_DONE_GET: number = 3;

    /**
     * 2, 成仙之路 可领取
     */
    static readonly GOD_LOAD_AWARD_WAIT_GET: number = 2;

    /**
     * 113, 抢红包
     */
    static readonly GRAB_REDBAG: number = 113;

    /**
     * 1, 地榜竞技
     */
    static readonly GRJJC_TYPE_DB: number = 1;

    /**
     * 3, 极速挑战
     */
    static readonly GRJJC_TYPE_JSTZ: number = 3;

    /**
     * 2, 天榜竞技
     */
    static readonly GRJJC_TYPE_TB: number = 2;

    /**
     * 0, 天命榜
     */
    static readonly GRJJC_TYPE_TMB: number = 0;

    /**
     * 2, 团购购买
     */
    static readonly GROUPBUY_BUY: number = 2;

    /**
     * 1, 开服团购信息查询
     */
    static readonly GROUPBUY_LIST: number = 1;

    /**
     * 0
     */
    static readonly GUILD_APPLY_CODE_APPLY: number = 0;

    /**
     * 2
     */
    static readonly GUILD_APPLY_CODE_ASKNOTIFY: number = 2;

    /**
     * 1
     */
    static readonly GUILD_APPLY_CODE_WITHDRAW: number = 1;

    /**
     * 2, 宗派每日分配次数
     */
    static readonly GUILD_ASSIGN_MAX_COUNT: number = 2;

    /**
     * 0, 宗派自动加入 无限制
     */
    static readonly GUILD_AUTO_JOIN_BIT_AUTO: number = 0;

    /**
     * 1, 宗派自动加入 等级限制
     */
    static readonly GUILD_AUTO_JOIN_BIT_LEVEL: number = 1;

    /**
     * 5, 宗派自动加入 不能自动加入
     */
    static readonly GUILD_AUTO_JOIN_BIT_NONE: number = 5;

    /**
     * 2, 宗派自动加入 VIP1级
     */
    static readonly GUILD_AUTO_JOIN_BIT_VIP1: number = 2;

    /**
     * 3, 宗派自动加入 VIP2级
     */
    static readonly GUILD_AUTO_JOIN_BIT_VIP2: number = 3;

    /**
     * 4, 宗派自动加入 VIP3级
     */
    static readonly GUILD_AUTO_JOIN_BIT_VIP3: number = 4;

    /**
     * 20, 宗派任务每日可完成次数的基数
     */
    static readonly GUILD_BASE_QUEST_NUM: number = 20;

    /**
     * 20, 家族邀请
     */
    static readonly GUILD_BROADCAST_INVITE: number = 20;

    /**
     * 69, 宗门召唤血战封魔
     */
    static readonly GUILD_CALL_XZFM: number = 69;

    /**
     * 8, 申请列表审核 --- 帮主与副帮主
     */
    static readonly GUILD_CHECK_JOIN: number = 8;

    /**
     * 3, 创建宗派
     */
    static readonly GUILD_CREATE_GUILD: number = 3;

    /**
     * 40, 角色等级达到40才能创建宗派
     */
    static readonly GUILD_CREATE_LEVEL_LIMIT: number = 40;

    /**
     * 2, 跨服对阵人数
     */
    static readonly GUILD_CROSSPVP_AGAINST_NUM: number = 2;

    /**
     * 2, 跨服可以报名的宗派等级
     */
    static readonly GUILD_CROSSPVP_APPLY_LEVEL: number = 2;

    /**
     * 5, 跨服宗派战-自动报名
     */
    static readonly GUILD_CROSSPVP_AUTOAPPLY: number = 5;

    /**
     * 7, 跨服宗派战-清空数据
     */
    static readonly GUILD_CROSSPVP_CLEAN_DATA: number = 7;

    /**
     * 12, 解散宗派 --- 帮主
     */
    static readonly GUILD_DISMISS_GUILD: number = 12;

    /**
     * 10074010, 宗派捐献用道具ID 非绑定的
     */
    static readonly GUILD_DONATE_ITEM_ID: number = 10074010;

    /**
     * 100, 宗派捐献 道具参数
     */
    static readonly GUILD_DONATE_ITEM_PARA: number = 100;

    /**
     * 15, 捐献
     */
    static readonly GUILD_DONATE_MONEY: number = 15;

    /**
     * 1, 捐献铜钱
     */
    static readonly GUILD_DONATE_MONEY_TYPE_TONGQIAN: number = 1;

    /**
     * 2, 捐献元宝
     */
    static readonly GUILD_DONATE_MONEY_TYPE_YUANBAO: number = 2;

    /**
     * 10000, 捐献铜钱10000基数
     */
    static readonly GUILD_DONATE_TONGQIAN_BASE: number = 10000;

    /**
     * 10000000, 捐献铜钱1000W
     */
    static readonly GUILD_DONATE_TONGQIAN_MAX_COUNT: number = 10000000;

    /**
     * 0, 捐献类型  道具
     */
    static readonly GUILD_DONATE_TYPE_ITEM: number = 0;

    /**
     * 1, 捐献类型  元宝
     */
    static readonly GUILD_DONATE_TYPE_YUANBAO: number = 1;

    /**
     * 2000, 捐献元宝2000
     */
    static readonly GUILD_DONATE_YUANBAO_MAX_COUNT: number = 2000;

    /**
     * 10, 宗派捐献 元宝参数
     */
    static readonly GUILD_DONATE_YUANBAO_PARA: number = 10;

    /**
     * 7, 踢出宗派 --- 帮主与副帮主
     */
    static readonly GUILD_DROP_OUT: number = 7;

    /**
     * 9, 领取礼包
     */
    static readonly GUILD_GET_DAY_GIFT: number = 9;

    /**
     * 1, 查看所在宗派摘要
     */
    static readonly GUILD_GET_GUILD_ABSTRACT_INFO: number = 1;

    /**
     * 19, 查看宗派审核列表
     */
    static readonly GUILD_GET_GUILD_CHECK_LIST_INFO: number = 19;

    /**
     * 2, 查看所有宗派列表
     */
    static readonly GUILD_GET_GUILD_LIST: number = 2;

    /**
     * 16, 查看宗派成员列表
     */
    static readonly GUILD_GET_GUILD_MEMBER_LIST_INFO: number = 16;

    /**
     * 11, 领取礼包
     */
    static readonly GUILD_GET_LEVEL_GIFT: number = 11;

    /**
     * 5, 查看其他宗派成员信息
     */
    static readonly GUILD_GET_OTHER_GUILD_INFO: number = 5;

    /**
     * 17, 查看寻宝列表
     */
    static readonly GUILD_GET_XUNBAO_LIST: number = 17;

    /**
     * 2, 宗派日志类型---宗派捐献
     */
    static readonly GUILD_GONGXIAN_LOG_TYPE_DONATE: number = 2;

    /**
     * 3, 宗派日志类型---宗派群英会
     */
    static readonly GUILD_GONGXIAN_LOG_TYPE_GUILD_BATTLE: number = 3;

    /**
     * 4, 宗派日志类型---宗派守卫战
     */
    static readonly GUILD_GONGXIAN_LOG_TYPE_GUILD_SAVE: number = 4;

    /**
     * 0, 宗派日志类型---其他类型
     */
    static readonly GUILD_GONGXIAN_LOG_TYPE_OTHER: number = 0;

    /**
     * 1, 宗派日志类型---宗派任务类型
     */
    static readonly GUILD_GONGXIAN_LOG_TYPE_TASK: number = 1;

    /**
     * 1, 族长
     */
    static readonly GUILD_GRADE_CHAIRMAN: number = 1;

    /**
     * 4, 长老
     */
    static readonly GUILD_GRADE_ELDER: number = 4;

    /**
     * 3, 普通会员
     */
    static readonly GUILD_GRADE_MEMBER: number = 3;

    /**
     * 2, 副族长
     */
    static readonly GUILD_GRADE_VICE_CHAIRMAN: number = 2;

    /**
     * 4, 加入宗派
     */
    static readonly GUILD_JOIN_GUILD: number = 4;

    /**
     * 62, 名将挑战 通知活动变化
     */
    static readonly GUILD_MJTZ_ACT_NOTFY: number = 62;

    /**
     * 61, 名将挑战 创建挑战副本
     */
    static readonly GUILD_MJTZ_CREATE_PIN: number = 61;

    /**
     * 57, 名将挑战 领取礼包
     */
    static readonly GUILD_MJTZ_GET_GIFT: number = 57;

    /**
     * 63, 名将挑战 GM指令操作
     */
    static readonly GUILD_MJTZ_GMCALL: number = 63;

    /**
     * 56, 名将挑战 查看伤害排行
     */
    static readonly GUILD_MJTZ_LIST_RANK: number = 56;

    /**
     * 55, 名将挑战 打开面板
     */
    static readonly GUILD_MJTZ_OPEN: number = 55;

    /**
     * 58, 名将挑战 鼓舞
     */
    static readonly GUILD_MJTZ_RAISE: number = 58;

    /**
     * 59, 名将挑战 更新数据到宗门
     */
    static readonly GUILD_MJTZ_SYN_BOSS_DATA: number = 59;

    /**
     * 60, 名将挑战 更新数据到宗门
     */
    static readonly GUILD_MJTZ_SYN_ROLE_DATA: number = 60;

    /**
     * 10, 10天榜上第一 宗主有额外奖励
     */
    static readonly GUILD_MONEY_RANK_DAY_REWARD: number = 10;

    /**
     * 38, 资金排行领取奖励
     */
    static readonly GUILD_MONEY_RANK_GET: number = 38;

    /**
     * 37, 资金排行拉取列表
     */
    static readonly GUILD_MONEY_RANK_LIST: number = 37;

    /**
     * 1, 宗派申请列表通知
     */
    static readonly GUILD_NOTIFY_APPLY_GUILD_LIST: number = 1;

    /**
     * 29, 召唤BOSS通知
     */
    static readonly GUILD_NOTIFY_CALL_BOSS: number = 29;

    /**
     * 31, 召唤FMTBOSS通知
     */
    static readonly GUILD_NOTIFY_CALL_FMTBOSS: number = 31;

    /**
     * 68, 宗门召唤血战封魔通知
     */
    static readonly GUILD_NOTIFY_CALL_XZFM: number = 68;

    /**
     * 32, 召唤活动通知 暂时用在宗门秘境
     */
    static readonly GUILD_NOTIFY_CALL_ZPFM: number = 32;

    /**
     * 6, 宗派解散通知
     */
    static readonly GUILD_NOTIFY_DISMISS: number = 6;

    /**
     * 8, 宗派解散通知 Svr To Svr
     */
    static readonly GUILD_NOTIFY_DISMISS_SS: number = 8;

    /**
     * 5, 职位改变通知
     */
    static readonly GUILD_NOTIFY_GRADE_CHANGE: number = 5;

    /**
     * 2, 加入宗派通知
     */
    static readonly GUILD_NOTIFY_JOIN_GUILD: number = 2;

    /**
     * 4, 被踢出宗派通知
     */
    static readonly GUILD_NOTIFY_KICK_OUT: number = 4;

    /**
     * 9, 宗派等级通知 Svr To Svr
     */
    static readonly GUILD_NOTIFY_LEVEL: number = 9;

    /**
     * 7, 宗派资产通知
     */
    static readonly GUILD_NOTIFY_MONEY: number = 7;

    /**
     * 12, 新来拍卖商品通知
     */
    static readonly GUILD_NOTIFY_PAIMAI_NEW: number = 12;

    /**
     * 3, 申请已转发给xx处理的通知
     */
    static readonly GUILD_NOTIFY_PROCESS_APPLY: number = 3;

    /**
     * 10, 宗派探矿步骤变更通知
     */
    static readonly GUILD_NOTIFY_TREASURE_HUNT_STEP_CHANGE: number = 10;

    /**
     * 11, 宗派至尊皇城Boss等级通知
     */
    static readonly GUILD_NOTIFY_ZZHC_BOSSLEVEL: number = 11;

    /**
     * 1, 同意
     */
    static readonly GUILD_OPERATION_AGREE: number = 1;

    /**
     * 2, 拒绝
     */
    static readonly GUILD_OPERATION_REFUSE: number = 2;

    /**
     * 49, 宗派拍卖 拍
     */
    static readonly GUILD_PAIMAI_BUY_GUILD: number = 49;

    /**
     * 50, 宗派拍卖 拍
     */
    static readonly GUILD_PAIMAI_BUY_WORLD: number = 50;

    /**
     * 2, 状态 清空期
     */
    static readonly GUILD_PAIMAI_CLEAR_PROSESS: number = 2;

    /**
     * 19, 每天19点清空拍卖信息
     */
    static readonly GUILD_PAIMAI_CLEAR_TIME: number = 19;

    /**
     * 0, 状态 宗派期
     */
    static readonly GUILD_PAIMAI_GUILD_PROSESS: number = 0;

    /**
     * 13, 每天13点宗派拍卖结束
     */
    static readonly GUILD_PAIMAI_GUILD_TIME: number = 13;

    /**
     * 52, 宗派拍卖 忽略
     */
    static readonly GUILD_PAIMAI_INGORE: number = 52;

    /**
     * 0, 拍卖物品状态 Bit0 是否售出
     */
    static readonly GUILD_PAIMAI_ITEM_BIT_0_SELL: number = 0;

    /**
     * 1, 拍卖物品状态 Bit1 是否进入世界拍卖
     */
    static readonly GUILD_PAIMAI_ITEM_BIT_1_WORLD: number = 1;

    /**
     * 2, 拍卖物品状态 Bit2 是否流拍
     */
    static readonly GUILD_PAIMAI_ITEM_BIT_2_FAIL: number = 2;

    /**
     * 3, 拍卖物品状态 Bit3 是否已分红
     */
    static readonly GUILD_PAIMAI_ITEM_BIT_3_FENHONG: number = 3;

    /**
     * 1, 拍卖物品CS状态 已卖出
     */
    static readonly GUILD_PAIMAI_ITEM_CS_STATUS_DONE: number = 1;

    /**
     * 4, 拍卖物品CS状态 已流拍
     */
    static readonly GUILD_PAIMAI_ITEM_CS_STATUS_FAIL: number = 4;

    /**
     * 8, 拍卖物品CS状态 已被他人拍下
     */
    static readonly GUILD_PAIMAI_ITEM_CS_STATUS_OTHERBUY: number = 8;

    /**
     * 7, 拍卖物品CS状态 已被他人竞价超越
     */
    static readonly GUILD_PAIMAI_ITEM_CS_STATUS_OTHERMORE: number = 7;

    /**
     * 9, 拍卖物品CS状态 已被他人一口价拍下
     */
    static readonly GUILD_PAIMAI_ITEM_CS_STATUS_OTHERSUPERBUY: number = 9;

    /**
     * 2, 拍卖物品CS状态 已出价
     */
    static readonly GUILD_PAIMAI_ITEM_CS_STATUS_SELF: number = 2;

    /**
     * 3, 拍卖物品CS状态 已拍到
     */
    static readonly GUILD_PAIMAI_ITEM_CS_STATUS_SELFDONE: number = 3;

    /**
     * 6, 拍卖物品CS状态 已被自身竞价超越
     */
    static readonly GUILD_PAIMAI_ITEM_CS_STATUS_SELFMORE: number = 6;

    /**
     * 10, 拍卖物品CS状态 已被自身一口价拍下
     */
    static readonly GUILD_PAIMAI_ITEM_CS_STATUS_SELFSUPERBUY: number = 10;

    /**
     * 0, 拍卖物品CS状态 拍卖中
     */
    static readonly GUILD_PAIMAI_ITEM_CS_STATUS_SELL: number = 0;

    /**
     * 53, 宗派拍卖 新商品上架通知
     */
    static readonly GUILD_PAIMAI_NEWNTF: number = 53;

    /**
     * 47, 宗派拍卖 面板
     */
    static readonly GUILD_PAIMAI_OPEN_GUILD: number = 47;

    /**
     * 48, 宗派拍卖 面板
     */
    static readonly GUILD_PAIMAI_OPEN_WORLD: number = 48;

    /**
     * 51, 宗派拍卖 我的
     */
    static readonly GUILD_PAIMAI_SELF: number = 51;

    /**
     * 1, 状态 世界期
     */
    static readonly GUILD_PAIMAI_WORLD_PROSESS: number = 1;

    /**
     * 16, 每天16点世界拍卖结束
     */
    static readonly GUILD_PAIMAI_WORLD_TIME: number = 16;

    /**
     * 3, 宗门群英会, 最多能容纳的宗主, 副宗主人数
     */
    static readonly GUILD_PVP_CHAIRMAN_COUNT: number = 3;

    /**
     * 13112, 宗派战的宗主神器模型ID
     */
    static readonly GUILD_PVP_CHAIRMAN_SHENQI_IMGID: number = 13112;

    /**
     * 1004, 宗派战的宗主称号ID
     */
    static readonly GUILD_PVP_CHAIRMAN_TITLE_ID: number = 1004;

    /**
     * 11117, 宗派战的宗主坐骑模型ID
     */
    static readonly GUILD_PVP_CHAIRMAN_ZUOQI_IMGID: number = 11117;

    /**
     * 2, 宗派战-领取奖励
     */
    static readonly GUILD_PVP_DO_REWARD: number = 2;

    /**
     * 200, 宗门实时在线需要处理的最大人数
     */
    static readonly GUILD_PVP_GUILD_BATTLE_COUNT: number = 200;

    /**
     * 1004, 宗派战的副宗主称号ID
     */
    static readonly GUILD_PVP_GUILD_VICE_TITLE_ID: number = 1004;

    /**
     * 11118, 宗派战的副宗主坐骑模型ID
     */
    static readonly GUILD_PVP_GUILD_VICE_ZUOQI_IMGID: number = 11118;

    /**
     * 1, 宗派战-查看面板信息
     */
    static readonly GUILD_PVP_PANEL_INFO: number = 1;

    /**
     * 3, 宗派战-宗门战比赛结果
     */
    static readonly GUILD_PVP_TYPE_BATTLE_RESULT: number = 3;

    /**
     * 4, 宗派战-宗门宗主雕像数据
     */
    static readonly GUILD_PVP_TYPE_GM_SYNC: number = 4;

    /**
     * 2, 宗派战-宗门战面板信息
     */
    static readonly GUILD_PVP_TYPE_PVPPANEL: number = 2;

    /**
     * 1, 宗派战-宗派战现在开启
     */
    static readonly GUILD_PVP_TYPE_START: number = 1;

    /**
     * 6, 退出 --- 不包括帮主
     */
    static readonly GUILD_QUIT_GUILD: number = 6;

    /**
     * 100000, 宗派角色经验，100000转1点宗派经验
     */
    static readonly GUILD_ROLEEXP_CHANGE_BASE: number = 100000;

    /**
     * 0, 宗派个人数据Bit0 已领取资金排名奖励
     */
    static readonly GUILD_ROLE_BIT_0: number = 0;

    /**
     * 1, 宗派个人数据Bit1 预留
     */
    static readonly GUILD_ROLE_BIT_1: number = 1;

    /**
     * 2, 宗派个人数据Bit2 预留
     */
    static readonly GUILD_ROLE_BIT_2: number = 2;

    /**
     * 3, 宗派个人数据Bit3 预留
     */
    static readonly GUILD_ROLE_BIT_3: number = 3;

    /**
     * 4, 宗派个人数据Bit4 预留
     */
    static readonly GUILD_ROLE_BIT_4: number = 4;

    /**
     * 5, 宗派个人数据Bit5 预留
     */
    static readonly GUILD_ROLE_BIT_5: number = 5;

    /**
     * 6, 宗派个人数据Bit6 预留
     */
    static readonly GUILD_ROLE_BIT_6: number = 6;

    /**
     * 7, 宗派个人数据Bit7 预留
     */
    static readonly GUILD_ROLE_BIT_7: number = 7;

    /**
     * 104, 宗派聊天，发送穿云箭（自己的位置信息）
     */
    static readonly GUILD_SEND_ROLE_POSTION: number = 104;

    /**
     * 24, 设置申请自动加入
     */
    static readonly GUILD_SET_AUTO_JOIN: number = 24;

    /**
     * 10, 设置宗派信息 --- 帮主与副帮主
     */
    static readonly GUILD_SET_INFO: number = 10;

    /**
     * 14, 设置宗派成员职位 --- 帮主
     */
    static readonly GUILD_SET_POSITION: number = 14;

    /**
     * 21, 仓库申请
     */
    static readonly GUILD_STORE_APPLY: number = 21;

    /**
     * 22, 仓库分配 宗主
     */
    static readonly GUILD_STORE_ASSIGN: number = 22;

    /**
     * 1, 仓库申请物品批准
     */
    static readonly GUILD_STORE_ASSIGN_ALLOW: number = 1;

    /**
     * 0, 仓库申请物品拒绝
     */
    static readonly GUILD_STORE_ASSIGN_REJECT: number = 0;

    /**
     * 28, 仓库删除
     */
    static readonly GUILD_STORE_DELETE: number = 28;

    /**
     * 35, SS仓库删除
     */
    static readonly GUILD_STORE_DELETE_SS: number = 35;

    /**
     * 25, 仓库存入
     */
    static readonly GUILD_STORE_INTO: number = 25;

    /**
     * 33, SS仓库存入
     */
    static readonly GUILD_STORE_INTO_SS: number = 33;

    /**
     * 23, 仓库拉取
     */
    static readonly GUILD_STORE_LIST: number = 23;

    /**
     * 3, 仓库分配
     */
    static readonly GUILD_STORE_LOG_ASSIGN: number = 3;

    /**
     * 1, 仓库存入
     */
    static readonly GUILD_STORE_LOG_INTO: number = 1;

    /**
     * 27, 仓库日志拉取
     */
    static readonly GUILD_STORE_LOG_LIST: number = 27;

    /**
     * 2, 仓库取出
     */
    static readonly GUILD_STORE_LOG_TAKEOUT: number = 2;

    /**
     * 30, 仓库设置捐献阶数
     */
    static readonly GUILD_STORE_SET_LIMITSTAGE: number = 30;

    /**
     * 36, 仓库排序
     */
    static readonly GUILD_STORE_SORT: number = 36;

    /**
     * 26, 仓库取出
     */
    static readonly GUILD_STORE_TAKEOUT: number = 26;

    /**
     * 34, SS仓库取出
     */
    static readonly GUILD_STORE_TAKEOUT_SS: number = 34;

    /**
     * 3, 事件_Boss
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_BOSS: number = 3;

    /**
     * 2, 事件_捐赠
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_DONATION: number = 2;

    /**
     * 6, 事件_结束
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_END: number = 6;

    /**
     * 5, 事件_移动
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_MOVE: number = 5;

    /**
     * 0, 事件_空
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_NONE: number = 0;

    /**
     * 45, 宗派探矿事件通报
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_NOTIFY: number = 45;

    /**
     * 43, 宗派探矿事件操作
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_OP: number = 43;

    /**
     * 3, 宗派探矿事件操作_打怪
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_OP_BOSSPK: number = 3;

    /**
     * 2, 宗派探矿事件操作_捐献
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_OP_DONATE: number = 2;

    /**
     * 1, 宗派探矿事件操作_接取任务
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_OP_QUESTACCEPT: number = 1;

    /**
     * 4, 宗派探矿事件操作_选择
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_OP_SELECT: number = 4;

    /**
     * 0, 宗派探矿事件操作_开始寻宝
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_OP_START: number = 0;

    /**
     * 4, 事件_选择
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_SELECT: number = 4;

    /**
     * 1, 事件_杀怪
     */
    static readonly GUILD_TREASURE_HUNT_EVENT_TASK: number = 1;

    /**
     * 42, 宗派探矿获取信息
     */
    static readonly GUILD_TREASURE_HUNT_GET_INFO: number = 42;

    /**
     * 44, 宗派探矿领取奖励
     */
    static readonly GUILD_TREASURE_HUNT_GET_REWARD: number = 44;

    /**
     * 46, 宗派探矿GM操作
     */
    static readonly GUILD_TREASURE_HUNT_GM_OP: number = 46;

    /**
     * 0, 宗派探矿普通奖励类型
     */
    static readonly GUILD_TREASURE_HUNT_REWARD_TYPE_NORMAL: number = 0;

    /**
     * 1, 宗派探矿特殊奖励类型
     */
    static readonly GUILD_TREASURE_HUNT_REWARD_TYPE_SPECIAL: number = 1;

    /**
     * 1, 宗派探矿选项1
     */
    static readonly GUILD_TREASURE_HUNT_SELECT_NO1: number = 1;

    /**
     * 2, 宗派探矿选项2
     */
    static readonly GUILD_TREASURE_HUNT_SELECT_NO2: number = 2;

    /**
     * 0, 宗派探矿选项未选
     */
    static readonly GUILD_TREASURE_HUNT_SELECT_NONE: number = 0;

    /**
     * 1, 展示类型_事件完成
     */
    static readonly GUILD_TREASURE_HUNT_SHOWTYPE_EVENTCOMPLETE: number = 1;

    /**
     * 0, 展示类型_事件开启
     */
    static readonly GUILD_TREASURE_HUNT_SHOWTYPE_EVENTSTART: number = 0;

    /**
     * 2, 展示类型_贡献增加
     */
    static readonly GUILD_TREASURE_HUNT_SHOWTYPE_GOTCONTRIBUTION: number = 2;

    /**
     * 1, 角色贡献增加，有可能减少
     */
    static readonly GUILD_VALUE_TYPE_ADDGONGXIAN: number = 1;

    /**
     * 2, 设置宗派资产增加
     */
    static readonly GUILD_VALUE_TYPE_ADDMONEY: number = 2;

    /**
     * 3, 设置宗派等级
     */
    static readonly GUILD_VALUE_TYPE_LEVEL: number = 3;

    /**
     * 4, OSS宗派解散
     */
    static readonly GUILD_VALUE_TYPE_OSS_DISMISS: number = 4;

    /**
     * 5, OSS新宗主
     */
    static readonly GUILD_VALUE_TYPE_OSS_NEWLEADER: number = 5;

    /**
     * 0, 宗派数据Bit0 宗主是否已领取资金排名奖励
     */
    static readonly GUILD_WORLD_BIT_0: number = 0;

    /**
     * 1, 宗派数据Bit1 宗主是否已领取连续10天第一奖励
     */
    static readonly GUILD_WORLD_BIT_1: number = 1;

    /**
     * 2, 宗派数据Bit2 宗主是否OSS设置禅让了
     */
    static readonly GUILD_WORLD_BIT_2: number = 2;

    /**
     * 3, 宗派数据Bit3 预留
     */
    static readonly GUILD_WORLD_BIT_3: number = 3;

    /**
     * 4, 宗派数据Bit4 预留
     */
    static readonly GUILD_WORLD_BIT_4: number = 4;

    /**
     * 5, 宗派数据Bit5 预留
     */
    static readonly GUILD_WORLD_BIT_5: number = 5;

    /**
     * 6, 宗派数据Bit6 预留
     */
    static readonly GUILD_WORLD_BIT_6: number = 6;

    /**
     * 7, 宗派数据Bit7 预留
     */
    static readonly GUILD_WORLD_BIT_7: number = 7;

    /**
     * 101, 宗派寻宝 充值条件达成
     */
    static readonly GUILD_XUNBAO_ARIVE: number = 101;

    /**
     * 60000301, 宗派寻宝，第一档次掉落
     */
    static readonly GUILD_XUNBAO_DROP_1: number = 60000301;

    /**
     * 60000302, 宗派寻宝，第二档次掉落
     */
    static readonly GUILD_XUNBAO_DROP_2: number = 60000302;

    /**
     * 60000303, 宗派寻宝，第三档次掉落
     */
    static readonly GUILD_XUNBAO_DROP_3: number = 60000303;

    /**
     * 13, 寻宝协助
     */
    static readonly GUILD_XUNBAO_HELP: number = 13;

    /**
     * 1000, 宗派寻宝，充值1000
     */
    static readonly GUILD_XUNBAO_LEVEL_1: number = 1000;

    /**
     * 2000, 宗派寻宝，充值2000
     */
    static readonly GUILD_XUNBAO_LEVEL_2: number = 2000;

    /**
     * 5000, 宗派寻宝，充值5000
     */
    static readonly GUILD_XUNBAO_LEVEL_3: number = 5000;

    /**
     * 3, 宗派寻宝，3个档次
     */
    static readonly GUILD_XUNBAO_LEVEL_COUNT: number = 3;

    /**
     * 2, 宗派寻宝，每种档次可以协助次数
     */
    static readonly GUILD_XUNBAO_LEVEL_HELP_COUNT: number = 2;

    /**
     * 5, 宗派寻宝，需要5次协助才能领奖
     */
    static readonly GUILD_XUNBAO_NEED_HELP_COUNT: number = 5;

    /**
     * 50, 宗派寻宝，最大记录数
     */
    static readonly GUILD_XUNBAO_RECORD_COUNT: number = 50;

    /**
     * 40, 至尊皇城 Boss喂养
     */
    static readonly GUILD_ZZHC_BOSS_FEED: number = 40;

    /**
     * 41, 至尊皇城 领取奖励
     */
    static readonly GUILD_ZZHC_GIFT_GET: number = 41;

    /**
     * 39, 至尊皇城 宗派页签面板打开
     */
    static readonly GUILD_ZZHC_PANNLE_LIST: number = 39;

    /**
     * 5, 国运活动，候选任务数
     */
    static readonly GUOYUN_CANDIDATE_QUEST_NUM: number = 5;

    /**
     * 1, 国运活动，护盾操作
     */
    static readonly GUOYUN_OPERATE_DEF_TYPE: number = 1;

    /**
     * 2, 国运活动，求救操作
     */
    static readonly GUOYUN_OPERATE_HELP_TYPE: number = 2;

    /**
     * 255, 国运活动，一键刷新到最高等级
     */
    static readonly GUOYUN_REFRESH_HIGHTEST_LEVEL_TYPE: number = 255;

    /**
     * 2, 国运活动，10元宝刷新
     */
    static readonly GUOYUN_REFRESH_MONEY_TYPE: number = 2;

    /**
     * 1, 国运活动，刷新石刷新
     */
    static readonly GUOYUN_REFRESH_STONE_TYPE: number = 1;

    /**
     * 26, 充值狂欢折扣充值
     */
    static readonly HAPPY_CHARGE_REBATE: number = 26;

    /**
     * 1, 黑暗侵袭使用落雷
     */
    static readonly HAQX_RAYL: number = 1;

    /**
     * 2, 黑暗侵袭使用激励
     */
    static readonly HAQX_URGE: number = 2;

    /**
     * 5, 宝箱有礼 限制保护掉落个数
     */
    static readonly HEFU_BXYL_LIMIT_DROP_COUNT: number = 5;

    /**
     * 31, 黑暗侵袭查询操作
     */
    static readonly HEIANQINXI_LIST: number = 31;

    /**
     * 30, 黑暗侵袭活动操作
     */
    static readonly HEIANQINXI_OPERATE: number = 30;

    /**
     * 1, 天珠镶嵌
     */
    static readonly HEROSUBSPEC_TZ_MOUNT: number = 1;

    /**
     * 3, 祝福系统形象
     */
    static readonly HEROSUB_IMAGE: number = 3;

    /**
     * 2, 祝福值补满
     */
    static readonly HEROSUB_LUCKY_FILL: number = 2;

    /**
     * 1, 祝福值保留
     */
    static readonly HEROSUB_LUCKY_KEEP: number = 1;

    /**
     * 11, 子系统_当前个数 每添加一个系统记得修改！
     */
    static readonly HERO_SUB_TYPE_CUR_COUNT: number = 11;

    /**
     * 6, 祝福_阵法
     */
    static readonly HERO_SUB_TYPE_FAZHEN: number = 6;

    /**
     * 8, 祝福_火精
     */
    static readonly HERO_SUB_TYPE_HUOJING: number = 8;

    /**
     * 5, 祝福_战灵
     */
    static readonly HERO_SUB_TYPE_JINGLING: number = 5;

    /**
     * 7, 祝福_足迹
     */
    static readonly HERO_SUB_TYPE_LEILING: number = 7;

    /**
     * 15, 子系统_最大个数
     */
    static readonly HERO_SUB_TYPE_MAX_COUNT: number = 15;

    /**
     * 1, 祝福_美人
     */
    static readonly HERO_SUB_TYPE_MEIREN: number = 1;

    /**
     * 0, 占位用
     */
    static readonly HERO_SUB_TYPE_RESERVER: number = 0;

    /**
     * 10, 祝福_圣灵
     */
    static readonly HERO_SUB_TYPE_SHENGLING: number = 10;

    /**
     * 11, 祝福_天珠
     */
    static readonly HERO_SUB_TYPE_TIANZHU: number = 11;

    /**
     * 3, 祝福_神器
     */
    static readonly HERO_SUB_TYPE_WUHUN: number = 3;

    /**
     * 4, 祝福_灵翼
     */
    static readonly HERO_SUB_TYPE_YUYI: number = 4;

    /**
     * 9, 祝福_战灵
     */
    static readonly HERO_SUB_TYPE_ZHANLING: number = 9;

    /**
     * 2, 祝福_契约兽
     */
    static readonly HERO_SUB_TYPE_ZUOQI: number = 2;

    /**
     * 13, 合服活动 宝箱有礼购买宝箱
     */
    static readonly HFHD_BXYL_BUY_CHEST: number = 13;

    /**
     * 12, 合服活动 宝箱有礼打开面板
     */
    static readonly HFHD_BXYL_OPEN_PANNEL: number = 12;

    /**
     * 14, 合服活动 宝箱有礼商店兑换
     */
    static readonly HFHD_BXYL_SHOP_EXCHANGE: number = 14;

    /**
     * 2, 合服活动_领取累计充值奖励
     */
    static readonly HFHD_GET_LJCZ_REWARD: number = 2;

    /**
     * 8, 合服活动_领取累计消费奖励
     */
    static readonly HFHD_GET_LJXF_REWARD: number = 8;

    /**
     * 6, 合服活动_领取七天登陆奖励
     */
    static readonly HFHD_GET_QTDL_REWARD: number = 6;

    /**
     * 9, 合服活动_至尊夺宝打开面板
     */
    static readonly HFHD_HFZZZD_OPEN_PANEL: number = 9;

    /**
     * 12, 合服活动累计充值档次最大7档
     */
    static readonly HFHD_LJCZ_MAX_LEVEL_COUNT: number = 12;

    /**
     * 1, 合服活动_打开累计充值奖励
     */
    static readonly HFHD_LJCZ_OPEN_PANEL: number = 1;

    /**
     * 12, 合服活动累计消费档次最大7档
     */
    static readonly HFHD_LJXF_MAX_LEVEL_COUNT: number = 12;

    /**
     * 7, 合服活动_打开累计消费奖励
     */
    static readonly HFHD_LJXF_OPEN_PANEL: number = 7;

    /**
     * 5, 合服活动_打开七天登陆面板
     */
    static readonly HFHD_QTDL_OPEN_PANEL: number = 5;

    /**
     * 11, 合服活动 招财猫领取奖励
     */
    static readonly HFHD_ZCM_GET_REWARD: number = 11;

    /**
     * 10, 合服活动 招财猫打开面板
     */
    static readonly HFHD_ZCM_OPEN_PANNEL: number = 10;

    /**
     * 1, 隐藏全部
     */
    static readonly HIDE_TYPE_ALL: number = 1;

    /**
     * 4, 隐藏敌方玩家
     */
    static readonly HIDE_TYPE_ENEMY: number = 4;

    /**
     * 2, 隐藏己方玩家
     */
    static readonly HIDE_TYPE_FRIEND: number = 2;

    /**
     * 32, 隐藏普通怪物
     */
    static readonly HIDE_TYPE_MONSTER: number = 32;

    /**
     * 64, 隐藏玩家随从
     */
    static readonly HIDE_TYPE_PET: number = 64;

    /**
     * 16, 隐藏场景特效
     */
    static readonly HIDE_TYPE_SENCE: number = 16;

    /**
     * 8, 隐藏技能特效
     */
    static readonly HIDE_TYPE_SKILL: number = 8;

    /**
     * 128, 隐藏玩家称号
     */
    static readonly HIDE_TYPE_TITLE: number = 128;

    /**
     * 256, 隐藏玩家翅膀
     */
    static readonly HIDE_TYPE_WING: number = 256;

    /**
     * 204, 铭文试炼 手动解锁下一层
     */
    static readonly HISTORICAL_ACT_NEXT_STAGE: number = 204;

    /**
     * 203, 铭文试炼 领取层通关奖励
     */
    static readonly HISTORICAL_GET_STAGEPASS_GIFT: number = 203;

    /**
     * 105, 铭文试炼 购买魔力色子次数
     */
    static readonly HISTORICAL_REMAINS_BUY_MAGIC_DICE_TIMES: number = 105;

    /**
     * 1, 铭文试炼 普通色子
     */
    static readonly HISTORICAL_REMAINS_DICE_TYPE_1: number = 1;

    /**
     * 2, 铭文试炼 魔力色子
     */
    static readonly HISTORICAL_REMAINS_DICE_TYPE_2: number = 2;

    /**
     * 104, 铭文试炼 走到指定位置
     */
    static readonly HISTORICAL_REMAINS_GOTO_POS: number = 104;

    /**
     * 20, 铭文试炼小BOSS副本 一键完成价格
     */
    static readonly HISTORICAL_REMAINS_MINI_BOSS_COST: number = 20;

    /**
     * 5, 铭文试炼小怪副本 一键完成价格
     */
    static readonly HISTORICAL_REMAINS_MINI_MONSTER_COST: number = 5;

    /**
     * 107, 铭文试炼 一键完成副本
     */
    static readonly HISTORICAL_REMAINS_ONE_KEY_FINISH: number = 107;

    /**
     * 50, 铭文试炼一键完成价格
     */
    static readonly HISTORICAL_REMAINS_ONE_KEY_FINISH_COST: number = 50;

    /**
     * 1, 铭文试炼 玩家操作
     */
    static readonly HISTORICAL_REMAINS_OPERATION_TYPE_1: number = 1;

    /**
     * 2, 铭文试炼 玩家操作
     */
    static readonly HISTORICAL_REMAINS_OPERATION_TYPE_2: number = 2;

    /**
     * 3, 铭文试炼 玩家操作
     */
    static readonly HISTORICAL_REMAINS_OPERATION_TYPE_3: number = 3;

    /**
     * 4, 铭文试炼 玩家操作
     */
    static readonly HISTORICAL_REMAINS_OPERATION_TYPE_4: number = 4;

    /**
     * 102, 铭文试炼  面板
     */
    static readonly HISTORICAL_REMAINS_PANEL: number = 102;

    /**
     * 5, 铭文试炼增援副本 一键完成价格
     */
    static readonly HISTORICAL_REMAINS_REINFORCE_PINSTANCE_COST: number = 5;

    /**
     * 106, 铭文试炼 重置当天活动次数
     */
    static readonly HISTORICAL_REMAINS_RESET_TIMES: number = 106;

    /**
     * 103, 铭文试炼 投色子
     */
    static readonly HISTORICAL_REMAINS_THROW_DICE: number = 103;

    /**
     * 1, 欢乐翻牌花钱领最好的礼包
     */
    static readonly HLFP_GETGIFT_BEST: number = 1;

    /**
     * 0, 欢乐翻牌领普通礼包
     */
    static readonly HLFP_GETGIFT_NORMAL: number = 0;

    /**
     * 26, 欢乐探宝 抽奖
     */
    static readonly HLTB_DRAW: number = 26;

    /**
     * 400, 欢乐探宝  仙境
     */
    static readonly HLTB_FEISHENG_COST: number = 400;

    /**
     * 900000, 欢乐探宝  仙境
     */
    static readonly HLTB_FUDI_10_COST: number = 900000;

    /**
     * 100000, 欢乐探宝  福地
     */
    static readonly HLTB_FUDI_1_COST: number = 100000;

    /**
     * 3, 欢乐探宝 档次个数
     */
    static readonly HLTB_GRADE_COUNT: number = 3;

    /**
     * 25, 欢乐探宝 打开面板
     */
    static readonly HLTB_PANNEL: number = 25;

    /**
     * 3, 欢乐探宝  飞升
     */
    static readonly HLTB_TYPE_FEISHENG: number = 3;

    /**
     * 1, 欢乐探宝  福地
     */
    static readonly HLTB_TYPE_FUDI: number = 1;

    /**
     * 2, 欢乐探宝  仙境
     */
    static readonly HLTB_TYPE_XIANJING: number = 2;

    /**
     * 2600, 欢乐探宝  福地
     */
    static readonly HLTB_XIANJ_10_COST: number = 2600;

    /**
     * 288, 欢乐探宝  飞升
     */
    static readonly HLTB_XIANJ_1_COST: number = 288;

    /**
     * 156, 欢乐转盘 转动机器
     */
    static readonly HLZP_DRAW: number = 156;

    /**
     * 155, 欢乐转盘 打开面板
     */
    static readonly HLZP_PANNEL: number = 155;

    /**
     * 157, 欢乐转盘 大奖记录
     */
    static readonly HLZP_RECORD: number = 157;

    /**
     * 5, BOSS之家 副本层数
     */
    static readonly HOME_BOSS_MAX_NUM_FLOOR: number = 5;

    /**
     * 1000, 坐骑喂养经验是1000
     */
    static readonly HORSE_FEED_BASE_EXP: number = 1000;

    /**
     * 2, 是否领取过百服登陆的奖励
     */
    static readonly HUNDRED_SVR_LOGIN: number = 2;

    /**
     * 2, 魂骨新合成 
     */
    static readonly HUNGU_OP_CREATE: number = 2;

    /**
     * 1, 魂骨 升华
     */
    static readonly HUNGU_OP_UPCOLOR: number = 1;

    /**
     * 110001, 魂环初始ID
     */
    static readonly HUNHUAN_FIRST_ID: number = 110001;

    /**
     * 2, 魂力完成通知
     */
    static readonly HUNLI_FINISH_NOTIFY: number = 2;

    /**
     * 7, 魂环激活
     */
    static readonly HUNLI_HUNHUAN_ACTIVE: number = 7;

    /**
     * 8, 魂环升级
     */
    static readonly HUNLI_HUNHUAN_LEVEL_UP: number = 8;

    /**
     * 6, 魂环注入
     */
    static readonly HUNLI_HUNHUAN_ZHURU: number = 6;

    /**
     * 1, 魂力进阶
     */
    static readonly HUNLI_LEVEL_UP: number = 1;

    /**
     * 3, 魂力奖励
     */
    static readonly HUNLI_REWARD: number = 3;

    /**
     * 3, 活跃度领取每日累计礼包
     */
    static readonly HYD_OPERATE_GET_DAY_GIFT: number = 3;

    /**
     * 2, 活跃度领取礼包
     */
    static readonly HYD_OPERATE_GET_GIFT: number = 2;

    /**
     * 1, 活跃度查询协议
     */
    static readonly HYD_OPERATE_LIST: number = 1;

    /**
     * 1, 申请结婚
     */
    static readonly HY_APPLY_MARRY: number = 1;

    /**
     * 4, 审批协议离婚请求
     */
    static readonly HY_DEAL_DIVORCE: number = 4;

    /**
     * 2, 审批结婚请求
     */
    static readonly HY_DEAL_MARRY: number = 2;

    /**
     * 5, 申请强制离婚
     */
    static readonly HY_DIVORCE_FORCE: number = 5;

    /**
     * 6, 申请失踪离婚
     */
    static readonly HY_DIVORCE_MISSING: number = 6;

    /**
     * 3, 申请协议离婚
     */
    static readonly HY_DIVORCE_PACT: number = 3;

    /**
     * 7, 送花
     */
    static readonly HY_GIVEFLOWER: number = 7;

    /**
     * 9, 查询仙缘相关数据
     */
    static readonly HY_LIST_XIANYUAN: number = 9;

    /**
     * 11, 婚姻求缘面板
     */
    static readonly HY_QIUYUAN_PANEL: number = 11;

    /**
     * 12, 婚姻求缘登记
     */
    static readonly HY_QIUYUAN_REGIST: number = 12;

    /**
     * 8, 婚姻系统状态通知
     */
    static readonly HY_STATUS_NOTIFY: number = 8;

    /**
     * 10, 仙缘升级
     */
    static readonly HY_XIANYUAN_UPLEVEL: number = 10;

    /**
     * 4, 首冲返利(返还元宝)
     */
    static readonly ICON_CHARGE_REBATE: number = 4;

    /**
     * 0, 返利大厅
     */
    static readonly ICON_FLDT: number = 0;

    /**
     * 3, 合服活动
     */
    static readonly ICON_MERGE_ACTIVITY: number = 3;

    /**
     * 1, 副本大厅
     */
    static readonly ICON_PINSTANCE_HOME: number = 1;

    /**
     * 5, 祈福
     */
    static readonly ICON_QIFU: number = 5;

    /**
     * 1, 免费祈福 - 经验
     */
    static readonly ICON_QIFU_FREE_EXP: number = 1;

    /**
     * 2, 免费祈福 - 银两
     */
    static readonly ICON_QIFU_FREE_SLIVER: number = 2;

    /**
     * 2, 开服活动
     */
    static readonly ICON_START_ACTIVITY: number = 2;

    /**
     * 5, 时装形象通知
     */
    static readonly IMAGE_NOTIFY_TYPE_DRESS: number = 5;

    /**
     * 2, 坐骑新形象通知
     */
    static readonly IMAGE_NOTIFY_TYPE_MOUNT: number = 2;

    /**
     * 3, 新散仙通知
     */
    static readonly IMAGE_NOTIFY_TYPE_PET: number = 3;

    /**
     * 1, 翅膀新形象通知
     */
    static readonly IMAGE_NOTIFY_TYPE_WING: number = 1;

    /**
     * 2, 化形象类型  时装系统
     */
    static readonly IMAGE_TRAIN_DRESS: number = 2;

    /**
     * 1, 幻化形象类型 祝福系统
     */
    static readonly IMAGE_TRAIN_HEROSUB: number = 1;

    /**
     * 3, 化形象类型  称号系统
     */
    static readonly IMAGE_TRAIN_TITLE: number = 3;

    /**
     * 3, 铭文试炼 魔力色子初始次数
     */
    static readonly INIT_MAGIC_DICE_TIMES: number = 3;

    /**
     * 6, 铭文试炼 普通色子初始次数
     */
    static readonly INIT_NORMAL_DICE_TIMES: number = 6;

    /**
     * 0, 电信
     */
    static readonly ISP_TELECOM: number = 0;

    /**
     * 1, 网通
     */
    static readonly ISP_UNICOM: number = 1;

    /**
     * 2, 当前在副本里
     */
    static readonly IS_IN_PINISTANCE: number = 2;

    /**
     * 1, 当前在场景里
     */
    static readonly IS_IN_SCENE: number = 1;

    /**
     * 30
     */
    static readonly ITEMTRANSPORT_TIME: number = 30;

    /**
     * 3
     */
    static readonly ITEMTRANSPORT_TYPE_FLIGHT_NPC: number = 3;

    /**
     * 4
     */
    static readonly ITEMTRANSPORT_TYPE_FLIGHT_POSITION: number = 4;

    /**
     * 2
     */
    static readonly ITEMTRANSPORT_TYPE_TRANSPORTED: number = 2;

    /**
     * 2, 宝石塔buff类型_杀
     */
    static readonly JEWEL_TOWER_BUFF_TYPE_KILL: number = 2;

    /**
     * 1, 宝石塔buff类型_桃
     */
    static readonly JEWEL_TOWER_BUFF_TYPE_PEACH: number = 1;

    /**
     * 2, 购买宝石塔buff
     */
    static readonly JEWEL_TOWER_PIN_BUY_BUFF: number = 2;

    /**
     * 3, 获取buff次数
     */
    static readonly JEWEL_TOWER_PIN_GET_BUFF_COUNT: number = 3;

    /**
     * 1, 使用宝石塔buff
     */
    static readonly JEWEL_TOWER_PIN_USE_BUFF: number = 1;

    /**
     * 4, 道宫九星-祝福值补满
     */
    static readonly JIUXING_LUCKY_FILL: number = 4;

    /**
     * 3, 道宫九星-祝福值保留
     */
    static readonly JIUXING_LUCKY_KEEP: number = 3;

    /**
     * 1, 打开道宫九星面板
     */
    static readonly JIUXING_OPEN_PANEL: number = 1;

    /**
     * 2, 道宫九星-升级
     */
    static readonly JIUXING_UPGRADE_LEVEL: number = 2;

    /**
     * 15, 最多奖励数量
     */
    static readonly JJCREWARD_MAX_CFG_SIZE: number = 15;

    /**
     * 10, 个人竞技场第一名膜拜礼包
     */
    static readonly JJC_MOBAI_1: number = 10;

    /**
     * 11, 个人竞技场第二名膜拜礼包
     */
    static readonly JJC_MOBAI_2: number = 11;

    /**
     * 12, 个人竞技场第三名膜拜礼包
     */
    static readonly JJC_MOBAI_3: number = 12;

    /**
     * 99, 进阶日领取奖励
     */
    static readonly JJR_GET_REWARD: number = 99;

    /**
     * 98, 进阶日打开面板
     */
    static readonly JJR_OPEN_PANNEL: number = 98;

    /**
     * 5, 聚划算购买
     */
    static readonly JUHS_BUY: number = 5;

    /**
     * 6, 聚划算领取
     */
    static readonly JUHS_GET: number = 6;

    /**
     * 1, 聚划算(投资计划)获取类型-常规奖励
     */
    static readonly JUHS_GET_TYPE_NORMAL: number = 1;

    /**
     * 2, 聚划算(投资计划)获取类型-在线奖励
     */
    static readonly JUHS_GET_TYPE_ONLINE: number = 2;

    /**
     * 3, 聚划算(投资计划)获取类型-VIP奖励
     */
    static readonly JUHS_GET_TYPE_VIP: number = 3;

    /**
     * 3, 聚划算3天不买就不给买了，前台图标去掉啊
     */
    static readonly JUHS_LIMIT_BUY_DAY: number = 3;

    /**
     * 10, 聚划算最多有10个 现有7个，预留3个
     */
    static readonly JUHS_MAX_CFG_SIZE: number = 10;

    /**
     * 60010088, 聚划算(投资计划)在线时长 第一个掉落礼包ID，后面的要按顺序配7个啊
     */
    static readonly JUHS_ONLINE_BASE_GIFT_ID: number = 60010088;

    /**
     * 7200, 聚划算(投资计划)在线时长
     */
    static readonly JUHS_ONLINE_TIME: number = 7200;

    /**
     * 19, 聚划算打开面板
     */
    static readonly JUHS_OPENPANEL: number = 19;

    /**
     * 60010095, 聚划算(投资计划)VIP等级 第一个掉落礼包ID，后面的要按顺序配7个啊
     */
    static readonly JUHS_VIP_BASE_GIFT_ID: number = 60010095;

    /**
     * 4, 聚划算VIP等级限制等级
     */
    static readonly JUHS_VIP_LEVEL: number = 4;

    /**
     * 12, 聚元系统的类型:创世神
     */
    static readonly JUYUAN_TYPE_CHUANGSHISHEN: number = 12;

    /**
     * 1, 聚元系统的类型:后天期
     */
    static readonly JUYUAN_TYPE_HOUTIAN: number = 1;

    /**
     * 9, 聚元系统的类型:界王
     */
    static readonly JUYUAN_TYPE_JIEWANG: number = 9;

    /**
     * 4, 聚元系统的类型:命陨期
     */
    static readonly JUYUAN_TYPE_MINGSUN: number = 4;

    /**
     * 6, 聚元系统的类型:神变期
     */
    static readonly JUYUAN_TYPE_SHENBIAN: number = 6;

    /**
     * 8, 聚元系统的类型:圣主
     */
    static readonly JUYUAN_TYPE_SHENGZHU: number = 8;

    /**
     * 5, 聚元系统的类型:神海期
     */
    static readonly JUYUAN_TYPE_SHENHAI: number = 5;

    /**
     * 7, 聚元系统的类型:神君期
     */
    static readonly JUYUAN_TYPE_SHENJUN: number = 7;

    /**
     * 10, 聚元系统的类型:天尊
     */
    static readonly JUYUAN_TYPE_TIANZUN: number = 10;

    /**
     * 2, 聚元系统的类型:先天期
     */
    static readonly JUYUAN_TYPE_XIANTIAN: number = 2;

    /**
     * 24, 聚元系统的类型:修罗天神
     */
    static readonly JUYUAN_TYPE_XIULUOTIANSHEN: number = 24;

    /**
     * 14, 聚元系统的类型:修神一转
     */
    static readonly JUYUAN_TYPE_XIUSHEN_1: number = 14;

    /**
     * 23, 聚元系统的类型:修神十转
     */
    static readonly JUYUAN_TYPE_XIUSHEN_10: number = 23;

    /**
     * 15, 聚元系统的类型:修神二转
     */
    static readonly JUYUAN_TYPE_XIUSHEN_2: number = 15;

    /**
     * 16, 聚元系统的类型:修神三转
     */
    static readonly JUYUAN_TYPE_XIUSHEN_3: number = 16;

    /**
     * 17, 聚元系统的类型:修神四转
     */
    static readonly JUYUAN_TYPE_XIUSHEN_4: number = 17;

    /**
     * 18, 聚元系统的类型:修神五转
     */
    static readonly JUYUAN_TYPE_XIUSHEN_5: number = 18;

    /**
     * 19, 聚元系统的类型:修神六转
     */
    static readonly JUYUAN_TYPE_XIUSHEN_6: number = 19;

    /**
     * 20, 聚元系统的类型:修神七转
     */
    static readonly JUYUAN_TYPE_XIUSHEN_7: number = 20;

    /**
     * 21, 聚元系统的类型:修神八转
     */
    static readonly JUYUAN_TYPE_XIUSHEN_8: number = 21;

    /**
     * 22, 聚元系统的类型:修神九转
     */
    static readonly JUYUAN_TYPE_XIUSHEN_9: number = 22;

    /**
     * 3, 聚元系统的类型:旋丹期
     */
    static readonly JUYUAN_TYPE_XUANDAN: number = 3;

    /**
     * 13, 聚元系统的类型:造物主
     */
    static readonly JUYUAN_TYPE_ZAOWUZHU: number = 13;

    /**
     * 11, 聚元系统的类型:真神
     */
    static readonly JUYUAN_TYPE_ZHENSHEN: number = 11;

    /**
     * 5, 极限挑战称号变化
     */
    static readonly JXTZ_TITLE_CHANGE: number = 5;

    /**
     * 147, 九转星宫活动-购买奖励倍励
     */
    static readonly JZXG_ACT_BUY: number = 147;

    /**
     * 146, 九转星宫活动-打开面板
     */
    static readonly JZXG_ACT_OPEN: number = 146;

    /**
     * 17, 7天累计充值 领取奖励
     */
    static readonly KF7DAY_LJCZ_GET: number = 17;

    /**
     * 16, 7天累计充值 打开面板
     */
    static readonly KF7DAY_LJCZ_OPEN: number = 16;

    /**
     * 300, 开服BOSS召唤冷却时间
     */
    static readonly KFBOSS_SUMMON_COOLING_TIME: number = 300;

    /**
     * 2, 开服BOSS召唤类型 -- 精英
     */
    static readonly KFBOSS_SUMMON_TYPE_ELITE: number = 2;

    /**
     * 1, 开服BOSS召唤类型 -- 普通
     */
    static readonly KFBOSS_SUMMON_TYPE_GENERAL: number = 1;

    /**
     * 3, 开服BOSS召唤类型 -- 传奇
     */
    static readonly KFBOSS_SUMMON_TYPE_LEGEND: number = 3;

    /**
     * 5, 预赛 每天免费PK次数
     */
    static readonly KFJDC_BASE_DAY_PK_CNT: number = 5;

    /**
     * 0, 投注FlagBit 是否投左边
     */
    static readonly KFJDC_FINAL_BEG_FLAG_0_LEFT: number = 0;

    /**
     * 1, 投注FlagBit 是否已领取
     */
    static readonly KFJDC_FINAL_BEG_FLAG_1_GET: number = 1;

    /**
     * 3, 投注结果 平局
     */
    static readonly KFJDC_FINAL_BEG_STATUS_DREW: number = 3;

    /**
     * 2, 投注结果 输
     */
    static readonly KFJDC_FINAL_BEG_STATUS_FAIL: number = 2;

    /**
     * 0, 投注结果 等待结果
     */
    static readonly KFJDC_FINAL_BEG_STATUS_NONE: number = 0;

    /**
     * 1, 投注结果 赢
     */
    static readonly KFJDC_FINAL_BEG_STATUS_WIN: number = 1;

    /**
     * 2, 失败
     */
    static readonly KFJDC_FINAL_PLAYER_LOST: number = 2;

    /**
     * 0, 无结果
     */
    static readonly KFJDC_FINAL_PLAYER_NONE: number = 0;

    /**
     * 3, 弃权
     */
    static readonly KFJDC_FINAL_PLAYER_WAIVER: number = 3;

    /**
     * 1, 胜利
     */
    static readonly KFJDC_FINAL_PLAYER_WIN: number = 1;

    /**
     * 1, 跨服决斗场决赛版本
     */
    static readonly KFJDC_FINAL_VERSION: number = 1;

    /**
     * 0, 跨服决斗场Bit0 每日段位奖励
     */
    static readonly KFJDC_ROLE_BIT_0_GRADE_REWARD: number = 0;

    /**
     * 1, 跨服决斗场Bit1 预留
     */
    static readonly KFJDC_ROLE_BIT_1: number = 1;

    /**
     * 2, 跨服决斗场Bit1 预留
     */
    static readonly KFJDC_ROLE_BIT_2: number = 2;

    /**
     * 3, 跨服决斗场Bit1 预留
     */
    static readonly KFJDC_ROLE_BIT_3: number = 3;

    /**
     * 4, 跨服决斗场Bit1 预留
     */
    static readonly KFJDC_ROLE_BIT_4: number = 4;

    /**
     * 5, 跨服决斗场Bit1 预留
     */
    static readonly KFJDC_ROLE_BIT_5: number = 5;

    /**
     * 6, 跨服决斗场Bit1 预留
     */
    static readonly KFJDC_ROLE_BIT_6: number = 6;

    /**
     * 7, 跨服决斗场Bit1 预留
     */
    static readonly KFJDC_ROLE_BIT_7: number = 7;

    /**
     * 10, 开服全民冲榜操作-清空排行数据
     */
    static readonly KFQMCB_OPERATE_CLEAR_RNAKINFO: number = 10;

    /**
     * 2, 开服全民冲榜操作-获取排行
     */
    static readonly KFQMCB_OPERATE_GET_RANK: number = 2;

    /**
     * 3, 开服全民冲榜操作-获取奖励
     */
    static readonly KFQMCB_OPERATE_GET_REWARD: number = 3;

    /**
     * 1, 开服全民冲榜操作-获取状态
     */
    static readonly KFQMCB_OPERATE_GET_STATE: number = 1;

    /**
     * 5, 开服全民冲榜操作-达成奖励通知
     */
    static readonly KFQMCB_OPERATE_REWARD_NOTIFY: number = 5;

    /**
     * 4, 开服全民冲榜操作-发放总排行奖励
     */
    static readonly KFQMCB_OPERATE_SEND_REWARD: number = 4;

    /**
     * 15, 开服消费返利 领取奖励
     */
    static readonly KFXFFL_GET: number = 15;

    /**
     * 14, 开服消费返利 打开面板
     */
    static readonly KFXFFL_OPEN: number = 14;

    /**
     * 21, 开服消费礼包 领取奖励
     */
    static readonly KFXFLB_GET: number = 21;

    /**
     * 20, 开服消费礼包 打开面板
     */
    static readonly KFXFLB_OPEN: number = 20;

    /**
     * 31180001, 跨服宗派战 柱子怪物ID
     */
    static readonly KFZPZ_MONSTER_ID_1: number = 31180001;

    /**
     * 31180002, 跨服宗派战 柱子怪物ID
     */
    static readonly KFZPZ_MONSTER_ID_2: number = 31180002;

    /**
     * 31180003, 跨服宗派战 柱子怪物ID
     */
    static readonly KFZPZ_MONSTER_ID_3: number = 31180003;

    /**
     * 5, 打开开服至尊争夺面板
     */
    static readonly KFZZZD_OPEN_PANEL: number = 5;

    /**
     * 16, 7天累计充值最多16档次
     */
    static readonly KF_7DAY_LJCZ_CFG_COUNT: number = 16;

    /**
     * 6, 7天累计充值每档道具个数
     */
    static readonly KF_7DAY_LJCZ_ITEM_COUNT: number = 6;

    /**
     * 1, 开服活动状态 达成
     */
    static readonly KF_ACT_STATUS_ARIVE: number = 1;

    /**
     * 0, 开服活动状态 未达成
     */
    static readonly KF_ACT_STATUS_NONE: number = 0;

    /**
     * 2, 开服活动状态 已领奖
     */
    static readonly KF_ACT_STATUS_REWARD: number = 2;

    /**
     * 30, 开服连充返利配置个数
     */
    static readonly KF_LCFL_CFG_COUNT: number = 30;

    /**
     * 19, 活动_开服连充返利 领取奖励
     */
    static readonly KF_LCFL_GET: number = 19;

    /**
     * 4, 开服连充返利道具个数
     */
    static readonly KF_LCFL_ITEM_COUNT: number = 4;

    /**
     * 18, 活动_开服连充返利 打开面板
     */
    static readonly KF_LCFL_OPEN: number = 18;

    /**
     * 7, 开服连充返利开放天数
     */
    static readonly KF_LCFL_OPEN_DAYS: number = 7;

    /**
     * 3, 开服连充返利类型个数
     */
    static readonly KF_LCFL_TTPE_COUNT: number = 3;

    /**
     * 4, 开服连充返利有4周,第五周开始循环
     */
    static readonly KF_LCFL_WEEK_COUNT: number = 4;

    /**
     * 35, 招财猫 抽奖
     */
    static readonly KF_LUCKYCAT_DRAW: number = 35;

    /**
     * 34, 招财猫 打开面板
     */
    static readonly KF_LUCKYCAT_OPEN_PANNEL: number = 34;

    /**
     * 7, 每日最多目标有7个(类型个数)
     */
    static readonly KF_MRMU_MAX_DAY_COUNT: number = 7;

    /**
     * 1, 封神台被杀
     */
    static readonly KILLED_TYPE_FST: number = 1;

    /**
     * 2, 跨服封神台被杀
     */
    static readonly KILLED_TYPE_KFFST: number = 2;

    /**
     * 0, 普通被杀
     */
    static readonly KILLED_TYPE_NORMAL: number = 0;

    /**
     * 20, 国王专属坐骑ImageID
     */
    static readonly KING_MOUNT_ID: number = 20;

    /**
     * 19, 国王专属翅膀ImageID
     */
    static readonly KING_WING_IMAGE_ID: number = 19;

    /**
     * 10, 怪物日常掉落 大于怪物10级以上 无法获得
     */
    static readonly LEVEL_MONSTER_DAILY_COUNT_DROP_LIMIT: number = 10;

    /**
     * 1, 老虎机一转
     */
    static readonly LHJ_1_TRUN: number = 1;

    /**
     * 3, 老虎机三转
     */
    static readonly LHJ_3_TRUN: number = 3;

    /**
     * 5, 老虎机五转
     */
    static readonly LHJ_5_TRUN: number = 5;

    /**
     * 148, 老虎机 打开面板
     */
    static readonly LHJ_PANNEL: number = 148;

    /**
     * 150, 老虎机 全服记录
     */
    static readonly LHJ_RECORD: number = 150;

    /**
     * 149, 老虎机 转动机器
     */
    static readonly LHJ_TRUN: number = 149;

    /**
     * 10381011, 猎魂召唤物品ID
     */
    static readonly LIEHUN_ZHAOHUAN_THINGID: number = 10381011;

    /**
     * 1, 1 宝典_超值礼包
     */
    static readonly LIFE_ONCE_BIT_BAODIAN_CZLB_1: number = 1;

    /**
     * 7, 7 宝典_超值限购
     */
    static readonly LIFE_ONCE_BIT_BAODIAN_CZXG_7: number = 7;

    /**
     * 3, 3 宝典_神兽飙升
     */
    static readonly LIFE_ONCE_BIT_BAODIAN_SSBS_3: number = 3;

    /**
     * 2, 2 宝典_一元礼包
     */
    static readonly LIFE_ONCE_BIT_BAODIAN_YYLB_2: number = 2;

    /**
     * 6, 6 宝典_再冲礼包
     */
    static readonly LIFE_ONCE_BIT_BAODIAN_ZCLB_6: number = 6;

    /**
     * 4, 4 宝典_斩妖灭魔
     */
    static readonly LIFE_ONCE_BIT_BAODIAN_ZYCM_4: number = 4;

    /**
     * 5, 5 宝典_主宰礼包
     */
    static readonly LIFE_ONCE_BIT_BAODIAN_ZZLB_5: number = 5;

    /**
     * 11, 11 强制设置玩家成年 优先级大于未成年
     */
    static readonly LIFE_ONCE_BIT_GM_ADULT_11: number = 11;

    /**
     * 12, 12 强制设置玩家未成年
     */
    static readonly LIFE_ONCE_BIT_GM_NOT_ADULT_12: number = 12;

    /**
     * 8, 8 GM 打印显示伤害
     */
    static readonly LIFE_ONCE_BIT_GM_PRINT_SHOWHURT: number = 8;

    /**
     * 8, 终生一次标志 数组长度 char个数 8个够记录64Bit
     */
    static readonly LIFE_ONCE_BIT_MAX_LENGTH: number = 8;

    /**
     * 14, 14 每分钟杀怪统计
     */
    static readonly LIFE_ONCE_BIT_MONSTER_KILL_14: number = 14;

    /**
     * 13, 13 进阶日排行 是否脏数据(未同步到本服排行榜)
     */
    static readonly LIFE_ONCE_BIT_STAGEDAYRANK_DIRTY_13: number = 13;

    /**
     * 9, 9 测服玩家充值返利领取
     */
    static readonly LIFE_ONCE_BIT_TEST_GIFT_9: number = 9;

    /**
     * 10, 10 武缘远征是否开启
     */
    static readonly LIFE_ONCE_BIT_WYYZ_OPEN_10: number = 10;

    /**
     * 101, 限时折扣购买
     */
    static readonly LIMIT_TIME_DISCOUNT_BUY: number = 101;

    /**
     * 100, 限时折扣打开面板
     */
    static readonly LIMIT_TIME_DISCOUNT_PANNEL: number = 100;

    /**
     * 1, 灵宝形象
     */
    static readonly LINGBAO_IMAGE: number = 1;

    /**
     * 1087, 灵狐仙府的商店ID
     */
    static readonly LINGHUXIANFU_STORE_ID: number = 1087;

    /**
     * 100000, 跨服联服区间 不在一个区间的服务器不能分为一个组
     */
    static readonly LINK_WORLD_SECTION_LENGTH: number = 100000;

    /**
     * 12, 累计充值档次最大12档
     */
    static readonly LJCZ_MAX_LEVEL_COUNT: number = 12;

    /**
     * 6, 打开累计充值面板
     */
    static readonly LJCZ_OPEN_PANEL: number = 6;

    /**
     * 8, 中国时区是8
     */
    static readonly LOCAT_TIME_ZONE: number = 8;

    /**
     * 3, 锁定背包
     */
    static readonly LOCK_BAGSTA: number = 3;

    /**
     * 1, 跨服副本切线
     */
    static readonly LOGINOUT_REAZON_CROSSPIN: number = 1;

    /**
     * 0, 正常上线下线
     */
    static readonly LOGINOUT_REAZON_NORMAL: number = 0;

    /**
     * 2, 登录类型 安卓
     */
    static readonly LOGIN_CLINT_TYPE_ANDROID: number = 2;

    /**
     * 3, 登录类型 模拟器
     */
    static readonly LOGIN_CLINT_TYPE_EMULATOR: number = 3;

    /**
     * 1, 登录类型 IOS
     */
    static readonly LOGIN_CLINT_TYPE_IOS: number = 1;

    /**
     * 255, 登录类型 其他
     */
    static readonly LOGIN_CLINT_TYPE_OHTER: number = 255;

    /**
     * 4, 登录类型 PC
     */
    static readonly LOGIN_CLINT_TYPE_PC: number = 4;

    /**
     * 1, 等出原因 跨到其他服 登出本服
     */
    static readonly LOGOUT_RESON_CROSS_TO_OTHER: number = 1;

    /**
     * 0, 等出原因 正常登出
     */
    static readonly LOGOUT_RESON_NONE: number = 0;

    /**
     * 3, 宝镜仓库炼化操作
     */
    static readonly LOTTERY_LIANHUA: number = 3;

    /**
     * 8, 招财猫抽奖物品的种类
     */
    static readonly LUCKYCAT_NUM_OF_DRAW_TYPE: number = 8;

    /**
     * 2, 魔方系统每日可以开启次数(不包括VIP)
     */
    static readonly MAGICCUBE_FREE_TIMES: number = 2;

    /**
     * 1, 魔方祝福值补充
     */
    static readonly MAGICCUBE_LUCKY_FILL: number = 1;

    /**
     * 2, 魔方祝福值保留
     */
    static readonly MAGICCUBE_LUCKY_KEEP: number = 2;

    /**
     * 10196010, 非绑定魔方精血ID
     */
    static readonly MAGICCUBE_OPEN_ITEMID: number = 10196010;

    /**
     * 10196011, 绑定魔方精血ID
     */
    static readonly MAGICCUBE_OPEN_ITEMID2: number = 10196011;

    /**
     * 20, 魔方系统刷新挑战列表所需道具数量
     */
    static readonly MAGICCUBE_OPEN_ITEM_COUNT: number = 20;

    /**
     * 5, 铭文试炼魔力色子 购买价格
     */
    static readonly MAGIC_DICE_BASE_COST: number = 5;

    /**
     * 2, 删除全部邮件
     */
    static readonly MAIL_DELETE_ALL: number = 2;

    /**
     * 3, 最大值，无意义
     */
    static readonly MAIL_DELETE_MAX: number = 3;

    /**
     * 0, 占位，无意义
     */
    static readonly MAIL_DELETE_MIN: number = 0;

    /**
     * 1, 删除已读取的邮件
     */
    static readonly MAIL_DELETE_READ: number = 1;

    /**
     * 2, 系统GM邮件
     */
    static readonly MAIL_TYPE_SYSTEM_GM: number = 2;

    /**
     * 1, 系统普通邮件
     */
    static readonly MAIL_TYPE_SYSTEM_NORMAL: number = 1;

    /**
     * 1041, 计价商城的商店ID，涉及商城的所有物品购买、计价，都从此商城获取，原1040商城，仅是前台展现使用
     */
    static readonly MARKET_STORE_ID: number = 1041;

    /**
     * 1042, 婚姻商店 商店ID 
     */
    static readonly MARRY_STORE_ID: number = 1042;

    /**
     * 10240, 长度为10240的长度
     */
    static readonly MAX_10240_LEN: number = 10240;

    /**
     * 128, 长度为128的长度
     */
    static readonly MAX_128_LEN: number = 128;

    /**
     * 256, 长度为256的长度
     */
    static readonly MAX_256_LEN: number = 256;

    /**
     * 32, 长度为32的长度
     */
    static readonly MAX_32_LEN: number = 32;

    /**
     * 33, 37玩平台用户名字最大长度32, 留一个\0
     */
    static readonly MAX_37PLAT_NAME_LEN: number = 33;

    /**
     * 33, 37玩平台校验sign最大长度32, 留一个\n
     */
    static readonly MAX_37PLAT_SIGN_LEN: number = 33;

    /**
     * 512, 长度为512的长度
     */
    static readonly MAX_512_LEN: number = 512;

    /**
     * 64, 长度为64的长度
     */
    static readonly MAX_64_LEN: number = 64;

    /**
     * 10, 每封邮件中最多有10个附件（道具、装备、散仙）
     */
    static readonly MAX_ACCESSORY_NUMBER: number = 10;

    /**
     * 1200, 最大成就总个数    50 * 24
     */
    static readonly MAX_ACHI_ALL_COUNT: number = 1200;

    /**
     * 3, 每个子类BitMap的Size3 就是每个子类最多有24个成就
     */
    static readonly MAX_ACHI_TYPE_BITMAP_SIZE: number = 3;

    /**
     * 35, 成就共有35个子类
     */
    static readonly MAX_ACHI_TYPE_COUNT: number = 35;

    /**
     * 24, 每个子类最多有24个成就
     */
    static readonly MAX_ACHI_TYPE_EACH_COUNT: number = 24;

    /**
     * 5, 单笔充值奖励配置物品最大个数
     */
    static readonly MAX_ACT124_ITEM_COUNT: number = 5;

    /**
     * 5, 循环充值奖励配置物品最大个数
     */
    static readonly MAX_ACT125_ITEM_COUNT: number = 5;

    /**
     * 10, 玩家学会主动技能个数
     */
    static readonly MAX_ACTIVE_SKILL_NUMBER: number = 10;

    /**
     * 8
     */
    static readonly MAX_ACTIVE_SKILL_NUMBER_PER_PET: number = 8;

    /**
     * 200, 最大活动数目
     */
    static readonly MAX_ACTIVITY_CONFIG_NUMBER: number = 200;

    /**
     * 150, 活动总数
     */
    static readonly MAX_ACTIVITY_NUM: number = 150;

    /**
     * 4, 盛典宝箱最大调整周数
     */
    static readonly MAX_ACTIVITY_SDBX_WEEK: number = 4;

    /**
     * 5, 跨服消费 配置总数量
     */
    static readonly MAX_ACT_126_CFG_COUNT: number = 5;

    /**
     * 6, 每件装备最多10个附加属性
     */
    static readonly MAX_ADDEDPROPEXT_PER_EQUIP: number = 6;

    /**
     * 4, 每件装备最多10个附加属性
     */
    static readonly MAX_ADDEDPROP_PER_EQUIP: number = 4;

    /**
     * 500, 护阵使者配置个数有500个
     */
    static readonly MAX_ARENA_GUARD_CONFIG_COUNT: number = 500;

    /**
     * 10, 最大玩家助攻数
     */
    static readonly MAX_ASSISTING_ROLE_NUM: number = 10;

    /**
     * 6
     */
    static readonly MAX_AUTO_BATTLE_SKILL_NUMBER: number = 6;

    /**
     * 5, 自动分解品质个数
     */
    static readonly MAX_AUTO_QUALITY_NUMBER: number = 5;

    /**
     * 5, 自动分解阶级个数
     */
    static readonly MAX_AUTO_STAGE_NUMBER: number = 5;

    /**
     * 7
     */
    static readonly MAX_AVATAR_NUMBER: number = 7;

    /**
     * 7
     */
    static readonly MAX_BAG_PASSWORD_LENGTH: number = 7;

    /**
     * 24
     */
    static readonly MAX_BAG_PASSWORD_MAIL_LENGTH: number = 24;

    /**
     * 20, 宝典总个数
     */
    static readonly MAX_BAO_DIAN_COUNT: number = 20;

    /**
     * 10, 批量洗练属性的次数
     */
    static readonly MAX_BATCH_EQUIP_WASH_PROP_NUM: number = 10;

    /**
     * 10, 批量洗练属性的次数
     */
    static readonly MAX_BATCH_SLOT_WASH_PROP_NUM: number = 10;

    /**
     * 15, 温泉活动，每天最多接受技能次数
     */
    static readonly MAX_BATHE_ACCEPT_SKILL_NUM: number = 15;

    /**
     * 5, 温泉活动，每天每个技能最多释放次数
     */
    static readonly MAX_BATHE_RELEASE_SKILL_NUM: number = 5;

    /**
     * 4, 温泉活动操作类型最大数，0索引的值为无效值,技能类型作为下标
     */
    static readonly MAX_BATHE_SKILL_NUM: number = 4;

    /**
     * 5, 武缘飞升最大次数
     */
    static readonly MAX_BEAUTY_FS_CNT: number = 5;

    /**
     * 10, 武缘寻宝免费次数
     */
    static readonly MAX_BEAUTY_XB_FREE_COUNT: number = 10;

    /**
     * 6, 武缘寻宝获取物品最大数量
     */
    static readonly MAX_BEAUTY_XB_GE_TTHING_COUNT: number = 6;

    /**
     * 20, 武缘阵图最大数量
     */
    static readonly MAX_BEAUTY_ZT_COUNT: number = 20;

    /**
     * 50, 百服庆典抽奖记录列表长度
     */
    static readonly MAX_BFQD_DRAW_RECORD_NUM: number = 50;

    /**
     * 4
     */
    static readonly MAX_BONUS_NUMBER: number = 4;

    /**
     * 7, 开服挑战BOSS活动持续天数
     */
    static readonly MAX_BOSSACT_OPEN_DAYS: number = 7;

    /**
     * 10
     */
    static readonly MAX_BOSSHURTRANK_NUMBER: number = 10;

    /**
     * 100, BOSSID提醒数组最大个数
     */
    static readonly MAX_BOSS_ATTENTION_NUMBER: number = 100;

    /**
     * 7, Boss每日脚本最大奖励次数
     */
    static readonly MAX_BOSS_DAY_SCRIPT_CNT: number = 7;

    /**
     * 10, Boss获得奖励记录10个
     */
    static readonly MAX_BOSS_ROLEREWARD_CNT: number = 10;

    /**
     * 24, Buff变更最大个数： buff+debuff的数量总和
     */
    static readonly MAX_BUFF_CHANGE_NUMBER: number = 24;

    /**
     * 16
     */
    static readonly MAX_BUFF_NUMBER: number = 16;

    /**
     * 20
     */
    static readonly MAX_BUFF_NUMBER_PER_SIGHT_REFRESH: number = 20;

    /**
     * 30, 步步高升配置最多个数
     */
    static readonly MAX_BU_BU_GAO_SHENG_CFG_NUMBER: number = 30;

    /**
     * 8, 步步高升活动，每天奖励最大个数(档数)
     */
    static readonly MAX_BU_BU_GAO_SHENG_DAY_NUM: number = 8;

    /**
     * 3, 步步高升活动，开启最大天数
     */
    static readonly MAX_BU_BU_GAO_SHENG_OPEN_DAY: number = 3;

    /**
     * 15, 宝箱争霸 采集宝箱的最大次数
     */
    static readonly MAX_BXZB_COLLECT_COUNT: number = 15;

    /**
     * 1
     */
    static readonly MAX_CASTER_UNIT_NUMBER: number = 1;

    /**
     * 17
     */
    static readonly MAX_CDKEY_LENGTH: number = 17;

    /**
     * 8, 充值返利档次个数
     */
    static readonly MAX_CHARGE_REBATE_COUNT: number = 8;

    /**
     * 242
     */
    static readonly MAX_CHAT_LENGTH: number = 242;

    /**
     * 8, 聊天验证码
     */
    static readonly MAX_CHECK_CHAT_CRC: number = 8;

    /**
     * 10000, 跨服宗派战-单平台最多允许报名的宗派
     */
    static readonly MAX_CLUSTER_APPLY_NUM: number = 10000;

    /**
     * 2000, Cluster 玩家Uin数据总个数 不要再扩大 不够放用m_uiSubKey来添加写入DB数据量
     */
    static readonly MAX_CLUSTER_ROLE_UIN_DB_CNT: number = 2000;

    /**
     * 15, 收集兑换档数最大个数
     */
    static readonly MAX_COLLECT_EXCHANGE_NUM: number = 15;

    /**
     * 2
     */
    static readonly MAX_COMPLETED_QUEST_GROUP_NUMBER: number = 2;

    /**
     * 1000
     */
    static readonly MAX_COMPLETED_QUEST_NUMBER: number = 1000;

    /**
     * 10, 消费排行个数
     */
    static readonly MAX_COMSUME_RANKLIST_NUM: number = 10;

    /**
     * 7, 消费排行活动持续时间
     */
    static readonly MAX_COMSUME_RANK_DAY_NUM: number = 7;

    /**
     * 1000, Act126跨服消费排版榜容量
     */
    static readonly MAX_CONSUME_RANK_NUM: number = 1000;

    /**
     * 10, 最多10个最近联系人
     */
    static readonly MAX_CONTACT_PER_ROLE: number = 10;

    /**
     * 15
     */
    static readonly MAX_CONTAINER_CHANGED_NUMBER: number = 15;

    /**
     * 50, 单元最大冷却个数
     */
    static readonly MAX_COOLDOWN_PER_UNIT: number = 50;

    /**
     * 20, 铜币数目排行榜的数组长度
     */
    static readonly MAX_COPPERRANKLIST_NUM: number = 20;

    /**
     * 5, 选举扣除5元宝
     */
    static readonly MAX_COUNRTY_VOTE_PER_COST_YUANBAO: number = 5;

    /**
     * 50, 国家日志个数
     */
    static readonly MAX_COUNTRY_FRAME_LOG_NUMBER: number = 50;

    /**
     * 10, 国家勋章个数
     */
    static readonly MAX_COUNTRY_FRAME_MEDAL_NUMBER: number = 10;

    /**
     * 2, 同一勋章个数
     */
    static readonly MAX_COUNTRY_FRAME_ONE_MEDAL_NUMBER: number = 2;

    /**
     * 30, 国家成员个数
     */
    static readonly MAX_COUNTRY_FRAME_ROLE_NUMBER: number = 30;

    /**
     * 7, 职位最大个数
     */
    static readonly MAX_COUNTRY_JOBTITLE_COUNT: number = 7;

    /**
     * 15, 摘星台配置最大个数
     */
    static readonly MAX_COUNTRY_LOTTERY_CONFIG_COUNT: number = 15;

    /**
     * 10, 勋章配置最大个数
     */
    static readonly MAX_COUNTRY_MEDAL_CONFIG_COUNT: number = 10;

    /**
     * 1, 最多1个国家
     */
    static readonly MAX_COUNTRY_NUM: number = 1;

    /**
     * 6, 预留2个
     */
    static readonly MAX_COUNTRY_REBATE_COUNT: number = 6;

    /**
     * 4, 当前有4个
     */
    static readonly MAX_COUNTRY_REBATE_CUR_COUNT: number = 4;

    /**
     * 1, 天灵宝莲
     */
    static readonly MAX_COUNTRY_REBATE_TIANLINGBAOLIAN: number = 1;

    /**
     * 4, 仙蕴宝藏
     */
    static readonly MAX_COUNTRY_REBATE_XIANYUNBAOZANG: number = 4;

    /**
     * 2, 英灵之魂
     */
    static readonly MAX_COUNTRY_REBATE_YINGLINGZHIHUN: number = 2;

    /**
     * 3, 摘星台
     */
    static readonly MAX_COUNTRY_REBATE_ZHAIXINGTAI: number = 3;

    /**
     * 181, 国家公告，最多60个汉字
     */
    static readonly MAX_COUNTRY_TEXT_LENGTH: number = 181;

    /**
     * 10, 英灵之魂每天花钱增加操作的最大值
     */
    static readonly MAX_COUNTRY_YINGLING_BUY_OPERATE_NUM: number = 10;

    /**
     * 5, 英灵之魂配置最大个数，（英雄数）4+1
     */
    static readonly MAX_COUNTRY_YINGLING_CONFIG_COUNT: number = 5;

    /**
     * 200, 英灵之魂同英雄互动友好度最大值
     */
    static readonly MAX_COUNTRY_YINGLING_FRIENDLY_COUNT: number = 200;

    /**
     * 5, 英灵之魂每天默认可操作的最大值
     */
    static readonly MAX_COUNTRY_YINGLING_REMAIN_OPERATE_NUM: number = 5;

    /**
     * 3, 跨服3V3活动赛季奖励只奖励前3名
     */
    static readonly MAX_CROSS3V3_REWARD_NUM: number = 3;

    /**
     * 6, 跨服3V3活动每天最高胜场奖励配置
     */
    static readonly MAX_CROSS3V3_WIN_TIME: number = 6;

    /**
     * 100, 跨服封神台，英雄榜最多人数
     */
    static readonly MAX_CROSSPKRANK_NUM: number = 100;

    /**
     * 100, 跨服PVP最多保留名次
     */
    static readonly MAX_CROSSPVP_COUNT: number = 100;

    /**
     * 100, 暂时只记录100届冠军
     */
    static readonly MAX_CROSSPVP_SESSION_COUNT: number = 100;

    /**
     * 15, 跨服PVP最多战区数量
     */
    static readonly MAX_CROSSPVP_WAR_CNT: number = 15;

    /**
     * 3, 这个版本跨服争夺3个城
     */
    static readonly MAX_CROSS_CITY_NUM: number = 3;

    /**
     * 3, 跨服四象斗兽场被挑战记录数
     */
    static readonly MAX_CROSS_COLOSSEUM_ATTACK_COUNT: number = 3;

    /**
     * 3, 跨服四象斗兽面板显示前几名
     */
    static readonly MAX_CROSS_COLOSSEUM_FRONT_COUNT: number = 3;

    /**
     * 25, 四象神兽段位奖励个数
     */
    static readonly MAX_CROSS_COLOSSEUM_GRADE_COUNT: number = 25;

    /**
     * 130, 跨服四象斗兽场活动最低参与等级
     */
    static readonly MAX_CROSS_COLOSSEUM_LEVEL: number = 130;

    /**
     * 100, 跨服四象斗兽场一次拉取排行榜列表记录数
     */
    static readonly MAX_CROSS_COLOSSEUM_ONERAN_COUNT: number = 100;

    /**
     * 3, 跨服四象斗兽场随机可挑战玩家数
     */
    static readonly MAX_CROSS_COLOSSEUM_RANDOM_COUNT: number = 3;

    /**
     * 2000, 跨服四象斗兽场排行保留名次
     */
    static readonly MAX_CROSS_COLOSSEUM_RANK_COUNT: number = 2000;

    /**
     * 100, 跨服3v3PVP最多保留名次
     */
    static readonly MAX_CROSS_MULTIPVP_COUNT: number = 100;

    /**
     * 50, 跨服排行榜单类型最大数
     */
    static readonly MAX_CROSS_RANK_COUNT: number = 50;

    /**
     * 10, 跨服_单次查看某副本队伍个数
     */
    static readonly MAX_CROSS_TEAM_NUM: number = 10;

    /**
     * 10000, 跨服云购最大购买数
     */
    static readonly MAX_CROSS_YUNGOU_BUY_COUNT: number = 10000;

    /**
     * 24, 跨服云购最大记录中奖记录数
     */
    static readonly MAX_CROSS_YUNGOU_LUCKY_COUNT: number = 24;

    /**
     * 5, 跨服至尊争夺 配置总数量
     */
    static readonly MAX_CROSS_ZZZD_CHARGE_RANK_COUNT: number = 5;

    /**
     * 11, 猎魂背包格子大小
     */
    static readonly MAX_CRYSTAL_NUM_IN_BAG: number = 11;

    /**
     * 40, 猎魂仓库格子大小
     */
    static readonly MAX_CRYSTAL_NUM_IN_STORE: number = 40;

    /**
     * 40, 货币最大种类数目
     */
    static readonly MAX_CURRENCY_NUMBER: number = 40;

    /**
     * 7, 开服集字送好礼持续天数
     */
    static readonly MAX_CWACT_OPEN_DAYS: number = 7;

    /**
     * 7, 充值送好礼每日最大兑换数量(额度阶段)
     */
    static readonly MAX_CZSHL_THING_COUNT: number = 7;

    /**
     * 8, 消费赠好礼档次
     */
    static readonly MAX_DAILYCONSUME_CFGID_NUM: number = 8;

    /**
     * 31, 一个月31天
     */
    static readonly MAX_DAYS_OF_MONTH: number = 31;

    /**
     * 10, 跨服点灯表格最大配置数
     */
    static readonly MAX_DDLRANK_CFG_COUNT: number = 10;

    /**
     * 8, 跨服点灯配置物品数量
     */
    static readonly MAX_DDL_ITEM_COUNT: number = 8;

    /**
     * 5, 跨服点灯列表数量
     */
    static readonly MAX_DDL_LIST_COUNT: number = 5;

    /**
     * 50, 点灯笼记录列表最大长度
     */
    static readonly MAX_DDL_RECORD_NUM: number = 50;

    /**
     * 50, 跨服点灯活动说明文字长度
     */
    static readonly MAX_DDL_TIPS_LENGTH: number = 50;

    /**
     * 16
     */
    static readonly MAX_DEBUFF_NUMBER: number = 16;

    /**
     * 32, 充值描述最大长度
     */
    static readonly MAX_DESC_LENGTH: number = 32;

    /**
     * 33, 设备信息字段, 留一个\0
     */
    static readonly MAX_DEVICEUID_INFO_LEN: number = 33;

    /**
     * 65, 设备唯一ID, 留一个\0
     */
    static readonly MAX_DEVICEUID_NAME_LEN: number = 65;

    /**
     * 3, 每件装备最多10个宝石孔
     */
    static readonly MAX_DIAMONDPROP_PER_EQUIP: number = 3;

    /**
     * 10, 装备镶嵌道具升级的最大等级限制
     */
    static readonly MAX_DIAMOND_UPLEVEL_LIMIT: number = 10;

    /**
     * 3, 魔方难度列表最大数
     */
    static readonly MAX_DIFFICULT_NUMBER: number = 3;

    /**
     * 5, 挖BOSS的最多得到道具个数
     */
    static readonly MAX_DIGBOSS_ITEM: number = 5;

    /**
     * 6, 地榜竞技Boss个数
     */
    static readonly MAX_DI_BANG_BOSS_NUM: number = 6;

    /**
     * 12, 地榜竞技称号个数
     */
    static readonly MAX_DI_BANG_TITLE_NUM: number = 12;

    /**
     * 48, 域名字串最大长度
     */
    static readonly MAX_DOMAIN_STR_NUM: number = 48;

    /**
     * 2, 充值双倍领取奖励最大次数
     */
    static readonly MAX_DOUBLE_REBATE_COUNT: number = 2;

    /**
     * 40, 最大时装形象个数
     */
    static readonly MAX_DRESS_IMAGE_NUMBER: number = 40;

    /**
     * 140, 掉落物上限
     */
    static readonly MAX_DROPPEDTHING_NUMBER_IN_SIGHT: number = 140;

    /**
     * 140
     */
    static readonly MAX_DROPPED_NUMBER_PER_SIGHT_REFRESH: number = 140;

    /**
     * 32
     */
    static readonly MAX_DROP_THING_NUMBER: number = 32;

    /**
     * 10, 端午达成档次最大数
     */
    static readonly MAX_DUANWU_REACH_NUM: number = 10;

    /**
     * 50, 最多有50个配置，现有20个，预留30个，不够策划剁手
     */
    static readonly MAX_EAT_THIS_CFG_COUNT: number = 50;

    /**
     * 20, 每个等级可以吃20个
     */
    static readonly MAX_EAT_THIS_PER_LEVEL_COUNT: number = 20;

    /**
     * 999, 体力上限
     */
    static readonly MAX_ENERGY_WY_NUM: number = 999;

    /**
     * 100
     */
    static readonly MAX_EQUIP_DESC_LENGTH: number = 100;

    /**
     * 15, 最大宝石等级
     */
    static readonly MAX_EQUIP_DIAMOND_LEVEL: number = 15;

    /**
     * 6, 最大宝石孔个数
     */
    static readonly MAX_EQUIP_DIAMOND_NUM: number = 6;

    /**
     * 6, 一次装备熔炼最大装备个数
     */
    static readonly MAX_EQUIP_FORGE_COUNT: number = 6;

    /**
     * 4, 最大随机属性条数
     */
    static readonly MAX_EQUIP_RANDATTR_NUM: number = 4;

    /**
     * 15, 最大星级
     */
    static readonly MAX_EQUIP_STAR_NUMBER: number = 15;

    /**
     * 30, 装备强化的最大等级
     */
    static readonly MAX_EQUIP_STRENG_LEVEL: number = 30;

    /**
     * 25, 装备塑魂最高等级
     */
    static readonly MAX_EQUIP_SUHUN_LEVEL: number = 25;

    /**
     * 7, 每次最大洗练属性数目(主要是用作缓存和返回客户端)
     */
    static readonly MAX_EQUIP_WASH_PROP_NUM: number = 7;

    /**
     * 4, 最大兑换次数
     */
    static readonly MAX_EXCHANGE_COUNT: number = 4;

    /**
     * 2, 最大额外礼包个数
     */
    static readonly MAX_EXTRA_GIFT_NUMBER: number = 2;

    /**
     * 22, 法宝最大个数
     */
    static readonly MAX_FABAO_COUNT: number = 22;

    /**
     * 100, 法宝最大等级
     */
    static readonly MAX_FABAO_LEVEL: number = 100;

    /**
     * 10, 法器数量
     */
    static readonly MAX_FAQI_COUNT: number = 10;

    /**
     * 7, 法则最大个数
     */
    static readonly MAX_FAZE_NUMBER: number = 7;

    /**
     * 10, 法阵最大个数
     */
    static readonly MAX_FAZHEN_COUNT: number = 10;

    /**
     * 4, 法阵部位最大个数
     */
    static readonly MAX_FAZHEN_PART_COUNT: number = 4;

    /**
     * 6, 丰财聚宝配置物品最大个数
     */
    static readonly MAX_FENGCAIJUBAO_CFG_ITEM_COUNT: number = 6;

    /**
     * 8, 丰财聚宝配置最多个数
     */
    static readonly MAX_FENG_CAI_JU_BAO_CFG_NUMBER: number = 8;

    /**
     * 10, 节日消费活动档数
     */
    static readonly MAX_FESTIVAL_CHARGE_COUNT: number = 10;

    /**
     * 3, 节日消费活动档数
     */
    static readonly MAX_FESTIVAL_CONSUME_COUNT: number = 3;

    /**
     * 3, 节日兑换活动档数
     */
    static readonly MAX_FESTIVAL_CONVERSION_COUNT: number = 3;

    /**
     * 20, 战斗力排行榜的数组长度
     */
    static readonly MAX_FIGHTRANK_NUM: number = 20;

    /**
     * 9000, 固定分组支持的最大服数
     */
    static readonly MAX_FIX_GROUP_WORLD_NUM: number = 9000;

    /**
     * 30, 玩家固定称号最大个数
     */
    static readonly MAX_FIX_TITLE_NUMBER: number = 30;

    /**
     * 4, 封魔塔 单层boss最大数量
     */
    static readonly MAX_FMT_ONELEVEL_BOSS_NUM: number = 4;

    /**
     * 255, 封号原因最大长度
     */
    static readonly MAX_FORBID_REASON_LENGTH: number = 255;

    /**
     * 32, 发送名字的最大长度
     */
    static readonly MAX_FROMROLENAME_LENGTH: number = 32;

    /**
     * 10, 功能激活列表的最大数量
     */
    static readonly MAX_FUNCACT_COUNT: number = 10;

    /**
     * 2048
     */
    static readonly MAX_GAMEMASTER_COMMAND_LENGTH: number = 2048;

    /**
     * 20
     */
    static readonly MAX_GAMEMASTER_PARAMETER_NUMBER: number = 20;

    /**
     * 1024
     */
    static readonly MAX_GAMEMASTER_PER_PARAMETER_LENGTH: number = 1024;

    /**
     * 200
     */
    static readonly MAX_GAMEMASTER_SYSTEM_MESSAGE_LENGTH: number = 200;

    /**
     * 50, 单角色的游戏黑名单列表的最大长度
     */
    static readonly MAX_GAME_BLACK_PER_ROLE: number = 50;

    /**
     * 50, 单角色的游戏宿敌列表的最大长度
     */
    static readonly MAX_GAME_ENEMY_PER_ROLE: number = 50;

    /**
     * 50, 单角色的游戏好友列表的最大长度:等于游戏好友+密友数量
     */
    static readonly MAX_GAME_FRIEND_PER_ROLE: number = 50;

    /**
     * 100
     */
    static readonly MAX_GENERAL_CAPTION_LENGTH: number = 100;

    /**
     * 10
     */
    static readonly MAX_GIFT_ITEM_NUMBER: number = 10;

    /**
     * 100, GIIS 过来的最多100条
     */
    static readonly MAX_GIIS_ACT_TIME_COUNT: number = 100;

    /**
     * 5, 最多GIIS可发附件数
     */
    static readonly MAX_GIIS_ITEM_NUMBER: number = 5;

    /**
     * 30, 最多回复排行榜的个数
     */
    static readonly MAX_GIIS_RANK_COUNT: number = 30;

    /**
     * 20, GIIS一次最多操作20个大区
     */
    static readonly MAX_GIIS_WORLD_COUNT: number = 20;

    /**
     * 20, 给玩家物品的最多种类
     */
    static readonly MAX_GIVE_THING_NUMBER: number = 20;

    /**
     * 1024, 全局的DB脚本变量
     */
    static readonly MAX_GLOB_SCRIPT_DATA: number = 1024;

    /**
     * 400, 查询GM号列表的数组长度
     */
    static readonly MAX_GMLIST_NUM: number = 400;

    /**
     * 256, 回复的大小
     */
    static readonly MAX_GMQA_ANSWER_SIZE: number = 256;

    /**
     * 5, 最多允许玩家提的问题个数
     */
    static readonly MAX_GMQA_COUNT: number = 5;

    /**
     * 768, 玩家问题,200个字
     */
    static readonly MAX_GMQA_QUESTION_SIZE: number = 768;

    /**
     * 400, 单件物品，玩家可以参与最大人数 
     */
    static readonly MAX_GROUPBUY_ROLE_NUM: number = 400;

    /**
     * 4, 每期团购最大售卖物品数
     */
    static readonly MAX_GROUPBUY_SELLTHING_NUM: number = 4;

    /**
     * 50, 成长目标相关 最大个数
     */
    static readonly MAX_GROW_UP_COUNT: number = 50;

    /**
     * 40, 挂机最多统计40个道具
     */
    static readonly MAX_GUAJI_COLOR_COUNT: number = 40;

    /**
     * 4000, 每日多倍经验 0.4倍
     */
    static readonly MAX_GUAJI_DBJY_4000: number = 4000;

    /**
     * 80000, 每日多倍经验 8倍
     */
    static readonly MAX_GUAJI_DBJY_80000: number = 80000;

    /**
     * 7200, 每日多倍经验 7200秒
     */
    static readonly MAX_GUAJI_DBJY_SEC: number = 7200;

    /**
     * 10, 分解道具最多10种
     */
    static readonly MAX_GUAJI_METL_COUNT: number = 10;

    /**
     * 72000, 挂机时长最多累计到1200分钟
     */
    static readonly MAX_GUAJI_TIME_VALUE: number = 72000;

    /**
     * 8, 宗派战最多就8个宗门报名.
     */
    static readonly MAX_GUILDPVP_APPLY_NUM: number = 8;

    /**
     * 2, 跨服宗派战单服最多2个宗门报名.
     */
    static readonly MAX_GUILDPVP_AUTOAPPLY_NUM: number = 2;

    /**
     * 6, 本服宗派战最多6个宗门报名.
     */
    static readonly MAX_GUILDPVP_GUILD_NUM: number = 6;

    /**
     * 50, 一个宗派中最多容纳的未处理的申请数
     */
    static readonly MAX_GUILD_APPLY_REQUEST_NUMBER: number = 50;

    /**
     * 8, 只有族长和副族长、长老有审批权限
     */
    static readonly MAX_GUILD_APPROVER_NUMBER: number = 8;

    /**
     * 10
     */
    static readonly MAX_GUILD_CONTRIBUTION_LOG_NUMBER: number = 10;

    /**
     * 10, 宗派最高等级
     */
    static readonly MAX_GUILD_LEVEL_SIZE: number = 10;

    /**
     * 10, 名将挑战单个排行榜角色最大数量
     */
    static readonly MAX_GUILD_MJTZ_RANK_ROLE_NUM: number = 10;

    /**
     * 5, 名将挑战排行榜最大数量
     */
    static readonly MAX_GUILD_MJTZ_RANK_TYPE_NUM: number = 5;

    /**
     * 5, 最多5个排名
     */
    static readonly MAX_GUILD_MONEY_RANK_COUNT: number = 5;

    /**
     * 15, 开服15天内有效
     */
    static readonly MAX_GUILD_MONEY_RANK_DAY: number = 15;

    /**
     * 25, 宗派名字的长度，最多8汉字
     */
    static readonly MAX_GUILD_NAME_LENGTH: number = 25;

    /**
     * 100, 宗派群英会PVP个人排行版信息数量
     */
    static readonly MAX_GUILD_PVP_RANK_NUM: number = 100;

    /**
     * 120, 查询宗派时最多显示120个
     */
    static readonly MAX_GUILD_QUERY_NUMBER: number = 120;

    /**
     * 3, 每个角色可以同时申请的宗派数
     */
    static readonly MAX_GUILD_ROLE_APPLY_REQUEST_NUMBER: number = 3;

    /**
     * 2
     */
    static readonly MAX_GUILD_SOCIAL_SKILL_NUMBER: number = 2;

    /**
     * 35, 单次可以存入宗派的物品最大个数
     */
    static readonly MAX_GUILD_STORE_IN_NUM: number = 35;

    /**
     * 35, 单次可以删除宗派仓库的物品最大个数
     */
    static readonly MAX_GUILD_STORE_OUT_NUM: number = 35;

    /**
     * 6, 单次从宗派仓库取出的物品最大个数
     */
    static readonly MAX_GUILD_STORE_TAKEOUT_NUM: number = 6;

    /**
     * 240, 宗派仓库格子数
     */
    static readonly MAX_GUILD_STROE_GRID_COUNT: number = 240;

    /**
     * 50, 宗派仓库的日志数
     */
    static readonly MAX_GUILD_STROE_LOG_COUNT: number = 50;

    /**
     * 181, 宗派说明文字的长度，最多60汉字
     */
    static readonly MAX_GUILD_TEXT_LENGTH: number = 181;

    /**
     * 2, 一个仓库物品最多的申请人数
     */
    static readonly MAX_GUILD_TIME_APPLY_NUMBER: number = 2;

    /**
     * 3, 奖励个数
     */
    static readonly MAX_GUILD_TREASURE_HUNT_BONUS_COUNT: number = 3;

    /**
     * 180, 每人每次PK BOSS的最大时间
     */
    static readonly MAX_GUILD_TREASURE_HUNT_BOSSPKTIME: number = 180;

    /**
     * 10000, 最大完成度
     */
    static readonly MAX_GUILD_TREASURE_HUNT_COMPLETENESS: number = 10000;

    /**
     * 200, 宗派探矿贡献排名最大人数
     */
    static readonly MAX_GUILD_TREASURE_HUNT_CONTRIBUTION_RANK_NUMBER: number = 200;

    /**
     * 100, 宗派探矿描述最多50个汉字
     */
    static readonly MAX_GUILD_TREASURE_HUNT_DES_LENGTH: number = 100;

    /**
     * 4, 捐赠类型个数
     */
    static readonly MAX_GUILD_TREASURE_HUNT_DONATIONTYPE_COUNT: number = 4;

    /**
     * 10, 最大事件类型
     */
    static readonly MAX_GUILD_TREASURE_HUNT_EVENT_TYPE: number = 10;

    /**
     * 80, 宗派探矿名字最多40个汉字
     */
    static readonly MAX_GUILD_TREASURE_HUNT_NAME_LENGTH: number = 80;

    /**
     * 20, 节点个数
     */
    static readonly MAX_GUILD_TREASURE_HUNT_POINT_COUNT: number = 20;

    /**
     * 3, 宗派探矿，最大体力数
     */
    static readonly MAX_GUILD_TREASURE_HUNT_POWER_NUM: number = 3;

    /**
     * 1800, 宗派探矿，任务有效期，单位为秒
     */
    static readonly MAX_GUILD_TREASURE_HUNT_QUEST_VALIDITY: number = 1800;

    /**
     * 2, 选择事件最大选项数
     */
    static readonly MAX_GUILD_TREASURE_HUNT_SELECT_NUMBER: number = 2;

    /**
     * 1, 特殊奖励个数
     */
    static readonly MAX_GUILD_TREASURE_HUNT_SPECIAL_BONUS_COUNT: number = 1;

    /**
     * 3, 国运活动，可操作最大数
     */
    static readonly MAX_GUOYUN_OPERATE_NUM: number = 3;

    /**
     * 16384
     */
    static readonly MAX_HANDLER_NUMBER: number = 16384;

    /**
     * 40, 祝福子系统最大幻化形象个数
     */
    static readonly MAX_HEROSUB_DRESS_IMAGE_NUMBER: number = 40;

    /**
     * 8, 子系统装备最大个数
     */
    static readonly MAX_HERO_SUB_THINGOBJ_LIST_NUMBER: number = 8;

    /**
     * 7, 合服服集字送好礼持续天数
     */
    static readonly MAX_HFCWACT_OPEN_DAYS: number = 7;

    /**
     * 120, 铭文试炼最大配置个数
     */
    static readonly MAX_HISTORICAL_REMAINS_CFG: number = 120;

    /**
     * 9, 铭文试炼
     */
    static readonly MAX_HISTORICAL_REMAINS_FINAL_BOSS_COUNT: number = 9;

    /**
     * 60303005, 铭文试炼 最终BOSS1 掉落ID
     */
    static readonly MAX_HISTORICAL_REMAINS_FINAL_BOSS_DROPID_1: number = 60303005;

    /**
     * 60303006, 铭文试炼 最终BOSS2 掉落ID
     */
    static readonly MAX_HISTORICAL_REMAINS_FINAL_BOSS_DROPID_2: number = 60303006;

    /**
     * 60303007, 铭文试炼 最终BOSS3 掉落ID
     */
    static readonly MAX_HISTORICAL_REMAINS_FINAL_BOSS_DROPID_3: number = 60303007;

    /**
     * 60303008, 铭文试炼 最终BOSS4 掉落ID
     */
    static readonly MAX_HISTORICAL_REMAINS_FINAL_BOSS_DROPID_4: number = 60303008;

    /**
     * 60303009, 铭文试炼 最终BOSS5 掉落ID
     */
    static readonly MAX_HISTORICAL_REMAINS_FINAL_BOSS_DROPID_5: number = 60303009;

    /**
     * 60303010, 铭文试炼 最终BOSS6 掉落ID
     */
    static readonly MAX_HISTORICAL_REMAINS_FINAL_BOSS_DROPID_6: number = 60303010;

    /**
     * 60303011, 铭文试炼 最终BOSS7 掉落ID
     */
    static readonly MAX_HISTORICAL_REMAINS_FINAL_BOSS_DROPID_7: number = 60303011;

    /**
     * 60303012, 铭文试炼 最终BOSS8 掉落ID
     */
    static readonly MAX_HISTORICAL_REMAINS_FINAL_BOSS_DROPID_8: number = 60303012;

    /**
     * 60303013, 铭文试炼 最终BOSS9 掉落ID
     */
    static readonly MAX_HISTORICAL_REMAINS_FINAL_BOSS_DROPID_9: number = 60303013;

    /**
     * 31, 铭文试炼最大格子数
     */
    static readonly MAX_HISTORICAL_REMAINS_GRID: number = 31;

    /**
     * 1, 铭文试炼最大重置次数
     */
    static readonly MAX_HISTORICAL_REMAINS_RESET_TIMES: number = 1;

    /**
     * 4, 铭文试炼最大层数
     */
    static readonly MAX_HISTORICLREMAINS_CFG_STAGE: number = 4;

    /**
     * 3, 欢乐翻牌 每局牌的最多数量
     */
    static readonly MAX_HLFP_CARD_NUM: number = 3;

    /**
     * 30, 欢乐探宝 最多配置个数
     */
    static readonly MAX_HLTB_CFG_COUNT: number = 30;

    /**
     * 30, 欢乐探宝  抽奖物品个数
     */
    static readonly MAX_HLTB_DRAW_ITEM_COUNT: number = 30;

    /**
     * 20, 欢乐转盘大奖记录
     */
    static readonly MAX_HLZP_RECORD_COUNT: number = 20;

    /**
     * 8, 每次晶核孔批量洗的最大次数
     */
    static readonly MAX_HOLE_WASH_PER_TIME: number = 8;

    /**
     * 10, 欢乐翻牌活动，配置最大个数
     */
    static readonly MAX_HUAN_LE_FAN_PAI_CFG_NUM: number = 10;

    /**
     * 9, 角色魂骨装备最大个数
     */
    static readonly MAX_HUNGU_EQUIP_NUMBER: number = 9;

    /**
     * 5, 魂骨装备最大随机属性条数
     */
    static readonly MAX_HUNGU_EQUIP_RANDATTR_NUM: number = 5;

    /**
     * 25, 魂力配置数量 存库字段 修改要小心
     */
    static readonly MAX_HUNLI_CFG_NUM: number = 25;

    /**
     * 3, 魂力条件个数 存库字段 修改要小心
     */
    static readonly MAX_HUNLI_CONDITON_NUMBER: number = 3;

    /**
     * 9, 最大魂力等级
     */
    static readonly MAX_HUNLI_LEVEL: number = 9;

    /**
     * 3, 魂力子等级 定了就不能改
     */
    static readonly MAX_HUNLI_SUB_LEVEL: number = 3;

    /**
     * 16, 活跃度各项领取记录数组的最大数量, 支持128条记录
     */
    static readonly MAX_HYD_GIFT_FALG_NUM: number = 16;

    /**
     * 100, 活跃度数据记录最大数量
     */
    static readonly MAX_HYD_TYPE_NUM: number = 100;

    /**
     * 200, 求缘列表最大人数
     */
    static readonly MAX_HY_QIUYUAN_INFO_NUM: number = 200;

    /**
     * 6, 图标数目
     */
    static readonly MAX_ICON_LIST_NUM: number = 6;

    /**
     * 30, 每个场景最多20个ICON类怪物
     */
    static readonly MAX_ICON_MONSTER_PER_SCENE: number = 30;

    /**
     * 19, 身份证号码长度
     */
    static readonly MAX_IDENTITY_CARD_LENGTH: number = 19;

    /**
     * 1024
     */
    static readonly MAX_INVITEKEY_LENGTH: number = 1024;

    /**
     * 32
     */
    static readonly MAX_IP_STR_LEN: number = 32;

    /**
     * 48, IP字串最大长度（两个IP）
     */
    static readonly MAX_IP_STR_NUM: number = 48;

    /**
     * 64, 最多可以存 64*8 个合成配方是否学习标志
     */
    static readonly MAX_ITEM_MERGE_FORMULA_NUMBER: number = 64;

    /**
     * 30, 跨服决斗场单战区最大服数
     */
    static readonly MAX_JDC_WAR_WORLD_NUM: number = 30;

    /**
     * 300, 道宫九星-最高等级数
     */
    static readonly MAX_JIUXING_LEVEL: number = 300;

    /**
     * 9, 道宫九星-最多的星星数
     */
    static readonly MAX_JIUXING_NUM: number = 9;

    /**
     * 10, 积善成礼活动，配置最大个数
     */
    static readonly MAX_JI_SHAN_CHENG_LI_CFG_NUM: number = 10;

    /**
     * 10, 竞技场称号 前十有
     */
    static readonly MAX_JJC_TITLE_RANK: number = 10;

    /**
     * 10, 系统进阶日最大奖励配置数
     */
    static readonly MAX_JJR_REWARD_COUNT: number = 10;

    /**
     * 5, 系统进阶日最大系统数数
     */
    static readonly MAX_JJR_SYSTEM_COUNT: number = 5;

    /**
     * 2, RMB战场每天最多参与次数
     */
    static readonly MAX_JOIN_RMBZC_NUM: number = 2;

    /**
     * 8, 聚宝盆配置物品最大个数
     */
    static readonly MAX_JUBAOPEN_CFG_ITEM_COUNT: number = 8;

    /**
     * 7, 聚划算现在是持续7天的奖励
     */
    static readonly MAX_JUHSA_ACT_DAY: number = 7;

    /**
     * 100, 聚元系统最多属性条数
     */
    static readonly MAX_JUYUAN_ATTR_COUNT: number = 100;

    /**
     * 999, 最大的聚元时装模型ID
     */
    static readonly MAX_JUYUAN_IMAGE_ID: number = 999;

    /**
     * 4, 聚元系统的主等级
     */
    static readonly MAX_JUYUAN_LEVEL: number = 4;

    /**
     * 20, 聚元系统的最大类型
     */
    static readonly MAX_JUYUAN_TYPE: number = 20;

    /**
     * 12, 聚宝盆配置最多个数 入库
     */
    static readonly MAX_JU_BAO_PENG_CFG_NUMBER: number = 12;

    /**
     * 3, 开服BOSS召唤类型 -- 传奇
     */
    static readonly MAX_KFBOSS_SUMMON_TYPE: number = 3;

    /**
     * 5, 开服奔跑兄弟每天配置条数
     */
    static readonly MAX_KFBPXD_MAX_CONFIG_COUNT: number = 5;

    /**
     * 7, 开服奔跑兄弟最高开到开服第7天
     */
    static readonly MAX_KFBPXD_MAX_STAY_DAYS: number = 7;

    /**
     * 12, 每个玩家最多投注12个比赛
     */
    static readonly MAX_KFJDC_BET_COUNT: number = 12;

    /**
     * 7, 开服全民冲榜持续天数
     */
    static readonly MAX_KFQMCB_OPEN_DAYS: number = 7;

    /**
     * 20, 开服全民冲榜详细排行个数
     */
    static readonly MAX_KFQMCB_RANK_DE_NUM: number = 20;

    /**
     * 8, 开服全民冲榜7天排行榜个数
     */
    static readonly MAX_KFQMCB_RANK_NUM: number = 8;

    /**
     * 6, 开服全民冲榜每天最高奖励个数
     */
    static readonly MAX_KFQMCB_REWARD_NUM: number = 6;

    /**
     * 18, 开服首充团购持每天奖励最大数
     */
    static readonly MAX_KFSCTG_ONEDAY_CFG: number = 18;

    /**
     * 7, 开服首充团购持续天数
     */
    static readonly MAX_KFSCTG_OPEN_DAYS: number = 7;

    /**
     * 7, 开服消费返利持续天数
     */
    static readonly MAX_KFXFFL_OPEN_DAYS: number = 7;

    /**
     * 30, 跨服鲜花排行榜，最多人数
     */
    static readonly MAX_KFXHB_RANK_NUM: number = 30;

    /**
     * 4, 开服一元夺宝，最高奖励等级
     */
    static readonly MAX_KFYYDB_LEVEL: number = 4;

    /**
     * 8, 开服至尊争夺配置物品数量
     */
    static readonly MAX_KFZZZD_ITEM_COUNT: number = 8;

    /**
     * 7, 开服至尊争夺最高开到开服第7天
     */
    static readonly MAX_KFZZZD_MAX_OPEN_DAYS: number = 7;

    /**
     * 50, 开服至尊争夺活动说明文字长度
     */
    static readonly MAX_KFZZZD_TIPS_LENGTH: number = 50;

    /**
     * 3, 击杀boss面板中玩家伤害排行榜最多显示玩家个数
     */
    static readonly MAX_KILLBOSS_ROLE_NUM: number = 3;

    /**
     * 5, 最多转5次
     */
    static readonly MAX_LHJ_ITEM_TYPE_COUNT: number = 5;

    /**
     * 20, 老虎机全服记录数
     */
    static readonly MAX_LHJ_RECORD_COUNT: number = 20;

    /**
     * 10, 特殊大奖数
     */
    static readonly MAX_LHJ_SPE_COUNT: number = 10;

    /**
     * 50, 终生限购道具最大种类
     */
    static readonly MAX_LIFE_SPECIALITEM_LIST_LENGTH: number = 50;

    /**
     * 4, 跨服联服的个数
     */
    static readonly MAX_LINK_WORLD_NUM: number = 4;

    /**
     * 6, 登录充值配置物品最大个数
     */
    static readonly MAX_LOGIN_CHARGE_CFG_ITEM_COUNT: number = 6;

    /**
     * 50, 登录充值配置最多个数
     */
    static readonly MAX_LOGIN_CHARGE_CFG_NUMBER: number = 50;

    /**
     * 3
     */
    static readonly MAX_LOTTERY_BOSS_NUMBER: number = 3;

    /**
     * 5
     */
    static readonly MAX_LOTTERY_EXCHANGE_ITEM_NUMBER: number = 5;

    /**
     * 3
     */
    static readonly MAX_LOTTERY_LEVEL: number = 3;

    /**
     * 150, 抽奖炼化的配置最大数量
     */
    static readonly MAX_LOTTERY_LH_CFG_NUM: number = 150;

    /**
     * 9, 乐透最大格子数
     */
    static readonly MAX_LOTTERY_NUM: number = 9;

    /**
     * 3, 摘星台设施，操作类型的最多数量，目前是两类，1、元宝；2、战勋
     */
    static readonly MAX_LOTTERY_OPERATE_TYPE_NUM: number = 3;

    /**
     * 1024, 脚本线变量大小
     */
    static readonly MAX_LSV_LENGTH: number = 1024;

    /**
     * 50, 招财猫的最大抽奖次数
     */
    static readonly MAX_LUCKYCAT_DRAW_RECORD_COUNT: number = 50;

    /**
     * 9, 连续返利 累计充值数组最大个数
     */
    static readonly MAX_LXFL_ACC_LIST_COUNT: number = 9;

    /**
     * 7, 连续返利 最多持续7天
     */
    static readonly MAX_LXFL_DAY_COUNT: number = 7;

    /**
     * 3, 连续返利 今日充值数组最大个数
     */
    static readonly MAX_LXFL_DAY_LIST_COUNT: number = 3;

    /**
     * 20, 邮件信息列表的数组长度
     */
    static readonly MAX_MAILCONTENT_NUM: number = 20;

    /**
     * 150, 单角色邮箱保存邮件上限
     */
    static readonly MAX_MAIL_PER_ROLE: number = 150;

    /**
     * 513, 邮件正文最多512个字节
     */
    static readonly MAX_MAIL_TEXT_LENGTH: number = 513;

    /**
     * 33, 邮件标题最多32个字节
     */
    static readonly MAX_MAIL_TITLE_LENGTH: number = 33;

    /**
     * 150, 一个宗派中的成员上线
     */
    static readonly MAX_MEMBER_NUMBER_PER_GUILD: number = 150;

    /**
     * 3, 跨服组队，每队最大人数
     */
    static readonly MAX_MEMBER_PER_CROSS_TEAM: number = 3;

    /**
     * 5, 本地组队，每队最大人数
     */
    static readonly MAX_MEMBER_PER_TEAM: number = 5;

    /**
     * 10
     */
    static readonly MAX_MENUNODE_NUMBER: number = 10;

    /**
     * 3, 合服活动都是开启三天的
     */
    static readonly MAX_MERGE_ACTIVITY_OPEN_DAYS: number = 3;

    /**
     * 3, 魔化战争 随从服务器有3个
     */
    static readonly MAX_MHZZ_SLAVE_SVR_COUNT: number = 3;

    /**
     * 25, 名将挑战boss总数量
     */
    static readonly MAX_MJTZ_BOSS_COUNT: number = 25;

    /**
     * 64
     */
    static readonly MAX_MONSTER_NAME_LENGTH: number = 64;

    /**
     * 128, 怪物上限
     */
    static readonly MAX_MONSTER_NUMBER_IN_SIGHT: number = 128;

    /**
     * 40
     */
    static readonly MAX_MONSTER_NUMBER_PER_SIGHT_REFRESH: number = 40;

    /**
     * 30, 每个场景最多10类怪物
     */
    static readonly MAX_MONSTER_TYPE_PER_SCENE: number = 30;

    /**
     * 3, 角色可拥有的月卡最大数
     */
    static readonly MAX_MONTH_CARD_COUNT: number = 3;

    /**
     * 40
     */
    static readonly MAX_MOUNT_IMAGE_NUMBER: number = 40;

    /**
     * 32000
     */
    static readonly MAX_MSGBODY_LENGTH: number = 32000;

    /**
     * 15, 最大聊天超链接个数
     */
    static readonly MAX_MSGDATA_NUM: number = 15;

    /**
     * 3, 跨服3V3奖励最大支持物品个数
     */
    static readonly MAX_MULTIPVP_THING_NUM: number = 3;

    /**
     * 120, 跨服3V3活动最低参与等级
     */
    static readonly MAX_MULTI_PVP_LEVEL: number = 120;

    /**
     * 32
     */
    static readonly MAX_NAME_LENGTH: number = 32;

    /**
     * 50, 附近玩家最大数
     */
    static readonly MAX_NEARROLEID_NUMBER: number = 50;

    /**
     * 50, 附近队伍最大数
     */
    static readonly MAX_NEARTEAMINFO_NUMBER: number = 50;

    /**
     * 10, 最近达成成就列表数量10
     */
    static readonly MAX_NEWES_ACHI_LIST_COUNT: number = 10;

    /**
     * 10
     */
    static readonly MAX_NODE_NUMBER_PER_QUEST: number = 10;

    /**
     * 6, 铭文试炼 普通色子最大次数
     */
    static readonly MAX_NORMAL_DICE_TIMES: number = 6;

    /**
     * 1024
     */
    static readonly MAX_NPCMENULIST_CAPTION_LENGTH: number = 1024;

    /**
     * 512
     */
    static readonly MAX_NPCMENU_CAPTION_LENGTH: number = 512;

    /**
     * 9, 一个商店，最多的额外随机物品个数
     */
    static readonly MAX_NPCSTORE_EXTHING_NUM: number = 9;

    /**
     * 10, 有随机物品商店的最多个数 
     */
    static readonly MAX_NPCSTORE_RANDOM_NUM: number = 10;

    /**
     * 50, 做多怪物坐标数
     */
    static readonly MAX_NPC_POSITION_IN_ONE_SCENCE: number = 50;

    /**
     * 150, 商店限购最多限制物品数量
     */
    static readonly MAX_NPC_STORE_SELLLIMIT_CONFIG_NUMBER: number = 150;

    /**
     * 12, 仙蕴宝藏商店最多呈现的物品数量
     */
    static readonly MAX_NPC_STORE_XIANYUN_CONFIG_NUMBER: number = 12;

    /**
     * 200, 单一商店，最多配置的随机物品数量 
     */
    static readonly MAX_NS_RANDOM_THING_NUM: number = 200;

    /**
     * 5, 随机物品商店每天最多自动刷新次数 
     */
    static readonly MAX_NS_REFRESH_TIMES_PER_DAY: number = 5;

    /**
     * 32, VIP一键完成最多给多少个道具
     */
    static readonly MAX_ONEKEYITEM_COUNT: number = 32;

    /**
     * 4, VIP一键完成配置最多给多少个道具
     */
    static readonly MAX_ONEKEYITEM_COUNT_CFG: number = 4;

    /**
     * 15, 单个进阶日活动，配置最大个数
     */
    static readonly MAX_ONE_STAGEDAY_COUNT: number = 15;

    /**
     * 20, 箱子奖励类型个数
     */
    static readonly MAX_OPEN_BOX_TYPE_NUM: number = 20;

    /**
     * 20, 做多有20个运营活动
     */
    static readonly MAX_OPERATING_ACT_COUNT: number = 20;

    /**
     * 30, 运营活动Rem1长度
     */
    static readonly MAX_OPERATING_ACT_REM1_LENGTH: number = 30;

    /**
     * 72, 运营活动Rem2长度
     */
    static readonly MAX_OPERATING_ACT_REM2_LENGTH: number = 72;

    /**
     * 8, 一个活动最多设置8个时间段
     */
    static readonly MAX_OPERATING_ACT_TIME_COUNT: number = 8;

    /**
     * 50, 宗派拍卖最多50个
     */
    static readonly MAX_PAIMAI_ITEM_COUNT_GUILD: number = 50;

    /**
     * 100, 宗派拍卖 一个玩家最多出价100次
     */
    static readonly MAX_PAIMAI_ITEM_COUNT_ROLE: number = 100;

    /**
     * 300, 直接拍卖最多300个
     */
    static readonly MAX_PAIMAI_ITEM_COUNT_WORLD: number = 300;

    /**
     * 20
     */
    static readonly MAX_PETNICKNAME_LENGTH: number = 20;

    /**
     * 80, 散仙上限
     */
    static readonly MAX_PET_NUMBER_IN_SIGHT: number = 80;

    /**
     * 20
     */
    static readonly MAX_PET_NUMBER_PER_SIGHT_REFRESH: number = 20;

    /**
     * 1, pet pvp目标最大个数
     */
    static readonly MAX_PET_PVP_TARGET_NUMBER: number = 1;

    /**
     * 2
     */
    static readonly MAX_PET_SHORTCUT_NUMBER: number = 2;

    /**
     * 100
     */
    static readonly MAX_PET_STRONG_LEVEL: number = 100;

    /**
     * 25, 散仙塑魂最高等级
     */
    static readonly MAX_PET_SUHUN_LEVEL: number = 25;

    /**
     * 3, 散仙做多携带的宝石个数
     */
    static readonly MAX_PET_TAKE_JEWEL_COUNT: number = 3;

    /**
     * 32, 副本大厅查询副本最大数目
     */
    static readonly MAX_PINHOME_LIST_NUM: number = 32;

    /**
     * 50, 副本排行榜最大数目
     */
    static readonly MAX_PINRANK_LIST_NUM: number = 50;

    /**
     * 7, 副本难度最高等级
     */
    static readonly MAX_PINSTANCE_DIFF_LEVEL: number = 7;

    /**
     * 10, 使用VIP体验技能副本最大层数
     */
    static readonly MAX_PINSTANCE_FLOOR_USE_VIP_SKILL: number = 10;

    /**
     * 10
     */
    static readonly MAX_PINSTANCE_TIMER_NUMBER: number = 10;

    /**
     * 3, 最佳通关本层玩家个数
     */
    static readonly MAX_PIN_FRONT_ROLE_NUM: number = 3;

    /**
     * 300, 最大副本层个数
     */
    static readonly MAX_PIN_LEVEL_NUM: number = 300;

    /**
     * 4, 爬塔副本个数
     */
    static readonly MAX_PIN_RANKLIST_NUM: number = 4;

    /**
     * 3, 占层排行榜个数
     */
    static readonly MAX_PIN_RANK_MOST_NUM: number = 3;

    /**
     * 200, 总排行榜个数
     */
    static readonly MAX_PIN_RANK_NUM: number = 200;

    /**
     * 16, 单个星级副本ID最大关卡
     */
    static readonly MAX_PIN_STAR_CUSTOMS: number = 16;

    /**
     * 3, 副本评级最高星级
     */
    static readonly MAX_PIN_STAR_LEVEL: number = 3;

    /**
     * 20, 开服-副本通关时间排行榜个数
     */
    static readonly MAX_PIN_TIME_ROLE_KF_NUM: number = 20;

    /**
     * 50, 副本通关时间排行榜个数
     */
    static readonly MAX_PIN_TIME_ROLE_NUM: number = 50;

    /**
     * 4, 最多面板上可PK的人的人个数
     */
    static readonly MAX_PKRANK_NUM: number = 4;

    /**
     * 3, 最多显示Top3的信息给封神台主面板
     */
    static readonly MAX_PKRANK_TOP: number = 3;

    /**
     * 100, PK模式:最大恶值
     */
    static readonly MAX_PK_VAL: number = 100;

    /**
     * 32, 平台信息的长度
     */
    static readonly MAX_PLAT_NAME_LENGTH: number = 32;

    /**
     * 20, 玩家等级排行榜的数组长度
     */
    static readonly MAX_PLAYER_LEVELRANK_NUM: number = 20;

    /**
     * 80
     */
    static readonly MAX_POSITION_NUMBER_IN_PATH: number = 80;

    /**
     * 15, 充值特惠活动奖励档数
     */
    static readonly MAX_PREFER_CHARGE_COUNT: number = 15;

    /**
     * 5, 充值特惠每天奖励档数
     */
    static readonly MAX_PREFER_CHARGE_DAY_COUNT: number = 5;

    /**
     * 3, 充值特惠最多连冲天数
     */
    static readonly MAX_PREFER_CHARGE_TOTAL_DAYS: number = 3;

    /**
     * 40, 最大预览奖励个数 入库
     */
    static readonly MAX_PREVIEW_REWARD_COUNT: number = 40;

    /**
     * 64
     */
    static readonly MAX_PRIMARYKEY_LENGTH: number = 64;

    /**
     * 172, 个人boss配置个数 MULTIPLAYER_BOSS_MAX_NUM       + MULTIPLAYER_BOSS_MAX_NUM
     */
    static readonly MAX_PRIVATE_BOSS_CFG_NUM: number = 172;

    /**
     * 100, 个人boss个数
     */
    static readonly MAX_PRIVATE_BOSS_NUM: number = 100;

    /**
     * 2, 职业类型个数
     */
    static readonly MAX_PROF_TYPE_NUM: number = 2;

    /**
     * 5
     */
    static readonly MAX_PROMPT_PARAMETER_NUMBER: number = 5;

    /**
     * 5, 最大属性丹种类个数
     */
    static readonly MAX_PROP_DRUG_NUM: number = 5;

    /**
     * 6, 每页显示最大单数
     */
    static readonly MAX_PSD_NUMBER_PER_PAGE: number = 6;

    /**
     * 5000, 跨服封神台最多记录5000个玩家封神台信息
     */
    static readonly MAX_PVP_CROSS_ROLE_COUNT: number = 5000;

    /**
     * 5, 单个玩家最大封神台挑战记录的日志条数
     */
    static readonly MAX_PVP_LOG_COUNT: number = 5;

    /**
     * 2000, 单个服最多记录2000个玩家封神台信息
     */
    static readonly MAX_PVP_ROLE_COUNT: number = 2000;

    /**
     * 2, 祈福暴击倍率
     */
    static readonly MAX_QIFU_BAOJI_MULTIPLE: number = 2;

    /**
     * 1, 银两祈福每日免费次数
     */
    static readonly MAX_QIFU_SLIVER_FREE_COUNT: number = 1;

    /**
     * 3, 祈福类型个数
     */
    static readonly MAX_QIFU_TYPE_COUNT: number = 3;

    /**
     * 3, 庆典消费排行个数
     */
    static readonly MAX_QINGDIAN_COMSUME_RANKLIST_NUM: number = 3;

    /**
     * 30, 全民嗨点活跃类最大个数
     */
    static readonly MAX_QMHD_ACTIVE_LIST_COUNT: number = 30;

    /**
     * 256, 回复的大小
     */
    static readonly MAX_QRY_ROLEDATA_SIZE: number = 256;

    /**
     * 5, 任务面板中最大任务数
     */
    static readonly MAX_QUESTPANEL_QUEST_NUM: number = 5;

    /**
     * 100
     */
    static readonly MAX_QUEST_NUMBER_PER_ROLE: number = 100;

    /**
     * 8, 单次随机角色信息最大次数
     */
    static readonly MAX_RAND_ROLE_INFO_NUM: number = 8;

    /**
     * 300, 最大榜单数量
     */
    static readonly MAX_RANKINFO_DISPNUMBER: number = 300;

    /**
     * 100, 每次从DB拉取的榜单最大长度 防止数据包太大导致下不来
     */
    static readonly MAX_RANKINFO_DISPNUMBER_ONECE: number = 100;

    /**
     * 6, 榜单列数组长度
     */
    static readonly MAX_RANKINFO_FIELD_NUMBER: number = 6;

    /**
     * 25, 榜单名字的长度，最多8汉字
     */
    static readonly MAX_RANKINFO_NAME_LENGTH: number = 25;

    /**
     * 500, 最大榜单数量
     */
    static readonly MAX_RANKINFO_STORENUMBER: number = 500;

    /**
     * 10, 封神台每次结算快照，记前10名
     */
    static readonly MAX_RANKSNAP_COUNT: number = 10;

    /**
     * 256
     */
    static readonly MAX_RECONFIRMINFO_LENGTH: number = 256;

    /**
     * 10, 招募队友,最多通知好友人数
     */
    static readonly MAX_RECRUITTEAM_ROLE_NUM: number = 10;

    /**
     * 3, 红包充值档次
     */
    static readonly MAX_REDBAG_LE_NUM: number = 3;

    /**
     * 20, 红包手气排行榜数量
     */
    static readonly MAX_REDBAG_RANK_NUM: number = 20;

    /**
     * 100, 红包发送记录列表
     */
    static readonly MAX_REDBAG_RECORD_NUM: number = 100;

    /**
     * 32, 充值流水号长度
     */
    static readonly MAX_REMARK_LENGTH: number = 32;

    /**
     * 20, 角色声望最大个数
     */
    static readonly MAX_REPUTATION_NUMBER: number = 20;

    /**
     * 100, 返回消息的长度
     */
    static readonly MAX_RETMSG_LEN: number = 100;

    /**
     * 10, 兑换rmb记录的最大角色数
     */
    static readonly MAX_RMBGETROLE_NUM: number = 10;

    /**
     * 11, 最多rmb战场宝箱怪物个数
     */
    static readonly MAX_RMBMONSTER_NUM: number = 11;

    /**
     * 5, rmb宝箱种类个数
     */
    static readonly MAX_RMB_BOX_NUM: number = 5;

    /**
     * 50, 一个国家最多50个机器人
     */
    static readonly MAX_ROBOT_COUNT_ONE_COUNTRY: number = 50;

    /**
     * 200
     */
    static readonly MAX_ROLEIDLIST_SIZE: number = 200;

    /**
     * 2
     */
    static readonly MAX_ROLESNAP_PER_FETCH: number = 2;

    /**
     * 300, 红颜系统装备最大个数
     */
    static readonly MAX_ROLE_BEAUTY_THINGOBJ_LIST_NUMBER: number = 300;

    /**
     * 17, 角色装备最大个数
     */
    static readonly MAX_ROLE_EQUIP_THINGOBJ_LIST_NUMBER: number = 17;

    /**
     * 500, 此等级修改, 需要确认系统参数表:参数_角色最高等级
     */
    static readonly MAX_ROLE_LEVEL: number = 500;

    /**
     * 80, 角色上限
     */
    static readonly MAX_ROLE_NUMBER_IN_SIGHT: number = 80;

    /**
     * 20
     */
    static readonly MAX_ROLE_NUMBER_PER_SIGHT_REFRESH: number = 20;

    /**
     * 100, 一个用户创建角色时系统为其分配的seq的范围，为[0, MAX_ROLE_NUMBER_PER_USER - 1]，其中的每个seq在所有的world上唯一
     */
    static readonly MAX_ROLE_NUMBER_PER_USER: number = 100;

    /**
     * 10, 一个用户在一个world上最多可以创建的角色个数
     */
    static readonly MAX_ROLE_NUMBER_PER_USER_WORLD: number = 10;

    /**
     * 3, 能发送的穿云箭的类型最大值
     */
    static readonly MAX_ROLE_SENDPOS_TYPE: number = 3;

    /**
     * 8, 限时秒杀数组最大个数
     */
    static readonly MAX_RUSH_PURCHASE_LIST_COUNT: number = 8;

    /**
     * 8, 场景信息八门信息数量
     */
    static readonly MAX_SCENEINFO_BM_INFO_NUM: number = 8;

    /**
     * 2, 场景信息最大支持按钮个数
     */
    static readonly MAX_SCENEINFO_BUTTON_NUM: number = 2;

    /**
     * 3, 场景信息排行榜最大支持列数量
     */
    static readonly MAX_SCENEINFO_FIELD_NUMBER: number = 3;

    /**
     * 15, 场景信息伤害排行最大数量
     */
    static readonly MAX_SCENEINFO_HURT_NUM: number = 15;

    /**
     * 10, 场景信息最大支持怪物头像个数
     */
    static readonly MAX_SCENEINFO_MONSTER_NUM: number = 10;

    /**
     * 15, 场景信息最大支持行
     */
    static readonly MAX_SCENEINFO_NUM: number = 15;

    /**
     * 50, 场景信息PVP排行版信息数量
     */
    static readonly MAX_SCENEINFO_PVP_RANK_NUM: number = 50;

    /**
     * 10, 场景信息排名数量
     */
    static readonly MAX_SCENEINFO_RANK_NUM: number = 10;

    /**
     * 15, 场景信息排行榜最大支持榜单数
     */
    static readonly MAX_SCENEINFO_RANK_NUMBER: number = 15;

    /**
     * 10, 场景信息最大支持角色头像个数
     */
    static readonly MAX_SCENEINFO_ROLE_NUM: number = 10;

    /**
     * 8, 场景信息最大支持物品个数
     */
    static readonly MAX_SCENEINFO_THING_NUM: number = 8;

    /**
     * 5, 场景信息任务数量
     */
    static readonly MAX_SCENETASK_NUM: number = 5;

    /**
     * 5, 场景信息任务奖励物品数量
     */
    static readonly MAX_SCENETASK_THING_NUM: number = 5;

    /**
     * 15
     */
    static readonly MAX_SCENE_FIELD_LINE: number = 15;

    /**
     * 4
     */
    static readonly MAX_SCENE_FIELD_NUMBER: number = 4;

    /**
     * 5
     */
    static readonly MAX_SCENE_GROUP_NUMBER: number = 5;

    /**
     * 2
     */
    static readonly MAX_SCENE_LASTFIELD_NUMBER: number = 2;

    /**
     * 15, 盛典宝箱排行配置最大数
     */
    static readonly MAX_SDBX_CFG_COUNT: number = 15;

    /**
     * 200, 盛典宝箱排行榜最大人数
     */
    static readonly MAX_SDBX_RANK_INFO_COUNT: number = 200;

    /**
     * 4, 镶嵌法器数量
     */
    static readonly MAX_SET_FAQI_COUNT: number = 4;

    /**
     * 4, 神兽数量
     */
    static readonly MAX_SHENSHOU_COUNT: number = 4;

    /**
     * 10, 守护神数量
     */
    static readonly MAX_SHIELDGOD_COUNT: number = 10;

    /**
     * 8, 角色技能栏槽位的最大个数
     */
    static readonly MAX_SHORTCUT_NUMBER: number = 8;

    /**
     * 12, 最多物品个数，要比最大邮件数多2个
     */
    static readonly MAX_SIMITEM_NUM: number = 12;

    /**
     * 10000, 跨服决斗场战区分组支持的最大服数
     */
    static readonly MAX_SINGLE_PVP_WAR_GROUP_WORLD_NUM: number = 10000;

    /**
     * 10
     */
    static readonly MAX_SKILL_CONSUME_NUMBER: number = 10;

    /**
     * 8
     */
    static readonly MAX_SKILL_EFFECT_NUMBER: number = 8;

    /**
     * 4, 能设置的羁绊技能个数
     */
    static readonly MAX_SKILL_FETTER_SET_NUMBER: number = 4;

    /**
     * 64
     */
    static readonly MAX_SKILL_NAME_LENGTH: number = 64;

    /**
     * 150, 玩家学会技能
     */
    static readonly MAX_SKILL_NUMBER_PER_ROLE: number = 150;

    /**
     * 2
     */
    static readonly MAX_SKILL_PROP_COUNT: number = 2;

    /**
     * 300, 技能公共CD间隔为300毫秒
     */
    static readonly MAX_SKILL_TIME_COMMON_CD_DIS: number = 300;

    /**
     * 64
     */
    static readonly MAX_SKILL_TIPS_TEXT_LENGTH: number = 64;

    /**
     * 9, 天宫宝境累计兑换次数
     */
    static readonly MAX_SKYLOTTERY_EXCHANGE_NUM: number = 9;

    /**
     * 12, 天宫宝境抽奖物品的最大数
     */
    static readonly MAX_SKYLOTTERY_ITEM_NUM: number = 12;

    /**
     * 50, 天宫宝境抽奖，查询抽奖记录，最多保留最新的20条,玩家本身记录
     */
    static readonly MAX_SKYLOTTERY_RECORD_NUM: number = 50;

    /**
     * 20, 天宫宝境抽奖，查询抽奖记录，全服记录20条
     */
    static readonly MAX_SKYLOTTERY_SERVER_RECORD_NUM: number = 20;

    /**
     * 3, 基础抽奖类型的的最大数
     */
    static readonly MAX_SKYLOTTERY_TYPE_NUM: number = 3;

    /**
     * 100, 天宫宝境抽奖仓库可存放物品最大数
     */
    static readonly MAX_SKYLOTTER_THINGOBJ_LIST_NUMBER: number = 100;

    /**
     * 2, 最大装备位套装类型数
     */
    static readonly MAX_SLOTSUIT_TYPE_NUM: number = 2;

    /**
     * 5, 装备位炼体神宝最大数量
     */
    static readonly MAX_SLOT_LSTB_NUM: number = 5;

    /**
     * 5, 可购买最大洗练属性数目
     */
    static readonly MAX_SLOT_WASH_BUY_NUM: number = 5;

    /**
     * 7, 每次最大洗练属性数目(主要是用作缓存和返回客户端)
     */
    static readonly MAX_SLOT_WASH_PROP_NUM: number = 7;

    /**
     * 3, 神魔遮天每个国家副本个数 3个
     */
    static readonly MAX_SMZT_COUNTRY_PINSTANCE_COUNT: number = 3;

    /**
     * 4, 神魔遮天怪物数量
     */
    static readonly MAX_SMZT_MONSTER_COUNT: number = 4;

    /**
     * 3, 神魔遮天 最多支持3周
     */
    static readonly MAX_SMZT_WEEK_COUNT: number = 3;

    /**
     * 70, 时限道具最大种类
     */
    static readonly MAX_SPECIALITEM_LIST_LENGTH: number = 70;

    /**
     * 5, 特殊特权最大等级
     */
    static readonly MAX_SPECIAL_PRI_LEVEL: number = 5;

    /**
     * 10
     */
    static readonly MAX_SPECIAL_SCORE_NUMBER: number = 10;

    /**
     * 100, 特殊同步暂时最多支持100个ID
     */
    static readonly MAX_SPEC_SYN_ID: number = 100;

    /**
     * 20, 春节充值配置最多个数
     */
    static readonly MAX_SPRING_CHARGE_CFG_NUMBER: number = 20;

    /**
     * 8, 进进阶日排行活动 物品最大数量
     */
    static readonly MAX_STAGEDAYRANK_CFG_ITEM_COUNT: number = 8;

    /**
     * 16, 进阶日保存是否领取状态的字节数 可以保存 8 * X 个
     */
    static readonly MAX_STAGEDAY_BYTE_COUNT: number = 16;

    /**
     * 100, 进阶日排行 最多100名
     */
    static readonly MAX_STAGEDAY_RANK_COUNT: number = 100;

    /**
     * 10, 进阶日排行 显示10名
     */
    static readonly MAX_STAGEDAY_RANK_SHOW: number = 10;

    /**
     * 7, 开服活动都是开启七天的
     */
    static readonly MAX_START_ACTIVITY_OPEN_DAYS: number = 7;

    /**
     * 60, 开服天数
     */
    static readonly MAX_START_DAYS: number = 60;

    /**
     * 20, 开服限购数量
     */
    static readonly MAX_START_LIMIT_SELL_NUM: number = 20;

    /**
     * 9, RMB战场开战人数
     */
    static readonly MAX_START_RMBZC_ROLE_NUM: number = 9;

    /**
     * 12, 星空宝库抽奖物品的最大数
     */
    static readonly MAX_STAR_LOTTERY_ITEM_NUM: number = 12;

    /**
     * 20, 星空宝库境抽奖，查询抽奖记录，最多保留最新的20条,玩家本身记录
     */
    static readonly MAX_STAR_LOTTERY_RECORD_NUM: number = 20;

    /**
     * 20, 星空宝库抽奖，查询抽奖记录，全服记录20条
     */
    static readonly MAX_STAR_LOTTERY_SERVER_RECORD_NUM: number = 20;

    /**
     * 100, 星空宝库抽奖仓库可存放物品最大数
     */
    static readonly MAX_STAR_LOTTER_THINGOBJ_LIST_NUMBER: number = 100;

    /**
     * 10, 星级洗炼最大个数
     */
    static readonly MAX_STAR_PROPS_NUMBER: number = 10;

    /**
     * 8, 每次星级批量洗的最大次数
     */
    static readonly MAX_STAR_WASH_PER_TIME: number = 8;

    /**
     * 64
     */
    static readonly MAX_STRING_INFO_SIZE: number = 64;

    /**
     * 600
     */
    static readonly MAX_STRING_PARAMETER_LENGTH: number = 600;

    /**
     * 4, 调查问卷答案最大数量
     */
    static readonly MAX_SURVEY_ANSWER_COUNT: number = 4;

    /**
     * 20, 调查问卷题目最大数量
     */
    static readonly MAX_SURVEY_ITEM_COUNT: number = 20;

    /**
     * 10, 散仙进阶日  累计充值消费各10个
     */
    static readonly MAX_SXJJ_MAX_COUNT: number = 10;

    /**
     * 128, 跨服同步内存大小
     */
    static readonly MAX_SYNDATA_SIZE: number = 128;

    /**
     * 10
     */
    static readonly MAX_SYSYTEM_SETTING_ID: number = 10;

    /**
     * 256
     */
    static readonly MAX_SYSYTEM_SETTING_NUM: number = 256;

    /**
     * 25, 返利大厅页签数目
     */
    static readonly MAX_TAB_STATUS_NUM: number = 25;

    /**
     * 50
     */
    static readonly MAX_TARGET_UNIT_NUMBER: number = 50;

    /**
     * 3, 天地玄门最多人数
     */
    static readonly MAX_TDXM_ROLE_COUNT: number = 3;

    /**
     * 30, 单个副本的最大记录数
     */
    static readonly MAX_TEAMNUMBER_ONE_PINSTANCE: number = 30;

    /**
     * 4
     */
    static readonly MAX_TEAM_MEMBER_NUMBER: number = 4;

    /**
     * 64
     */
    static readonly MAX_TEAM_NAME_LENGTH: number = 64;

    /**
     * 50, 队伍上限
     */
    static readonly MAX_TEAM_NUMBER_IN_SIGHT: number = 50;

    /**
     * 36, 组队特殊说明最大长度
     */
    static readonly MAX_TEAM_PLATFORM_LENGTH: number = 36;

    /**
     * 100, 天罡地煞最大配置个数
     */
    static readonly MAX_TGDS_CFG_COUNT: number = 100;

    /**
     * 250
     */
    static readonly MAX_THINGOBJ_LIST_NUMBER: number = 250;

    /**
     * 60
     */
    static readonly MAX_THING_NAME_LENGTH: number = 60;

    /**
     * 250, 最大容器物品数量
     */
    static readonly MAX_THING_NUMBER_PER_CONTAINER: number = 250;

    /**
     * 3, 天灵宝莲设施，操作类型的最多数量，目前是两类，1、元宝；2、战勋
     */
    static readonly MAX_TIANLING_OPERATE_TYPE_NUM: number = 3;

    /**
     * 12, 天珠镶嵌位置的最大数量
     */
    static readonly MAX_TIANZHU_POS_NUM: number = 12;

    /**
     * 24, 限时折扣最大物品数量
     */
    static readonly MAX_TIME_LIMIT_DISCOUNT_ITEM_COUNT: number = 24;

    /**
     * 31, 限时折扣最大物品循环天数
     */
    static readonly MAX_TIME_LIMIT_DISCOUNT_ITEM_DATE: number = 31;

    /**
     * 30, 天降福神采集宝箱的最大次数
     */
    static readonly MAX_TJFS_COLLECT_COUNT: number = 30;

    /**
     * 5, 特卖大酬宾每日最大售卖数量
     */
    static readonly MAX_TMDCB_THING_COUNT: number = 5;

    /**
     * 6, 最多一次跳跃点数Quest.Main.xml要同步改
     */
    static readonly MAX_TRANS_JUMP_COUNT: number = 6;

    /**
     * 5, zone上唯一称号个数
     */
    static readonly MAX_UNIQUE_TITLE_NUM: number = 5;

    /**
     * 64
     */
    static readonly MAX_UNIT_ATTRIBUTE_ID: number = 64;

    /**
     * 240, 视野单位总数
     */
    static readonly MAX_UNIT_NUMBER_IN_SIGHT: number = 240;

    /**
     * 64
     */
    static readonly MAX_USERNAME_LENGTH: number = 64;

    /**
     * 4, 翅膀，坐骑最多有效技能个数
     */
    static readonly MAX_VALID_SKILL_NUM: number = 4;

    /**
     * 10, 特权最大活跃等级
     */
    static readonly MAX_VIPPRI_HY_LEVEL: number = 10;

    /**
     * 100, 特权活跃最大数据记录
     */
    static readonly MAX_VIPPRI_HY_NUM: number = 100;

    /**
     * 20, VIP能保留活动、日常任务等限次类型的最大种类
     */
    static readonly MAX_VIP_DAILY_ACTIVITY_TYPE_NUM: number = 20;

    /**
     * 13, VIP最大等级
     */
    static readonly MAX_VIP_LEVEL: number = 13;

    /**
     * 3, VIP月卡最大等级
     */
    static readonly MAX_VIP_MONTH_LEVEL: number = 3;

    /**
     * 26, 世界杯最大比分数量
     */
    static readonly MAX_WCUP_BETSCOR_COUNT: number = 26;

    /**
     * 15, 世界杯冠军之路最大比赛数量
     */
    static readonly MAX_WCUP_CHAMPION_GAME_COUNT: number = 15;

    /**
     * 4, 世界杯冠军之路最大轮次数量
     */
    static readonly MAX_WCUP_CHAMPION_GAME_ROUND_COUNT: number = 4;

    /**
     * 64, 世界杯最大比赛数量
     */
    static readonly MAX_WCUP_GAME_COUNT: number = 64;

    /**
     * 2, 世界杯每轮最大比赛数量
     */
    static readonly MAX_WCUP_GAME_ONEC_COUNT: number = 2;

    /**
     * 32, 世界杯最大比赛轮数
     */
    static readonly MAX_WCUP_GAME_ROUND: number = 32;

    /**
     * 20, 世界杯竞猜排行榜数量
     */
    static readonly MAX_WCUP_RANK_COUNT: number = 20;

    /**
     * 40, 最大翅膀形象个数
     */
    static readonly MAX_WING_IMAGE_NUMBER: number = 40;

    /**
     * 2, 一天最多2次世界杯比赛
     */
    static readonly MAX_WORLD_CUP_COUNT_ONEDAY: number = 2;

    /**
     * 2000, 世界杯一场比赛投注作对1000人
     */
    static readonly MAX_WORLD_CUP_ROLE_COUNT: number = 2000;

    /**
     * 250, IDIP那边最大包为25KB，所以查询所有world，只能支持250个，后面超过后，则需要分页
     */
    static readonly MAX_WORLD_IDIP_SUPPORT: number = 250;

    /**
     * 500, 世界拍卖 最多允许有300个道具同时卖
     */
    static readonly MAX_WORLD_PAIMAI_ACT_ITEM_CNT: number = 500;

    /**
     * 50, 世界拍卖 每个活动参与分成玩家个数
     */
    static readonly MAX_WORLD_PAIMAI_ACT_ROLE_CNT: number = 50;

    /**
     * 15, 世界拍卖 支持同时有15个活动进行
     */
    static readonly MAX_WORLD_PAIMAI_ACT_RUN_CNT: number = 15;

    /**
     * 65000, 这个不能改小，有50000服啊
     */
    static readonly MAX_WORLD_PER_CLUSTER: number = 65000;

    /**
     * 64, 武缘副本最大层数
     */
    static readonly MAX_WYFB_DIFF_LEVEL: number = 64;

    /**
     * 8, 武缘副本通关状态记录数量
     */
    static readonly MAX_WYFB_STAUTS_NUM: number = 8;

    /**
     * 3
     */
    static readonly MAX_WYYZ_BUY_BUFF_COUNT: number = 3;

    /**
     * 3, 武缘远征 出战武缘个数
     */
    static readonly MAX_WYYZ_FIGHT_PET_COUNT: number = 3;

    /**
     * 30, 武缘远征最多30关
     */
    static readonly MAX_WYYZ_LEVEL: number = 30;

    /**
     * 2
     */
    static readonly MAX_WYYZ_REWARD_ITEM_COUNT: number = 2;

    /**
     * 30, 星愿活动经验雨间隔
     */
    static readonly MAX_XINGYUAN_INTERVAL: number = 30;

    /**
     * 3600, 星愿活动经验雨时长
     */
    static readonly MAX_XINGYUAN_TIME: number = 3600;

    /**
     * 4, 幸运转盘可领奖列表
     */
    static readonly MAX_XYZP_GIFT_LIST_COUNT: number = 4;

    /**
     * 12, 幸运转盘最多物品个数
     */
    static readonly MAX_XYZP_ITEM_TYPE_COUNT: number = 12;

    /**
     * 50, 幸运转盘大奖记录
     */
    static readonly MAX_XYZP_ROLE_COUNT: number = 50;

    /**
     * 12, 血战封魔BOSS个数
     */
    static readonly MAX_XZFM_BOSS_COUNT: number = 12;

    /**
     * 3, 血战封魔 每种宝箱最大捡取3个
     */
    static readonly MAX_XZFM_BX_PICK_CNT: number = 3;

    /**
     * 3, 血战封魔 副本有3层
     */
    static readonly MAX_XZFM_LAYER_COUNT: number = 3;

    /**
     * 3, 血战封魔后上周前3
     */
    static readonly MAX_XZFM_PRE_RANK_COUNT: number = 3;

    /**
     * 10, 血战封魔排行个数
     */
    static readonly MAX_XZFM_RANK_COUNT: number = 10;

    /**
     * 100, 血战封魔后台排行个数
     */
    static readonly MAX_XZFM_RANK_REAL_COUNT: number = 100;

    /**
     * 5, 血战封魔奖励个数
     */
    static readonly MAX_XZFM_REWARD_COUNT: number = 5;

    /**
     * 3, 血战封魔任务 最大个数
     */
    static readonly MAX_XZFM_TASK_COUNT: number = 3;

    /**
     * 10, 血战封魔每日任务最大个数
     */
    static readonly MAX_XZFM_TASK_DAILY_COUNT: number = 10;

    /**
     * 20, 一本万利活动，配置最大个数
     */
    static readonly MAX_YI_BEN_WAN_LI_CFG_NUM: number = 20;

    /**
     * 2, 元神战场最多2个阵营
     */
    static readonly MAX_YSZC_CAMP_NUM: number = 2;

    /**
     * 100, 最多可购买次数，需求变了等同角色数
     */
    static readonly MAX_YUNGGOU_ROLE_NUM: number = 100;

    /**
     * 60, 一元夺宝最多记录参与人数
     */
    static readonly MAX_YYDB_ROLE_NUM: number = 60;

    /**
     * 10, 招财猫最多10条记录
     */
    static readonly MAX_ZCM_RECORD_COUNT: number = 10;

    /**
     * 100, 直购礼包最大配置个数
     */
    static readonly MAX_ZGLBCFG_NUM: number = 100;

    /**
     * 4, 直购礼包充值档次
     */
    static readonly MAX_ZGLB_CONDITION_NUM: number = 4;

    /**
     * 3, 直购礼包配置 物品个数
     */
    static readonly MAX_ZGLB_ITEM_COUNT: number = 3;

    /**
     * 4, 宗门争霸图标开启4天
     */
    static readonly MAX_ZMZB_OPEN_DAYS: number = 4;

    /**
     * 5
     */
    static readonly MAX_ZONE_PER_WORLD: number = 5;

    /**
     * 6, 中元节充值档数
     */
    static readonly MAX_ZYJCZFL_CHARGE_COUNT: number = 6;

    /**
     * 3, 阵营战最多3个阵营
     */
    static readonly MAX_ZYZ_CAMP_NUM: number = 3;

    /**
     * 1, 阵营战每个阵营最多1个角色在战场内佩戴称号展示
     */
    static readonly MAX_ZYZ_CAMP_TOP_NUM: number = 1;

    /**
     * 7, 至尊皇城城市个数7个
     */
    static readonly MAX_ZZHC_CITY_COUNT: number = 7;

    /**
     * 4, 至尊皇城 护城4个
     */
    static readonly MAX_ZZHC_FU_CITY_COUNT: number = 4;

    /**
     * 1, 至尊皇城 主城1个
     */
    static readonly MAX_ZZHC_MAIN_CITY_COUNT: number = 1;

    /**
     * 2, 至尊皇城 卫城2个
     */
    static readonly MAX_ZZHC_WEI_CITY_COUNT: number = 2;

    /**
     * 100, 至尊争夺排行个数
     */
    static readonly MAX_ZZZD_CHARGE_RANKLIST_NUM: number = 100;

    /**
     * 10, 至尊争夺本服 表格最大配置数
     */
    static readonly MAX_ZZZD_CHARGE_RANK_COUNT: number = 10;

    /**
     * 1, 继续副本的操作由脚本来处理
     */
    static readonly MENU_CONTINUEPINSTANCE_INDEX: number = 1;

    /**
     * 103, 确认挖掘的操作由脚本处理
     */
    static readonly MENU_DIGOK_INDEX: number = 103;

    /**
     * 248, 地宫副本菜单命令max
     */
    static readonly MENU_DIGONG_BOSS_MAX: number = 248;

    /**
     * 240, 地宫副本菜单命令min
     */
    static readonly MENU_DIGONG_BOSS_MIN: number = 240;

    /**
     * 103, 副本内排行榜按钮
     */
    static readonly MENU_PINSTANCE_RANK: number = 103;

    /**
     * 102, 放弃挖掘的操作由脚本处理
     */
    static readonly MENU_QUITDIG_INDEX: number = 102;

    /**
     * 100, 离开副本 按钮的索引
     */
    static readonly MENU_QUITPINSTANCE_INDEX: number = 100;

    /**
     * 2, 结算退出按钮索引
     */
    static readonly MENU_SUMMARYEXIT_INDEX: number = 2;

    /**
     * 102, 诅咒宝箱点击确认按钮
     */
    static readonly MENU_TREASURE_CONFIRM: number = 102;

    /**
     * 70, 宗派创建，角色等级限制
     */
    static readonly MIN_CREATE_GUILD_ROLE_LEVEL: number = 70;

    /**
     * 1, 宗派创建，VIP最低等级限制
     */
    static readonly MIN_CREATE_GUILD_VIP_LEVEL: number = 1;

    /**
     * 25, 恶人临界值25
     */
    static readonly MIN_EVIL_VAL: number = 25;

    /**
     * 900, 最小的聚元时装模型ID
     */
    static readonly MIN_JUYUAN_IMAGE_ID: number = 900;

    /**
     * 3, 设置宗派公告的宗派最低等级限制
     */
    static readonly MIN_SETINFO_GUILD_LEVEL: number = 3;

    /**
     * 2, 宗门秘镜，参与宗门最低等级
     */
    static readonly MIN_ZPFM_ACT_GUILD_LEVEL: number = 2;

    /**
     * 60165013, 手机注册礼包掉落方案ID
     */
    static readonly MOBILE_MAIL_DROP_ID: number = 60165013;

    /**
     * 1, 元宝绑元比率
     */
    static readonly MONEY_YUANBAO_RATE: number = 1;

    /**
     * 2, 怪物int型的属性改变
     */
    static readonly MONSTER_ATTR_CHANGE_INT: number = 2;

    /**
     * 1, 怪物名字改变
     */
    static readonly MONSTER_ATTR_CHANGE_NAME: number = 1;

    /**
     * 3, 怪物称号改变
     */
    static readonly MONSTER_ATTR_CHANGE_TITLE: number = 3;

    /**
     * 4, 怪物的pvp阵营信息
     */
    static readonly MONSTER_ATTR_INT_ARMYID: number = 4;

    /**
     * 1, 怪物国家属性
     */
    static readonly MONSTER_ATTR_INT_COUNTRY: number = 1;

    /**
     * 3, 怪物在rmb战场剩余时间
     */
    static readonly MONSTER_ATTR_INT_RMBBOXTIME: number = 3;

    /**
     * 2, 怪物ScriptTagID
     */
    static readonly MONSTER_ATTR_INT_SCRIPTTAGID: number = 2;

    /**
     * 30, 怪物每天击杀计数统计个数
     */
    static readonly MONSTER_COUNT_KILL_NUM: number = 30;

    /**
     * 64, 怪物每天杀char的个数*8=512
     */
    static readonly MONSTER_DAILY_KILL_NUM: number = 64;

    /**
     * 64, 怪物首杀char的个数*8=512
     */
    static readonly MONSTER_FIRST_KILL_NUM: number = 64;

    /**
     * 0, 普通怪物
     */
    static readonly MONSTER_SIGH_TYPE_NONE: number = 0;

    /**
     * 2, 宠物怪
     */
    static readonly MONSTER_SIGH_TYPE_PET: number = 2;

    /**
     * 1, 人形怪
     */
    static readonly MONSTER_SIGH_TYPE_ROLE: number = 1;

    /**
     * 2, 怪物变身成散仙形象
     */
    static readonly MONSTER_TO_PET: number = 2;

    /**
     * 1, 怪物变身成角色形象
     */
    static readonly MONSTER_TO_ROLE: number = 1;

    /**
     * 1, 打开月卡面板
     */
    static readonly MONTH_CARD_OPEN_PANEL: number = 1;

    /**
     * 3, 坐骑资质个数
     */
    static readonly MOUNT_ATTR_MAX: number = 3;

    /**
     * 5, 移动_通过命令让前台自己寻路移动
     */
    static readonly MOVE_MODE_CLIENT: number = 5;

    /**
     * 4, 移动_通过命令让主角移动
     */
    static readonly MOVE_MODE_CMD: number = 4;

    /**
     * 3, 移动_答题踢飞效果
     */
    static readonly MOVE_MODE_DATI: number = 3;

    /**
     * 0, 移动_无效果
     */
    static readonly MOVE_MODE_NONE: number = 0;

    /**
     * 2, 移动_拉扯效果
     */
    static readonly MOVE_MODE_PULL: number = 2;

    /**
     * 1, 移动_震退效果
     */
    static readonly MOVE_MODE_PUSH_SHOCK: number = 1;

    /**
     * 5, 活动
     */
    static readonly MSGDATATYPE_ACTIVITY: number = 5;

    /**
     * 95, 步步高升(金玉满堂) 面板链接
     */
    static readonly MSGDATATYPE_BBGS_PANEL: number = 95;

    /**
     * 70, 武缘升阶 面板链接
     */
    static readonly MSGDATATYPE_BEAUTY_STAGEUP: number = 70;

    /**
     * 83, 百服庆典抽奖面板链接
     */
    static readonly MSGDATATYPE_BFQD_DRAW: number = 83;

    /**
     * 93, 充值红包 面板链接
     */
    static readonly MSGDATATYPE_CHARGE_REDBAG: number = 93;

    /**
     * 35, 打开充值奖励领取面板
     */
    static readonly MSGDATATYPE_CHARGE_REWARD: number = 35;

    /**
     * 17, 聊天的骰子
     */
    static readonly MSGDATATYPE_CHAT_DICE: number = 17;

    /**
     * 106, 打开首冲超级返利面板
     */
    static readonly MSGDATATYPE_CJFL: number = 106;

    /**
     * 18, 国家聊天发送穿云箭
     */
    static readonly MSGDATATYPE_COUNTRY_CHAT_SEND_POS: number = 18;

    /**
     * 92, 猎魂 面板链接
     */
    static readonly MSGDATATYPE_CRYSTAL: number = 92;

    /**
     * 36, 打开每日首充礼包面板
     */
    static readonly MSGDATATYPE_DAILY_CHARGE: number = 36;

    /**
     * 32, 星星点灯 面板链接
     */
    static readonly MSGDATATYPE_DDL: number = 32;

    /**
     * 24, 宝石镶嵌 面板链接
     */
    static readonly MSGDATATYPE_DIAMONDINSERT: number = 24;

    /**
     * 25, 宝石合成 面板链接
     */
    static readonly MSGDATATYPE_DIAMONDMERGE: number = 25;

    /**
     * 53, 时装升级 面板链接
     */
    static readonly MSGDATATYPE_DRESSLEVELUP: number = 53;

    /**
     * 52, 时装属性链接
     */
    static readonly MSGDATATYPE_DRESSPROP: number = 52;

    /**
     * 87, 端午活动 面板链接
     */
    static readonly MSGDATATYPE_DUANWU: number = 87;

    /**
     * 21, 进入宗派领地 超链接
     */
    static readonly MSGDATATYPE_ENTER_GUILD_ZONE: number = 21;

    /**
     * 90, 装备炼器 面板链接
     */
    static readonly MSGDATATYPE_EQUIPLQ_PANEL: number = 90;

    /**
     * 11, 装备强化 面板链接
     */
    static readonly MSGDATATYPE_EQUIPSTRENG: number = 11;

    /**
     * 57, 装备塑魂
     */
    static readonly MSGDATATYPE_EQUIPSUHUN: number = 57;

    /**
     * 14, 装备升阶 面板链接
     */
    static readonly MSGDATATYPE_EQUIPUPCOLOR: number = 14;

    /**
     * 23, 装备升级 面板链接
     */
    static readonly MSGDATATYPE_EQUIPUPLEVEL: number = 23;

    /**
     * 13, 装备洗炼 面板链接
     */
    static readonly MSGDATATYPE_EQUIPWASH: number = 13;

    /**
     * 81, 阵法 紫极魔瞳 面板链接 
     */
    static readonly MSGDATATYPE_FAZHEN: number = 81;

    /**
     * 68, 打开封魔塔面板链接
     */
    static readonly MSGDATATYPE_FENGMOTA_PANEL: number = 68;

    /**
     * 34, 打开首冲礼包面板
     */
    static readonly MSGDATATYPE_FIRST_CHARGE: number = 34;

    /**
     * 46, 打开封魔塔BOSS面板链接
     */
    static readonly MSGDATATYPE_FMT_BOSS_PANEL: number = 46;

    /**
     * 15, 招揽散仙 面板链接
     */
    static readonly MSGDATATYPE_GETPET: number = 15;

    /**
     * 73, 个人竞技场
     */
    static readonly MSGDATATYPE_GRJJC: number = 73;

    /**
     * 40, 开服团购面板
     */
    static readonly MSGDATATYPE_GROUPBUY: number = 40;

    /**
     * 7, 创建宗派 面板链接
     */
    static readonly MSGDATATYPE_GUILDCREATE: number = 7;

    /**
     * 86, 跨服宗门争霸战 打开宗门争霸战面板链接
     */
    static readonly MSGDATATYPE_GUILDCROSSPVP_PANEL: number = 86;

    /**
     * 75, 宗门争霸战 打开宗门争霸战面板链接
     */
    static readonly MSGDATATYPE_GUILDPVP_PANEL: number = 75;

    /**
     * 19, 宗派聊天发送穿云箭
     */
    static readonly MSGDATATYPE_GUILD_CHAT_SEND_POS: number = 19;

    /**
     * 41, 宗派集结令，玩家发送坐标
     */
    static readonly MSGDATATYPE_GUILD_JIJIELING: number = 41;

    /**
     * 101, 宗派探矿
     */
    static readonly MSGDATATYPE_GUILD_TREASURE_HUNT: number = 101;

    /**
     * 4, 宗派寻宝
     */
    static readonly MSGDATATYPE_GUILD_XB: number = 4;

    /**
     * 8, 国运求救
     */
    static readonly MSGDATATYPE_GUOYUN: number = 8;

    /**
     * 82, 合服合服累计充值面板链接
     */
    static readonly MSGDATATYPE_HFLJCZ: number = 82;

    /**
     * 55, 欢乐翻牌
     */
    static readonly MSGDATATYPE_HLFP_INFO: number = 55;

    /**
     * 56, 欢乐探宝
     */
    static readonly MSGDATATYPE_HLTB_JOIN: number = 56;

    /**
     * 94, 欢乐转盘
     */
    static readonly MSGDATATYPE_HLZP_JOIN: number = 94;

    /**
     * 110, 魂骨封装
     */
    static readonly MSGDATATYPE_HUNGU_FZ: number = 110;

    /**
     * 112, 魂力 面板
     */
    static readonly MSGDATATYPE_HUNLI_PANEL: number = 112;

    /**
     * 31, 幻化 面板链接
     */
    static readonly MSGDATATYPE_IMAGE: number = 31;

    /**
     * 48, 打开聚宝盆面板
     */
    static readonly MSGDATATYPE_JBP_INFO: number = 48;

    /**
     * 111, 玄天功 面板
     */
    static readonly MSGDATATYPE_JIUXING: number = 111;

    /**
     * 49, 打开积善成礼面板
     */
    static readonly MSGDATATYPE_JSCL_INFO: number = 49;

    /**
     * 71, 聚源突破 面板链接
     */
    static readonly MSGDATATYPE_JUYUAN_STAGEUP: number = 71;

    /**
     * 45, 开服累计充值面板链接
     */
    static readonly MSGDATATYPE_KFLJCZ: number = 45;

    /**
     * 88, 连充活动 面板链接
     */
    static readonly MSGDATATYPE_LCACT_PANEL: number = 88;

    /**
     * 104, 等级礼包
     */
    static readonly MSGDATATYPE_LEVEL_GIFT: number = 104;

    /**
     * 91, 老虎机 面板链接
     */
    static readonly MSGDATATYPE_LHJ_JOIN: number = 91;

    /**
     * 47, 累计充值面板链接
     */
    static readonly MSGDATATYPE_LJCZ: number = 47;

    /**
     * 103, 七天登录 面板链接
     */
    static readonly MSGDATATYPE_LOGIN_GIFT: number = 103;

    /**
     * 113, 魔方 控鹤擒龙
     */
    static readonly MSGDATATYPE_MAGIC: number = 113;

    /**
     * 44, 结婚面板链接
     */
    static readonly MSGDATATYPE_MARRY: number = 44;

    /**
     * 150, 最大消息类型
     */
    static readonly MSGDATATYPE_MAX: number = 150;

    /**
     * 108, 跨服决斗场决赛
     */
    static readonly MSGDATATYPE_MC_FINAL: number = 108;

    /**
     * 43, 合服活动充值回馈面板链接
     */
    static readonly MSGDATATYPE_MERGE_CHARGE: number = 43;

    /**
     * 0, 占位
     */
    static readonly MSGDATATYPE_MIN: number = 0;

    /**
     * 42, 魔帝秘藏 面板链接
     */
    static readonly MSGDATATYPE_MJLOTTERY: number = 42;

    /**
     * 64, 打开月卡激活面板
     */
    static readonly MSGDATATYPE_MONTHVIP_PANEL: number = 64;

    /**
     * 59, 坐骑塑魂
     */
    static readonly MSGDATATYPE_MOUNTSUHUN: number = 59;

    /**
     * 16, 坐骑升级 面板链接
     */
    static readonly MSGDATATYPE_MOUNTUPLEVEL: number = 16;

    /**
     * 97, 打开面板
     */
    static readonly MSGDATATYPE_OPEN_PANEL: number = 97;

    /**
     * 3, 散仙
     */
    static readonly MSGDATATYPE_PET: number = 3;

    /**
     * 39, 散仙宝石合成
     */
    static readonly MSGDATATYPE_PETJWELMERGE: number = 39;

    /**
     * 28, 散仙强化 面板链接
     */
    static readonly MSGDATATYPE_PETSTRONG: number = 28;

    /**
     * 61, 副本信息
     */
    static readonly MSGDATATYPE_PINSTANCE_INFO: number = 61;

    /**
     * 10, 某个场景的坐标 面板链接
     */
    static readonly MSGDATATYPE_POSITION: number = 10;

    /**
     * 22, 寄售吆喝
     */
    static readonly MSGDATATYPE_PPSTORECALL: number = 22;

    /**
     * 2, 道具, 查表查不出来，通过guid， thingid
     */
    static readonly MSGDATATYPE_PROP: number = 2;

    /**
     * 66, 打开祈福面板链接
     */
    static readonly MSGDATATYPE_QIFU_PANEL: number = 66;

    /**
     * 60, 跨服副本，招募队友
     */
    static readonly MSGDATATYPE_RECRUIT_TEAM_INFO: number = 60;

    /**
     * 72, 红包
     */
    static readonly MSGDATATYPE_REDBAG: number = 72;

    /**
     * 20, 角色信息
     */
    static readonly MSGDATATYPE_ROLE_INFO: number = 20;

    /**
     * 62, 终生首冲领取
     */
    static readonly MSGDATATYPE_ROLE_SC_GET: number = 62;

    /**
     * 107, 限时秒杀
     */
    static readonly MSGDATATYPE_RUSH_PURCHASE: number = 107;

    /**
     * 63, 首冲领取
     */
    static readonly MSGDATATYPE_SC_GET: number = 63;

    /**
     * 27, 送花 面板链接
     */
    static readonly MSGDATATYPE_SEND_FLOWER: number = 27;

    /**
     * 30, 引导进入试练之地
     */
    static readonly MSGDATATYPE_SHILIANZHIDI: number = 30;

    /**
     * 105, 技能
     */
    static readonly MSGDATATYPE_SKILL: number = 105;

    /**
     * 9, 魔帝宝库 面板链接
     */
    static readonly MSGDATATYPE_SKYLOTTERY: number = 9;

    /**
     * 26, 星石合成 面板链接
     */
    static readonly MSGDATATYPE_STARMERGE: number = 26;

    /**
     * 109, 星斗宝库
     */
    static readonly MSGDATATYPE_STAR_LOTTERY: number = 109;

    /**
     * 6, 物品, 查表能查出来，通过thingid
     */
    static readonly MSGDATATYPE_THING: number = 6;

    /**
     * 65, 打开宝库商店
     */
    static readonly MSGDATATYPE_TREASURE_PANEL: number = 65;

    /**
     * 50, 天地玄门，我也要参加
     */
    static readonly MSGDATATYPE_TXXM_JOIN: number = 50;

    /**
     * 96, 天下兑换 面板链接
     */
    static readonly MSGDATATYPE_TX_EXCHANGE: number = 96;

    /**
     * 37, URL超链接
     */
    static readonly MSGDATATYPE_URL: number = 37;

    /**
     * 33, 打开VIP面板
     */
    static readonly MSGDATATYPE_VIP_PANEL: number = 33;

    /**
     * 98, 语音消息信息
     */
    static readonly MSGDATATYPE_VOICE_INFO: number = 98;

    /**
     * 12, 翅膀强化 面板链接
     */
    static readonly MSGDATATYPE_WINGSTRENG: number = 12;

    /**
     * 58, 翅膀塑魂
     */
    static readonly MSGDATATYPE_WINGSUHUN: number = 58;

    /**
     * 85, 极品兑换 面板链接
     */
    static readonly MSGDATATYPE_WJ_EXCHANGE: number = 85;

    /**
     * 74, 世界boss挖宝，打开世界boss面板链接
     */
    static readonly MSGDATATYPE_WORLDBOSS_DIG: number = 74;

    /**
     * 67, 打开世界boss面板链接
     */
    static readonly MSGDATATYPE_WORLDBOSS_PANEL: number = 67;

    /**
     * 69, 神器升级 面板链接
     */
    static readonly MSGDATATYPE_WUHUNUPLEVEL: number = 69;

    /**
     * 89, 限时云购 面板链接
     */
    static readonly MSGDATATYPE_XSYG_PANEL: number = 89;

    /**
     * 29, 引导进入虚空战场
     */
    static readonly MSGDATATYPE_XUKONGZHANCHANG: number = 29;

    /**
     * 54, 幸运转盘
     */
    static readonly MSGDATATYPE_XYZP_JOIN: number = 54;

    /**
     * 102, 血战封魔 呼救
     */
    static readonly MSGDATATYPE_XZFM_HELP: number = 102;

    /**
     * 51, 一本万利，我也要领取
     */
    static readonly MSGDATATYPE_YBWL_JOIN: number = 51;

    /**
     * 84, 足迹 鬼影迷踪 面板链接
     */
    static readonly MSGDATATYPE_ZUJI: number = 84;

    /**
     * 100, 至尊皇城 宗门领地被攻击
     */
    static readonly MSGDATATYPE_ZZHC_CITY_ATTACK: number = 100;

    /**
     * 99, 至尊皇城 面板链接
     */
    static readonly MSGDATATYPE_ZZHC_PANNEL: number = 99;

    /**
     * 38, 至尊争夺超链接
     */
    static readonly MSGDATATYPE_ZZZD: number = 38;

    /**
     * 72, 本地多人boss最大数量
     */
    static readonly MULTIPLAYER_BOSS_MAX_NUM: number = 72;

    /**
     * 6, 本地多人每个副本boss数量
     */
    static readonly MULTIPLAYER_ONE_FLOOR_BOSS_NUM: number = 6;

    /**
     * 2, 旗下
     */
    static readonly MountRide_Down: number = 2;

    /**
     * 1, 骑乘
     */
    static readonly MountRide_Up: number = 1;

    /**
     * 4066, AAS整点通知
     */
    static readonly MsgID_AASAddict_Notify: number = 4066;

    /**
     * 4070, AAS玩家实名验证通知
     */
    static readonly MsgID_AASIdentity_Notify: number = 4070;

    /**
     * 4068, AAS玩家实名验证请求
     */
    static readonly MsgID_AASIdentity_Record_Request: number = 4068;

    /**
     * 4069, AAS玩家实名验证回复
     */
    static readonly MsgID_AASIdentity_Record_Response: number = 4069;

    /**
     * 4067, AAS状态设置，全面相信客户端
     */
    static readonly MsgID_AASSetStatus_Request: number = 4067;

    /**
     * 801
     */
    static readonly MsgID_Account_CreateRole_Request: number = 801;

    /**
     * 802
     */
    static readonly MsgID_Account_CreateRole_Response: number = 802;

    /**
     * 803
     */
    static readonly MsgID_Account_DeleteRole_Request: number = 803;

    /**
     * 804
     */
    static readonly MsgID_Account_DeleteRole_Response: number = 804;

    /**
     * 805
     */
    static readonly MsgID_Account_ListRole_Request: number = 805;

    /**
     * 806
     */
    static readonly MsgID_Account_ListRole_Response: number = 806;

    /**
     * 811
     */
    static readonly MsgID_Account_ModifyRole_Request: number = 811;

    /**
     * 812
     */
    static readonly MsgID_Account_ModifyRole_Response: number = 812;

    /**
     * 452, 一条成就改变通知
     */
    static readonly MsgID_AchiChange_Notify: number = 452;

    /**
     * 450, 拉取成就数据请求 登陆时候拉一次
     */
    static readonly MsgID_AchiGet_Request: number = 450;

    /**
     * 451, 拉取成就数据响应
     */
    static readonly MsgID_AchiGet_Response: number = 451;

    /**
     * 5120, 活动数据变化
     */
    static readonly MsgID_ActivityDataChange_Notify: number = 5120;

    /**
     * 5030, 活动面板打开通知
     */
    static readonly MsgID_ActivityPannel_Notify: number = 5030;

    /**
     * 5029, 活动状态变化通知
     */
    static readonly MsgID_ActivityStatusChange_Notify: number = 5029;

    /**
     * 255, 玩家问问题请求
     */
    static readonly MsgID_AddGMQA_Request: number = 255;

    /**
     * 256, 玩家问问题回复
     */
    static readonly MsgID_AddGMQA_Response: number = 256;

    /**
     * 125, 后台给玩家主动学上某技能通知
     */
    static readonly MsgID_AddSpecSkill_Notify: number = 125;

    /**
     * 5160, 登陆尾随数据包
     */
    static readonly MsgID_AfterLoginData_Notify: number = 5160;

    /**
     * 130, 自动战斗设置通知
     */
    static readonly MsgID_AutoBattleSetting_Notify: number = 130;

    /**
     * 129, 自动战斗设置请求
     */
    static readonly MsgID_AutoBattleSetting_Request: number = 129;

    /**
     * 4102, 时装改变
     */
    static readonly MsgID_AvatarChange_Notify: number = 4102;

    /**
     * 406
     */
    static readonly MsgID_BagPassword_Request: number = 406;

    /**
     * 407
     */
    static readonly MsgID_BagPassword_Response: number = 407;

    /**
     * 4854, 宝典功能请求
     */
    static readonly MsgID_BaoDian_Request: number = 4854;

    /**
     * 4855, 宝典功能回复
     */
    static readonly MsgID_BaoDian_Response: number = 4855;

    /**
     * 160, 被杀通知
     */
    static readonly MsgID_BeKilled_Notify: number = 160;

    /**
     * 264, 红颜激活请求
     */
    static readonly MsgID_BeautyActive_Request: number = 264;

    /**
     * 265, 红颜激活响应
     */
    static readonly MsgID_BeautyActive_Response: number = 265;

    /**
     * 262, 红颜出战请求
     */
    static readonly MsgID_BeautyBattle_Request: number = 262;

    /**
     * 263, 红颜出战响应
     */
    static readonly MsgID_BeautyBattle_Response: number = 263;

    /**
     * 260, 红颜升阶请求
     */
    static readonly MsgID_BeautyStageUp_Request: number = 260;

    /**
     * 261, 红颜升阶响应
     */
    static readonly MsgID_BeautyStageUp_Response: number = 261;

    /**
     * 474, 红颜觉醒请求
     */
    static readonly MsgID_Beauty_Awake_Request: number = 474;

    /**
     * 475, 红颜觉醒响应
     */
    static readonly MsgID_Beauty_Awake_Response: number = 475;

    /**
     * 266, 红颜吃丹药请求
     */
    static readonly MsgID_Beauty_Drug_Request: number = 266;

    /**
     * 267, 红颜吃丹药响应
     */
    static readonly MsgID_Beauty_Drug_Response: number = 267;

    /**
     * 470, 红颜聚神请求
     */
    static readonly MsgID_Beauty_JuShen_Request: number = 470;

    /**
     * 471, 红颜聚神响应
     */
    static readonly MsgID_Beauty_JuShen_Response: number = 471;

    /**
     * 268, 红颜使用功法书请求
     */
    static readonly MsgID_Beauty_KF_Request: number = 268;

    /**
     * 269, 红颜使用功法书响应
     */
    static readonly MsgID_Beauty_KF_Response: number = 269;

    /**
     * 472, 红颜阵图请求
     */
    static readonly MsgID_Beauty_ZhenTu_Request: number = 472;

    /**
     * 473, 红颜阵图响应
     */
    static readonly MsgID_Beauty_ZhenTu_Response: number = 473;

    /**
     * 2103, Buff通知
     */
    static readonly MsgID_Buff_Notify: number = 2103;

    /**
     * 2101, Buff请求
     */
    static readonly MsgID_Buff_Request: number = 2101;

    /**
     * 2102, Buff响应
     */
    static readonly MsgID_Buff_Response: number = 2102;

    /**
     * 2402, 购买大红大蓝请求
     */
    static readonly MsgID_Buy_BigHPMp_Request: number = 2402;

    /**
     * 2403, 购买大红大蓝响应
     */
    static readonly MsgID_Buy_BigHPMp_Response: number = 2403;

    /**
     * 277, CS跨服请求请求
     */
    static readonly MsgID_CROSS_CS_Request: number = 277;

    /**
     * 278, CS跨服请求响应
     */
    static readonly MsgID_CROSS_CS_Response: number = 278;

    /**
     * 275, SS跨服请求入口
     */
    static readonly MsgID_CROSS_SS_Request: number = 275;

    /**
     * 276, SS跨服请求响应
     */
    static readonly MsgID_CROSS_SS_Response: number = 276;

    /**
     * 5322, 阵营战连杀通知
     */
    static readonly MsgID_CampBattleKill_Notify: number = 5322;

    /**
     * 183
     */
    static readonly MsgID_CastSkill_Notify: number = 183;

    /**
     * 181
     */
    static readonly MsgID_CastSkill_Request: number = 181;

    /**
     * 182
     */
    static readonly MsgID_CastSkill_Response: number = 182;

    /**
     * 5313, CDKey兑换请求
     */
    static readonly MsgID_ChangeCDKey_Request: number = 5313;

    /**
     * 5314, CDKey兑换返回
     */
    static readonly MsgID_ChangeCDKey_Response: number = 5314;

    /**
     * 5319, 改变坐骑形象请求
     */
    static readonly MsgID_ChangeMountImage_Request: number = 5319;

    /**
     * 5046, 充值金额变化
     */
    static readonly MsgID_ChargeMoney_Notify: number = 5046;

    /**
     * 5453, 充值双倍返利面板 请求
     */
    static readonly MsgID_Charge_Rebate_Panel_Request: number = 5453;

    /**
     * 5454, 充值双倍返利面板 应答
     */
    static readonly MsgID_Charge_Rebate_Panel_Response: number = 5454;

    /**
     * 5455, 充值双倍返利 请求
     */
    static readonly MsgID_Charge_Rebate_Request: number = 5455;

    /**
     * 5456, 充值双倍返利 应答
     */
    static readonly MsgID_Charge_Rebate_Response: number = 5456;

    /**
     * 1703, 聊天通知
     */
    static readonly MsgID_Chat_Notify: number = 1703;

    /**
     * 1701, 聊天请求
     */
    static readonly MsgID_Chat_Request: number = 1701;

    /**
     * 1702, 聊天响应
     */
    static readonly MsgID_Chat_Response: number = 1702;

    /**
     * 5375, 主城城主形象改变通知
     */
    static readonly MsgID_CityKingChange_Notify: number = 5375;

    /**
     * 212
     */
    static readonly MsgID_ClickMenu_Request: number = 212;

    /**
     * 309, 设置客户端面板状态请求
     */
    static readonly MsgID_ClientPanelSet_Request: number = 309;

    /**
     * 310, 设置客户端面板状态回复
     */
    static readonly MsgID_ClientPanelSet_Response: number = 310;

    /**
     * 5154, 完成循环任务
     */
    static readonly MsgID_CompleteRewardQuest_Request: number = 5154;

    /**
     * 186, 条件不足提示
     */
    static readonly MsgID_Condition_Invalid_Notify: number = 186;

    /**
     * 404
     */
    static readonly MsgID_ContainerBuySpace_Request: number = 404;

    /**
     * 403
     */
    static readonly MsgID_ContainerChanged_Notify: number = 403;

    /**
     * 213
     */
    static readonly MsgID_CountdownTimer_Notify: number = 213;

    /**
     * 283, 血脉操作请求
     */
    static readonly MsgID_CrazyBlood_Request: number = 283;

    /**
     * 284, 血脉操作响应
     */
    static readonly MsgID_CrazyBlood_Response: number = 284;

    /**
     * 5212, 角色猎命星魂容器通知
     */
    static readonly MsgID_CrystalContainerChanged_Notify: number = 5212;

    /**
     * 3115, 时装装备操作请求
     */
    static readonly MsgID_DRESS_Request: number = 3115;

    /**
     * 3116, 时装装备操作回复
     */
    static readonly MsgID_DRESS_Response: number = 3116;

    /**
     * 190, 按天操作的请求
     */
    static readonly MsgID_DayOperate_Request: number = 190;

    /**
     * 191, 按天操作的回复
     */
    static readonly MsgID_DayOperate_Response: number = 191;

    /**
     * 3117, 地榜竞技请求
     */
    static readonly MsgID_DiBang_Request: number = 3117;

    /**
     * 3118, 地榜竞技回复
     */
    static readonly MsgID_DiBang_Response: number = 3118;

    /**
     * 187, 挖BOSS通知
     */
    static readonly MsgID_DigBoss_Notify: number = 187;

    /**
     * 188, 挖BOSS开挖
     */
    static readonly MsgID_DigBoss_Request: number = 188;

    /**
     * 189, 挖BOSS开挖结果
     */
    static readonly MsgID_DigBoss_Response: number = 189;

    /**
     * 5027, 进行活动
     */
    static readonly MsgID_DoActivity_Request: number = 5027;

    /**
     * 5028, 活动操作结果
     */
    static readonly MsgID_DoActivity_Response: number = 5028;

    /**
     * 4601, 开启关闭传送点
     */
    static readonly MsgID_EnableWayPoint_Notify: number = 4601;

    /**
     * 1900, 进入事件区域请求
     */
    static readonly MsgID_EnterRegion_Request: number = 1900;

    /**
     * 3109, 装备强化请求
     */
    static readonly MsgID_EquipProp_Request: number = 3109;

    /**
     * 3110, 装备强化回复
     */
    static readonly MsgID_EquipProp_Response: number = 3110;

    /**
     * 414, 法宝激活请求
     */
    static readonly MsgID_FaBaoActive_Request: number = 414;

    /**
     * 415, 法宝激活响应
     */
    static readonly MsgID_FaBaoActive_Response: number = 415;

    /**
     * 412, 法宝升级请求
     */
    static readonly MsgID_FaBaoLevelUp_Request: number = 412;

    /**
     * 413, 法宝升级响应
     */
    static readonly MsgID_FaBaoLevelUp_Response: number = 413;

    /**
     * 410, 法宝面板拉取请求
     */
    static readonly MsgID_FaBaoPannel_Request: number = 410;

    /**
     * 411, 法宝面板拉取响应
     */
    static readonly MsgID_FaBaoPannel_Response: number = 411;

    /**
     * 416, 法宝显示形象请求
     */
    static readonly MsgID_FaBaoShow_Request: number = 416;

    /**
     * 417, 法宝显示形象响应
     */
    static readonly MsgID_FaBaoShow_Response: number = 417;

    /**
     * 418, 法宝镶嵌请求
     */
    static readonly MsgID_FaBaoXiangQian_Request: number = 418;

    /**
     * 419, 法宝镶嵌响应
     */
    static readonly MsgID_FaBaoXiangQian_Response: number = 419;

    /**
     * 2001, 法器列表主动通知
     */
    static readonly MsgID_FaQiList_Notify: number = 2001;

    /**
     * 2002, 法器祝福请求
     */
    static readonly MsgID_FaQiOperate_Request: number = 2002;

    /**
     * 2003, 法器祝福响应
     */
    static readonly MsgID_FaQiOperate_Response: number = 2003;

    /**
     * 298, 法则操作请求
     */
    static readonly MsgID_FaZe_Request: number = 298;

    /**
     * 299, 法则操作回复
     */
    static readonly MsgID_FaZe_Response: number = 299;

    /**
     * 5362, 法阵部位激活通知
     */
    static readonly MsgID_FaZhen_PartActive_Notify: number = 5362;

    /**
     * 5360, 法阵操作请求
     */
    static readonly MsgID_FaZhen_Request: number = 5360;

    /**
     * 5361, 法阵操作回复
     */
    static readonly MsgID_FaZhen_Response: number = 5361;

    /**
     * 161, 礼包可领变化通知
     */
    static readonly MsgID_FirstOpenChange_Notify: number = 161;

    /**
     * 162, 领取某个礼包请求
     */
    static readonly MsgID_FirstOpenChange_Request: number = 162;

    /**
     * 163, 领取某个礼包回复
     */
    static readonly MsgID_FirstOpenChange_Response: number = 163;

    /**
     * 257, GM图标闪烁通知
     */
    static readonly MsgID_FlashGMQA_Notify: number = 257;

    /**
     * 3014, 加好友请求
     */
    static readonly MsgID_Friend_Add_Request: number = 3014;

    /**
     * 3015, 加好友响应
     */
    static readonly MsgID_Friend_Add_Response: number = 3015;

    /**
     * 3012, 申请加好友请求
     */
    static readonly MsgID_Friend_Apply_Request: number = 3012;

    /**
     * 3013, 申请加好友响应
     */
    static readonly MsgID_Friend_Apply_Response: number = 3013;

    /**
     * 3026
     */
    static readonly MsgID_Friend_Contact_Request: number = 3026;

    /**
     * 3027
     */
    static readonly MsgID_Friend_Contact_Response: number = 3027;

    /**
     * 3016, 删好友请求
     */
    static readonly MsgID_Friend_Delete_Request: number = 3016;

    /**
     * 3017, 删好友响应
     */
    static readonly MsgID_Friend_Delete_Response: number = 3017;

    /**
     * 3010, 拉取游戏好友列表请求
     */
    static readonly MsgID_Friend_FetchGameFriend_Request: number = 3010;

    /**
     * 3011, 拉取游戏好友列表响应
     */
    static readonly MsgID_Friend_FetchGameFriend_Response: number = 3011;

    /**
     * 3019, 好友上下线通知
     */
    static readonly MsgID_Friend_LogInOut_Notify: number = 3019;

    /**
     * 3028, 拉取好友的散仙信息
     */
    static readonly MsgID_Friend_PetInfo_Request: number = 3028;

    /**
     * 3029, 拉取好友的散仙信息
     */
    static readonly MsgID_Friend_PetInfo_Response: number = 3029;

    /**
     * 3020, 拉取好友的角色信息、装备信息请求
     */
    static readonly MsgID_Friend_RoleInfo_Request: number = 3020;

    /**
     * 3021, 拉取好友的角色信息、装备信息响应
     */
    static readonly MsgID_Friend_RoleInfo_Response: number = 3021;

    /**
     * 3024
     */
    static readonly MsgID_Friend_Search_Request: number = 3024;

    /**
     * 3025
     */
    static readonly MsgID_Friend_Search_Response: number = 3025;

    /**
     * 287, 功能激活请求
     */
    static readonly MsgID_FunctionAct_Request: number = 287;

    /**
     * 288, 功能激活响应
     */
    static readonly MsgID_FunctionAct_Response: number = 288;

    /**
     * 1003
     */
    static readonly MsgID_GameMaster_Notify: number = 1003;

    /**
     * 1001
     */
    static readonly MsgID_GameMaster_Request: number = 1001;

    /**
     * 1002
     */
    static readonly MsgID_GameMaster_Response: number = 1002;

    /**
     * 1107
     */
    static readonly MsgID_GetDroppedThing_Request: number = 1107;

    /**
     * 1108
     */
    static readonly MsgID_GetDroppedThing_Response: number = 1108;

    /**
     * 204, 取本场景的M地图标怪20个
     */
    static readonly MsgID_GetIconMonster_Request: number = 204;

    /**
     * 205, 取本场景的M地图标怪20个
     */
    static readonly MsgID_GetIconMonster_Response: number = 205;

    /**
     * 176, 获取玩家等级礼包请求
     */
    static readonly MsgID_GetLevelBag_Request: number = 176;

    /**
     * 177, 获取玩家等级礼包回复
     */
    static readonly MsgID_GetLevelBag_Response: number = 177;

    /**
     * 178, 获取玩家月卡信息请求
     */
    static readonly MsgID_GetMonthCard_Request: number = 178;

    /**
     * 179, 获取玩家月卡信息回复
     */
    static readonly MsgID_GetMonthCard_Response: number = 179;

    /**
     * 174, 获取Monster坐标列表请求
     */
    static readonly MsgID_GetNpcPostionList_Request: number = 174;

    /**
     * 175, 获取Monster坐标列表响应
     */
    static readonly MsgID_GetNpcPostionList_Response: number = 175;

    /**
     * 147, 查看玩家所在位置
     */
    static readonly MsgID_GetPlayerPos_Request: number = 147;

    /**
     * 148, 查看玩家所在位置
     */
    static readonly MsgID_GetPlayerPos_Response: number = 148;

    /**
     * 202, 取本场景的怪物ID列表最多10个，不含采集物
     */
    static readonly MsgID_GetSceneMonster_Request: number = 202;

    /**
     * 203, 取本场景的怪物ID列表最多10个，不含采集物
     */
    static readonly MsgID_GetSceneMonster_Response: number = 203;

    /**
     * 133, 取装备属性信息
     */
    static readonly MsgID_GetThingPropertyByGuid_Request: number = 133;

    /**
     * 134, 返回装备属性信息
     */
    static readonly MsgID_GetThingPropertyByGuid_Response: number = 134;

    /**
     * 194, 团购协议请求
     */
    static readonly MsgID_GroupBuy_Request: number = 194;

    /**
     * 195, 团购协议响应
     */
    static readonly MsgID_GroupBuy_Response: number = 195;

    /**
     * 325, 挂机通知
     */
    static readonly MsgID_GuajiNormal_Notify: number = 325;

    /**
     * 903, CS跨服宗派战请求
     */
    static readonly MsgID_Guild_CROSSPVP_CS_Request: number = 903;

    /**
     * 904, CS跨服宗派战响应
     */
    static readonly MsgID_Guild_CROSSPVP_CS_Response: number = 904;

    /**
     * 898, 家族请求
     */
    static readonly MsgID_Guild_CS_Request: number = 898;

    /**
     * 899, 家族响应
     */
    static readonly MsgID_Guild_CS_Response: number = 899;

    /**
     * 968
     */
    static readonly MsgID_Guild_ChangedNotify: number = 968;

    /**
     * 900, 家族通知
     */
    static readonly MsgID_Guild_Notify: number = 900;

    /**
     * 907, CS宗派战请求
     */
    static readonly MsgID_Guild_PVP_CS_Request: number = 907;

    /**
     * 908, CS宗派战响应
     */
    static readonly MsgID_Guild_PVP_CS_Response: number = 908;

    /**
     * 4837, 宗派群英会个人排行榜请求
     */
    static readonly MsgID_Guild_PVP_Rank_Request: number = 4837;

    /**
     * 4838, 宗派群英会个人排行榜回复
     */
    static readonly MsgID_Guild_PVP_Rank_Response: number = 4838;

    /**
     * 5367, 合服活动数据请求
     */
    static readonly MsgID_HFActInfo_Request: number = 5367;

    /**
     * 5368, 合服活动数据响应
     */
    static readonly MsgID_HFActInfo_Response: number = 5368;

    /**
     * 3030, 活跃度请求
     */
    static readonly MsgID_HYDOperate_Request: number = 3030;

    /**
     * 3031, 活跃度回复
     */
    static readonly MsgID_HYDOperate_Response: number = 3031;

    /**
     * 244, 吃丹药请求
     */
    static readonly MsgID_HeroSub_Drug_Request: number = 244;

    /**
     * 246, 化形变更通知
     */
    static readonly MsgID_HeroSub_ImageChange_Notify: number = 246;

    /**
     * 248, 幻化升级
     */
    static readonly MsgID_HeroSub_ImageLevel_Request: number = 248;

    /**
     * 240, 拉取所有子模块请求
     */
    static readonly MsgID_HeroSub_List_Request: number = 240;

    /**
     * 241, 拉取所有子模块回复
     */
    static readonly MsgID_HeroSub_List_Response: number = 241;

    /**
     * 247, 对幸运值的操作请求
     */
    static readonly MsgID_HeroSub_Lucky_Request: number = 247;

    /**
     * 245, 一个子系统通知
     */
    static readonly MsgID_HeroSub_One_Notify: number = 245;

    /**
     * 243, 设置形象请求
     */
    static readonly MsgID_HeroSub_Show_Request: number = 243;

    /**
     * 249, 对单个系统的特别操作请求
     */
    static readonly MsgID_HeroSub_Spec_Request: number = 249;

    /**
     * 242, 祝福请求
     */
    static readonly MsgID_HeroSub_Wish_Request: number = 242;

    /**
     * 5300, 隐藏周围玩家
     */
    static readonly MsgID_HideSightRole_Request: number = 5300;

    /**
     * 3709, 隐藏视野特效
     */
    static readonly MsgID_HideSight_Notify: number = 3709;

    /**
     * 3403, 开启/关闭UI
     */
    static readonly MsgID_HideUI_Notify: number = 3403;

    /**
     * 331, 魂骨合成请求
     */
    static readonly MsgID_HunGuMerge_Request: number = 331;

    /**
     * 332, 魂骨合成响应
     */
    static readonly MsgID_HunGuMerge_Response: number = 332;

    /**
     * 5489, 魂力请求
     */
    static readonly MsgID_HunLi_Request: number = 5489;

    /**
     * 5490, 魂力响应
     */
    static readonly MsgID_HunLi_Response: number = 5490;

    /**
     * 5366, 形象过期通知
     */
    static readonly MsgID_Image_OverDue_Notify: number = 5366;

    /**
     * 5121, 玩家打断技能
     */
    static readonly MsgID_InterruptSkill_Request: number = 5121;

    /**
     * 321, 物品合成请求
     */
    static readonly MsgID_ItemMerge_Request: number = 321;

    /**
     * 322, 物品合成响应
     */
    static readonly MsgID_ItemMerge_Response: number = 322;

    /**
     * 169, 道具跳转
     */
    static readonly MsgID_ItemTransport_Request: number = 169;

    /**
     * 171
     */
    static readonly MsgID_ItemTransport_Response: number = 171;

    /**
     * 465, 道宫九星玩法请求
     */
    static readonly MsgID_JiuXing_Request: number = 465;

    /**
     * 466, 道宫九星玩法响应
     */
    static readonly MsgID_JiuXing_Response: number = 466;

    /**
     * 464, 聚元主动通知
     */
    static readonly MsgID_JuYuanNotify: number = 464;

    /**
     * 462, 聚元升级请求
     */
    static readonly MsgID_JuYuanUpgrade_Request: number = 462;

    /**
     * 463, 聚元升级响应
     */
    static readonly MsgID_JuYuanUpgrade_Response: number = 463;

    /**
     * 460, 聚元面板详情拉取请求
     */
    static readonly MsgID_JuYuan_Request: number = 460;

    /**
     * 461, 聚元面板详情拉取响应
     */
    static readonly MsgID_JuYuan_Response: number = 461;

    /**
     * 5364, 开服活动数据请求
     */
    static readonly MsgID_KFActInfo_Request: number = 5364;

    /**
     * 5365, 开服活动数据响应
     */
    static readonly MsgID_KFActInfo_Response: number = 5365;

    /**
     * 5352, 开服每日目标拉取数据请求
     */
    static readonly MsgID_KFMRMBGetInfo_Request: number = 5352;

    /**
     * 5353, 开服每日目标拉取数据响应
     */
    static readonly MsgID_KFMRMBGetInfo_Response: number = 5353;

    /**
     * 5354, 开服每日目标领取奖励请求
     */
    static readonly MsgID_KFMRMBGetReward_Request: number = 5354;

    /**
     * 5355, 开服每日目标领取奖励响应
     */
    static readonly MsgID_KFMRMBGetReward_Response: number = 5355;

    /**
     * 5346, 开服全民冲榜拉取榜单第一数据请求
     */
    static readonly MsgID_KFQMCBGetInfo_Request: number = 5346;

    /**
     * 5347, 开服全民冲榜拉取榜单第一数据响应
     */
    static readonly MsgID_KFQMCBGetInfo_Response: number = 5347;

    /**
     * 5348, 开服全民冲榜领取奖励请求
     */
    static readonly MsgID_KFQMCBGetReward_Request: number = 5348;

    /**
     * 5349, 开服全民冲榜领取奖励响应
     */
    static readonly MsgID_KFQMCBGetReward_Response: number = 5349;

    /**
     * 5358, 开服全民冲榜拉取个人数据请求
     */
    static readonly MsgID_KFQMCBGetRoleInfo_Request: number = 5358;

    /**
     * 5359, 开服全民冲榜拉取个人数据响应
     */
    static readonly MsgID_KFQMCBGetRoleInfo_Response: number = 5359;

    /**
     * 5342, 开服首充团购拉取数据请求
     */
    static readonly MsgID_KFSCTGGetInfo_Request: number = 5342;

    /**
     * 5343, 开服首充团购拉取数据响应
     */
    static readonly MsgID_KFSCTGGetInfo_Response: number = 5343;

    /**
     * 5344, 开服首充团购领取奖励请求
     */
    static readonly MsgID_KFSCTGGetReward_Request: number = 5344;

    /**
     * 5345, 开服首充团购领取奖励响应
     */
    static readonly MsgID_KFSCTGGetReward_Response: number = 5345;

    /**
     * 5372, 开服首充团购发送推荐传闻请求
     */
    static readonly MsgID_KFSCTGSendMsg_Request: number = 5372;

    /**
     * 224
     */
    static readonly MsgID_ListActivityLimit_Request: number = 224;

    /**
     * 225
     */
    static readonly MsgID_ListActivityLimit_Response: number = 225;

    /**
     * 5026, 列出活动
     */
    static readonly MsgID_ListActivity_Request: number = 5026;

    /**
     * 5031, 列出活动响应
     */
    static readonly MsgID_ListActivity_Response: number = 5031;

    /**
     * 5210, 角色猎命拉取星魂列表
     */
    static readonly MsgID_ListCrystal_Request: number = 5210;

    /**
     * 253, 玩家问题列表
     */
    static readonly MsgID_ListGMQA_Request: number = 253;

    /**
     * 254, 玩家问题列表回复
     */
    static readonly MsgID_ListGMQA_Response: number = 254;

    /**
     * 210
     */
    static readonly MsgID_ListMenu_Request: number = 210;

    /**
     * 211
     */
    static readonly MsgID_ListMenu_Response: number = 211;

    /**
     * 307
     */
    static readonly MsgID_ListNearTeamInfo_Request: number = 307;

    /**
     * 308
     */
    static readonly MsgID_ListNearTeamInfo_Response: number = 308;

    /**
     * 121
     */
    static readonly MsgID_ListQuestProgress_Request: number = 121;

    /**
     * 122
     */
    static readonly MsgID_ListQuestProgress_Response: number = 122;

    /**
     * 103, 通知登录
     */
    static readonly MsgID_LoginServer_Notify: number = 103;

    /**
     * 101, 请求登录
     */
    static readonly MsgID_LoginServer_Request: number = 101;

    /**
     * 102, 响应登录
     */
    static readonly MsgID_LoginServer_Response: number = 102;

    /**
     * 113, 通知登出
     */
    static readonly MsgID_LogoutServer_Notify: number = 113;

    /**
     * 111, 请求登出
     */
    static readonly MsgID_LogoutServer_Request: number = 111;

    /**
     * 112, 响应登出
     */
    static readonly MsgID_LogoutServer_Response: number = 112;

    /**
     * 5457, 魔化战争 面板
     */
    static readonly MsgID_MHZZ_Pannel_Request: number = 5457;

    /**
     * 5458, 魔化战争 面板
     */
    static readonly MsgID_MHZZ_Pannel_Response: number = 5458;

    /**
     * 292, 魔方升级请求
     */
    static readonly MsgID_MagicCubeLevelUp_Request: number = 292;

    /**
     * 293, 魔方升级响应
     */
    static readonly MsgID_MagicCubeLevelUp_Response: number = 293;

    /**
     * 294, 魔方大请求
     */
    static readonly MsgID_MagicCubePannel_Request: number = 294;

    /**
     * 295, 魔方大响应
     */
    static readonly MsgID_MagicCubePannel_Response: number = 295;

    /**
     * 2902, 拉取邮件列表请求
     */
    static readonly MsgID_Mail_FetchList_Request: number = 2902;

    /**
     * 2903, 拉取邮件列表响应
     */
    static readonly MsgID_Mail_FetchList_Response: number = 2903;

    /**
     * 2904, 拉取一封邮件内容请求
     */
    static readonly MsgID_Mail_FetchMail_Request: number = 2904;

    /**
     * 2905, 拉取一封邮件内容响应
     */
    static readonly MsgID_Mail_FetchMail_Response: number = 2905;

    /**
     * 2901, 邮件列表变更通知
     */
    static readonly MsgID_Mail_ListChange_Notify: number = 2901;

    /**
     * 2912, 通知未读取的邮件封数
     */
    static readonly MsgID_Mail_MailNumber_Notify: number = 2912;

    /**
     * 2906, 收取全部附件请求
     */
    static readonly MsgID_Mail_PickAccessory_Request: number = 2906;

    /**
     * 2907, 收取全部附件响应
     */
    static readonly MsgID_Mail_PickAccessory_Response: number = 2907;

    /**
     * 219, 婚姻系统请求
     */
    static readonly MsgID_Marriage_Request: number = 219;

    /**
     * 220, 婚姻系统响应
     */
    static readonly MsgID_Marriage_Response: number = 220;

    /**
     * 159, 改怪物属性的名字
     */
    static readonly MsgID_MonsterAttrChange_Notify: number = 159;

    /**
     * 144, 怪物变形象通知
     */
    static readonly MsgID_MonsterChangeAvatar_Notify: number = 144;

    /**
     * 3602, 怪物AI说话，用在出生死亡等
     */
    static readonly MsgID_Monster_AITalk_Notify: number = 3602;

    /**
     * 3601, 场景内的怪物说话
     */
    static readonly MsgID_Monster_Talk_Notify: number = 3601;

    /**
     * 5084, 变更玩家骑乘状态请求
     */
    static readonly MsgID_MountRideChange_Request: number = 5084;

    /**
     * 5085, 变更玩家骑乘状态响应
     */
    static readonly MsgID_MountRideChange_Response: number = 5085;

    /**
     * 153
     */
    static readonly MsgID_MovePosition_Notify: number = 153;

    /**
     * 151
     */
    static readonly MsgID_MovePosition_Request: number = 151;

    /**
     * 152
     */
    static readonly MsgID_MovePosition_Response: number = 152;

    /**
     * 131
     */
    static readonly MsgID_NPCBehaviour_Request: number = 131;

    /**
     * 132
     */
    static readonly MsgID_NPCBehaviour_Response: number = 132;

    /**
     * 4610, 拉取限购列表
     */
    static readonly MsgID_NPCStoreLimitList_Request: number = 4610;

    /**
     * 4611, 拉取限购响应
     */
    static readonly MsgID_NPCStoreLimitList_Response: number = 4611;

    /**
     * 172, NPC跳转
     */
    static readonly MsgID_NPCTransport_Request: number = 172;

    /**
     * 2202, 新的货币变化
     */
    static readonly MsgID_NewCurrencyChanged_Notify: number = 2202;

    /**
     * 1000, 发玩家视野属性改变场景所有人
     */
    static readonly MsgID_ONE_SIGHT_ARRTI_CHANGE_Notify: number = 1000;

    /**
     * 319, VIP一键完成获取请求
     */
    static readonly MsgID_OneKeyGet_Request: number = 319;

    /**
     * 320, VIP一键完成获取响应
     */
    static readonly MsgID_OneKeyGet_Response: number = 320;

    /**
     * 317, VIP一键完成打开面请求
     */
    static readonly MsgID_OneKeyOpen_Request: number = 317;

    /**
     * 318, VIP一键完成打开面回复
     */
    static readonly MsgID_OneKeyOpen_Response: number = 318;

    /**
     * 5204, 角色猎命操作请求
     */
    static readonly MsgID_OpCrystal_Request: number = 5204;

    /**
     * 5205, 角色猎命操作响应
     */
    static readonly MsgID_OpCrystal_Response: number = 5205;

    /**
     * 258, 红颜面板请求
     */
    static readonly MsgID_OpenBeautyPannel_Request: number = 258;

    /**
     * 259, 红颜面板响应
     */
    static readonly MsgID_OpenBeautyPannel_Response: number = 259;

    /**
     * 5393, 开宝箱通知
     */
    static readonly MsgID_OpenBox_Notify: number = 5393;

    /**
     * 290, 魔方面板请求
     */
    static readonly MsgID_OpenMagicCubePannel_Request: number = 290;

    /**
     * 291, 魔方面板响应
     */
    static readonly MsgID_OpenMagicCubePannel_Response: number = 291;

    /**
     * 208, 打开超级VIP按钮
     */
    static readonly MsgID_OpenSuperVIP_Request: number = 208;

    /**
     * 209, 打开超级VIP按钮回复
     */
    static readonly MsgID_OpenSuperVIP_Response: number = 209;

    /**
     * 401
     */
    static readonly MsgID_OperateContainer_Request: number = 401;

    /**
     * 127
     */
    static readonly MsgID_OperateOneQuest_Request: number = 127;

    /**
     * 128
     */
    static readonly MsgID_OperateOneQuest_Response: number = 128;

    /**
     * 1301
     */
    static readonly MsgID_OperateShortcut_Request: number = 1301;

    /**
     * 1302
     */
    static readonly MsgID_OperateShortcut_Response: number = 1302;

    /**
     * 1201
     */
    static readonly MsgID_OperateSkill_Request: number = 1201;

    /**
     * 1202
     */
    static readonly MsgID_OperateSkill_Response: number = 1202;

    /**
     * 303
     */
    static readonly MsgID_OperateTeam_Notify: number = 303;

    /**
     * 301
     */
    static readonly MsgID_OperateTeam_Request: number = 301;

    /**
     * 302
     */
    static readonly MsgID_OperateTeam_Response: number = 302;

    /**
     * 226, PK状态变更请求
     */
    static readonly MsgID_PKStatus_Request: number = 226;

    /**
     * 2702, 买入请求
     */
    static readonly MsgID_PPStoreBuy_Request: number = 2702;

    /**
     * 2703, 买入回复
     */
    static readonly MsgID_PPStoreBuy_Response: number = 2703;

    /**
     * 2725, 吆喝请求
     */
    static readonly MsgID_PPStoreCall_Request: number = 2725;

    /**
     * 2726, 吆喝响应
     */
    static readonly MsgID_PPStoreCall_Response: number = 2726;

    /**
     * 2708, 取消我的寄售请求
     */
    static readonly MsgID_PPStoreCancelMy_Request: number = 2708;

    /**
     * 2730, 取消我的寄售回复
     */
    static readonly MsgID_PPStoreCancelMy_Response: number = 2730;

    /**
     * 2707, 显示我的寄售请求
     */
    static readonly MsgID_PPStoreDispMy_Request: number = 2707;

    /**
     * 2709, 显示我的寄售回复
     */
    static readonly MsgID_PPStoreDispMy_Response: number = 2709;

    /**
     * 2727, 获取所有售卖物品数量请求
     */
    static readonly MsgID_PPStoreGetAllThingNum_Request: number = 2727;

    /**
     * 2728, 获取所有售卖物品数量回复
     */
    static readonly MsgID_PPStoreGetAllThingNum_Response: number = 2728;

    /**
     * 2704, 查询请求
     */
    static readonly MsgID_PPStoreQuery_Request: number = 2704;

    /**
     * 2705, 查询回复
     */
    static readonly MsgID_PPStoreQuery_Response: number = 2705;

    /**
     * 2700, 卖出请求
     */
    static readonly MsgID_PPStoreSell_Request: number = 2700;

    /**
     * 2701, 卖出回复
     */
    static readonly MsgID_PPStoreSell_Response: number = 2701;

    /**
     * 2706, 排序请求
     */
    static readonly MsgID_PPStoreSort_Request: number = 2706;

    /**
     * 4813, PVP阵营关系通知
     */
    static readonly MsgID_PVPCampRelation_Notify: number = 4813;

    /**
     * 142, CS封神台请求
     */
    static readonly MsgID_PVPRank_CS_Request: number = 142;

    /**
     * 143, CS封神台响应
     */
    static readonly MsgID_PVPRank_CS_Response: number = 143;

    /**
     * 215
     */
    static readonly MsgID_PinstanceComplete_Notify: number = 215;

    /**
     * 3305, 进入副本请求
     */
    static readonly MsgID_PinstanceEnter_Request: number = 3305;

    /**
     * 201
     */
    static readonly MsgID_PinstanceFinish_Notify: number = 201;

    /**
     * 206, 副本大厅的相关操作请求
     */
    static readonly MsgID_PinstanceHome_Request: number = 206;

    /**
     * 207, 副本大厅相关操作的回复
     */
    static readonly MsgID_PinstanceHome_Response: number = 207;

    /**
     * 216
     */
    static readonly MsgID_PinstanceQuit_Request: number = 216;

    /**
     * 217, 副本排行榜操作请求
     */
    static readonly MsgID_PinstanceRank_Request: number = 217;

    /**
     * 218, 副本排行榜操作的回复
     */
    static readonly MsgID_PinstanceRank_Response: number = 218;

    /**
     * 3200, 玩家移动
     */
    static readonly MsgID_Position_Notify: number = 3200;

    /**
     * 5493, 功能预览领取请求
     */
    static readonly MsgID_Preview_Reward_Request: number = 5493;

    /**
     * 5494, 功能预览领取响应
     */
    static readonly MsgID_Preview_Reward_Response: number = 5494;

    /**
     * 2500, 提示消息通告
     */
    static readonly MsgID_PromptMsg_Notify: number = 2500;

    /**
     * 5380, 祈福操作请求
     */
    static readonly MsgID_QiFu_Request: number = 5380;

    /**
     * 5381, 祈福操作回复
     */
    static readonly MsgID_QiFu_Response: number = 5381;

    /**
     * 5386, 乾坤炉请求
     */
    static readonly MsgID_Qiankunlu_Request: number = 5386;

    /**
     * 5387, 乾坤炉回复
     */
    static readonly MsgID_Qiankunlu_Response: number = 5387;

    /**
     * 430
     */
    static readonly MsgID_QuestPanel_Request: number = 430;

    /**
     * 431
     */
    static readonly MsgID_QuestPanel_Response: number = 431;

    /**
     * 5321, 答题活动通知
     */
    static readonly MsgID_QuestionActivity_Notify: number = 5321;

    /**
     * 5200, 角色猎命召唤炼金师请求
     */
    static readonly MsgID_RecruitAlchemist_Request: number = 5200;

    /**
     * 5201, 角色猎命召唤炼金师响应
     */
    static readonly MsgID_RecruitAlchemist_Response: number = 5201;

    /**
     * 157
     */
    static readonly MsgID_RefreshDroppedSightInfo_Notify: number = 157;

    /**
     * 158
     */
    static readonly MsgID_RefreshLeavedSightInfo_Notify: number = 158;

    /**
     * 156
     */
    static readonly MsgID_RefreshMonsterSightInfo_Notify: number = 156;

    /**
     * 155
     */
    static readonly MsgID_RefreshPetSightInfo_Notify: number = 155;

    /**
     * 4830, 拉取排行榜请求
     */
    static readonly MsgID_RefreshRankInfo_Request: number = 4830;

    /**
     * 4831, 拉取排行榜回复
     */
    static readonly MsgID_RefreshRankInfo_Response: number = 4831;

    /**
     * 5152, 刷新循环任务
     */
    static readonly MsgID_RefreshRewardQuest_Request: number = 5152;

    /**
     * 154
     */
    static readonly MsgID_RefreshRoleSightInfo_Notify: number = 154;

    /**
     * 1906, 移除相位NPC通告
     */
    static readonly MsgID_Remove_NPC_Notify: number = 1906;

    /**
     * 2400, 复活请求消息ID
     */
    static readonly MsgID_Revival_Request: number = 2400;

    /**
     * 2401, 复活响应消息ID
     */
    static readonly MsgID_Revival_Response: number = 2401;

    /**
     * 285, 玩家回归请求
     */
    static readonly MsgID_RoleReturnOperate_Request: number = 285;

    /**
     * 286, 玩家回归响应
     */
    static readonly MsgID_RoleReturnOperate_Response: number = 286;

    /**
     * 5401, 玩家信息修改请求
     */
    static readonly MsgID_RoleWing_Request: number = 5401;

    /**
     * 5402, 玩家信息修改响应
     */
    static readonly MsgID_RoleWing_Response: number = 5402;

    /**
     * 5335, 首冲拉取数据请求
     */
    static readonly MsgID_SCGetInfo_Request: number = 5335;

    /**
     * 5336, 首冲拉取数据响应
     */
    static readonly MsgID_SCGetInfo_Response: number = 5336;

    /**
     * 5337, 首冲领取奖励请求
     */
    static readonly MsgID_SCGetReward_Request: number = 5337;

    /**
     * 5338, 首冲领取奖励响应
     */
    static readonly MsgID_SCGetReward_Response: number = 5338;

    /**
     * 5339, 首充礼包可以领取通知
     */
    static readonly MsgID_SC_CanGetReward_Notify: number = 5339;

    /**
     * 603, 赛季 激活外显
     */
    static readonly MsgID_SaiJiActive_Request: number = 603;

    /**
     * 604, 赛季 激活外显
     */
    static readonly MsgID_SaiJiActive_Response: number = 604;

    /**
     * 601, 赛季 打开面板
     */
    static readonly MsgID_SaiJiPannel_Request: number = 601;

    /**
     * 602, 赛季 打开面板
     */
    static readonly MsgID_SaiJiPannel_Response: number = 602;

    /**
     * 227
     */
    static readonly MsgID_SceneInfo_Notify: number = 227;

    /**
     * 200
     */
    static readonly MsgID_SceneRightInfo_Notify: number = 200;

    /**
     * 214
     */
    static readonly MsgID_ScriptToClient_Notify: number = 214;

    /**
     * 117, 服务器参数通知到client
     */
    static readonly MsgID_ServerParameter_Notify: number = 117;

    /**
     * 5451, 设置双倍充值 请求
     */
    static readonly MsgID_Set_Charge_Rebate_Request: number = 5451;

    /**
     * 5452, 设置双倍充值 应答
     */
    static readonly MsgID_Set_Charge_Rebate_Response: number = 5452;

    /**
     * 5333, 七日投资请求
     */
    static readonly MsgID_SevenDay_Fund_Request: number = 5333;

    /**
     * 5334, 七日投资响应
     */
    static readonly MsgID_SevenDay_Fund_Response: number = 5334;

    /**
     * 2004, 神兽列表主动通知
     */
    static readonly MsgID_ShenShouList_Notify: number = 2004;

    /**
     * 2005, 神兽请求
     */
    static readonly MsgID_ShenShouOperate_Request: number = 2005;

    /**
     * 2006, 神兽响应
     */
    static readonly MsgID_ShenShouOperate_Response: number = 2006;

    /**
     * 2011, 守护神请求
     */
    static readonly MsgID_ShieldGodOperate_Request: number = 2011;

    /**
     * 2012, 守护神响应
     */
    static readonly MsgID_ShieldGodOperate_Response: number = 2012;

    /**
     * 3710, 显示区域视野特效
     */
    static readonly MsgID_ShowArea_Notify: number = 3710;

    /**
     * 3708, 播放倒计时特效
     */
    static readonly MsgID_ShowCountDownEffect_Notify: number = 3708;

    /**
     * 3702, 某个位置播放特效
     */
    static readonly MsgID_ShowEffect_At_Position_Notify: number = 3702;

    /**
     * 3113, 天宫宝境抽奖请求
     */
    static readonly MsgID_SkyLottery_Request: number = 3113;

    /**
     * 3114, 天宫宝境抽奖回复
     */
    static readonly MsgID_SkyLottery_Response: number = 3114;

    /**
     * 4101, 列表改变
     */
    static readonly MsgID_SpecialItemListChanged_Notify: number = 4101;

    /**
     * 5356, 特殊物品可使用通知
     */
    static readonly MsgID_SpecialThingUsable_Notify: number = 5356;

    /**
     * 173, 特殊传送，直接跳转到某个场景或者副本
     */
    static readonly MsgID_SpecialTransport_Request: number = 173;

    /**
     * 180, 特殊传送，直接跳转到某个场景或者副本回复
     */
    static readonly MsgID_SpecialTransport_Response: number = 180;

    /**
     * 3251, 星斗宝库抽奖请求
     */
    static readonly MsgID_StarLottery_Request: number = 3251;

    /**
     * 3252, 星斗宝库抽奖回复
     */
    static readonly MsgID_StarLottery_Response: number = 3252;

    /**
     * 5491, 问卷调查
     */
    static readonly MsgID_Survey_Request: number = 5491;

    /**
     * 5492, 问卷调查
     */
    static readonly MsgID_Survey_Response: number = 5492;

    /**
     * 402
     */
    static readonly MsgID_SwapContainer_Request: number = 402;

    /**
     * 5211, 角色猎命交换星魂
     */
    static readonly MsgID_SwapCrystal_Request: number = 5211;

    /**
     * 5213, 角色猎命交换星魂响应
     */
    static readonly MsgID_SwapCrystal_Response: number = 5213;

    /**
     * 2603, 同步时间客户端响应
     */
    static readonly MsgID_SyncTime_Client_Request: number = 2603;

    /**
     * 2601, 同步时间请求
     */
    static readonly MsgID_SyncTime_Request: number = 2601;

    /**
     * 2602, 同步时间响应
     */
    static readonly MsgID_SyncTime_Response: number = 2602;

    /**
     * 118, 通知系统设置
     */
    static readonly MsgID_SystemSetting_Notify: number = 118;

    /**
     * 453, 标签状态变化通知
     */
    static readonly MsgID_TabStatus_Change_Notify: number = 453;

    /**
     * 2802, 变更正在展示的称号请求
     */
    static readonly MsgID_Title_ActiveChange_Request: number = 2802;

    /**
     * 2803, 变更正在展示的称号响应
     */
    static readonly MsgID_Title_ActiveChange_Response: number = 2803;

    /**
     * 168
     */
    static readonly MsgID_Transport_Notify: number = 168;

    /**
     * 166, 传送点跳转
     */
    static readonly MsgID_Transport_Request: number = 166;

    /**
     * 167
     */
    static readonly MsgID_Transport_Response: number = 167;

    /**
     * 165
     */
    static readonly MsgID_UnitAttributeChanged_Notify: number = 165;

    /**
     * 3707, 某个单位播放动作
     */
    static readonly MsgID_Unit_PlayAction_Notify: number = 3707;

    /**
     * 126
     */
    static readonly MsgID_UpdateQuestProgress_Notify: number = 126;

    /**
     * 119, 请求系统设置
     */
    static readonly MsgID_UpdateSystemSetting_Request: number = 119;

    /**
     * 5050, vip操作请求
     */
    static readonly MsgID_VIPOperate_Request: number = 5050;

    /**
     * 5051, vip操作请求回复
     */
    static readonly MsgID_VIPOperate_Response: number = 5051;

    /**
     * 4852, 我要变强拉取请求
     */
    static readonly MsgID_WYBQ_Get_Request: number = 4852;

    /**
     * 4853, 我要变强拉取回复
     */
    static readonly MsgID_WYBQ_Get_Response: number = 4853;

    /**
     * 5464, 武缘远征 Buff购买
     */
    static readonly MsgID_WYYZ_BuyBuff_Request: number = 5464;

    /**
     * 5465, 武缘远征 Buff购买
     */
    static readonly MsgID_WYYZ_BuyBuff_Response: number = 5465;

    /**
     * 5470, 武缘远征 出战设置
     */
    static readonly MsgID_WYYZ_FightSet_Request: number = 5470;

    /**
     * 5471, 武缘远征 出战设置
     */
    static readonly MsgID_WYYZ_FightSet_Response: number = 5471;

    /**
     * 5466, 武缘远征 领取奖励
     */
    static readonly MsgID_WYYZ_GetReward_Request: number = 5466;

    /**
     * 5467, 武缘远征 领取奖励
     */
    static readonly MsgID_WYYZ_GetReward_Response: number = 5467;

    /**
     * 5462, 武缘远征 挑战
     */
    static readonly MsgID_WYYZ_PK_Request: number = 5462;

    /**
     * 5463, 武缘远征 挑战
     */
    static readonly MsgID_WYYZ_PK_Response: number = 5463;

    /**
     * 5460, 武缘远征 面板
     */
    static readonly MsgID_WYYZ_Pannel_Request: number = 5460;

    /**
     * 5461, 武缘远征 面板
     */
    static readonly MsgID_WYYZ_Pannel_Response: number = 5461;

    /**
     * 5468, 武缘远征 释放技能
     */
    static readonly MsgID_WYYZ_Skill_Request: number = 5468;

    /**
     * 5469, 武缘远征 释放技能
     */
    static readonly MsgID_WYYZ_Skill_Response: number = 5469;

    /**
     * 5487, 武缘寻宝请求
     */
    static readonly MsgID_WY_TreasureHunt_Request: number = 5487;

    /**
     * 5488, 武缘寻宝响应
     */
    static readonly MsgID_WY_TreasureHunt_Response: number = 5488;

    /**
     * 5478, 世界拍卖 竞价购买 请求
     */
    static readonly MsgID_WorldPaiMai_Buy_Request: number = 5478;

    /**
     * 5479, 世界拍卖 竞价购买 应答
     */
    static readonly MsgID_WorldPaiMai_Buy_Response: number = 5479;

    /**
     * 5482, 世界拍卖 新的可拍卖活动 通知
     */
    static readonly MsgID_WorldPaiMai_NewAct_Notify: number = 5482;

    /**
     * 5476, 世界拍卖 打开面板 请求
     */
    static readonly MsgID_WorldPaiMai_Pannel_Request: number = 5476;

    /**
     * 5477, 世界拍卖 打开面板 应答
     */
    static readonly MsgID_WorldPaiMai_Pannel_Response: number = 5477;

    /**
     * 5443, 血战封魔 请求
     */
    static readonly MsgID_XZFM_Request: number = 5443;

    /**
     * 5444, 血战封魔 响应
     */
    static readonly MsgID_XZFM_Response: number = 5444;

    /**
     * 323, 祝福装备升阶请求
     */
    static readonly MsgID_ZFEquipUpColor_Request: number = 323;

    /**
     * 324, 祝福装备升阶响应
     */
    static readonly MsgID_ZFEquipUpColor_Response: number = 324;

    /**
     * 420, 资源找回面板拉取请求
     */
    static readonly MsgID_ZYZHPannel_Request: number = 420;

    /**
     * 421, 资源找回面板拉取响应
     */
    static readonly MsgID_ZYZHPannel_Response: number = 421;

    /**
     * 422, 资源找回领取奖励请求
     */
    static readonly MsgID_ZYZHReward_Request: number = 422;

    /**
     * 423, 资源找回领取奖励响应
     */
    static readonly MsgID_ZYZHReward_Response: number = 423;

    /**
     * 5445, 至尊皇城 面板
     */
    static readonly MsgID_ZZHC_Pannel_Request: number = 5445;

    /**
     * 5446, 至尊皇城 面板
     */
    static readonly MsgID_ZZHC_Pannel_Response: number = 5446;

    /**
     * 5449, 至尊皇城 推荐攻打领地
     */
    static readonly MsgID_ZZHC_Recommond_Request: number = 5449;

    /**
     * 5450, 至尊皇城 推荐攻打领地
     */
    static readonly MsgID_ZZHC_Recommond_Response: number = 5450;

    /**
     * 5447, 至尊皇城 领奖
     */
    static readonly MsgID_ZZHC_Reward_Request: number = 5447;

    /**
     * 5448, 至尊皇城 领奖
     */
    static readonly MsgID_ZZHC_Reward_Response: number = 5448;

    /**
     * 5340, 打坐请求
     */
    static readonly MsgID_Zazen_Request: number = 5340;

    /**
     * 5341, 打坐响应
     */
    static readonly MsgID_Zazen_Response: number = 5341;

    /**
     * 5161, 0点刷新数据包
     */
    static readonly MsgID_ZeroRefreshData_Notify: number = 5161;

    /**
     * 30, 角色可拥有的红颜最大数
     */
    static readonly NEW_ROLE_MAX_BEAUTY_COUNT: number = 30;

    /**
     * 50, 角色可拥有最大散仙个数
     */
    static readonly NEW_ROLE_MAX_PET_COUNT: number = 50;

    /**
     * 0, 进入普通zone
     */
    static readonly NOMARL_ZONE_TYPE: number = 0;

    /**
     * 120, 铭文试炼 普通色子回复时间 120秒
     */
    static readonly NORMAL_DICE_RESET_TIME: number = 120;

    /**
     * 2, 未购买时推送
     */
    static readonly NOTBUY_TYPE: number = 2;

    /**
     * 1, 买
     */
    static readonly NPC_BEHAVIOUR_BUY: number = 1;

    /**
     * 2, 回购
     */
    static readonly NPC_BEHAVIOUR_BUYBACK: number = 2;

    /**
     * 6, 治疗
     */
    static readonly NPC_BEHAVIOUR_HEAL: number = 6;

    /**
     * 7, 拉取商店额外随机物品
     */
    static readonly NPC_BEHAVIOUR_RANDOM_LIST: number = 7;

    /**
     * 8, 重置商店额外随机物品
     */
    static readonly NPC_BEHAVIOUR_RANDOM_RESET: number = 8;

    /**
     * 4, 退款
     */
    static readonly NPC_BEHAVIOUR_REFUND: number = 4;

    /**
     * 3, 卖
     */
    static readonly NPC_BEHAVIOUR_SELL: number = 3;

    /**
     * 9, 拍卖行回购
     */
    static readonly NPC_BEHAVIOUR_STORE_BUYBACK: number = 9;

    /**
     * 3, 随机物品商店刷新的类型，0点的自动刷新 
     */
    static readonly NS_REFRESH_TYPE_AUTO: number = 3;

    /**
     * 1, 随机物品商店刷新的类型,手动刷新 
     */
    static readonly NS_REFRESH_TYPE_MANUEL: number = 1;

    /**
     * 2, 随机物品商店刷新的类型，非0点的自动刷新 
     */
    static readonly NS_REFRESH_TYPE_ZERO: number = 2;

    /**
     * 2, 宗派名字
     */
    static readonly NameType_GuildID: number = 2;

    /**
     * 3, 角色名字修改
     */
    static readonly NameType_Modify_RoleID: number = 3;

    /**
     * 1, 角色名字
     */
    static readonly NameType_RoleID: number = 1;

    /**
     * 1, 增加名字
     */
    static readonly NameUpdateType_Add: number = 1;

    /**
     * 3, 修改名字
     */
    static readonly NameUpdateType_Modify: number = 3;

    /**
     * 2, 删除名字
     */
    static readonly NameUpdateType_Remove: number = 2;

    /**
     * 1, 需要回复
     */
    static readonly NeedResponse: number = 1;

    /**
     * 3, 某个好友信息
     */
    static readonly OFFLINE_FRIEND_INFO: number = 3;

    /**
     * 4, 变性卡改名卡更改
     */
    static readonly OFFLINE_GENDER_NAME_CHANGE: number = 4;

    /**
     * 2, 婚姻数据中配偶的离线时间
     */
    static readonly OFFLINE_LOVER_LOGOUT: number = 2;

    /**
     * 1, 婚姻数据
     */
    static readonly OFFLINE_MARRY_STATUS: number = 1;

    /**
     * 100, 一次喂养消耗精魄
     */
    static readonly ONCE_USE_SOUL_POINT: number = 100;

    /**
     * 2, Cluster 玩家Uin数据Bit数组size 可扩大
     */
    static readonly ONE_CLUSTER_ROLE_UIN_BIT_CNT: number = 2;

    /**
     * 13, 获取在线礼包
     */
    static readonly ONLINEGIFTACT_GET: number = 13;

    /**
     * 12, 在线礼包活动
     */
    static readonly ONLINEGIFTACT_SHOW: number = 12;

    /**
     * 116, 加载红包手气榜
     */
    static readonly ONLOAD_REDBAG_RANK: number = 116;

    /**
     * 111, 打开红包面板
     */
    static readonly OPEN_REDBAG_PANEL: number = 111;

    /**
     * 103, 清CD
     */
    static readonly OPERATE_SKILL_CLEANCD: number = 103;

    /**
     * 104, 设置羁绊技能
     */
    static readonly OPERATE_SKILL_SET_FETTER: number = 104;

    /**
     * 101, 学习
     */
    static readonly OPERATE_SKILL_STUDY: number = 101;

    /**
     * 102, 设置翅膀|    坐骑技能有效列表
     */
    static readonly OPERATE_SKILL_VALID: number = 102;

    /**
     * 999999999, OSS 消息标志
     */
    static readonly OSS_MSG_TAG: number = 999999999;

    /**
     * 4, 玩家是否单笔充值超过SuperVIP要求了
     */
    static readonly OSS_WAIT_CHARGESUPERVIP: number = 4;

    /**
     * 8, OSS设置该玩家是聊天GM
     */
    static readonly OSS_WAIT_ISGM: number = 8;

    /**
     * 16, OSS设置该玩家是个托，交易元宝受限
     */
    static readonly OSS_WAIT_ISTUO: number = 16;

    /**
     * 1, OSS来的手机注册礼包
     */
    static readonly OSS_WAIT_MOBILE: number = 1;

    /**
     * 2, OSS来的SuperVIP礼包
     */
    static readonly OSS_WAIT_SUPERVIP: number = 2;

    /**
     * 1, 过期推送
     */
    static readonly OVERTIME_TYPE: number = 1;

    /**
     * 5
     */
    static readonly OperateTeam_ApplyTeam: number = 5;

    /**
     * 17
     */
    static readonly OperateTeam_AutoApply: number = 17;

    /**
     * 15
     */
    static readonly OperateTeam_CancelConvene: number = 15;

    /**
     * 11
     */
    static readonly OperateTeam_ChangeName: number = 11;

    /**
     * 6
     */
    static readonly OperateTeam_CheckApplicant: number = 6;

    /**
     * 14
     */
    static readonly OperateTeam_CheckConvene: number = 14;

    /**
     * 9
     */
    static readonly OperateTeam_Convene: number = 9;

    /**
     * 1
     */
    static readonly OperateTeam_CreateTeam: number = 1;

    /**
     * 12
     */
    static readonly OperateTeam_DealWithInvitationResult: number = 12;

    /**
     * 10
     */
    static readonly OperateTeam_DeleteTeam: number = 10;

    /**
     * 16
     */
    static readonly OperateTeam_GetTeam: number = 16;

    /**
     * 8
     */
    static readonly OperateTeam_Hook: number = 8;

    /**
     * 4
     */
    static readonly OperateTeam_InviteRole: number = 4;

    /**
     * 2
     */
    static readonly OperateTeam_KickMember: number = 2;

    /**
     * 3
     */
    static readonly OperateTeam_LeaveTeam: number = 3;

    /**
     * 13
     */
    static readonly OperateTeam_NewCaptain: number = 13;

    /**
     * 7
     */
    static readonly OperateTeam_SharePrivilige: number = 7;

    /**
     * 6, 物品个数
     */
    static readonly PAIMAI_CFG_ITEM_COUNT: number = 6;

    /**
     * 3, 材料副本 每个章节3个难度
     */
    static readonly PER_CLFB_SECTION_DIFF: number = 3;

    /**
     * 2, 每个赛程最多投注2个比赛
     */
    static readonly PER_KFJDC_BET_COUNT: number = 2;

    /**
     * 2, 散仙塑魂涨经验
     */
    static readonly PET_SUHUN_ADDEXP: number = 2;

    /**
     * 0, 散仙塑魂升级失败
     */
    static readonly PET_SUHUN_UPFAIL: number = 0;

    /**
     * 1, 散仙塑魂升级成功
     */
    static readonly PET_SUHUN_UPSUCCESS: number = 1;

    /**
     * 10, VIP购买刷新次数（多人boss）
     */
    static readonly PINHOME_BUY_MUlTIBOSS_REFRESH_TIMES: number = 10;

    /**
     * 3, 副本领取礼包
     */
    static readonly PINHOME_GETGIFT: number = 3;

    /**
     * 5, 副本排行信息查询
     */
    static readonly PINHOME_GET_RANKINFO: number = 5;

    /**
     * 6, 副本时间排行信息查询
     */
    static readonly PINHOME_GET_RANKTIMEINFO: number = 6;

    /**
     * 1, 副本信息查询
     */
    static readonly PINHOME_LIST: number = 1;

    /**
     * 4, 副本列表信息查询
     */
    static readonly PINHOME_LIST_ALL: number = 4;

    /**
     * 7, 刷新奔跑吧兄弟副本时间
     */
    static readonly PINHOME_RANKTIME_REFRESH: number = 7;

    /**
     * 9, 个人boss刷新CD
     */
    static readonly PINHOME_REFRESH_PRIVATE_BOSS: number = 9;

    /**
     * 2, 副本重置
     */
    static readonly PINHOME_RESET: number = 2;

    /**
     * 8, VIP购买次数（个人boss）
     */
    static readonly PINHOME_VIP_BUY_TIMES: number = 8;

    /**
     * 1, 副本奖励类型_每日首通
     */
    static readonly PINSTANCE_BONUS_TYPE_DAILY: number = 1;

    /**
     * 2, 副本奖励类型_终生首通
     */
    static readonly PINSTANCE_BONUS_TYPE_LIFE: number = 2;

    /**
     * 300082, 温泉副本ID
     */
    static readonly PINSTANCE_ID_BATHE: number = 300082;

    /**
     * 300024, 奔跑吧兄弟副本ID
     */
    static readonly PINSTANCE_ID_BPXD: number = 300024;

    /**
     * 300042, 阵营战副本ID
     */
    static readonly PINSTANCE_ID_CAMP_BATTLE_ACT: number = 300042;

    /**
     * 300084, 四象斗兽场-副本ID
     */
    static readonly PINSTANCE_ID_COLOSSEUM: number = 300084;

    /**
     * 300080, 跨服封神台副本ID
     */
    static readonly PINSTANCE_ID_CROSS_PVP: number = 300080;

    /**
     * 300035, 地榜竞技
     */
    static readonly PINSTANCE_ID_DIBANG: number = 300035;

    /**
     * 300090, 地宫副本ID
     */
    static readonly PINSTANCE_ID_DIGONG: number = 300090;

    /**
     * 300054, 小鸡小鸡了副本ID
     */
    static readonly PINSTANCE_ID_DYZSPIN: number = 300054;

    /**
     * 300053, 封魔塔副本ID
     */
    static readonly PINSTANCE_ID_FMT: number = 300053;

    /**
     * 300107, 落日森林
     */
    static readonly PINSTANCE_ID_FOREST_BOSS: number = 300107;

    /**
     * 300013, 夫妻副本ID
     */
    static readonly PINSTANCE_ID_FUQI: number = 300013;

    /**
     * 300014, 挂机图
     */
    static readonly PINSTANCE_ID_GUAJI: number = 300014;

    /**
     * 300031, 宗派群英会
     */
    static readonly PINSTANCE_ID_GUILDPVP: number = 300031;

    /**
     * 300049, 跨服宗派群英会
     */
    static readonly PINSTANCE_ID_GUILDPVP_CROSS: number = 300049;

    /**
     * 300093, 宗派探矿
     */
    static readonly PINSTANCE_ID_GUILD_TREASURE_HUNT: number = 300093;

    /**
     * 300056, 铭文试炼
     */
    static readonly PINSTANCE_ID_HISTORICAL_REMAINS: number = 300056;

    /**
     * 300063, 魂力试练
     */
    static readonly PINSTANCE_ID_HLSL: number = 300063;

    /**
     * 300086, BOSS之家
     */
    static readonly PINSTANCE_ID_HOME_BOSS: number = 300086;

    /**
     * 300001, 剑帝遗迹
     */
    static readonly PINSTANCE_ID_JDYJ: number = 300001;

    /**
     * 300006, 剑帝遗迹2
     */
    static readonly PINSTANCE_ID_JDYJ_2: number = 300006;

    /**
     * 300051, 聚元副本ID
     */
    static readonly PINSTANCE_ID_JUYUAN: number = 300051;

    /**
     * 300081, 九转星宫战场副本ID
     */
    static readonly PINSTANCE_ID_JZXG: number = 300081;

    /**
     * 300052, 跨服年兽
     */
    static readonly PINSTANCE_ID_KFNS: number = 300052;

    /**
     * 300057, 个人引导RMB副本ID
     */
    static readonly PINSTANCE_ID_LEAD_RMB: number = 300057;

    /**
     * 300050, 魔方副本ID
     */
    static readonly PINSTANCE_ID_MAGICCUBE: number = 300050;

    /**
     * 300045, 魔化战争副本
     */
    static readonly PINSTANCE_ID_MHZZ: number = 300045;

    /**
     * 300044, 名将挑战副本
     */
    static readonly PINSTANCE_ID_MJTZ: number = 300044;

    /**
     * 300102, 魔瞳进修 材料副本
     */
    static readonly PINSTANCE_ID_MTJX: number = 300102;

    /**
     * 300100, 多人BOSS
     */
    static readonly PINSTANCE_ID_MULTIPLAYER_BOSS: number = 300100;

    /**
     * 300083, 跨服多人-3V3-副本ID
     */
    static readonly PINSTANCE_ID_MULTIPVP: number = 300083;

    /**
     * 300005, 魔焰窟寻宝副本ID
     */
    static readonly PINSTANCE_ID_MYKXB: number = 300005;

    /**
     * 300104, 迷踪进修 材料副本
     */
    static readonly PINSTANCE_ID_MZJX: number = 300104;

    /**
     * 300033, 南蛮入侵副本ID
     */
    static readonly PINSTANCE_ID_NMRQ: number = 300033;

    /**
     * 300085, 个人BOSS副本
     */
    static readonly PINSTANCE_ID_PERSONALBOSS: number = 300085;

    /**
     * 300061, 个人boss
     */
    static readonly PINSTANCE_ID_PRIVATE_BOSS: number = 300061;

    /**
     * 300012, 封神台副本ID
     */
    static readonly PINSTANCE_ID_PVP: number = 300012;

    /**
     * 300105, 擒龙进修 材料副本
     */
    static readonly PINSTANCE_ID_QLJX: number = 300105;

    /**
     * 300021, 答题副本ID
     */
    static readonly PINSTANCE_ID_QUESTION: number = 300021;

    /**
     * 300058, rmb战场副本ID
     */
    static readonly PINSTANCE_ID_RMBZC: number = 300058;

    /**
     * 300017, 神盾副本
     */
    static readonly PINSTANCE_ID_SHENDUN: number = 300017;

    /**
     * 300016, 圣器副本
     */
    static readonly PINSTANCE_ID_SHENGQI: number = 300016;

    /**
     * 300003, 守护女神
     */
    static readonly PINSTANCE_ID_SHNS: number = 300003;

    /**
     * 300039, 跨服单人PVP副本
     */
    static readonly PINSTANCE_ID_SINGLEPVP: number = 300039;

    /**
     * 300040, 跨服单人PVP副本（决赛）
     */
    static readonly PINSTANCE_ID_SINGLEPVP_FINALID: number = 300040;

    /**
     * 300059, 神魔遮天
     */
    static readonly PINSTANCE_ID_SMZT: number = 300059;

    /**
     * 300030, 特殊特权副本
     */
    static readonly PINSTANCE_ID_SPECIALPRI: number = 300030;

    /**
     * 300037, 死亡战场
     */
    static readonly PINSTANCE_ID_SWZC: number = 300037;

    /**
     * 300032, 神选之路
     */
    static readonly PINSTANCE_ID_SXZL: number = 300032;

    /**
     * 300036, 天榜竞技
     */
    static readonly PINSTANCE_ID_TIANBANG: number = 300036;

    /**
     * 300106, VIP副本
     */
    static readonly PINSTANCE_ID_VIP: number = 300106;

    /**
     * 300091, 王侯将相 副本ID
     */
    static readonly PINSTANCE_ID_WHJX: number = 300091;

    /**
     * 300041, 世界Boss副本
     */
    static readonly PINSTANCE_ID_WORLDBOSS: number = 300041;

    /**
     * 300004, 武神坛
     */
    static readonly PINSTANCE_ID_WST: number = 300004;

    /**
     * 300002, 武缘副本
     */
    static readonly PINSTANCE_ID_WYFB: number = 300002;

    /**
     * 300043, 武缘远征副本
     */
    static readonly PINSTANCE_ID_WYYZ: number = 300043;

    /**
     * 300103, 玄天进修 材料副本
     */
    static readonly PINSTANCE_ID_XTJX: number = 300103;

    /**
     * 300092, 血战封魔副本
     */
    static readonly PINSTANCE_ID_XZFM: number = 300092;

    /**
     * 300047, 原梦战场
     */
    static readonly PINSTANCE_ID_YMZC: number = 300047;

    /**
     * 300060, 原神战场
     */
    static readonly PINSTANCE_ID_YSZC: number = 300060;

    /**
     * 300015, 祝福装备副本
     */
    static readonly PINSTANCE_ID_ZFZB: number = 300015;

    /**
     * 300038, 珍珑棋局
     */
    static readonly PINSTANCE_ID_ZLQJ: number = 300038;

    /**
     * 300011, 宗门秘境
     */
    static readonly PINSTANCE_ID_ZPFM: number = 300011;

    /**
     * 300101, 坐骑副本 材料副本
     */
    static readonly PINSTANCE_ID_ZQFB: number = 300101;

    /**
     * 300025, 祝融祭坛_1
     */
    static readonly PINSTANCE_ID_ZRJT_1: number = 300025;

    /**
     * 300026, 祝融祭坛_2
     */
    static readonly PINSTANCE_ID_ZRJT_2: number = 300026;

    /**
     * 300027, 祝融祭坛_3
     */
    static readonly PINSTANCE_ID_ZRJT_3: number = 300027;

    /**
     * 300048, 至尊皇城
     */
    static readonly PINSTANCE_ID_ZZHC: number = 300048;

    /**
     * 1, 原梦战场查询
     */
    static readonly PINSTANCE_RANK_YMZC: number = 1;

    /**
     * 2, 元神战场查询
     */
    static readonly PINSTANCE_RANK_YSZC: number = 2;

    /**
     * 4, 齐神
     */
    static readonly PINSTANCE_RESULT_GOD: number = 4;

    /**
     * 5, 超神
     */
    static readonly PINSTANCE_RESULT_HIGHGOD: number = 5;

    /**
     * 2, 输
     */
    static readonly PINSTANCE_RESULT_LOST: number = 2;

    /**
     * 3, 望神
     */
    static readonly PINSTANCE_RESULT_LOWGOD: number = 3;

    /**
     * 6, 过关
     */
    static readonly PINSTANCE_RESULT_PASS: number = 6;

    /**
     * 3, 副本击杀boss面板
     */
    static readonly PINSTANCE_RESULT_TYPE_BOSS: number = 3;

    /**
     * 4, 有难度的副本结算面板
     */
    static readonly PINSTANCE_RESULT_TYPE_DIFF: number = 4;

    /**
     * 1, 副本通用面板
     */
    static readonly PINSTANCE_RESULT_TYPE_GENERAL: number = 1;

    /**
     * 2, 副本过关面板
     */
    static readonly PINSTANCE_RESULT_TYPE_PASS: number = 2;

    /**
     * 1, 赢
     */
    static readonly PINSTANCE_RESULT_WIN: number = 1;

    /**
     * 0, 副本附加信息 占位
     */
    static readonly PIN_EXTRA_NULL: number = 0;

    /**
     * 2, 个人boss副本附加信息
     */
    static readonly PIN_EXTRA_PRIVATEBOSS: number = 2;

    /**
     * 1, 武缘副本附加信息
     */
    static readonly PIN_EXTRA_WYFB: number = 1;

    /**
     * 1, 副本排行，守护女神(神凰秘境)
     */
    static readonly PIN_RANK_SHNS: number = 1;

    /**
     * 50, 守护女神(神凰秘境)总榜第一称号 最低层
     */
    static readonly PIN_RANK_SHNS_FIRST_LEVEL: number = 50;

    /**
     * 3, 副本排行，神选之路
     */
    static readonly PIN_RANK_SXZL: number = 3;

    /**
     * 0, 副本排行，武神坛(帝者之路)
     */
    static readonly PIN_RANK_WST: number = 0;

    /**
     * 15, 武神坛(帝者之路)总榜第一称号 最低层
     */
    static readonly PIN_RANK_WST_FIRST_LEVEL: number = 15;

    /**
     * 15, 武神坛(帝者之路)速度第一称号 最低层
     */
    static readonly PIN_RANK_WST_MOST_LEVEL: number = 15;

    /**
     * 2, 副本排行，武缘副本
     */
    static readonly PIN_RANK_WYFB: number = 2;

    /**
     * 15, 武缘副本总榜第一称号 最低层
     */
    static readonly PIN_RANK_WYFB_FIRST_LEVEL: number = 15;

    /**
     * 15, 武缘副本速度第一称号 最低层
     */
    static readonly PIN_RANK_WYFB_MOST_LEVEL: number = 15;

    /**
     * 3, PK模式:全体
     */
    static readonly PK_STATUS_ALL: number = 3;

    /**
     * 5, PK模式:军团
     */
    static readonly PK_STATUS_ARMY: number = 5;

    /**
     * 6, PK模式:当前最大值 加一个模式扩一下
     */
    static readonly PK_STATUS_CURMAX: number = 6;

    /**
     * 4, PK模式:善恶
     */
    static readonly PK_STATUS_EVIL: number = 4;

    /**
     * 2, PK模式:宗派
     */
    static readonly PK_STATUS_GUILD: number = 2;

    /**
     * 0, PK模式:和平
     */
    static readonly PK_STATUS_PEACE: number = 0;

    /**
     * 1, PK模式:队伍
     */
    static readonly PK_STATUS_TEAM: number = 1;

    /**
     * 6, PK模式:区服
     */
    static readonly PK_STATUS_ZONE: number = 6;

    /**
     * 1001, 联合平台1标志 
     */
    static readonly PLAT_FORM_LINK_1: number = 1001;

    /**
     * 1010, 联合平台10标志
     */
    static readonly PLAT_FORM_LINK_10: number = 1010;

    /**
     * 1002, 联合平台2标志
     */
    static readonly PLAT_FORM_LINK_2: number = 1002;

    /**
     * 1003, 联合平台3标志
     */
    static readonly PLAT_FORM_LINK_3: number = 1003;

    /**
     * 1004, 联合平台4标志
     */
    static readonly PLAT_FORM_LINK_4: number = 1004;

    /**
     * 1005, 联合平台5标志
     */
    static readonly PLAT_FORM_LINK_5: number = 1005;

    /**
     * 1006, 联合平台6标志
     */
    static readonly PLAT_FORM_LINK_6: number = 1006;

    /**
     * 1007, 联合平台7标志
     */
    static readonly PLAT_FORM_LINK_7: number = 1007;

    /**
     * 1008, 联合平台8标志
     */
    static readonly PLAT_FORM_LINK_8: number = 1008;

    /**
     * 1009, 联合平台9标志
     */
    static readonly PLAT_FORM_LINK_9: number = 1009;

    /**
     * 74, 008WAN平台标志
     */
    static readonly PLAT_FORM_TYPE_008WAN: number = 74;

    /**
     * 14, 11平台标志
     */
    static readonly PLAT_FORM_TYPE_11: number = 14;

    /**
     * 72, 134wan平台标志
     */
    static readonly PLAT_FORM_TYPE_134WAN: number = 72;

    /**
     * 53, 152G 平台标志
     */
    static readonly PLAT_FORM_TYPE_152G: number = 53;

    /**
     * 58, 160YX 平台标志
     */
    static readonly PLAT_FORM_TYPE_160YX: number = 58;

    /**
     * 31, 16768平台标志
     */
    static readonly PLAT_FORM_TYPE_16768: number = 31;

    /**
     * 56, 17iy 平台标志
     */
    static readonly PLAT_FORM_TYPE_17IY: number = 56;

    /**
     * 20, 2144平台标志
     */
    static readonly PLAT_FORM_TYPE_2144: number = 20;

    /**
     * 45, 218you 平台标志
     */
    static readonly PLAT_FORM_TYPE_218YOU: number = 45;

    /**
     * 29, 2217平台标志
     */
    static readonly PLAT_FORM_TYPE_2217: number = 29;

    /**
     * 66, 265G平台标志
     */
    static readonly PLAT_FORM_TYPE_265G: number = 66;

    /**
     * 5, 360平台标志
     */
    static readonly PLAT_FORM_TYPE_360: number = 5;

    /**
     * 42, 360UU 平台标志
     */
    static readonly PLAT_FORM_TYPE_360UU: number = 42;

    /**
     * 1, 37玩平台标志
     */
    static readonly PLAT_FORM_TYPE_37WAN: number = 1;

    /**
     * 73, 3DM平台标志
     */
    static readonly PLAT_FORM_TYPE_3DM: number = 73;

    /**
     * 41, 43u   平台标志
     */
    static readonly PLAT_FORM_TYPE_43U: number = 41;

    /**
     * 32, 49游平台标志
     */
    static readonly PLAT_FORM_TYPE_49YOU: number = 32;

    /**
     * 75, 4JYX平台标志
     */
    static readonly PLAT_FORM_TYPE_4JYX: number = 75;

    /**
     * 63, 501wan 平台标志
     */
    static readonly PLAT_FORM_TYPE_501WAN: number = 63;

    /**
     * 21, 51平台标志
     */
    static readonly PLAT_FORM_TYPE_51: number = 21;

    /**
     * 26, 511玩平台标志
     */
    static readonly PLAT_FORM_TYPE_511WAN: number = 26;

    /**
     * 25, 51玩平台标志
     */
    static readonly PLAT_FORM_TYPE_51WAN: number = 25;

    /**
     * 11, 5599平台标志
     */
    static readonly PLAT_FORM_TYPE_5599: number = 11;

    /**
     * 47, 579u   平台标志
     */
    static readonly PLAT_FORM_TYPE_579U: number = 47;

    /**
     * 19, 602 平台标志
     */
    static readonly PLAT_FORM_TYPE_602: number = 19;

    /**
     * 50, 655a 平台标志
     */
    static readonly PLAT_FORM_TYPE_655A: number = 50;

    /**
     * 59, 66YOU 平台标志
     */
    static readonly PLAT_FORM_TYPE_66YOU: number = 59;

    /**
     * 4, 6711平台标志
     */
    static readonly PLAT_FORM_TYPE_6711: number = 4;

    /**
     * 30, 7477平台标志
     */
    static readonly PLAT_FORM_TYPE_7477: number = 30;

    /**
     * 70, 7676平台标志
     */
    static readonly PLAT_FORM_TYPE_7676: number = 70;

    /**
     * 39, 8090YXS平台标志
     */
    static readonly PLAT_FORM_TYPE_8090YXS: number = 39;

    /**
     * 61, 844a 平台标志
     */
    static readonly PLAT_FORM_TYPE_844A: number = 61;

    /**
     * 46, 白鹭  平台标志
     */
    static readonly PLAT_FORM_TYPE_86HUD: number = 46;

    /**
     * 71, 877wan平台标志
     */
    static readonly PLAT_FORM_TYPE_877WAN: number = 71;

    /**
     * 60, 883WAN 平台标志
     */
    static readonly PLAT_FORM_TYPE_883WAN: number = 60;

    /**
     * 24, 9377平台标志
     */
    static readonly PLAT_FORM_TYPE_9377: number = 24;

    /**
     * 34, 97971平台标志
     */
    static readonly PLAT_FORM_TYPE_97971: number = 34;

    /**
     * 40, 9ewan 平台标志
     */
    static readonly PLAT_FORM_TYPE_9EWAN: number = 40;

    /**
     * 67, afgame平台标志
     */
    static readonly PLAT_FORM_TYPE_AFGAME: number = 67;

    /**
     * 69, 爱页游平台标志
     */
    static readonly PLAT_FORM_TYPE_AIYEYOU: number = 69;

    /**
     * 35, 傲天平台标志
     */
    static readonly PLAT_FORM_TYPE_AOTIAN: number = 35;

    /**
     * 2, 百度平台标志
     */
    static readonly PLAT_FORM_TYPE_BAIDU: number = 2;

    /**
     * 48, 大猪 平台标志
     */
    static readonly PLAT_FORM_TYPE_BIGZHU: number = 48;

    /**
     * 43, ccjoy 平台标志
     */
    static readonly PLAT_FORM_TYPE_CCJOY: number = 43;

    /**
     * 17, 飞火网平台标志
     */
    static readonly PLAT_FORM_TYPE_FEIHUO: number = 17;

    /**
     * 10, 风行平台标志
     */
    static readonly PLAT_FORM_TYPE_FX: number = 10;

    /**
     * 18, G2平台标志
     */
    static readonly PLAT_FORM_TYPE_G2: number = 18;

    /**
     * 44, huanleren 平台标志
     */
    static readonly PLAT_FORM_TYPE_HUANLEREN: number = 44;

    /**
     * 38, KU25平台标志
     */
    static readonly PLAT_FORM_TYPE_KU25: number = 38;

    /**
     * 22, 快玩平台标志
     */
    static readonly PLAT_FORM_TYPE_KUAIWAN: number = 22;

    /**
     * 12, 酷狗平台标志
     */
    static readonly PLAT_FORM_TYPE_KUGOU: number = 12;

    /**
     * 55, 酷客玩 平台标志
     */
    static readonly PLAT_FORM_TYPE_KUKEWAN: number = 55;

    /**
     * 7, 酷我平台标志
     */
    static readonly PLAT_FORM_TYPE_KUWO: number = 7;

    /**
     * 27, 开心玩平台标志
     */
    static readonly PLAT_FORM_TYPE_KXWAN: number = 27;

    /**
     * 64, 乐嗨嗨 平台标志
     */
    static readonly PLAT_FORM_TYPE_LEHIHI: number = 64;

    /**
     * 3, 猎豹平台标志
     */
    static readonly PLAT_FORM_TYPE_LIEBAO: number = 3;

    /**
     * 51, 圈圈 平台标志
     */
    static readonly PLAT_FORM_TYPE_NPCKA: number = 51;

    /**
     * 23, PPS	平台标志
     */
    static readonly PLAT_FORM_TYPE_PPS: number = 23;

    /**
     * 28, PPTV平台标志
     */
    static readonly PLAT_FORM_TYPE_PPTV: number = 28;

    /**
     * 13, 起点平台标志
     */
    static readonly PLAT_FORM_TYPE_QD: number = 13;

    /**
     * 6, 顺网平台标志
     */
    static readonly PLAT_FORM_TYPE_SHUNWANG: number = 6;

    /**
     * 8, 搜狗平台标志
     */
    static readonly PLAT_FORM_TYPE_SOGOU: number = 8;

    /**
     * 6, 手游北鲲平台
     */
    static readonly PLAT_FORM_TYPE_SYBEIKUN: number = 6;

    /**
     * 3, 手游动网平台
     */
    static readonly PLAT_FORM_TYPE_SYDONGWANG: number = 3;

    /**
     * 4, 手游公会平台
     */
    static readonly PLAT_FORM_TYPE_SYGONGHUI: number = 4;

    /**
     * 5, 手游三象平台
     */
    static readonly PLAT_FORM_TYPE_SYSANXIANG: number = 5;

    /**
     * 8, 手游问剑逆天平台
     */
    static readonly PLAT_FORM_TYPE_SYWJLT: number = 8;

    /**
     * 1, 手游咸鱼平台
     */
    static readonly PLAT_FORM_TYPE_SYXIANYU: number = 1;

    /**
     * 2, 手游西游平台
     */
    static readonly PLAT_FORM_TYPE_SYXIYOU: number = 2;

    /**
     * 7, 手游轩辕问剑平台
     */
    static readonly PLAT_FORM_TYPE_SYXYWJ: number = 7;

    /**
     * 30000, 腾讯平台标志
     */
    static readonly PLAT_FORM_TYPE_TX: number = 30000;

    /**
     * 52, u8wan 平台标志
     */
    static readonly PLAT_FORM_TYPE_U8WAN: number = 52;

    /**
     * 65, UC669平台标志
     */
    static readonly PLAT_FORM_TYPE_UC669: number = 65;

    /**
     * 62, uu1758 平台标志
     */
    static readonly PLAT_FORM_TYPE_UU1758: number = 62;

    /**
     * 68, V1平台标志
     */
    static readonly PLAT_FORM_TYPE_V1: number = 68;

    /**
     * 54, 玩机之家 平台标志
     */
    static readonly PLAT_FORM_TYPE_WANJIZHIJIA: number = 54;

    /**
     * 500, 360绩效平台标志
     */
    static readonly PLAT_FORM_TYPE_WUJI: number = 500;

    /**
     * 2000, 西游网平台
     */
    static readonly PLAT_FORM_TYPE_XIYOU: number = 2000;

    /**
     * 9, 迅雷平台标志
     */
    static readonly PLAT_FORM_TYPE_XUNLEI: number = 9;

    /**
     * 49, 页游吧 平台标志
     */
    static readonly PLAT_FORM_TYPE_YEYOU8: number = 49;

    /**
     * 36, 易乐玩平台标志
     */
    static readonly PLAT_FORM_TYPE_YILEWAN: number = 36;

    /**
     * 16, 趣游游戏平台标志
     */
    static readonly PLAT_FORM_TYPE_YOUXI: number = 16;

    /**
     * 37, 缘来网平台标志
     */
    static readonly PLAT_FORM_TYPE_YUANLAI: number = 37;

    /**
     * 57, YX58 平台标志
     */
    static readonly PLAT_FORM_TYPE_YX58: number = 57;

    /**
     * 15, yy平台标志
     */
    static readonly PLAT_FORM_TYPE_YY: number = 15;

    /**
     * 33, 紫霞平台标志
     */
    static readonly PLAT_FORM_TYPE_ZIXIA: number = 33;

    /**
     * 0, 360V计划平台会员, 不需要检测
     */
    static readonly PLAT_VIP_360V_NONE: number = 0;

    /**
     * 2, 360V计划平台会员, 普通会员
     */
    static readonly PLAT_VIP_360V_NORMAL_VIP: number = 2;

    /**
     * 1, 360V计划平台会员, 不是会员
     */
    static readonly PLAT_VIP_360V_NOT_VIP: number = 1;

    /**
     * 3, 360V计划平台会员, 年费会员
     */
    static readonly PLAT_VIP_360V_YEAR_VIP: number = 3;

    /**
     * 0, 寄售面板查看
     */
    static readonly PPSTORESORT_DISP_FLAG_NORMAL: number = 0;

    /**
     * 2000, 单次购买最大数量
     */
    static readonly PPSTORESORT_MAX_BUY_NUMBER: number = 2000;

    /**
     * 1, 按升序
     */
    static readonly PPSTORESORT_REQUEST_ASC: number = 1;

    /**
     * 2, 按降序
     */
    static readonly PPSTORESORT_REQUEST_DESC: number = 2;

    /**
     * 2, 按颜色
     */
    static readonly PPSTORESORT_REQUEST_TYPE_COLOR: number = 2;

    /**
     * 1, 按等级
     */
    static readonly PPSTORESORT_REQUEST_TYPE_LEVEL: number = 1;

    /**
     * 3, 按价格
     */
    static readonly PPSTORESORT_REQUEST_TYPE_PRICE: number = 3;

    /**
     * 100000, 拍卖铜钱为10W的倍数
     */
    static readonly PPSTORESORT_TONGQIAN_BASE: number = 100000;

    /**
     * 100000000, 每单最大购买和挂售铜钱上限为1亿
     */
    static readonly PPSTORESORT_TONGQIAN_MAX_ONE_BILL: number = 100000000;

    /**
     * 250, 最大售卖物品种类数量
     */
    static readonly PPSTORE_MAX_MAX_ALLITEM_NUM: number = 250;

    /**
     * 12, 每个玩家最大提单数目
     */
    static readonly PPSTORE_MAX_PSB_NUMBER_PER_ROLE: number = 12;

    /**
     * 84, 充值特惠  领取奖励
     */
    static readonly PREFER_CHARGE_GET: number = 84;

    /**
     * 83, 充值特惠  面板
     */
    static readonly PREFER_CHARGE_PANEL: number = 83;

    /**
     * 1, 充值特惠每日奖励
     */
    static readonly PREFER_CHARGE_TYPE_DAY: number = 1;

    /**
     * 2, 充值特惠达成奖励
     */
    static readonly PREFER_CHARGE_TYPE_GET: number = 2;

    /**
     * 10021021, 门派任务完成10轮后的奖励物品
     */
    static readonly PROF_QUEST_REWARD_THING_ID: number = 10021021;

    /**
     * 50001301, 器魂师VIP技能ID
     */
    static readonly PROF_TYPE_HUNTER_VIP_REAL_SKILL_ID: number = 50001301;

    /**
     * 50041001, 器魂师VIP体验技能ID
     */
    static readonly PROF_TYPE_HUNTER_VIP_SKILL_ID: number = 50041001;

    /**
     * 50002001, 兽魂师VIP技能ID
     */
    static readonly PROF_TYPE_WARRIOR_VIP_REAL_SKILL_ID: number = 50002001;

    /**
     * 50041101, 兽魂师VIP体验技能ID
     */
    static readonly PROF_TYPE_WARRIOR_VIP_SKILL_ID: number = 50041101;

    /**
     * 1, prompt特效消息，结婚2挡特效
     */
    static readonly PROMPTMSG_EFFECT_MARRY1: number = 1;

    /**
     * 2, prompt特效消息，结婚3挡特效
     */
    static readonly PROMPTMSG_EFFECT_MARRY2: number = 2;

    /**
     * 12, 诅咒宝箱来了
     */
    static readonly PROMPTMSG_TYPE_BOX_COMMING: number = 12;

    /**
     * 16, 特效消息
     */
    static readonly PROMPTMSG_TYPE_EFFECT: number = 16;

    /**
     * 7, 敌军热点制压 
     */
    static readonly PROMPTMSG_TYPE_ENEMY_ZHIYA: number = 7;

    /**
     * 5, 重要通知
     */
    static readonly PROMPTMSG_TYPE_IMPORTANT: number = 5;

    /**
     * 6, 连杀终结通知 m_uiTypeValue为角色性别* 100 + 职业
     */
    static readonly PROMPTMSG_TYPE_LASTKILL_END: number = 6;

    /**
     * 15, 灵宝触发消息
     */
    static readonly PROMPTMSG_TYPE_LINGBAO: number = 15;

    /**
     * 16, 提示消息最大值
     */
    static readonly PROMPTMSG_TYPE_MAX: number = 16;

    /**
     * 4, 中间显示信息
     */
    static readonly PROMPTMSG_TYPE_MIDDLE: number = 4;

    /**
     * 9, 怪物全灭
     */
    static readonly PROMPTMSG_TYPE_MONSTER_ALLDEAD: number = 9;

    /**
     * 10, 怪物来袭
     */
    static readonly PROMPTMSG_TYPE_MONSTER_COMMING: number = 10;

    /**
     * 11, 怪物死亡 m_uiTypeValue为怪物ID
     */
    static readonly PROMPTMSG_TYPE_MONSTER_ONEDEAD: number = 11;

    /**
     * 13, 显示为鼠标位置提示消息
     */
    static readonly PROMPTMSG_TYPE_MOUSE: number = 13;

    /**
     * 1, 显示为固定提示消息
     */
    static readonly PROMPTMSG_TYPE_POPUP: number = 1;

    /**
     * 3, 中间滚动显示系统消息
     */
    static readonly PROMPTMSG_TYPE_ROLL: number = 3;

    /**
     * 8, 自军热点制压
     */
    static readonly PROMPTMSG_TYPE_SELF_ZHIYA: number = 8;

    /**
     * 14, 技能触发消息
     */
    static readonly PROMPTMSG_TYPE_SKILL: number = 14;

    /**
     * 1
     */
    static readonly PROMPT_PARAMETER_TYPE_INTEGER: number = 1;

    /**
     * 2
     */
    static readonly PROMPT_PARAMETER_TYPE_STRING: number = 2;

    /**
     * 5, 推送过期的月卡信息
     */
    static readonly PUSH_OVERTIME_CARD_INFO: number = 5;

    /**
     * 3, 购买挑战次数
     */
    static readonly PVPRANK_BUY_TIMES: number = 3;

    /**
     * 19, 看看自已有没有被杀过
     */
    static readonly PVPRANK_CROSS_BEKILL_INFO: number = 19;

    /**
     * 14, 跨服_购买挑战次数
     */
    static readonly PVPRANK_CROSS_BUY_TIMES: number = 14;

    /**
     * 50, 跨服_封神台GM
     */
    static readonly PVPRANK_CROSS_GM: number = 50;

    /**
     * 11, 跨服_封神台主面板消息
     */
    static readonly PVPRANK_CROSS_OPEN_PANEL: number = 11;

    /**
     * 12, 跨服_封神台英雄榜消息
     */
    static readonly PVPRANK_CROSS_OPEN_RANK: number = 12;

    /**
     * 16, 跨服_单场结果上报WORLD
     */
    static readonly PVPRANK_CROSS_PK_RESULT: number = 16;

    /**
     * 18, 跨服_发奖啦
     */
    static readonly PVPRANK_CROSS_RANK_REWARD: number = 18;

    /**
     * 13, 跨服_选中某人开打
     */
    static readonly PVPRANK_CROSS_START_PK: number = 13;

    /**
     * 17, 跨服_同步本次的排行榜
     */
    static readonly PVPRANK_CROSS_SYN_RANK: number = 17;

    /**
     * 20, 查看自己的排名信息
     */
    static readonly PVPRANK_CROSS_USERRANK_INFO: number = 20;

    /**
     * 8, 领取最大排名奖励
     */
    static readonly PVPRANK_GET_MAXRANK_REWARD: number = 8;

    /**
     * 6, 领取PVP奖励
     */
    static readonly PVPRANK_GET_REWARD: number = 6;

    /**
     * 7, 膜拜
     */
    static readonly PVPRANK_MOBAI: number = 7;

    /**
     * 1, 封神台主面板消息
     */
    static readonly PVPRANK_OPEN_PANEL: number = 1;

    /**
     * 5, 单场结果上报WORLD
     */
    static readonly PVPRANK_PK_RESULT: number = 5;

    /**
     * 2, 选中某人开打
     */
    static readonly PVPRANK_START_PK: number = 2;

    /**
     * 3, 散仙战斗
     */
    static readonly Pet_Status_Fight: number = 3;

    /**
     * 2, 散仙跟随
     */
    static readonly Pet_Status_Follow: number = 2;

    /**
     * 0, 散仙不存在
     */
    static readonly Pet_Status_NotExist: number = 0;

    /**
     * 1, 散仙休息
     */
    static readonly Pet_Status_Rest: number = 1;

    /**
     * 2, 执行祈福
     */
    static readonly QIFU_DOIT: number = 2;

    /**
     * 1, 打开祈福面板
     */
    static readonly QIFU_OPEN_PANEL: number = 1;

    /**
     * 117, 庆典消费排行榜打开面板
     */
    static readonly QINGDIAN_CONSUME_PANEL: number = 117;

    /**
     * 3, 购买双倍机会
     */
    static readonly QKL_BUY_DOUBLE: number = 3;

    /**
     * 4, 兑换乾坤炉奖励
     */
    static readonly QKL_DUIHUAN: number = 4;

    /**
     * 2, 执行炼制
     */
    static readonly QKL_LIANZHI: number = 2;

    /**
     * 1, 打开乾坤炉面板
     */
    static readonly QKL_OPEN_PANEL: number = 1;

    /**
     * 1, 查询背包状态
     */
    static readonly QUERY_BAGSTAT: number = 1;

    /**
     * 3, 通知答案
     */
    static readonly QUESTION_ACTIVITY_NTF_ANSWER: number = 3;

    /**
     * 1, 通知准备
     */
    static readonly QUESTION_ACTIVITY_NTF_PREPARE: number = 1;

    /**
     * 2, 通知出题
     */
    static readonly QUESTION_ACTIVITY_NTF_QEUSTION: number = 2;

    /**
     * 9, 日常任务保留次数找回
     */
    static readonly QUESTPANEL_DAILY_QUEST_SAVE: number = 9;

    /**
     * 4, 仅针对门派任务的，获取额外奖励
     */
    static readonly QUESTPANEL_GET_EXTRA_REWARD: number = 4;

    /**
     * 8, 皇榜任务找回
     */
    static readonly QUESTPANEL_HUANGBANG_SAVE: number = 8;

    /**
     * 1, 查询任务面板中的任务信息
     */
    static readonly QUESTPANEL_LIST: number = 1;

    /**
     * 5, 皇榜/前线/门派任务的一键完成
     */
    static readonly QUESTPANEL_ONEKEY_COMPLETE: number = 5;

    /**
     * 3, 任务刷新操作，仅对皇榜任务有效
     */
    static readonly QUESTPANEL_OPERATE_REFRESH: number = 3;

    /**
     * 2, 任务升星操作,仅对前线任务、门派任务有效
     */
    static readonly QUESTPANEL_OPERATE_UPLEVEL: number = 2;

    /**
     * 6, 仅针对虚空任务的任务刷新
     */
    static readonly QUESTPANEL_OPERATE_XUKONG_REFRESH: number = 6;

    /**
     * 7, 任务的快速一键完成
     */
    static readonly QUESTPANEL_QUICK_ONEKEY_COMPLETE: number = 7;

    /**
     * 60001601, 宗门任务全部完成奖励id
     */
    static readonly QUEST_DAILY_EXTRA_REWARD_ID: number = 60001601;

    /**
     * 2, 任务升星，一键满星
     */
    static readonly QUEST_FULL_UPLEVEL: number = 2;

    /**
     * 60001602, 日常任务全部完成奖励id
     */
    static readonly QUEST_GUILD_EXTRA_REWARD_ID: number = 60001602;

    /**
     * 1, 接任务跳
     */
    static readonly QUEST_JUMP_ACCEPT: number = 1;

    /**
     * 2, 完成任务跳
     */
    static readonly QUEST_JUMP_FINISH: number = 2;

    /**
     * 5, 任务满星等级
     */
    static readonly QUEST_MAX_FULL_LEVLE: number = 5;

    /**
     * 1, 任务升星，单次升星
     */
    static readonly QUEST_ONCE_UPLEVEL: number = 1;

    /**
     * 2, 任务状态，任务已放弃,放弃任务更新
     */
    static readonly QUEST_STATUS_CANCEL: number = 2;

    /**
     * 1, 任务状态，任务已领取奖励, 完成任务领取奖励时更新
     */
    static readonly QUEST_STATUS_COMPLETED: number = 1;

    /**
     * 103, 每日任务， 此处需要同keyword中的值保持一致
     */
    static readonly QUEST_TYPE_DAILY: number = 103;

    /**
     * 111, 前线任务， 此处需要同keyword中的值保持一致
     */
    static readonly QUEST_TYPE_FRONT_LINE: number = 111;

    /**
     * 113, 皇榜任务， 此处需要同keyword中的值保持一致
     */
    static readonly QUEST_TYPE_HUANG_BANG: number = 113;

    /**
     * 104, 门派任务， 此处需要同keyword中的值保持一致
     */
    static readonly QUEST_TYPE_PROF: number = 104;

    /**
     * 105, 虚空任务， 此处需要同keyword中的值保持一致
     */
    static readonly QUEST_TYPE_XUKONG: number = 105;

    /**
     * 101, 离开副本 的操作由脚本来处理
     */
    static readonly QUITPINSTANCE_BY_SCRIPT: number = 101;

    /**
     * 3
     */
    static readonly RANKINFO_FIELD_TYPE_BYTE: number = 3;

    /**
     * 1
     */
    static readonly RANKINFO_FIELD_TYPE_STRING: number = 1;

    /**
     * 2
     */
    static readonly RANKINFO_FIELD_TYPE_UINT: number = 2;

    /**
     * 2, 该角色未加入宗派
     */
    static readonly RANKINFO_RESPONSE_RESULTID_NOGUILD: number = 2;

    /**
     * 1, 该排行榜功能未开放
     */
    static readonly RANKINFO_RESPONSE_RESULTID_NOOPEN: number = 1;

    /**
     * 0, 成功返回数据
     */
    static readonly RANKINFO_RESPONSE_RESULTID_OK: number = 0;

    /**
     * 0, 排行版DB操作，默认
     */
    static readonly RANK_DB_OPERATE_DEFALUT: number = 0;

    /**
     * 1, 排行版DB操作，删除
     */
    static readonly RANK_DB_OPERATE_DELETE: number = 1;

    /**
     * 4, 排行版附带数据类型 红颜系统
     */
    static readonly RANK_EXTRA_TYPE_BEAUTY: number = 4;

    /**
     * 6, 排行版附带数据类型 法器系统
     */
    static readonly RANK_EXTRA_TYPE_FAQI: number = 6;

    /**
     * 2, 排行版附带数据类型 祝福子系统
     */
    static readonly RANK_EXTRA_TYPE_HEROSUB: number = 2;

    /**
     * 5, 排行版附带数据类型 道宫九星系统
     */
    static readonly RANK_EXTRA_TYPE_JIUXING: number = 5;

    /**
     * 3, 排行版附带数据类型 聚元系统
     */
    static readonly RANK_EXTRA_TYPE_JUYUAN: number = 3;

    /**
     * 0, 排行版附带数据类型 保留
     */
    static readonly RANK_EXTRA_TYPE_RESERVER: number = 0;

    /**
     * 1, 排行版附带数据类型 角色
     */
    static readonly RANK_EXTRA_TYPE_ROLE: number = 1;

    /**
     * 2, 降低
     */
    static readonly RANK_POS_DOWN: number = 2;

    /**
     * 0, 没变化
     */
    static readonly RANK_POS_SAME: number = 0;

    /**
     * 1, 升高
     */
    static readonly RANK_POS_UP: number = 1;

    /**
     * 2, 战力榜
     */
    static readonly RANK_TYPE_BATTLE: number = 2;

    /**
     * 4, 宝石榜
     */
    static readonly RANK_TYPE_DIAMOND: number = 4;

    /**
     * 17, 法器榜
     */
    static readonly RANK_TYPE_FAQI: number = 17;

    /**
     * 10, 阵法榜
     */
    static readonly RANK_TYPE_FZ: number = 10;

    /**
     * 12, 火精榜
     */
    static readonly RANK_TYPE_HJ: number = 12;

    /**
     * 14, 红颜榜
     */
    static readonly RANK_TYPE_HONGYAN: number = 14;

    /**
     * 20, 魂骨战力榜
     */
    static readonly RANK_TYPE_HUNGU: number = 20;

    /**
     * 19, 魂力等级榜
     */
    static readonly RANK_TYPE_HUNLI: number = 19;

    /**
     * 16, 玄天功榜
     */
    static readonly RANK_TYPE_JIUXING: number = 16;

    /**
     * 9, 精灵榜
     */
    static readonly RANK_TYPE_JL: number = 9;

    /**
     * 15, 聚元榜
     */
    static readonly RANK_TYPE_JUYUAN: number = 15;

    /**
     * 1, 等级榜
     */
    static readonly RANK_TYPE_LEVEL: number = 1;

    /**
     * 11, 足迹榜
     */
    static readonly RANK_TYPE_LL: number = 11;

    /**
     * 18, 魔方榜
     */
    static readonly RANK_TYPE_MAGIC: number = 18;

    /**
     * 21, 排行榜最大值
     */
    static readonly RANK_TYPE_MAX: number = 21;

    /**
     * 7, 美人榜
     */
    static readonly RANK_TYPE_MR: number = 7;

    /**
     * 3, 强化榜
     */
    static readonly RANK_TYPE_STRENGTH: number = 3;

    /**
     * 8, 武魂榜
     */
    static readonly RANK_TYPE_WH: number = 8;

    /**
     * 6, 羽翼榜
     */
    static readonly RANK_TYPE_YY: number = 6;

    /**
     * 13, 战灵榜
     */
    static readonly RANK_TYPE_ZL: number = 13;

    /**
     * 5, 坐骑榜
     */
    static readonly RANK_TYPE_ZQ: number = 5;

    /**
     * 97, 兑换充值奖励
     */
    static readonly RECHARGE_EXCHANGE_GET: number = 97;

    /**
     * 96, 打开充值兑换活动面板
     */
    static readonly RECHARGE_EXCHANGE_PANEL: number = 96;

    /**
     * 1, 招募队友类型，好友
     */
    static readonly RECRUITTEAM_TYPE_ROLE: number = 1;

    /**
     * 0, 招募队友类型，世界招募
     */
    static readonly RECRUITTEAM_TYPE_WORLD: number = 0;

    /**
     * 10, 每次最多能发10个红包
     */
    static readonly REDBAG_MAX_NUM: number = 10;

    /**
     * 2, 回归玩家已领取充值礼包
     */
    static readonly RETURNROLE_CHARGEGIFT_HAVE: number = 2;

    /**
     * 0, 回归玩家不可领取充值礼包
     */
    static readonly RETURNROLE_CHARGEGIFT_NOT: number = 0;

    /**
     * 1, 回归玩家待领取充值礼包
     */
    static readonly RETURNROLE_CHARGEGIFT_WAIT: number = 1;

    /**
     * 2, rmb战场已开战
     */
    static readonly RMBZC_STATUS_IN: number = 2;

    /**
     * 1, rmb战场已报名
     */
    static readonly RMBZC_STATUS_JOIN: number = 1;

    /**
     * 0, rmb战场未报名
     */
    static readonly RMBZC_STATUS_NONE: number = 0;

    /**
     * 1, 表示查的是散仙
     */
    static readonly ROLEINFO_REQUEST_TYPE_PET: number = 1;

    /**
     * 0, 表示查的是角色
     */
    static readonly ROLEINFO_REQUEST_TYPE_ROLE: number = 0;

    /**
     * 16
     */
    static readonly ROLE_IDENTITY_BAGLOCKCHECKED: number = 16;

    /**
     * 8
     */
    static readonly ROLE_IDENTITY_BAGLOCKED: number = 8;

    /**
     * 64
     */
    static readonly ROLE_IDENTITY_ZEROGUIDCHECKED: number = 64;

    /**
     * 2, 修改性别
     */
    static readonly ROLE_INFO_MODIFY_TYPE_GENDER: number = 2;

    /**
     * 1, 改名卡改名
     */
    static readonly ROLE_INFO_MODIFY_TYPE_NAME_CARD: number = 1;

    /**
     * 0, 新创角改名
     */
    static readonly ROLE_INFO_MODIFY_TYPE_NAME_NEW: number = 0;

    /**
     * 3, 资源找回最大天数
     */
    static readonly ROLE_MAX_ZYZH_DAY_COUNT: number = 3;

    /**
     * 26, 资源找回系统个数
     */
    static readonly ROLE_MAX_ZYZH_LIST_COUNT: number = 26;

    /**
     * 5, 资源找回系统最大物品个数
     */
    static readonly ROLE_MAX_ZYZH_MAX_THING_COUNT: number = 5;

    /**
     * 8, 资源找回需要保存参与信息的最大个数 BitMap可以保存8 * 8个
     */
    static readonly ROLE_MAX_ZYZH_SAVE_BITMAP_SIZE: number = 8;

    /**
     * 3, 资源找回系统额外物品个数
     */
    static readonly ROLE_MAX_ZYZH_THING_COUNT: number = 3;

    /**
     * 10452011, 改名卡物品ID
     */
    static readonly ROLE_NAME_CHANGE_CARD_THINGID: number = 10452011;

    /**
     * 1, 国家国王面向全国发送的穿云箭
     */
    static readonly ROLE_SENDPOS_TYPE_COUNTRY_KING: number = 1;

    /**
     * 2, 宗派帮宗面向所有帮宗发送的穿云箭
     */
    static readonly ROLE_SENDPOS_TYPE_GUILD: number = 2;

    /**
     * 0, 国运求救的穿云箭
     */
    static readonly ROLE_SENDPOS_TYPE_GUOYUN: number = 0;

    /**
     * 7, 玩家军团ID变化
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_ARMYID: number = 7;

    /**
     * 6, 玩家属性变化
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_ATT: number = 6;

    /**
     * 1, 国家职位
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_COUNTRY_JOB: number = 1;

    /**
     * 4, 玩家固定称号发生改变
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_FIX_TITLE: number = 4;

    /**
     * 3, 家族职位发生改变
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_GUILD_GRADE: number = 3;

    /**
     * 14, 极速挑战称号ID变化
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_JSTZ: number = 14;

    /**
     * 13, 累计终生消费
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_LIFE_CONSUME: number = 13;

    /**
     * 10, 玩家配偶名称发生改变
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_MARRY: number = 10;

    /**
     * 5, PK数值变化
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_PKINFO: number = 5;

    /**
     * 12, 人民币战场宝箱信息发生改变
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_RMBZCINFO: number = 12;

    /**
     * 15, 守护神ID变化
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_SHS: number = 15;

    /**
     * 9, 聚元称号ID变化
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_TD_JUYUAN: number = 9;

    /**
     * 8, 天地竞技称号ID变化
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_TD_TITLE: number = 8;

    /**
     * 16, 队伍ID变化
     */
    static readonly ROLE_SIGH_ATTIR_TYPE_TEAM: number = 16;

    /**
     * 0, 普通玩家
     */
    static readonly ROLE_TYPE_NORMAL: number = 0;

    /**
     * 1, 回归玩家
     */
    static readonly ROLE_TYPE_RETURN: number = 1;

    /**
     * 1, 玩家翅膀合成
     */
    static readonly ROLE_WING_CREATE: number = 1;

    /**
     * 2, 玩家翅膀强化
     */
    static readonly ROLE_WING_STRENGTHEN: number = 2;

    /**
     * 2, 显示场景名字消息
     */
    static readonly RPOMPTMSG_TYPE_SCENCE: number = 2;

    /**
     * 0, 显示为系统消息
     */
    static readonly RPOMPTMSG_TYPE_SYSTEM: number = 0;

    /**
     * 3, 获取呼朋唤友的礼包
     */
    static readonly RR_CALL_FRIEND: number = 3;

    /**
     * 4, 回归玩家获取充值礼包
     */
    static readonly RR_GET_CHARGE_GIFT: number = 4;

    /**
     * 2, 获取回归玩家的登录礼包
     */
    static readonly RR_GET_LOGIN_GIFT: number = 2;

    /**
     * 1, 查询玩家的回归信息
     */
    static readonly RR_LIST: number = 1;

    /**
     * 3, 原地满状态立刻秒活
     */
    static readonly RevivalType_RevivaInstant: number = 3;

    /**
     * 1, 复活点复活
     */
    static readonly RevivalType_ReviveAtPoint: number = 1;

    /**
     * 4, 复活点满状态复活
     */
    static readonly RevivalType_ReviveFullStateAtPoint: number = 4;

    /**
     * 2, 原地满状态复活
     */
    static readonly RevivalType_ReviveInSitu: number = 2;

    /**
     * 1, 活着
     */
    static readonly RoleStatusType_Alive: number = 1;

    /**
     * 2, 死亡
     */
    static readonly RoleStatusType_Dead: number = 2;

    /**
     * 0, 非法值
     */
    static readonly RoleStatusType_Invalid: number = 0;

    /**
     * 4, 第1赛季4天
     */
    static readonly SAIJI_1_4: number = 4;

    /**
     * 7, 第2赛季7天
     */
    static readonly SAIJI_2_7: number = 7;

    /**
     * 10, 第3赛季11天
     */
    static readonly SAIJI_3_11: number = 10;

    /**
     * 14, 第4赛季14天 之后一直都是14天
     */
    static readonly SAIJI_4_14: number = 14;

    /**
     * 4, 4个赛季一轮
     */
    static readonly SAIJI_LOOP_CNT: number = 4;

    /**
     * 32, 外显最多配置32个
     */
    static readonly SAIJI_SHOW_CNT: number = 32;

    /**
     * 3, 赛季开服第3天开启
     */
    static readonly SAIJI_START_DAY: number = 3;

    /**
     * 6, 美术字
     */
    static readonly SCENEINFOTYPE_ARTWORDS: number = 6;

    /**
     * 4, boss引导面板
     */
    static readonly SCENEINFOTYPE_BOSSGUIDE: number = 4;

    /**
     * 7, 阵营信息
     */
    static readonly SCENEINFOTYPE_CAMP: number = 7;

    /**
     * 8, 双倍经验
     */
    static readonly SCENEINFOTYPE_DOUBLE_EXP: number = 8;

    /**
     * 2, 伤害面板
     */
    static readonly SCENEINFOTYPE_HURT: number = 2;

    /**
     * 5, 场景进度信息
     */
    static readonly SCENEINFOTYPE_PROCESS: number = 5;

    /**
     * 3, 结算面板
     */
    static readonly SCENEINFOTYPE_RESULT: number = 3;

    /**
     * 1, 右侧信息
     */
    static readonly SCENEINFOTYPE_RIGHT: number = 1;

    /**
     * 9, 特殊面板
     */
    static readonly SCENEINFOTYPE_SPECIAL_INFO: number = 9;

    /**
     * 3, 召集
     */
    static readonly SCENEINFO_BUTTON_CALL: number = 3;

    /**
     * 1, 退出
     */
    static readonly SCENEINFO_BUTTON_EXIT: number = 1;

    /**
     * 2, 战斗
     */
    static readonly SCENEINFO_BUTTON_FIGHT: number = 2;

    /**
     * 4, 原梦战场排行榜
     */
    static readonly SCENEINFO_BUTTON_RANK_YMZC: number = 4;

    /**
     * 5, 元神战场排行榜
     */
    static readonly SCENEINFO_BUTTON_RANK_YSZC: number = 5;

    /**
     * 3, 宗派群英会排行
     */
    static readonly SCENEINFO_BUTTON_RANK_ZPQYH: number = 3;

    /**
     * 6, 元神战场奖励
     */
    static readonly SCENEINFO_BUTTON_REWARD_YSZC: number = 6;

    /**
     * 2, 伤害面板，15人
     */
    static readonly SCENEINFO_HURTTYPE_15: number = 2;

    /**
     * 1, 伤害面板，5人
     */
    static readonly SCENEINFO_HURTTYPE_5: number = 1;

    /**
     * 5, 结算信息
     */
    static readonly SCENERESULT_ACT_FAIL: number = 5;

    /**
     * 0, 不显示
     */
    static readonly SCENERESULT_BUT_NONE: number = 0;

    /**
     * 1, 显示
     */
    static readonly SCENERESULT_BUT_SHOW: number = 1;

    /**
     * 2, 带倒计时的显示
     */
    static readonly SCENERESULT_BUT_SHOW_TIMER: number = 2;

    /**
     * 9, 四象斗兽结算信息
     */
    static readonly SCENERESULT_COLOSSEUM_REWARD: number = 9;

    /**
     * 0, 失败信息
     */
    static readonly SCENERESULT_FAIL: number = 0;

    /**
     * 6, 跨服宗门战结算面板
     */
    static readonly SCENERESULT_GUILD_CROSSPVP: number = 6;

    /**
     * 7, 跨服3V3奖励信息
     */
    static readonly SCENERESULT_MULTIPVP_REWARD: number = 7;

    /**
     * 2, 通关信息
     */
    static readonly SCENERESULT_PASS: number = 2;

    /**
     * 3, PVP排名信息
     */
    static readonly SCENERESULT_PVP_RANK: number = 3;

    /**
     * 4, 跨服角斗场奖励信息
     */
    static readonly SCENERESULT_SINGLEPVP_REWARD: number = 4;

    /**
     * 1, 胜利信息
     */
    static readonly SCENERESULT_SUCCESS: number = 1;

    /**
     * 10, 武缘远征结算信息
     */
    static readonly SCENERESULT_WYYZ: number = 10;

    /**
     * 8, 珍珑棋局
     */
    static readonly SCENERESULT_ZLQJ: number = 8;

    /**
     * 12, 带血量百分比的怪物头像
     */
    static readonly SCENERIGHT_BLOOD_MONSTER: number = 12;

    /**
     * 10, 八门信息
     */
    static readonly SCENERIGHT_BM_INFO: number = 10;

    /**
     * 7, 按钮
     */
    static readonly SCENERIGHT_BUTTON: number = 7;

    /**
     * 3, 倒计时
     */
    static readonly SCENERIGHT_COUNTDOWN: number = 3;

    /**
     * 39, 跨服决斗场决赛 右侧面板
     */
    static readonly SCENERIGHT_FINAL_RIGHT: number = 39;

    /**
     * 13, 图片
     */
    static readonly SCENERIGHT_IMAGE: number = 13;

    /**
     * 4, 分割线
     */
    static readonly SCENERIGHT_LINE: number = 4;

    /**
     * 0, 占位
     */
    static readonly SCENERIGHT_MIN: number = 0;

    /**
     * 6, 怪物头像
     */
    static readonly SCENERIGHT_MONSTER: number = 6;

    /**
     * 1, 进度条
     */
    static readonly SCENERIGHT_PROGRESS: number = 1;

    /**
     * 9, 排行面板
     */
    static readonly SCENERIGHT_RANK: number = 9;

    /**
     * 15, 角色信息
     */
    static readonly SCENERIGHT_ROLE: number = 15;

    /**
     * 14, 目标坐标
     */
    static readonly SCENERIGHT_TARGET_COORD: number = 14;

    /**
     * 11, 任务信息
     */
    static readonly SCENERIGHT_TASK_INFO: number = 11;

    /**
     * 2, 文本
     */
    static readonly SCENERIGHT_TEXT: number = 2;

    /**
     * 5, 物品icon
     */
    static readonly SCENERIGHT_THING: number = 5;

    /**
     * 8, 挂机经验
     */
    static readonly SCENERIGHT_ZAZENEXP: number = 8;

    /**
     * 2, 攻方图片ID
     */
    static readonly SCENE_INFO_IMAGE_ID_ATTACT: number = 2;

    /**
     * 1, 守方图片ID
     */
    static readonly SCENE_INFO_IMAGE_ID_DEFENSE: number = 1;

    /**
     * 1, 跨服决斗场决赛 选手信息
     */
    static readonly SCENE_SPECIAL_FINAL_PLAYER: number = 1;

    /**
     * 3, 跨服决斗场决赛 胜负情况
     */
    static readonly SCENE_SPECIAL_FINAL_RESULT: number = 3;

    /**
     * 2, 跨服决斗场决赛 倒计时
     */
    static readonly SCENE_SPECIAL_FINAL_TIME: number = 2;

    /**
     * 4, 组队副本再次挑战
     */
    static readonly SCENE_SPECIAL_GROUPPIN_AGAIN: number = 4;

    /**
     * 6, 副本内怪物死亡特效
     */
    static readonly SCENE_SPECIAL_MONDEAD_EFFECT: number = 6;

    /**
     * 5, 普通面板信息
     */
    static readonly SCENE_SPECIAL_NORMAL_BOARD: number = 5;

    /**
     * 1, 脚本操作菜单
     */
    static readonly SCRIPT_OPERATE_MENU: number = 1;

    /**
     * 10, 脚本 珍珑棋局排行个数
     */
    static readonly SCRIPT_ZLQJ_RANK_COUNT: number = 10;

    /**
     * 50, 最多有50次首冲
     */
    static readonly SC_CONFIG_MAX_ALL_COUNT: number = 50;

    /**
     * 3, 首冲前3天
     */
    static readonly SC_CONFIG_MAX_FRIST_CCOUNT: number = 3;

    /**
     * 3, 首冲每天最多3个档次
     */
    static readonly SC_CONFIG_MAX_ONE_DAY: number = 3;

    /**
     * 1, 首冲第一档次
     */
    static readonly SC_GET_FLAG_BIT_1ST: number = 1;

    /**
     * 2, 首冲第二档次
     */
    static readonly SC_GET_FLAG_BIT_2ND: number = 2;

    /**
     * 4, 首冲第三档次
     */
    static readonly SC_GET_FLAG_BIT_3RD: number = 4;

    /**
     * 8, 首冲第四档次
     */
    static readonly SC_GET_FLAG_BIT_4RD: number = 8;

    /**
     * 128, 首冲完成标志
     */
    static readonly SC_GET_FLAG_BIT_DONE: number = 128;

    /**
     * 1, 角色终生首冲第一档次
     */
    static readonly SC_GET_FLAG_BIT_ROLE: number = 1;

    /**
     * 1, 角色终生首冲完成标志
     */
    static readonly SC_GET_RFC_DONE: number = 1;

    /**
     * 7, 循环首充循环7天配置
     */
    static readonly SC_KAIFU_LOOP_DAYS: number = 7;

    /**
     * 14, 开服首充持续14天
     */
    static readonly SC_KAIFU_STAY_DAYS: number = 14;

    /**
     * 7, 循环累充循环7天配置
     */
    static readonly SC_LJCZ_LOOP_DAYS: number = 7;

    /**
     * 14, 开服累计充值持续14天
     */
    static readonly SC_LJCZ_STAY_DAYS: number = 14;

    /**
     * 7, 首充档次最大7档
     */
    static readonly SC_MAX_LEVEL_COUNT: number = 7;

    /**
     * 1043, 盛典宝箱的商店ID
     */
    static readonly SDBX_STORE_ID: number = 1043;

    /**
     * 3, 神凰秘境副本任务关卡个数
     */
    static readonly SECRET_AREA_MAX_NUM: number = 3;

    /**
     * 115, 查看红包信息
     */
    static readonly SEE_REDBAG_INFO: number = 115;

    /**
     * 112, 发红包
     */
    static readonly SEND_REDBAG: number = 112;

    /**
     * 1, 开服时间变更通知
     */
    static readonly SERVER_PARAMETER_TYPE_STARTTIME: number = 1;

    /**
     * 2, 拥挤
     */
    static readonly SERVER_STATE_BUSY: number = 2;

    /**
     * 3, 爆满
     */
    static readonly SERVER_STATE_FULL: number = 3;

    /**
     * 1, 畅通
     */
    static readonly SERVER_STATE_IDLE: number = 1;

    /**
     * 1, 七天投资购买
     */
    static readonly SEVEN_DAY_FUND_BUY: number = 1;

    /**
     * 2, 七天投资每日奖励领取
     */
    static readonly SEVEN_DAY_FUND_GET: number = 2;

    /**
     * 3, 七日投资类型 目前只有三个
     */
    static readonly SEVEN_DAY_FUND_TYPE_SIZE: number = 3;

    /**
     * 2, 激活神兽
     */
    static readonly SHENSHOU_OP_ACT: number = 2;

    /**
     * 5, 卸下法器
     */
    static readonly SHENSHOU_OP_DOWN_FAQI: number = 5;

    /**
     * 1, 神兽面板
     */
    static readonly SHENSHOU_OP_PANEL: number = 1;

    /**
     * 4, 镶嵌法器
     */
    static readonly SHENSHOU_OP_SET_FAQI: number = 4;

    /**
     * 3, 升级神兽
     */
    static readonly SHENSHOU_OP_UPLEVEL: number = 3;

    /**
     * 1, 激活守护神
     */
    static readonly SHIELDGOD_OP_ACT: number = 1;

    /**
     * 3, 守护神切换
     */
    static readonly SHIELDGOD_OP_CHANGE: number = 3;

    /**
     * 4, 获取守护神信息
     */
    static readonly SHIELDGOD_OP_LIST: number = 4;

    /**
     * 2, 升级守护神
     */
    static readonly SHIELDGOD_OP_UPLEVEL: number = 2;

    /**
     * 2
     */
    static readonly SHORTCUT_TYPE_PET: number = 2;

    /**
     * 1
     */
    static readonly SHORTCUT_TYPE_ROLE: number = 1;

    /**
     * 202, 双倍领取请求
     */
    static readonly SIGN_DOUBLE_REQ: number = 202;

    /**
     * 28, 签到
     */
    static readonly SIGN_IN_REQ: number = 28;

    /**
     * 27, 查询签到列表
     */
    static readonly SIGN_LIST_QRY: number = 27;

    /**
     * 4, 最多签到字节数*8
     */
    static readonly SIGN_MAX_SIZE: number = 4;

    /**
     * 29, 累计礼包领取请求
     */
    static readonly SIGN_PRIZE_REQ: number = 29;

    /**
     * 4, 风月宝鉴领取抽奖次数奖励
     */
    static readonly SKYLOTTERY_EXTRA_REWARD: number = 4;

    /**
     * 2, 天宫宝境查询抽奖记录
     */
    static readonly SKYLOTTERY_LIST_RECORD: number = 2;

    /**
     * 5, 天宫宝境打开面板
     */
    static readonly SKYLOTTERY_OPEN_PANEL: number = 5;

    /**
     * 1, 天宫宝境抽奖操作
     */
    static readonly SKYLOTTERY_OPERATE: number = 1;

    /**
     * 2, 天宫宝境抽奖，查询玩家记录的请求类型
     */
    static readonly SKYLOTTERY_ROLE_RECORD_TYPE: number = 2;

    /**
     * 10, 天宫宝境抽奖，查询全服购买记录的请求类型(比较特别)
     */
    static readonly SKYLOTTERY_SERVER_BUY_RECORD_TYPE: number = 10;

    /**
     * 1, 天宫宝境抽奖，查询全服记录的请求类型
     */
    static readonly SKYLOTTERY_SERVER_RECORD_TYPE: number = 1;

    /**
     * 20, 打开神魔遮天面板
     */
    static readonly SMZT_OPEN_PANNEL: number = 20;

    /**
     * 184, 神魔遮天场景ID
     */
    static readonly SMZT_SCENE_ID: number = 184;

    /**
     * 0, 称号
     */
    static readonly SPECIAL_THING_TYPE_TITLE: number = 0;

    /**
     * 4, 宝石属性
     */
    static readonly SPECTHING_DIAMOND: number = 4;

    /**
     * 1, 装备
     */
    static readonly SPECTHING_EQUIP: number = 1;

    /**
     * 6, 魂骨装备属性
     */
    static readonly SPECTHING_HUNGU_EQUIP: number = 6;

    /**
     * 3, 抽奖宝箱
     */
    static readonly SPECTHING_LOTTERY_BOX: number = 3;

    /**
     * 7, 类型最大值，无意义
     */
    static readonly SPECTHING_MAX: number = 7;

    /**
     * 0, 占位，无意义
     */
    static readonly SPECTHING_MIN: number = 0;

    /**
     * 2, 坐骑装备
     */
    static readonly SPECTHING_MONNT_EQUIP: number = 2;

    /**
     * 2, 祝福装备
     */
    static readonly SPECTHING_ZHUFUEQUIP: number = 2;

    /**
     * 10, 特殊同步，跨服活动Buff
     */
    static readonly SPEC_SYN_ID_ACTBUFF: number = 10;

    /**
     * 9, 特殊同步，活跃度
     */
    static readonly SPEC_SYN_ID_ACTIVEDEGREE: number = 9;

    /**
     * 2, 特殊同步，洗澡活动
     */
    static readonly SPEC_SYN_ID_BATHE: number = 2;

    /**
     * 16, 特殊同步，出战武缘
     */
    static readonly SPEC_SYN_ID_BATTLEPET: number = 16;

    /**
     * 6, 特殊同步，技能CD
     */
    static readonly SPEC_SYN_ID_CD: number = 6;

    /**
     * 17, 特殊同步，跨服3v3相关
     */
    static readonly SPEC_SYN_ID_CROSS3V3: number = 17;

    /**
     * 7, 特殊同步，副本每日完成状态
     */
    static readonly SPEC_SYN_ID_DAY_PINFINISH: number = 7;

    /**
     * 11, 特殊同步，主要是为了同步当前血
     */
    static readonly SPEC_SYN_ID_EUAI: number = 11;

    /**
     * 15, 特殊同步，九转星宫相关的
     */
    static readonly SPEC_SYN_ID_JZXG: number = 15;

    /**
     * 8, 特殊同步，副本终生完成状态
     */
    static readonly SPEC_SYN_ID_LIFE_PINFINISH: number = 8;

    /**
     * 5, 特殊同步，副本次数
     */
    static readonly SPEC_SYN_ID_PIN_TIMES: number = 5;

    /**
     * 13, 特殊同步，跨服RMB相关的
     */
    static readonly SPEC_SYN_ID_RMB: number = 13;

    /**
     * 1, 特殊同步，单人PVP次数
     */
    static readonly SPEC_SYN_ID_SINGLETIMES: number = 1;

    /**
     * 3, 特殊同步，世界BOSS
     */
    static readonly SPEC_SYN_ID_WORLDBOSS: number = 3;

    /**
     * 4, 特殊同步，世界BOSS击杀榜
     */
    static readonly SPEC_SYN_ID_WORLDBOSSKILL: number = 4;

    /**
     * 51019001, 无条件加速Buff
     */
    static readonly SPEED_BUFF_ID: number = 51019001;

    /**
     * 1, 宗派活动提前结束通知到world
     */
    static readonly SS_ACTIVITY_GUILD_ACT_EARLY_END: number = 1;

    /**
     * 27, 进阶日排行面板
     */
    static readonly STAGEDAT_RANK_OPEN_PANEL: number = 27;

    /**
     * 8, 打开进阶日活动面板
     */
    static readonly STAGEDAY_OPEN_PANEL: number = 8;

    /**
     * 1, 开服活动 boss召唤
     */
    static readonly START_ACT_BOSS_SUMMON: number = 1;

    /**
     * 2010, 注意：上线后应该设置为2014
     */
    static readonly START_YEAR: number = 2010;

    /**
     * 5, 星斗宝库选择头奖
     */
    static readonly STAR_LOTTERY_CHOOSE_TOP_PRIZE: number = 5;

    /**
     * 4, 星斗宝库获取头奖
     */
    static readonly STAR_LOTTERY_GET_TOP_PRIZE: number = 4;

    /**
     * 7, 星斗宝库获分红包
     */
    static readonly STAR_LOTTERY_HONGBAO_ENTRY: number = 7;

    /**
     * 8, 星斗宝库红包记录
     */
    static readonly STAR_LOTTERY_HONGBAO_HISTORY: number = 8;

    /**
     * 3, 星斗宝库查询抽奖记录
     */
    static readonly STAR_LOTTERY_LIST_RECORD: number = 3;

    /**
     * 2, 星斗宝库抽奖操作
     */
    static readonly STAR_LOTTERY_OPERATE: number = 2;

    /**
     * 1, 星斗宝库面板
     */
    static readonly STAR_LOTTERY_PANEL: number = 1;

    /**
     * 10, 星空宝库分红历史排行人数
     */
    static readonly STAR_LOTTERY_RANK_HISTORY_ROLE_NUM: number = 10;

    /**
     * 300, 星空宝库排行人数
     */
    static readonly STAR_LOTTERY_RANK_ROLE_NUM: number = 300;

    /**
     * 2, 星空宝库抽奖，查询玩家记录的请求类型
     */
    static readonly STAR_LOTTERY_ROLE_RECORD_TYPE: number = 2;

    /**
     * 9, 全服消费金额通知
     */
    static readonly STAR_LOTTERY_SERVER_COST_NOTIFY: number = 9;

    /**
     * 6, 星斗宝库获取头奖奖励配置
     */
    static readonly STAR_LOTTERY_TOP_PRIZE_CFG: number = 6;

    /**
     * 3, 仓库
     */
    static readonly STORE_TYPE: number = 3;

    /**
     * 1, 星空宝库抽奖，查询全服记录的请求类型
     */
    static readonly STRA_LOTTERY_SERVER_RECORD_TYPE: number = 1;

    /**
     * 15, 变强_当前值
     */
    static readonly STRONG_TYPE_CUR: number = 15;

    /**
     * 25, 变强_最大值
     */
    static readonly STRONG_TYPE_MAX: number = 25;

    /**
     * 24, 调查问卷礼包
     */
    static readonly SURVEY_GIFT: number = 24;

    /**
     * 0, 同步道具：获得道具|    货币
     */
    static readonly SYN_ITEM_GET: number = 0;

    /**
     * 1, 同步道具：扣除道具|    货币
     */
    static readonly SYN_ITEM_USE: number = 1;

    /**
     * 3, 抗锯齿
     */
    static readonly SYSTEM_SETTING_ANTI_ALIASING: number = 3;

    /**
     * 0, 声音设置
     */
    static readonly SYSTEM_SETTING_MUSIC: number = 0;

    /**
     * 2, 场景特效
     */
    static readonly SYSTEM_SETTING_SCENE_EFFECT: number = 2;

    /**
     * 4, 阴影
     */
    static readonly SYSTEM_SETTING_SHADOW: number = 4;

    /**
     * 1, 技能特效
     */
    static readonly SYSTEM_SETTING_SKILL_EFFECT: number = 1;

    /**
     * 5, 组队邀请，0是允许，1是拒绝
     */
    static readonly SYSTEM_SETTING_TEAM_INVITE: number = 5;

    /**
     * 1, 合服宗门争霸
     */
    static readonly TAB_MERGEACT_STATUS_1: number = 1;

    /**
     * 2, 合服集字送好礼
     */
    static readonly TAB_MERGEACT_STATUS_2: number = 2;

    /**
     * 3, 合服累计充值
     */
    static readonly TAB_MERGEACT_STATUS_3: number = 3;

    /**
     * 4, 合服7天登陆
     */
    static readonly TAB_MERGEACT_STATUS_4: number = 4;

    /**
     * 1, 宗门争霸
     */
    static readonly TAB_STARTACT_STATUS_1: number = 1;

    /**
     * 2, 集字送好礼
     */
    static readonly TAB_STARTACT_STATUS_2: number = 2;

    /**
     * 3, 挑战BOSS活动
     */
    static readonly TAB_STARTACT_STATUS_3: number = 3;

    /**
     * 1, 首冲团购
     */
    static readonly TAB_STATUS_1: number = 1;

    /**
     * 10, 特卖大酬宾
     */
    static readonly TAB_STATUS_10: number = 10;

    /**
     * 11, 极品大兑换
     */
    static readonly TAB_STATUS_11: number = 11;

    /**
     * 12, 消费赠好礼
     */
    static readonly TAB_STATUS_12: number = 12;

    /**
     * 13, 充值送好礼
     */
    static readonly TAB_STATUS_13: number = 13;

    /**
     * 14, 充值大兑换
     */
    static readonly TAB_STATUS_14: number = 14;

    /**
     * 15, 累计充值
     */
    static readonly TAB_STATUS_15: number = 15;

    /**
     * 16, BOSS召唤
     */
    static readonly TAB_STATUS_16: number = 16;

    /**
     * 2, 每日目标
     */
    static readonly TAB_STATUS_2: number = 2;

    /**
     * 3, 侠客榜
     */
    static readonly TAB_STATUS_3: number = 3;

    /**
     * 4, 美人进阶日
     */
    static readonly TAB_STATUS_4: number = 4;

    /**
     * 5, 坐骑进阶日
     */
    static readonly TAB_STATUS_5: number = 5;

    /**
     * 6, 羽翼进阶日
     */
    static readonly TAB_STATUS_6: number = 6;

    /**
     * 7, 武魂进阶日
     */
    static readonly TAB_STATUS_7: number = 7;

    /**
     * 8, 精灵进阶日
     */
    static readonly TAB_STATUS_8: number = 8;

    /**
     * 9, 法阵进阶日
     */
    static readonly TAB_STATUS_9: number = 9;

    /**
     * 20, 冥门扣除元宝
     */
    static readonly TDXM_COST_YUANBAO_MINGMEN: number = 20;

    /**
     * 50, 星门扣除元宝
     */
    static readonly TDXM_COST_YUANBAO_XINGMEN: number = 50;

    /**
     * 33, 天地玄门 创建
     */
    static readonly TDXM_CREATE: number = 33;

    /**
     * 60120001, 队长 冥门 奖励掉落方案
     */
    static readonly TDXM_DROPID_CAPTAIN_MINGMEN: number = 60120001;

    /**
     * 60120002, 队长 星门 奖励掉落方案
     */
    static readonly TDXM_DROPID_CAPTAIN_XINGMEN: number = 60120002;

    /**
     * 60120003, 队员 冥门 奖励掉落方案
     */
    static readonly TDXM_DROPID_MEMBERS_MINGMEN: number = 60120003;

    /**
     * 60120004, 队员 星门 奖励掉落方案
     */
    static readonly TDXM_DROPID_MEMBERS_XINGMEN: number = 60120004;

    /**
     * 36, 天地玄门 加入
     */
    static readonly TDXM_JOIN: number = 36;

    /**
     * 50, 最大创建次数
     */
    static readonly TDXM_MAX_CREATE_COUNT: number = 50;

    /**
     * 10, 最大参与次数
     */
    static readonly TDXM_MAX_JOIN_COUNT: number = 10;

    /**
     * 32, 天地玄门 打开面板
     */
    static readonly TDXM_PANNEL: number = 32;

    /**
     * 35, 天地玄门 招募
     */
    static readonly TDXM_RECRUIT: number = 35;

    /**
     * 34, 天地玄门 开始
     */
    static readonly TDXM_START: number = 34;

    /**
     * 0, 天地玄门状态 空
     */
    static readonly TDXM_STATUS_NONE: number = 0;

    /**
     * 1, 天地玄门状态 招募
     */
    static readonly TDXM_STATUS_READY: number = 1;

    /**
     * 2, 天地玄门状态 进行
     */
    static readonly TDXM_STATUS_RUN: number = 2;

    /**
     * 1, 天地玄门类型 冥门
     */
    static readonly TDXM_TYPE_MINGMEN: number = 1;

    /**
     * 0, 天地玄门类型 空
     */
    static readonly TDXM_TYPE_NONE: number = 0;

    /**
     * 2, 天地玄门类型 星门
     */
    static readonly TDXM_TYPE_XINGMEN: number = 2;

    /**
     * 60, 1个人需要时间
     */
    static readonly TDXM_WAIT_TIME_1_ROLE: number = 60;

    /**
     * 30, 2个人需要时间
     */
    static readonly TDXM_WAIT_TIME_2_ROLE: number = 30;

    /**
     * 5, 3个人需要时间
     */
    static readonly TDXM_WAIT_TIME_3_ROLE: number = 5;

    /**
     * 1021, 特惠礼包 商店ID
     */
    static readonly THLB_STORE_ID: number = 1021;

    /**
     * 2, 获取玩家已获得的称号数据
     */
    static readonly TITLE_LIST_DATA: number = 2;

    /**
     * 1, 设置玩家需要显示的称号
     */
    static readonly TITLE_SET_SHOW: number = 1;

    /**
     * 30330001, 天降福神宝箱怪物ID
     */
    static readonly TJFS_MONSTER_ID: number = 30330001;

    /**
     * 51012601, 主主播变身buffId
     */
    static readonly TRANS_BUFF_ID: number = 51012601;

    /**
     * 51012602, 副主播变身buffId
     */
    static readonly TRANS_BUFF_ID2: number = 51012602;

    /**
     * 1, 飞鞋大跳（本场景长距离跳）
     */
    static readonly TRANS_TYPE_BIG_JUMP: number = 1;

    /**
     * 3, 任务跳
     */
    static readonly TRANS_TYPE_QUEST_JUMP: number = 3;

    /**
     * 2, 飞鞋跨场景
     */
    static readonly TRANS_TYPE_SHOE: number = 2;

    /**
     * 4, 短距离跳
     */
    static readonly TRANS_TYPE_SHORT_JUMP: number = 4;

    /**
     * 121, 图腾祝福 领取祝福奖励
     */
    static readonly TTZF_ACT_GET_REWARD: number = 121;

    /**
     * 119, 图腾祝福打开面板
     */
    static readonly TTZF_ACT_PANEL: number = 119;

    /**
     * 120, 图腾祝福 执行祝福
     */
    static readonly TTZF_ACT_WISH: number = 120;

    /**
     * 10315011, 图腾祝福物品ID
     */
    static readonly TTZF_WISH_THINGID: number = 10315011;

    /**
     * 10, 图腾祝福一次消耗元宝数量
     */
    static readonly TTZF_WISH_YUANBAO_NUM: number = 10;

    /**
     * 1, TX任务集市 获得一件紫装
     */
    static readonly TX_TASK_EQUIP_PURPLE: number = 1;

    /**
     * 2, TX任务集市 门派任务礼包
     */
    static readonly TX_TASK_ITEM_10021021: number = 2;

    /**
     * 64, TX任务集市 蔡家铺子
     */
    static readonly TX_TASK_PIN_300001: number = 64;

    /**
     * 32, TX任务集市 渡劫
     */
    static readonly TX_TASK_PIN_300003: number = 32;

    /**
     * 128, TX任务集市 蓬莱仙境
     */
    static readonly TX_TASK_PIN_300005: number = 128;

    /**
     * 4, TX任务集市 封神台
     */
    static readonly TX_TASK_PIN_300012: number = 4;

    /**
     * 16, TX任务集市 封印法阵
     */
    static readonly TX_TASK_PIN_300019: number = 16;

    /**
     * 8, TX任务集市 石灵仙府
     */
    static readonly TX_TASK_PIN_300023: number = 8;

    /**
     * 65535, 未定义的容器位置
     */
    static readonly UNDEFINED_CONTAINER_POSITION: number = 65535;

    /**
     * 0
     */
    static readonly UNION_OCCUPY_POSITION: number = 0;

    /**
     * 3003, 守护女神(神凰秘境)总榜第一称号ID
     */
    static readonly UNIQUE_TITLE_SHNS_FIRST: number = 3003;

    /**
     * 3001, 武神坛(帝者之路)总榜第一称号ID
     */
    static readonly UNIQUE_TITLE_WST_FIRST: number = 3001;

    /**
     * 3002, 武神坛(帝者之路)速度第一称号ID
     */
    static readonly UNIQUE_TITLE_WST_MOST: number = 3002;

    /**
     * 3004, 武神坛(帝者之路)总榜第一称号ID
     */
    static readonly UNIQUE_TITLE_WYFB_FIRST: number = 3004;

    /**
     * 3005, 武神坛(帝者之路)速度第一称号ID
     */
    static readonly UNIQUE_TITLE_WYFB_MOST: number = 3005;

    /**
     * 2, 解锁背包状态
     */
    static readonly UNLOCK_BAGSTA: number = 2;

    /**
     * 700, 请手工保证与Metalib的version一致
     */
    static readonly VERSION: number = 700;

    /**
     * 5, 蔡家铺子的活动次数的类型
     */
    static readonly VIPDATA_ACTIVITY_TYPE_PINSTANCE_CAIJIAPUZI: number = 5;

    /**
     * 6, 幻象谷的活动次数的类型
     */
    static readonly VIPDATA_ACTIVITY_TYPE_PINSTANCE_HUANXIANGGU: number = 6;

    /**
     * 10, 蓬莱仙境的活动次数的类型
     */
    static readonly VIPDATA_ACTIVITY_TYPE_PINSTANCE_PENGLAIXIANJING: number = 10;

    /**
     * 8, 奇珍阁的活动次数的类型
     */
    static readonly VIPDATA_ACTIVITY_TYPE_PINSTANCE_QIZHENGE: number = 8;

    /**
     * 1, 仙灵石府的活动次数的类型。
     */
    static readonly VIPDATA_ACTIVITY_TYPE_PINSTANCE_XIANLINGSHIFU: number = 1;

    /**
     * 7, 镇妖塔的活动次数的类型
     */
    static readonly VIPDATA_ACTIVITY_TYPE_PINSTANCE_ZHENYAOTA: number = 7;

    /**
     * 3, 前线任务保留的活动次数的类型
     */
    static readonly VIPDATA_ACTIVITY_TYPE_QUEST_FRONTLINE: number = 3;

    /**
     * 4, 国运任务保留的活动次数的类型
     */
    static readonly VIPDATA_ACTIVITY_TYPE_QUEST_GUOYUN: number = 4;

    /**
     * 2, 皇榜任务保留的活动次数的类型
     */
    static readonly VIPDATA_ACTIVITY_TYPE_QUEST_HUANGBANG: number = 2;

    /**
     * 23, VIP商店 购买
     */
    static readonly VIPSHOP_BUY: number = 23;

    /**
     * 22, VIP商店 打开面板
     */
    static readonly VIPSHOP_PANEL: number = 22;

    /**
     * 1, vip领取类型，周礼包,可以用此宏定义来做位移，获取领取状态
     */
    static readonly VIP_GET_TYPE_WEEKGIFT: number = 1;

    /**
     * 1, vip月卡礼包领取类型，每日礼包
     */
    static readonly VIP_MONTH_GET_TYPE_DAILY: number = 1;

    /**
     * 2, vip月卡礼包领取类型，最高等级的终生礼包
     */
    static readonly VIP_MONTH_GET_TYPE_LIFTLONG: number = 2;

    /**
     * 7, vip购买副本次数协议
     */
    static readonly VIP_OPERATE_BUY_PINSTANCE: number = 7;

    /**
     * 2, vip领取礼包
     */
    static readonly VIP_OPERATE_GET_GIFT: number = 2;

    /**
     * 3, vip领取终生礼包
     */
    static readonly VIP_OPERATE_GET_LIFELONG_GIFT: number = 3;

    /**
     * 1, vip查询协议
     */
    static readonly VIP_OPERATE_LIST: number = 1;

    /**
     * 5, vip购买月卡信息协议
     */
    static readonly VIP_OPERATE_MONTH_BUY: number = 5;

    /**
     * 6, vip领取月卡礼包协议
     */
    static readonly VIP_OPERATE_MONTH_GET_GIFT: number = 6;

    /**
     * 4, vip查询月卡信息协议
     */
    static readonly VIP_OPERATE_MONTH_LIST: number = 4;

    /**
     * 10, VIP月卡活跃信息查询
     */
    static readonly VIP_OPERATE_PRIHY_LIST: number = 10;

    /**
     * 8, 特殊特权购买
     */
    static readonly VIP_OPERATE_SPECIAL_BUY: number = 8;

    /**
     * 9, 特殊特权查询
     */
    static readonly VIP_OPERATE_SPECIAL_LIST: number = 9;

    /**
     * 50, VIP单个商店最大物品
     */
    static readonly VIP_SHOP_ITEM_COUNT: number = 50;

    /**
     * 15, VIP商店开始循环天数
     */
    static readonly VIP_SHOP_LOOP_START_DAY: number = 15;

    /**
     * 21, VIP商店最大天数
     */
    static readonly VIP_SHOP_MAX_DAY: number = 21;

    /**
     * 51004801, vip体验buff的id,仅体现为无限筋斗云
     */
    static readonly VIP_TRIALCARD_BUFF_ID: number = 51004801;

    /**
     * 7000002, 微信礼包,GIFTID
     */
    static readonly WEIXING_GIFT_ID: number = 7000002;

    /**
     * 7, 开服第7天开启购买
     */
    static readonly WHJX_ACT_BUY_START_DAY: number = 7;

    /**
     * 3, 王侯将相 挑战结果 3 活动已经结束
     */
    static readonly WHJX_APPLY_RESULT_ACTEND: number = 3;

    /**
     * 5, 王侯将相 挑战结果 5 不能重复申请挑战
     */
    static readonly WHJX_APPLY_RESULT_APPLYAGAIN: number = 5;

    /**
     * 4, 王侯将相 挑战结果 4 已经占领一个位置 不能重复占领
     */
    static readonly WHJX_APPLY_RESULT_HOLDBEFORE: number = 4;

    /**
     * 0, 王侯将相 挑战结果 0 挑战者接受挑战
     */
    static readonly WHJX_APPLY_RESULT_NONE: number = 0;

    /**
     * 2, 王侯将相 挑战结果 2 其他玩家正在挑战中
     */
    static readonly WHJX_APPLY_RESULT_WAITING: number = 2;

    /**
     * 1, 王侯将相 挑战结果 1 挑战者认输
     */
    static readonly WHJX_APPLY_RESULT_WIN: number = 1;

    /**
     * 1, 王侯将相 报名道具状态
     */
    static readonly WHJX_BUY_ITEM_BIT_1: number = 1;

    /**
     * 2, 王侯将相 报名道具状态
     */
    static readonly WHJX_BUY_ITEM_BIT_2: number = 2;

    /**
     * 120, 王侯将相 最长PK时间
     */
    static readonly WHJX_MAX_APPLY_PK_TIME: number = 120;

    /**
     * 5, 王侯将相 战斗结束 5秒钟安全期
     */
    static readonly WHJX_MAX_APPLY_SAVE_TIME: number = 5;

    /**
     * 10, 王侯将相 PK申请最长等待时间
     */
    static readonly WHJX_MAX_APPLY_WAIT_TIME: number = 10;

    /**
     * 2, 王侯将相 PK状态 挑战战斗中
     */
    static readonly WHJX_PK_STATUS_DEAL: number = 2;

    /**
     * 0, 王侯将相 PK状态 无
     */
    static readonly WHJX_PK_STATUS_NONE: number = 0;

    /**
     * 3, 王侯将相 PK状态 挑战结束安全期
     */
    static readonly WHJX_PK_STATUS_SAVE: number = 3;

    /**
     * 1, 王侯将相 PK状态 挑战申请中
     */
    static readonly WHJX_PK_STATUS_WAIT: number = 1;

    /**
     * 100, 王侯将相_最大排行个数
     */
    static readonly WHJX_RANK_MAX: number = 100;

    /**
     * 3, 王侯将相 SS转发流程 主服务器到源服务器器
     */
    static readonly WHJX_SS_FLOW_MAIN_TO_SRC: number = 3;

    /**
     * 1, 王侯将相 SS转发流程 主服务器到目的服务器
     */
    static readonly WHJX_SS_FLOW_MAIN_TO_TARGET: number = 1;

    /**
     * 0, 王侯将相 SS转发流程 源服务器到主服务器
     */
    static readonly WHJX_SS_FLOW_SRC_TO_MAIN: number = 0;

    /**
     * 2, 王侯将相 SS转发流程 目的服务器到主服务器
     */
    static readonly WHJX_SS_FLOW_TARGET_TO_MAIN: number = 2;

    /**
     * 7, 王侯将相_最大类型
     */
    static readonly WHJX_TYPE_MAX: number = 7;

    /**
     * 8, 翅膀属性个数
     */
    static readonly WING_ATTR_MAX: number = 8;

    /**
     * 2, 翅膀升级类型_批量
     */
    static readonly WING_LEVEL_TYPE_BATCH: number = 2;

    /**
     * 1, 翅膀升级类型_单次
     */
    static readonly WING_LEVEL_TYPE_ONCE: number = 1;

    /**
     * 40, 世界boss总共的最大数量
     */
    static readonly WORLD_ALL_BOSS_MAX_NUM: number = 40;

    /**
     * 20, 世界boss最大数量
     */
    static readonly WORLD_BIG_BOSS_MAX_NUM: number = 20;

    /**
     * 3, 世界Boss开服前3天不跨服包括第三天
     */
    static readonly WORLD_BOSS_KF_LIMIT_DAY: number = 3;

    /**
     * 0, 世界拍卖 拍卖物品状态 Bit0 是否售出
     */
    static readonly WORLD_PAIMAI_ITEM_BIT_0_SELL: number = 0;

    /**
     * 1, 世界拍卖 拍卖物品状态 Bit1 是否流拍
     */
    static readonly WORLD_PAIMAI_ITEM_BIT_1_FAIL: number = 1;

    /**
     * 2, 世界拍卖 拍卖物品状态 Bit3 是否已分红
     */
    static readonly WORLD_PAIMAI_ITEM_BIT_2_FENHONG: number = 2;

    /**
     * 20, Mini世界boss最大数量
     */
    static readonly WORLD_SMALL_BOSS_MAX_NUM: number = 20;

    /**
     * 1057, 限购礼包 商店ID 结束
     */
    static readonly XGLB_STORE_ID_MAX: number = 1057;

    /**
     * 1051, 限购礼包 商店ID 开始
     */
    static readonly XGLB_STORE_ID_MIN: number = 1051;

    /**
     * 1086, 仙灵之店的商店ID
     */
    static readonly XIANLENGZHIDIAN_STORE_ID: number = 1086;

    /**
     * 1085, 仙蕴宝藏的商店ID
     */
    static readonly XIANYUNBAOZANG_STORE_ID: number = 1085;

    /**
     * 2, 星语心愿活动_贡献
     */
    static readonly XINGYUAN_CONTRIBUTE: number = 2;

    /**
     * 4, 星语心愿活动_补拿经验
     */
    static readonly XINGYUAN_GET_EXP: number = 4;

    /**
     * 3, 星语心愿活动_拿礼包
     */
    static readonly XINGYUAN_GET_PRIZE: number = 3;

    /**
     * 1, 星语心愿活动_打开板子
     */
    static readonly XINGYUAN_OPENPANEL: number = 1;

    /**
     * 100, 幸运转盘抽奖一次花费
     */
    static readonly XYZP_COST_ONE_TIMES: number = 100;

    /**
     * 38, 幸运转盘 抽奖
     */
    static readonly XYZP_DRAW: number = 38;

    /**
     * 3, 幸运转盘免费次数
     */
    static readonly XYZP_FREE_TIMES: number = 3;

    /**
     * 39, 幸运转盘 领取奖励
     */
    static readonly XYZP_GET_GIFT: number = 39;

    /**
     * 100000, 幸运转盘 初始奖池
     */
    static readonly XYZP_INIT_TOTAL_MONEY: number = 100000;

    /**
     * 37, 幸运转盘 打开面板
     */
    static readonly XYZP_PANNEL: number = 37;

    /**
     * 52, 幸运转盘 大奖记录
     */
    static readonly XYZP_RECORD: number = 52;

    /**
     * 4, 血战封魔 Boss通知
     */
    static readonly XZFM_BOSSNTF: number = 4;

    /**
     * 31420001, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_1: number = 31420001;

    /**
     * 31420010, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_10: number = 31420010;

    /**
     * 31420011, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_11: number = 31420011;

    /**
     * 31420012, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_12: number = 31420012;

    /**
     * 31420002, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_2: number = 31420002;

    /**
     * 31420003, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_3: number = 31420003;

    /**
     * 31420004, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_4: number = 31420004;

    /**
     * 31420005, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_5: number = 31420005;

    /**
     * 31420006, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_6: number = 31420006;

    /**
     * 31420007, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_7: number = 31420007;

    /**
     * 31420008, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_8: number = 31420008;

    /**
     * 31420009, 血战封魔 boss怪物ID
     */
    static readonly XZFM_BOSS_MONSTER_ID_9: number = 31420009;

    /**
     * 31420003, 血战封魔 宝箱怪物ID
     */
    static readonly XZFM_BX_MONSTER_ID_1: number = 31420003;

    /**
     * 31420004, 血战封魔 宝箱怪物ID
     */
    static readonly XZFM_BX_MONSTER_ID_2: number = 31420004;

    /**
     * 2, 血战封魔 领取
     */
    static readonly XZFM_GET: number = 2;

    /**
     * 1, 血战封魔 面板
     */
    static readonly XZFM_PANEL: number = 1;

    /**
     * 3, 血战封魔 排行榜
     */
    static readonly XZFM_RANK: number = 3;

    /**
     * 1000, 元宝商城ID
     */
    static readonly YUANBAO_STORE_ID: number = 1000;

    /**
     * 8, 一元夺宝充值奖励配置数量
     */
    static readonly YYDB_CHARGE_CFG_MAX_NUM: number = 8;

    /**
     * 12, 一元夺宝领取充值礼包
     */
    static readonly YYDB_GET_GIFT: number = 12;

    /**
     * 5, 一元夺宝礼包最大配置数量
     */
    static readonly YYDB_GIFT_CFG_MAX_NUM: number = 5;

    /**
     * 13, 一元夺宝参与夺宝
     */
    static readonly YYDB_JOIN: number = 13;

    /**
     * 11, 一元夺宝打开面板
     */
    static readonly YYDB_OPEN_PANEL: number = 11;

    /**
     * 51003701, 打坐buffID
     */
    static readonly ZAZEN_BUFF_ID: number = 51003701;

    /**
     * 2, 结束打坐--后台主动通知
     */
    static readonly ZAZEN_END: number = 2;

    /**
     * 5, 打坐奖励通知
     */
    static readonly ZAZEN_EXP_NOTIFY: number = 5;

    /**
     * 3, 领取打坐奖励--前台发起
     */
    static readonly ZAZEN_OFFLINE_QUERY: number = 3;

    /**
     * 4, 领取打坐奖励--前台发起
     */
    static readonly ZAZEN_OFFLINE_REWARD: number = 4;

    /**
     * 10, 没10秒1跳
     */
    static readonly ZAZEN_PER_SEC: number = 10;

    /**
     * 1, 开始打坐--前台发起
     */
    static readonly ZAZEN_START: number = 1;

    /**
     * 37, 直购礼包领取礼包
     */
    static readonly ZGLB_GET_GIFT: number = 37;

    /**
     * 36, 直购礼包打开面板
     */
    static readonly ZGLB_OPEN_PANEL: number = 36;

    /**
     * 1000, ImageID*1000 + Level
     */
    static readonly ZHUFU_AVATAR_IMAGE_RATE: number = 1000;

    /**
     * 5, 珍珑棋局 一边有5人
     */
    static readonly ZLQJ_SIDE_ROLE_COUNT: number = 5;

    /**
     * 152, 中元节充值返利  领取奖励
     */
    static readonly ZYJ_CZFL_GET_REWARD: number = 152;

    /**
     * 151, 中元节充值返利  面板
     */
    static readonly ZYJ_CZFL_PANEL: number = 151;

    /**
     * 6, 至尊皇城 开服前6天不跨服
     */
    static readonly ZZHC_ACT_CROSS_DAY_LIMIT: number = 6;

    /**
     * 6, 至尊皇城 本服第六天必开
     */
    static readonly ZZHC_ACT_SELF_SVR_DAY: number = 6;

    /**
     * 4, 至尊皇城 护城1ID
     */
    static readonly ZZHC_FU_CITY_ID1: number = 4;

    /**
     * 5, 至尊皇城 护城2ID
     */
    static readonly ZZHC_FU_CITY_ID2: number = 5;

    /**
     * 6, 至尊皇城 护城3ID
     */
    static readonly ZZHC_FU_CITY_ID3: number = 6;

    /**
     * 7, 至尊皇城 护城4ID
     */
    static readonly ZZHC_FU_CITY_ID4: number = 7;

    /**
     * 4, 至尊皇城 宗派奖励有4档
     */
    static readonly ZZHC_GUILD_GIFT_COUNT: number = 4;

    /**
     * 2, 至尊皇城 宗派最少需要2级
     */
    static readonly ZZHC_GUILD_NEED_LEVEL: number = 2;

    /**
     * 60009038, 至尊皇城 宗派奖励物品
     */
    static readonly ZZHC_GUILD_REWARD_ITEM_1: number = 60009038;

    /**
     * 60009037, 至尊皇城 宗派奖励物品
     */
    static readonly ZZHC_GUILD_REWARD_ITEM_2: number = 60009037;

    /**
     * 60009036, 至尊皇城 宗派奖励物品
     */
    static readonly ZZHC_GUILD_REWARD_ITEM_3: number = 60009036;

    /**
     * 60009035, 至尊皇城 宗派奖励物品
     */
    static readonly ZZHC_GUILD_REWARD_ITEM_4: number = 60009035;

    /**
     * 100, 至尊皇城 宗派奖励贡献要求
     */
    static readonly ZZHC_GUILD_REWARD_SCORE_1: number = 100;

    /**
     * 200, 至尊皇城 宗派奖励贡献要求
     */
    static readonly ZZHC_GUILD_REWARD_SCORE_2: number = 200;

    /**
     * 500, 至尊皇城 宗派奖励贡献要求
     */
    static readonly ZZHC_GUILD_REWARD_SCORE_3: number = 500;

    /**
     * 1000, 至尊皇城 宗派奖励贡献要求
     */
    static readonly ZZHC_GUILD_REWARD_SCORE_4: number = 1000;

    /**
     * 1, 至尊皇城 主城ID
     */
    static readonly ZZHC_MAIN_CITY_ID: number = 1;

    /**
     * 80, 至尊皇城 玩家参与等级下限
     */
    static readonly ZZHC_ROLE_JOIN_MIN_LEVEL: number = 80;

    /**
     * 60009045, 至尊皇城 个人奖励物品 10分
     */
    static readonly ZZHC_ROLE_REWARD_ITEM_1: number = 60009045;

    /**
     * 60009046, 至尊皇城 个人奖励物品 20分
     */
    static readonly ZZHC_ROLE_REWARD_ITEM_2: number = 60009046;

    /**
     * 60009047, 至尊皇城 个人奖励物品 50分
     */
    static readonly ZZHC_ROLE_REWARD_ITEM_3: number = 60009047;

    /**
     * 10, 至尊皇城 奖励积分 10分
     */
    static readonly ZZHC_ROLE_REWARD_SCORE_1: number = 10;

    /**
     * 20, 至尊皇城 奖励积分 20分
     */
    static readonly ZZHC_ROLE_REWARD_SCORE_2: number = 20;

    /**
     * 50, 至尊皇城 奖励积分 50分
     */
    static readonly ZZHC_ROLE_REWARD_SCORE_3: number = 50;

    /**
     * 1, 至尊皇城 奖励类型 10分
     */
    static readonly ZZHC_ROLE_REWARD_TYPE_1: number = 1;

    /**
     * 2, 至尊皇城 奖励类型 20分
     */
    static readonly ZZHC_ROLE_REWARD_TYPE_2: number = 2;

    /**
     * 3, 至尊皇城 奖励类型 50分
     */
    static readonly ZZHC_ROLE_REWARD_TYPE_3: number = 3;

    /**
     * 2, 至尊皇城 卫城1ID
     */
    static readonly ZZHC_WEI_CITY_ID1: number = 2;

    /**
     * 3, 至尊皇城 卫城2ID
     */
    static readonly ZZHC_WEI_CITY_ID2: number = 3;

    /**
     * 118, 至尊争夺排行榜打开面板
     */
    static readonly ZZZD_CHARGE_PANEL: number = 118;

    
}
