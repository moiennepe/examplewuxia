﻿import { UILayer } from "System/uilib/CommonForm"
import { FloatTip } from 'System/floatTip/FloatTip'

export class AboveFloatTip extends FloatTip {
    constructor() {
        super(UILayer.FloatTip);
    }
}