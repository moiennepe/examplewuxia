﻿import { UILayer } from "System/uilib/CommonForm"
import { FloatTip } from 'System/floatTip/FloatTip'

export class BelowFloatTip extends FloatTip {
    constructor() {
        super(UILayer.Base);
    }
}