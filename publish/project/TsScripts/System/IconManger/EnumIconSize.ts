﻿export class EnumIconSize {
    /**
    * 正方形32尺寸 
    */
    static SMALL: number = 32;
    /**
    * 正方形40尺寸 
    */
    static NORMAL: number = 40;
    /**
    * 正方形48尺寸 
    */
    static LARGE: number = 48;
    /**
    * 正方形68尺寸 
    */
    static EXTRA_LARGE: number = 68;

    /**
    * 圆角32尺寸 (ActShiwuRewardItem不包含这个背景，非常少用)
    */
    static FILLET_SMALL: number = 33;
    /**
    * 圆角40尺寸
    */
    static FILLET_NORMAL: number = 41;
    /**
    * 圆角68尺寸 
    */
    static FILLET_EXTRA_LARGE: number = 69;

	/**
    * 超小，货币图标。
    */
    static EXTRA_SMALL: number = 20;

	/**
    * 右下角文本大小(通用) 
    */
    static NUM_NORMAL: number = 12;
	/**
    * 右下角文本大小(小图标装备) 
    */
    static NUM_SMALL: number = 14;
	/**
    * 右下角文本大小(中图标装备) 
    */
    static NUM_MID: number = 16;
	/**
    * 右下角文本大小(大图标装备) 
    */
    static NUM_BIG: number = 18;
}