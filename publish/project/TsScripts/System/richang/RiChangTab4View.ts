﻿import { RiChangBaseView } from "System/richang/RiChangBaseView"
import { KeyWord } from 'System/constants/KeyWord'
export class RiChangTab4View extends RiChangBaseView {
    constructor() {
        super(KeyWord.OTHER_FUNCTION_RICHANG_SL, KeyWord.RECOMMEND_TITLE_SL);
    }
}