﻿import { RiChangBaseView } from "System/richang/RiChangBaseView"
import { KeyWord } from 'System/constants/KeyWord'
export class RiChangTab1View extends RiChangBaseView {
    constructor() {
        super(KeyWord.OTHER_FUNCTION_RICHANG_JY, KeyWord.RECOMMEND_TITLE_JY);
    }
}