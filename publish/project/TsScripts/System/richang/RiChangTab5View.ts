﻿import { RiChangBaseView } from "System/richang/RiChangBaseView"
import { KeyWord } from 'System/constants/KeyWord'
export class RiChangTab5View extends RiChangBaseView {
    constructor() {
        super(KeyWord.OTHER_FUNCTION_RICHANG_HD, KeyWord.RECOMMEND_TITLE_HD);
    }
}