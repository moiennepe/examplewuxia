﻿import { RiChangBaseView } from "System/richang/RiChangBaseView"
import { KeyWord } from 'System/constants/KeyWord'
export class RiChangTab2View extends RiChangBaseView {
    constructor() {
        super(KeyWord.OTHER_FUNCTION_RICHANG_ZB, KeyWord.RECOMMEND_TITLE_ZB);
    }
}