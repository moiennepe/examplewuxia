﻿import { RiChangBaseView } from "System/richang/RiChangBaseView"
import { KeyWord } from 'System/constants/KeyWord'
export class RiChangTab3View extends RiChangBaseView {
    constructor() {
        super(KeyWord.OTHER_FUNCTION_RICHANG_CL, KeyWord.RECOMMEND_TITLE_CL);
    }
}