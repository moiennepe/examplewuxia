﻿let Datalist = [
    'SystemParameterConfigM',
    'NPCConfigM',
    'SceneConfigM',
    'TeleportConfigM',
    'MonsterConfigM',
    'SkillConfigM',
    'EquipConfigM',
    'ActorMonsterM',
    'ThingConfigM',
    'RoleLevelConfigM',
    'QuestConfigM',
    'QuestFlyConfigM',
    'QuestSectionM',
    'NPCFunctionLimitM',
    'NewFeatPreConfigM',
    'ActIconOrderM',
    'NavigationConfigM',
    'PinstanceDiffBonusM',
    'PinstanceConfigM',
    'GuildLevelM',
    'MonsterConfigM',
    'ClientMonsterConfigM',
    'ErrnoConfigM',
    'SysTipsConfigM',
    'BuffConfigM',
    'VIPFunctionConfigM',
    'VIPParameterConfigM',
    'VipReserveTimesM',
    'CardDiscountCfgM',
    'EquipWishM',
    'ZFEquipUpColorM',
    'ZFEquipMergeM',
    'ItemMergeM',
    'ZhuFuConfigM',
    'EquipLQM',
    'EquipAllBodyStrengPropM',
    'DiamondAllBodyMountPropM',
    'EquipUpColorM',
    'EquipAllColorPropM',
    'DiamondPropM',
    'BeautySuitPropM',
    'ZhuFuDrugConfigM',
    'ZhuFuEquipMapM',
    'XianYuanPropCfgM',
    'ZhuFuImageConfigM',
    'BloodCfgM',
    'BeautyAttrM',
    'HongYanFateConfigM',
    'BeautyStageM',
    'BeautyEquipSuitPreViewM',
    'HongYanJuShenConfigM',
    'EquipStageConditionM',
    'EquipUpColorM',
    'EquipStageConditionM',
    'FaBaoCfgM',
    'FaQiCfgM',
    'FaQiZhuHunCfgM',
    'NPCSellConfigM',
    'NPCSellLimitConfigM',
    'NPCRandomStoreCfgM',
    'MonthCardBaseM',
    'ActivityConfigM',
    'AchiAttrConfigM',
    'AchiConfigM',
    'CrossSingleM',
    'JuYuanAttrCfgM',
    'JuYuanCfgM',
    'LangCfgM',
    'DressImageConfigM',
    'LingBaoCfgM',
    'TitleListConfigM',
    'LingBaoCfgM',
    'GiftBagConfigM',
    'ActHomeConfigM',
    'TimeLimitConfigM',
    'FMTCfgM',
    'JJCRankRewardConfigM',
    'ArenaMaxRankRewardCfgM',
    'MagicCubeBaseCfgM',
    'MagicCubeFBCfgM',
    'DropConfigM',
    'ZYCMCfgM',
    'RankConfInfoM',
    'VIPMonthParameterConfigM',
    'JiuXingConfigM',
    'QiFuConfigM',
    'JSTZM',
    'JSTZRankM',
    'VIPAdCfgM',
    'SkyLotteryConfigM',
    'SevenDayCfgM',
    'GetEquipCfgM',
    'QuestionActivityConfigM',
    'HistoricalRemainsCfgM',
    'WYBQConfigM',
    'WYBQTuJingConfigM',
    'ZYZHCfgM',
    'KFLuckyCatActM',
    'SurveyCfgM',
    'NewsConfigM',
    'ChallengeBossCfgM',
    'ChargeGiftConfigM',
    'JUHSActCfgM',
    'StageDayCfgM',
    'KFQMCBCfgM',
    'CollectWordActCfgM',
    'KFSCTGCfgM',
    'GuideTipConfigM',
    'QuestGuildConfigM',
    'LoadingPageConfigM',
    'Cross3V3SeasonM',
    'Cross3V3WinM',
    'Cross3V3GradeM',
    'DDLActCfgM',
    'CrossDDLM',
    'JPDDHActCfgM',
    'CDKeyGiftConfigM',
    'EquipSlotUpLvM',
    "EquipSlotStrengthenM",
    'BaoDianCfgM',
    'WHJXCfgM',
    'SevenDayFundCfgM',
    'KFMRMBCfgM',
    'ClientDefaultM',
    'HFSevenSignActCfgM',
    'HFLJCZCfgM',
    'HFLJXFCfgM',
    'HFZhaoCaiMaoCfgM',
    'DiamondMountM',
    'KFSCLBCfgM',
    'KFXFLBCfgM',
    'BeautyZTJHCfgM',
    'BeautyZTUpLvCfgM',
    'EquipWishRandomM',
    'ShenShouCfgM',
    'ColosseumGradeM',
    'ColosseumRankM',
    'GuildMoneyRankM',
    'EquipFinalPropM',
    'GuildTreasureHuntEventM',
    'GuildTreasureHuntProcessM',
    'GuildZZHCBossM',
    'HFBXYLDropCfgM',
    'HFBXYLShopCfgM',
    'SpecialPriCfgM',
    'WYYZLevelCfgM',
    'WYYZPetCfgM',
    'VIPRebateM',
    'VIPPriHYParamM',
    'VIPPriHYFuncM',
    'DressQHM',
    'EquipSlotSuitActM',
    'EquipSlotSuitUpM',
    'SkillFetterCfgM',
    'IOSPushMsgConfigM',
    'GuildPaiMaiCfgM',
    'MJTZCfgM',
    'EquipSlotLianTiM',
    'EquipSlotLTSBM',
    'QQGroupShowConfigM',
    'ModelUIScaleM',
    'PlayerNameM',
    'FMTGuajiCfgM',
    'AncientQuestConfigM',
    'CZKHRebateConfigM',
    //'CZKHVIPGetConfigM',
    'ZhuFuFSConfigM',
    'HomeBossCostCfgM',
    'GroupPinLevelDropCfgM',
    'ShieldGodCfgM',
    'FaBaoLevelCfgM',
    'FaBaoXiangqianCfgM',
    'FaBaoSkillCfgM',
    'CameraAnimM',
    'WingStrengthM',
    'WingCreateM',
    'ChatBubbleM',
    'WorldCupIndexCfgM',
    'LXFLActCfgM',
    'BeautyAwakeCfgM',
    'NPCStoreBuyBackCfgM',

    'AllPeopleHiConfigM',
    'HiPointConfigConfigM',
    'CrystalConfigM',
    'CrystalExpM',
    'CollectExchangeActCfgM',
    'IOSDabaoM',
    'TianZhuMountM',
    'WYTreasureHuntCfgM',
    'HunLiConfigM',
    'HunHuanConfigM',
    'RebirthSuitCfgM',
    'RebirthRefineCfgM',
    'RebirthRefineSuitCfgM',
    'KFJDCCfgM',
    'KFJDCFinalCfgM',
    'PrivatBossCfgM',
    'HunGuFZConfigM',
    'HunGuTZConfigM',
    'TaskRecommendCfgM',
    'LevelRecommendCfgM',
    'RCRecommendCfgM',
    'HunGuSlotUpLvM',
    'HunHuanLevelUpConfigM',
    'HunGuSlotStrengthenM',
    'HunGuWashRandomM',
    'HunGuWashM',
    'VIPBossCfgM',
    'HunGuMergeM',
    'HunGuCreateM',
    'HunGuEquipRandPropM',
    'ForestBossCfgM',
    'LotteryExchangeCfgM',
    'WorldBossRewardCfgM',
    'AdsShowConfigM',
    'NPCFunctionPreviewM',
    'SaiJiConfigM',
    'DynamicThingM',
    'ConsumeRankActCfgM',
    "HunGuSkillFZConfigM",
    'FightPointGuideConfigM'
]
let DateKey = [];

export class ConfigManager {
    private _cfgs = {};
    constructor() {
        let t = UnityEngine.Time.realtimeSinceStartup;
        let dataPath = ConfigManager.DataPath;
        for (let i = 0, len = dataPath.length; i < len; i++) {
            let asset = Game.ResLoader.LoadAsset(dataPath[i]).textAsset;
            this._cfgs[DateKey[i]] = ConfigManager.parseData(asset);
        }
        uts.log('load json data time:' + (UnityEngine.Time.realtimeSinceStartup - t));
    }

    getCfg<T>(pathkey: string): T[] {
        return this._cfgs[pathkey];
    }

    static get DataPath(): string[] {
        let dataDir = 'data';
        if (this.isJscBinaryData)
            dataDir = 'bjsondata';
        else if (this.isBinaryData)
            dataDir = 'binarydata';

        let jsonExt = this.isBinaryData ? 'bytes' : 'json';

        let retdata: string[] = [];
        for (let i = 0, len = Datalist.length; i < len; i++) {
            DateKey[i] = ConfigManager.makeKey(Datalist[i]);
            retdata.push(uts.format("{0}/{1}.{2}", dataDir, Datalist[i], jsonExt));
        }
        return retdata;
    }

    private static makeKey(name: string): string {
        return 'data/' + name + '.json';
    }

    private static parseData(asset: UnityEngine.TextAsset) {
        if (this.isBinaryData)
            return __parse_bjson(asset.bytes);
        else
            return JSON.parse(asset.text);
    }

    private static get isBinaryData(): boolean {
        if (Duktape.isJsc) {
            return this.isJscBinaryData;
        }
        else {
            return true;
        }
    }

    private static get isJscBinaryData(): boolean {
        return (Duktape.isJsc === true && Duktape.hasBJson === true);
    }
}