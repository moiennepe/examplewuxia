﻿/**
 * 极速挑战数据
 * @author jesse
 */
export class JstzItemData {
    rank: number = 0;
    cfg: GameConfig.JSTZRankM;
    roleID: Protocol.RoleID;
    time: number = 0;
    roleName: string;
    /** 是否是主角 */
    isHero: boolean = false;
}

