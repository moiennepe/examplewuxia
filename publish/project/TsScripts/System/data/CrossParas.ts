﻿/**
 * 跨服游戏参数变量。
 */
export class CrossParas {
    worldId = 0;
    domain: string;
    serverIp: string = "";
    serverPort: number = 0;
}