﻿/**
* 锁仙台队伍Item的Vo。
* 
* 本文件代码由模板生成，你可能需要继续修改其他代码文件才能正常使用。
* 
* @author teppei
* 
*/
export class SxtTeamItemData {
    /**队伍信息。*/
    info: Protocol.Cross_SimpleOneTeam;

    /**相关副本ID。*/
    pistanceID: number = 0;

    /**推荐人数。*/
    bestNum: number = 0;
}
