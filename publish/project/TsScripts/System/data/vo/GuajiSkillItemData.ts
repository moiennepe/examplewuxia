﻿/**
	 * 挂机技能Item的Vo。
	 * 
	 * 本文件代码由模板生成，你可能需要继续修改其他代码文件才能正常使用。
	 * 
	 * @author teppei
	 * 
	 */
export class GuajiSkillItemData {
    /**技能配置。*/
    config: GameConfig.SkillConfigM;

    /**是否选中。*/
    isSelected: boolean;


}
