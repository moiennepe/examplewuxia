﻿export class DropItemData {
    /**物品配置。*/
    config: GameConfig.ThingConfigM;

    /**物品数量*/
    number: number = 0;
}
