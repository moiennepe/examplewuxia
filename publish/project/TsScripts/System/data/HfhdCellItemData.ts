﻿import { RewardIconItemData } from 'System/data/vo/RewardIconItemData'

/**
 * ...
 * @author jesse
 */
export class HfhdCellItemData {

    frame: number = 0;
    rewardItemnData: RewardIconItemData;
}

