﻿/**
* 副本难度缓存。
* @author teppei
* 
*/
export class PinstanceDiffCache {
    pid: number = 0;
    diff: number = 0;
    score: number = 0;
    maxScore: number = 0;
    finishCnt: number = 0;
}
