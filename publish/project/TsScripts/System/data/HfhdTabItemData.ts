﻿/**
 * 合服活动标签数据
 * @author jesse
 */
export class HfhdTabItemData {
    index: number = 0;
    type: number = 0;
    mcRewardStatus: boolean = false;
}

