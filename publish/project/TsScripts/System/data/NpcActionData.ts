﻿/**
* NPC动作数据。
* @author teppei
* 
*/
export class NpcActionData {

    public id: number = 0;

    public func: GameConfig.NPCFunction;

    public limitLevel: number = 0;

    public type = 0;

    public title: string;

    public groupld: number = 0;

    public unitID: number = 0;

    public icon: number = 0;
}