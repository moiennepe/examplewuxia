﻿    /**
	 * 容器变更原因的枚举类。
	 * @author xiaojialin
	 * 
	 */
export class EnumContainerChangeReason {
    /**不明原因。*/
    static UNKNOWN: number = 0;

    /**换装备。*/
    static CHANGE_EQUIP: number = 1;

    /**
     * <FONT COLOR='FF0000'>禁止调用构造函数。</FONT>
     * 
     */

}
