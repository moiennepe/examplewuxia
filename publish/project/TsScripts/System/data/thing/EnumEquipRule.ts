﻿import { ThingItemData } from 'System/data/thing/ThingItemData'
import { Global as G } from 'System/global'
import { ThingData } from "System/data/thing/ThingData"


/**
 * <在此键入枚举的中文名称>枚举类。
 * Code generated by Bat&Perl.
 *
 * @author Administrator
 *
 */
export class EnumEquipRule {
    /**<黄色宝石>。*/
    static TYPE_YELLOW: number = 2;
    /**<蓝色宝石>。*/
    static TYPE_BLUE: number = 1;
    /**<紫色宝石>。*/
    static TYPE_PURPLE: number = 3;

    /**<蓝色宝石在1,2格>。*/
    static GRID_BLUE: number = 2;
    /**<黄色宝石在3,4格>。*/
    static GRID_YELLOW: number = 4;
    /**<紫色宝石在5,6格>。*/
    static GRID_PURPLE: number = 6;

    /**装备强化数量*/
    static EQUIP_ENHANCE_COUNT: number = 8;
    /**装备强化数量*/
    static EQUIP_ENHANCE_COUNT_HUNGU: number = 9;

    /**1品质不足*/
    static TYPE_QUALITY_NOT_ENOUGH: number = 1;
    /**2等级不足*/
    static TYPE_LEVEL_NOT_ENOUGH: number = 2;

    /**斩魔战力属性ID*/
    static ZM_PROP_ID: number = 999999;

    /**斩魔弹提示阶级>6*/
    static ZM_TIPS_LEVEL1: number = 6;
     /**斩魔弹提示阶级<15*/
    static ZM_TIPS_LEVEL2: number = 15;

    /**宝石进化每页数量*/
    static DIAMOND_UP_PAGE_COUNT = 14;
		
    /**宝石进化最大个数*/
    static DIAMOND_UP_MATERIAL_MAX_COUNT = 6;

    /**
     * <FONT COLOR='FF0000'>禁止调用构造函数。</FONT>
     *
     */


    /**
     * 获取宝石文字描述
     * @param	type
     * @return
     */
    static getInserDiamondTypeStr(type: number): string {
        switch (type) {
            case EnumEquipRule.TYPE_YELLOW:
                return '黄色';              
            case EnumEquipRule.TYPE_BLUE:
                return '蓝色';            
            case EnumEquipRule.TYPE_PURPLE:
                return '紫色';            
            default:
        }
        return '';
    }


    /**获取斩魔+几*/
    static getZmLevelStr(obj: any): string {
        if (!obj) {
            return '';
        }
        let str: string;
        let zmLevel: number;
        if (obj instanceof ThingItemData)
        {
            if ((<ThingItemData>obj).data) {
                zmLevel =(<ThingItemData>obj).data.m_stThingProperty.m_stSpecThingProperty.m_stEquipInfo.m_stLQ.m_ucLQLevel;
            }
        }
        else if (obj instanceof Protocol.SpecThingProperty)
        {
            if (<Protocol.SpecThingProperty>(obj)) {
                zmLevel = (<Protocol.SpecThingProperty>obj).m_stEquipInfo.m_stLQ.m_ucLQLevel;
            }
        }
        else if (obj instanceof Number)
        {
            zmLevel = <number>(obj);
        }
		else
		{
			if (defines.has('_DEBUG')) {
                uts.log('不支持斩魔类型：{0}');
		    }
        }
        // return zmLevel > 0 ? substitute(' 斩+{0}', zmLevel) : '';
        return zmLevel > 0 ? zmLevel.toString() : '';
    }


    /**获取宝石加成总经验*/
    static getDiamondUpAllExp(thingVo: ThingItemData): number {
        let addVal: number = 0;
        let gemHasVal: number = 0;
        if (thingVo) {
            gemHasVal = thingVo.data.m_stThingProperty.m_stSpecThingProperty.m_stDiamondProcessInfo.m_uiProcess;
            if (gemHasVal > 0) {
                addVal += thingVo.data.m_stThingProperty.m_stSpecThingProperty.m_stDiamondProcessInfo.m_uiProcess;
            }
            let gemPropCfg: GameConfig.DiamondPropM = G.DataMgr.equipStrengthenData.getDiamondConfig(thingVo.config.m_iID);
            if (gemPropCfg) {
                addVal += gemPropCfg.m_uiProp1process;
            }
        }
        return addVal;
    }

    /**获取宝石升级总经验经验*/
    static getDiamondMaxExp(gemId: number): number {
        let val: number = 0;
        let nextGemCfg: GameConfig.ThingConfigM = ThingData.m_itemMap[gemId + 10];
        if (nextGemCfg) {
            let crtGemPropCfg: GameConfig.DiamondPropM = G.DataMgr.equipStrengthenData.getDiamondConfig(gemId);
            let nextGemPropCfg: GameConfig.DiamondPropM = G.DataMgr.equipStrengthenData.getDiamondConfig(nextGemCfg.m_iID);
            val = nextGemPropCfg.m_uiProp1process - crtGemPropCfg.m_uiProp1process;
        }
        return val;
    }

}


	