﻿export class GateInfo {
    x: number;
    y: number;

    gateID: number = 0;

    gateName: string;

    destSceneID: number = 0;

    destX: number = 0;

    destY: number = 0;

    direction: number = 0;

    isEnable: boolean = true;
}
