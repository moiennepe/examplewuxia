﻿export enum MapSignType {
    Map_None = 0,
    Map_Monster = 1,
    Map_Npc = 2,
    Map_Npc_Accept_Quest = 5,
    Map_Npc_Get_Reward = 6,
    Map_Npc_Quest_Doing = 7,

    Map_TeamMember = 3,
    Map_TeamLeader = 8,
    Map_Waypoint = 4,

    Map_Icon = 9,

    Map_Role = 10,
}
export default MapSignType;