﻿export enum PlatIdDef {
    FENGYUE = 0,
    XIANYU = 1,
    XIYOU = 2,
    DONGWANG = 3,
    GONGHUI = 4,
    SANXIANG = 5,
    BEIKUN = 6,
    HUANLEPAI = 7,
    LEHUIHI = 8,
    UU = 9,
    MZB = 10,
}

export enum GameIdDef {
    ZSZL = 1,  // 风月.真神之路
    ZSZL_DEBUG = 2,  // 风月.真神之路(调试版)
    ZSZL_PLAT_TEST = 3,  // 全平台测试包
    ZSZL_PLAT_CDN_TEST = 4,  // 全平台测试包
    DOULUO_SLKGL = 1000,//斗罗大陆史莱克归来

    DOULUO_ZIYOU = 2000,//自有斗罗（斗罗觉醒）
    DOULUO_ZIYOU1 = 2001,//自有斗罗(斗罗武魂)
    DOULUO_ZIYOU2 = 2002,//自有斗罗(终极斗罗)

    SLKGL = 4000, //史莱克归来 - 买量包
    DLDL = 4001, //斗罗大罗之史莱克归来 - 渠道包

    C7477_DLDL = 5001, //7477.斗罗大陆

    ASDK_DLDL = 6001, //asdk.斗罗大陆
    ASDK_FYPAY_DLDL = 6002, //asdk.斗罗大陆.风月自有支付
}