import { Macros } from 'System/protocol/Macros'
/**
 * <在此键入枚举的中文名称>枚举类。
 * Code generated by Bat&Perl.
 * 
 * @author Administrator
 * 
 */
export class EnumTgbjRule {
    /**<记录最大数量>。*/
    static MAX_RECORE_COUNT: number = 50;

    /**魔帝宝库抽奖记录类型*/
    static MDBK_LOTTERY_RECORD_TYPE: number = Macros.SKYLOTTERY_SERVER_RECORD_TYPE;

    /**魔帝宝库兑换记录类型*/
    static MDBK_EXCHANGE_RECORD_TYPE: number = Macros.SKYLOTTERY_SERVER_BUY_RECORD_TYPE;

    /**魔帝宝库全服记录类型*/
    static RECORD_ALL_TYPE = 0;
		
    /**魔帝宝库个人记录类型*/
    static RECORD_SELF_TYPE = 1;
    /**
     * <FONT COLOR='FF0000'>禁止调用构造函数。</FONT>
     * 
     */

}
