﻿export class ShopRule {
    /** 商城限购第一个ID */
    static readonly LIMIT_BUY_START_ID: number = 1051;
    /** 商城限购固定循环第一个ID */
    static readonly LIMIT_BUY_LOOP_ID: number = 1101;
    /** 多少天后开始循环 */
    static readonly MAX_ITEM_LIMIT: number = 15;
    /** 副本基础商店ID */
    static readonly BASE_FSD_SHOP_ID: number = 1031;
}

