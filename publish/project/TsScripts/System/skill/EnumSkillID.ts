﻿/**
	* 技能ID枚举类。
	* @author xiaojialin
	* 
	*/
export enum EnumSkillID {
    /**
     * 回城技能 
     */
    TOWN_PORTAL = 50013001,
}
export default EnumSkillID;