import { TipType } from 'System/constants/GameEnum'
export interface ITipData {
    tipDataType: TipType;
}