﻿import { KeyWord } from 'System/constants/KeyWord'
export class EnumJJRTab {
    /**兑换*/
    static REWARD = 0;
    /**强化月卡*/
    static VIP_CARD = 1; 
    /**神器*/
    static WEAPON = 2; 
    /**法阵*/
    static FA_ZHEN = 3; 
    /**坐骑*/
    static HORSE = 4; 
    /**美人*/
    static BEAUTY = 5; 
		
    /**累计充值*/
    static RECHARGE = 6;
		
    /**极品大兑换*/
    static EXCHANGE = 7; 
		
    /**真迹*/
    static SHENJI = 8; 
		
    /**宝物*/
    static FAQI = 9; 
    /** 每日限购 */
   static  MRXG: number = 10;
    /** 天下兑换 */
   static TX_EXCHANGE: number = 11;
    /**宝石*/
   static MINGWEN: number = 12;
}
