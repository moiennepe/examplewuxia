﻿import { KeyWord } from 'System/constants/KeyWord'
import { JinJieRankPanel } from 'System/jinjieri/JinJieRankPanel'

export class KaiFuJJRRankPanel extends JinJieRankPanel {

    constructor() {
        super(KeyWord.OTHER_FUNCTION_JJR_RANK);
    }

}