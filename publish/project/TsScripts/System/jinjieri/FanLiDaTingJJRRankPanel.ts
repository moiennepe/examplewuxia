﻿import { KeyWord } from 'System/constants/KeyWord'
import { JinJieRankPanel } from 'System/jinjieri/JinJieRankPanel'

export class FanLiDaTingJJRRankPanel extends JinJieRankPanel {
  
    constructor() {
        super(KeyWord.OTHER_FUNCTION_JJR_RANK_AFTER7DAY);
    }

}