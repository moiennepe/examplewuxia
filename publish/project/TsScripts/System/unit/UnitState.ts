﻿export enum UnitState {
    None,
    Stand,
    Move,
    Fight,
    Dead,
    Jump,
    Pick,
    Born,
    Enter,
    FlyEnd,
}