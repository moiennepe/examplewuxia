﻿export abstract class ListItemCtrl {
    abstract setComponents(go: UnityEngine.GameObject, ...args);
    abstract update(...args);
}