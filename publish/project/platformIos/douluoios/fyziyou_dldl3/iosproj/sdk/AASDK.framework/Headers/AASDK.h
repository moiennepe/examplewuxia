//
//  AA.h
//  AA
//
//  Created by zhangwen on 2017/3/16.
//
//

#import <UIKit/UIKit.h>

//! Project version number for AA.
FOUNDATION_EXPORT double AAVersionNumber;

//! Project version string for AA.
FOUNDATION_EXPORT const unsigned char AAVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AA/PublicHeader.h>


#import <AASDK/AAClearSDKManager.h>
