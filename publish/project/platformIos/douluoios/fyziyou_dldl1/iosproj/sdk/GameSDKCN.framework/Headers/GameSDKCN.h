//
//  GameSDKCN.h
//  GameSDKCN
//
//  Created by 向平 on 2017/6/24.
//  Copyright © 2017年 iqiyigame. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for GameSDKCN.
FOUNDATION_EXPORT double GameSDKCNVersionNumber;

//! Project version string for GameSDKCN.
FOUNDATION_EXPORT const unsigned char GameSDKCNVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GameSDKCN/PublicHeader.h>
#import <GameSDKCN/GameSDK.h>

