
#import <Foundation/Foundation.h>

@interface GameAppInfo : NSObject

@property (nonatomic, copy) NSString *gameId;//游戏ID
@property (nonatomic, copy) NSString *gameName;//游戏名称
@property (nonatomic, copy) NSString *gameAppId;//游戏Appid
@property (nonatomic, copy) NSString *promoteId;//推广id
@property (nonatomic, copy) NSString *promoteAccount;//推广名称
@property (nonatomic, copy) NSString *gamekey;//游戏访问密钥

@end
