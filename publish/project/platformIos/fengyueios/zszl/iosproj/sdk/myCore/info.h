static NSString *pid = @"1";
static NSString *cid = @"13";
static NSString *aid = @"101";
static NSString *game = @"108";
static NSString *gameversion = @"2.0.103";
static NSString *areaid = @"1";
static NSString *adtype = @"1";
static NSString *appkey = @"108";
static NSString *pkid = @"267";
static NSString *sdktype = @"1";


///////////////////////TS变量定义//////////////////////////////////
//MsgType
static const NSString *Value_ResultOk=@"0";
static const NSString *Value_ResultFailed=@"-1";
static const NSString *Value_Init_Sdk=@"1";
static const NSString *Value_Login=@"3";
static const NSString *Value_Logout=@"4";
static const NSString *Value_Pay=@"5";
static const NSString *Value_RequestServer=@"15";
static const NSString *Value_UpdateRecordState=@"26";
//其他变量
static const NSString *Ts_msgtype=@"msgtype";
static const NSString *Ts_openid=@"openid";
static const NSString *Ts_userid=@"userid";
static const NSString *Ts_username=@"username";
static const NSString *Ts_token=@"token";
static const NSString *Ts_servers=@"servers";
static const NSString *Ts_result=@"result";