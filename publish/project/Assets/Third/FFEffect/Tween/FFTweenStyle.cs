﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FFTweenStyle
{
    Once,
    PingPong,
    Loop,
    Forever,
}
