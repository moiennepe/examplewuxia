﻿public enum AssetPriority : int
{
    Low1 = 0,
    Low2,
    Low3,
    BelowNormal1,
    BelowNormal2,
    BelowNormal3,
    Normal1,
    Normal2,
    Normal3,
    High1,
    High2,
    High3,
}